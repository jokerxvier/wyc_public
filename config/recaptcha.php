<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Keys
    |--------------------------------------------------------------------------
    |
    | Set the public and private API keys as provided by reCAPTCHA.
    |
    | In version 2 of reCAPTCHA, public_key is the Site key,
    | and private_key is the Secret key.
    |
    */
    // 'public_key'    => '6Lf2fQoTAAAAAFXzMk_u1liDwrkhNW-y0nPPhfxR', -- old
    'public_key'    => '6LcwtxETAAAAACETZcsLJ9Ccr2KqZSKweaTGrn6c',
    // 'private_key'    => '6Lf2fQoTAAAAAJrwzQ0Xfb8EVCzb5Pjcx9fh_RY9', -- old
    'private_key'    => '6LcwtxETAAAAAH0diF4rDmP1Jcd1FRc6yzrT6Hw4',

    /*
    |--------------------------------------------------------------------------
    | Template
    |--------------------------------------------------------------------------
    |
    | Set a template to use if you don't want to use the standard one.
    |
    */
    'template'    => '',

    /*
    |--------------------------------------------------------------------------
    | Driver
    |--------------------------------------------------------------------------
    |
    | Determine how to call out to get response; values are 'curl' or 'native'.
    | Only applies to v2.
    |
    */
    'driver'      => 'curl',

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | Various options for the driver
    |
    */
    'options'     => [

        'curl_timeout' => 1,

    ],

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Set which version of ReCaptcha to use.
    |
    */

    'version'     => 1,

];
