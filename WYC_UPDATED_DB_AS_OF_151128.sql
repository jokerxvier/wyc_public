-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: wyc2
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_address_book`
--

DROP TABLE IF EXISTS `tbl_address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_address_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` enum('Default','Billing','Shipping') DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `region` varchar(60) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_address_book_tbl_users1_idx` (`user_id`),
  CONSTRAINT `fk_tbl_address_book_tbl_users1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_address_book`
--

LOCK TABLES `tbl_address_book` WRITE;
/*!40000 ALTER TABLE `tbl_address_book` DISABLE KEYS */;
INSERT INTO `tbl_address_book` VALUES (12,1,'Default','Makayan','Bongabong','Oriental Mindoro','ph','2564','2015-11-16 00:24:08','2015-11-16 02:32:26'),(13,1,'Billing','T. De Castro','Ibajay','Aklan','ph','324','2015-11-16 00:27:14','2015-11-16 02:28:48');
/*!40000 ALTER TABLE `tbl_address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_adverts`
--

DROP TABLE IF EXISTS `tbl_adverts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_adverts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `desc` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `onclick_url` varchar(45) DEFAULT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_adverts_tbl_adverts_type1_idx` (`type_id`),
  CONSTRAINT `fk_tbl_adverts_tbl_adverts_type1` FOREIGN KEY (`type_id`) REFERENCES `tbl_adverts_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_adverts`
--

LOCK TABLES `tbl_adverts` WRITE;
/*!40000 ALTER TABLE `tbl_adverts` DISABLE KEYS */;
INSERT INTO `tbl_adverts` VALUES (1,NULL,1,'home-main-slide-1',NULL,'home-main-slide-1.jpg',NULL,'2015-01-19 08:31:15','2020-09-19 08:31:15',NULL,NULL,NULL),(2,NULL,1,'home-main-slide-2',NULL,'home-main-slide-2.jpg',NULL,'2015-01-19 08:31:15','2020-09-19 08:31:15',NULL,NULL,NULL),(3,NULL,2,'home-latest-product-1',NULL,'home-latest-product-1.jpg',NULL,'2015-01-19 08:31:15','2020-09-19 08:31:15',NULL,NULL,NULL),(4,NULL,2,'home-latest-product-2',NULL,'home-latest-product-2.jpg',NULL,'2015-01-19 08:31:15','2020-09-19 08:31:15',NULL,NULL,NULL),(5,NULL,2,'home-latest-product-3',NULL,'home-latest-product-3.jpg',NULL,'2015-01-19 08:31:15','2020-09-19 08:31:15',NULL,NULL,NULL),(6,NULL,3,'home-featured-shop',NULL,'home-featured-shop.jpg',NULL,'2015-01-19 08:31:15','2020-09-19 08:31:15',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_adverts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_adverts_type`
--

DROP TABLE IF EXISTS `tbl_adverts_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_adverts_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `page` varchar(60) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_adverts_type`
--

LOCK TABLES `tbl_adverts_type` WRITE;
/*!40000 ALTER TABLE `tbl_adverts_type` DISABLE KEYS */;
INSERT INTO `tbl_adverts_type` VALUES (1,'Home Main Slider','homepage',457,518,NULL),(2,'Home Latest Products','homepage',195,152,NULL),(3,'Home Featured Shop','homepage',195,300,NULL),(4,'Sidebar Category Popup','',172,147,NULL);
/*!40000 ALTER TABLE `tbl_adverts_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_brand_category`
--

DROP TABLE IF EXISTS `tbl_brand_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_brand_category` (
  `cat_id` int(10) unsigned DEFAULT NULL,
  `brand_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_tbl_brand_category_tbl_brands1_idx` (`brand_id`),
  KEY `fk_tbl_brand_category_tbl_category1_idx` (`cat_id`),
  CONSTRAINT `fk_tbl_brand_category_tbl_brands1` FOREIGN KEY (`brand_id`) REFERENCES `tbl_brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_brand_category_tbl_category1` FOREIGN KEY (`cat_id`) REFERENCES `tbl_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_brand_category`
--

LOCK TABLES `tbl_brand_category` WRITE;
/*!40000 ALTER TABLE `tbl_brand_category` DISABLE KEYS */;
INSERT INTO `tbl_brand_category` VALUES (8,11),(8,12),(8,13),(8,14),(8,15),(1,16),(2,16);
/*!40000 ALTER TABLE `tbl_brand_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_brands`
--

DROP TABLE IF EXISTS `tbl_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_alt` varchar(100) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_brands_tbl_stores1_idx` (`store_id`),
  CONSTRAINT `fk_tbl_brands_tbl_stores1` FOREIGN KEY (`store_id`) REFERENCES `tbl_stores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_brands`
--

LOCK TABLES `tbl_brands` WRITE;
/*!40000 ALTER TABLE `tbl_brands` DISABLE KEYS */;
INSERT INTO `tbl_brands` VALUES (11,4,'Apple','apple','5653797b938c2.png','56537b38d5a92.jpg',1),(12,4,'Asus','asus','56537b7054f76.png','5653aa4b34858.jpg',1),(13,4,'Samsung','samsung','5653aaea81bab.png','5653aae48b645.jpg',1),(14,4,'Lenovo','lenovo','5653aafbe4d0e.png','5653ab177a01e.jpg',1),(15,4,'LG','lg','5653ab59aa496.png','5653ab70f2b46.jpg',1),(16,1,'Mint','mint','5656895a9ebe5.png','565689531a768.jpg',1);
/*!40000 ALTER TABLE `tbl_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `featured` tinyint(4) DEFAULT '0',
  `icon` varchar(100) DEFAULT NULL,
  `img_primary` varchar(100) DEFAULT 'no-image.png',
  `img_home` varchar(100) DEFAULT 'no-image.png',
  `tree_parent` varchar(100) DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT '0',
  `status` enum('Pending','Approved','Rejected') DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_category`
--

LOCK TABLES `tbl_category` WRITE;
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` VALUES (1,0,'Women\'s Clothing','womens-clothing',NULL,1,1,'icon-dresses6','no-image.png','no-image.png',NULL,0,NULL,NULL),(2,0,'Men\'s Clothing','mens-clothing',NULL,1,1,'icon-sportive5','no-image.png','no-image.png',NULL,0,NULL,NULL),(3,0,'Sports & Outdoor','sports-outdoor',NULL,1,1,'icon-basketball32','no-image.png','no-image.png',NULL,0,NULL,NULL),(4,0,'Kids Wear','kids-wear',NULL,1,1,'icon-shoes31','no-image.png','no-image.png',NULL,0,NULL,NULL),(5,0,'Baby Products','baby-products',NULL,1,1,'icon-rocking1','no-image.png','no-image.png',NULL,0,NULL,NULL),(6,0,'Household Products','household-products',NULL,1,1,'icon-wiping11','no-image.png','no-image.png',NULL,0,NULL,NULL),(7,0,'Wedding Supplies','wedding-supplies',NULL,1,1,'icon-elegant3','no-image.png','no-image.png',NULL,0,NULL,NULL),(8,0,'Digital Products','digital-products',NULL,1,1,'icon-iphone26','no-image.png','no-image.png',NULL,0,NULL,NULL),(9,0,'Home Appliances','home-appliances',NULL,1,1,'icon-kitchenpack25','no-image.png','no-image.png',NULL,0,NULL,NULL),(10,0,'Beauty Care','beauty-care',NULL,1,1,'icon-makeup7','no-image.png','no-image.png',NULL,0,NULL,NULL),(11,0,'Discounted Products','discounted-products',NULL,1,1,'icon-briefcase13','no-image.png','no-image.png',NULL,0,NULL,NULL),(12,1,'Dress','dress',NULL,1,1,NULL,'no-image.png','no-image.png','1',0,NULL,NULL),(13,1,'Shoes','shoes-women',NULL,1,1,NULL,'no-image.png','no-image.png','1',0,NULL,NULL),(14,1,'Bags','bags-women',NULL,1,1,NULL,'no-image.png','no-image.png','1',0,NULL,NULL),(15,1,'Jeans','jeans-women',NULL,1,1,NULL,'no-image.png','no-image.png','1',0,NULL,NULL),(16,12,'T-Shirt','t-shirt-women',NULL,1,1,NULL,'no-image.png','no-image.png','1-12',0,NULL,NULL),(17,12,'Sexy Vest','sexy-vest',NULL,1,1,NULL,'no-image.png','no-image.png','1-12',0,NULL,NULL),(18,12,'Leisure Suit','leisure-suit',NULL,1,1,NULL,'no-image.png','no-image.png','1-12',0,NULL,NULL),(19,12,'Sweater','sweater-women',NULL,1,1,NULL,'no-image.png','no-image.png','1-12',0,NULL,NULL),(20,13,'Pointed Shoes','pointed-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','1-13',0,NULL,NULL),(21,13,'Heeled Sandals','heeled-sandals',NULL,1,1,NULL,'no-image.png','no-image.png','1-13',0,NULL,NULL),(22,13,'Heeled Shoes','heeled-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','1-13',0,NULL,NULL),(23,13,'Carrefour Shoes','carrefour-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','1-13',0,NULL,NULL),(24,14,'Shoulder Bags','shoulder-bags',NULL,1,1,NULL,'no-image.png','no-image.png','1-14',0,NULL,NULL),(25,14,'Clutch','clutch',NULL,1,1,NULL,'no-image.png','no-image.png','1-14',0,NULL,NULL),(26,14,'Purse','purse',NULL,1,1,NULL,'no-image.png','no-image.png','1-14',0,NULL,NULL),(27,15,'Denim','denim-jeans',NULL,1,1,NULL,'no-image.png','no-image.png','1-15',0,NULL,NULL),(28,15,'Ripped Jeans','ripped-jeans',NULL,1,1,NULL,'no-image.png','no-image.png','1-15',0,NULL,NULL),(29,15,'Hip Huggers','hip-huggers',NULL,1,1,NULL,'no-image.png','no-image.png','1-15',0,NULL,NULL),(36,2,'Shirts','shirts-men',NULL,1,1,NULL,'no-image.png','no-image.png','2',0,NULL,NULL),(37,2,'Shoes','shoes-men',NULL,1,1,NULL,'no-image.png','no-image.png','2',0,NULL,NULL),(38,2,'Shorts','shorts-men',NULL,1,1,NULL,'no-image.png','no-image.png','2',0,NULL,NULL),(39,2,'Pants','pants-men',NULL,1,1,NULL,'no-image.png','no-image.png','2',0,NULL,NULL),(48,36,'Shirt','shirt-men',NULL,1,1,NULL,'no-image.png','no-image.png','2-36',0,NULL,NULL),(49,36,'Jacket','jacket-men',NULL,1,1,NULL,'no-image.png','no-image.png','2-36',0,NULL,NULL),(50,36,'Suit','suit-men',NULL,1,1,NULL,'no-image.png','no-image.png','2-36',0,NULL,NULL),(51,36,'Sweater','sweater-men',NULL,1,1,NULL,'no-image.png','no-image.png','2-36',0,NULL,NULL),(52,37,'Canvas Shoes','canvas-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','2-37',0,NULL,NULL),(53,37,'Casual Shoes','casual-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','2-37',0,NULL,NULL),(54,37,'Running Shoes','running-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','2-37',0,NULL,NULL),(55,38,'Men Shorts','men-shorts',NULL,1,1,NULL,'no-image.png','no-image.png','2-38',0,NULL,NULL),(56,38,'Boxer Shorts','boxer-shorts',NULL,1,1,NULL,'no-image.png','no-image.png','2-38',0,NULL,NULL),(57,38,'Underwear','men-underwear',NULL,1,1,NULL,'no-image.png','no-image.png','2-38',0,NULL,NULL),(58,39,'Fitted','fitted-pants',NULL,1,1,NULL,'no-image.png','no-image.png','2-39',0,NULL,NULL),(59,39,'Jogging Pants','jogging-pants',NULL,1,1,NULL,'no-image.png','no-image.png','2-39',0,NULL,NULL),(60,39,'Casual','casual-pants',NULL,1,1,NULL,'no-image.png','no-image.png','2-39',0,NULL,NULL),(61,3,'Outdoor Gears','outdoor-gears',NULL,1,1,NULL,'no-image.png','no-image.png','3',0,NULL,NULL),(62,3,'Sport Shoes','sport-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','3',0,NULL,NULL),(63,3,'Sports Gears','sports-gears',NULL,1,1,NULL,'no-image.png','no-image.png','3',0,NULL,NULL),(64,3,'Swim Gears','swim-gears',NULL,1,1,NULL,'no-image.png','no-image.png','3',0,NULL,NULL),(65,61,'Outdoor Kettle','outdoor-kettle',NULL,1,1,'','no-image.png','no-image.png','3-61',0,NULL,NULL),(66,61,'Telescope','telescope',NULL,1,1,'','no-image.png','no-image.png','3-61',0,NULL,NULL),(67,61,'Tent','tent',NULL,1,1,'','no-image.png','no-image.png','3-61',0,NULL,NULL),(68,62,'Badminton','badminton-shoes',NULL,1,1,'','no-image.png','no-image.png','3-62',0,NULL,NULL),(69,62,'Tennis','tennis-shoes',NULL,1,1,'','no-image.png','no-image.png','3-62',0,NULL,NULL),(70,62,'Sports Safety','sports-safety',NULL,1,1,'','no-image.png','no-image.png','3-62',0,NULL,NULL),(71,63,'Sport Skirt','sport-skirt',NULL,1,1,'','no-image.png','no-image.png','3-63',0,NULL,NULL),(72,63,'Sneakers','sneakers',NULL,1,1,'','no-image.png','no-image.png','3-63',0,NULL,NULL),(73,64,'Nemesis Fins','nemesis-fins',NULL,1,1,'','no-image.png','no-image.png','3-64',0,NULL,NULL),(74,64,'Aqua Shoes','aqua-shoes',NULL,1,1,'','no-image.png','no-image.png','3-64',0,NULL,NULL),(75,64,'Hydro Belt','hydro-belt',NULL,1,1,'','no-image.png','no-image.png','3-64',0,NULL,NULL),(76,64,'Snorkel','snorkel',NULL,1,1,'','no-image.png','no-image.png','3-64',0,NULL,NULL),(77,4,'Clothes','clothes-kids',NULL,1,1,NULL,'no-image.png','no-image.png','4',0,NULL,NULL),(78,4,'Shoes','kids-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','4',0,NULL,NULL),(79,77,'Pyjamas','pyjamas-kids',NULL,1,1,NULL,'no-image.png','no-image.png','4-77',0,NULL,NULL),(80,77,'Sweater','kids-sweater',NULL,1,1,NULL,'no-image.png','no-image.png','4-77',0,NULL,NULL),(81,77,'Jacket','kids-jacket',NULL,1,1,NULL,'no-image.png','no-image.png','4-77',0,NULL,NULL),(82,77,'Shirt','kids-shirt',NULL,1,1,NULL,'no-image.png','no-image.png','4-77',0,NULL,NULL),(83,78,'Sport Shoes','kids-sport-shoes',NULL,1,1,NULL,'no-image.png','no-image.png','4-78',0,NULL,NULL),(84,78,'Snow Boots','kids-snow-boots',NULL,1,1,NULL,'no-image.png','no-image.png','4-78',0,NULL,NULL),(85,78,'Socks','kids-socks',NULL,1,1,NULL,'no-image.png','no-image.png','4-78',0,NULL,NULL),(86,78,'Tights','kids-tights',NULL,1,1,NULL,'no-image.png','no-image.png','4-78',0,NULL,NULL),(87,5,'Maternal Products','maternal-products',NULL,1,1,NULL,'no-image.png','no-image.png','5',0,NULL,NULL),(88,5,'Feeding Supplies','feeding-supplies',NULL,1,1,NULL,'no-image.png','no-image.png','5',0,NULL,NULL),(89,5,'Baby Supplies','baby-supplies',NULL,1,1,NULL,'no-image.png','no-image.png','5',0,NULL,NULL),(90,87,'Care Belly Pants','care-belly-pants',NULL,1,1,NULL,'no-image.png','no-image.png','5-87',0,NULL,NULL),(91,87,'Waist Pillow','waist-pillow',NULL,1,1,NULL,'no-image.png','no-image.png','5-87',0,NULL,NULL),(92,87,'Stretch Marks','stretch-marks',NULL,1,1,NULL,'no-image.png','no-image.png','5-87',0,NULL,NULL),(93,87,'Leggings','maternal-leggings',NULL,1,1,NULL,'no-image.png','no-image.png','5-87',0,NULL,NULL),(94,87,'Dress','maternal-dress',NULL,1,1,NULL,'no-image.png','no-image.png','5-87',0,NULL,NULL),(95,88,'Nursing Bra','nursing-bra',NULL,1,1,NULL,'no-image.png','no-image.png','5-88',0,NULL,NULL),(96,88,'Warmer','baby-warmer',NULL,1,1,NULL,'no-image.png','no-image.png','5-88',0,NULL,NULL),(97,88,'Breast Pump','breast-pump',NULL,1,1,NULL,'no-image.png','no-image.png','5-88',0,NULL,NULL),(98,88,'Breast Pads','breast-pads',NULL,1,1,NULL,'no-image.png','no-image.png','5-88',0,NULL,NULL),(99,88,'Mummy Bag','mummy-bag',NULL,1,1,NULL,'no-image.png','no-image.png','5-88',0,NULL,NULL),(106,89,'Strollers','strollers',NULL,1,1,NULL,'no-image.png','no-image.png','5-89',0,NULL,NULL),(107,89,'Crib','crib',NULL,1,1,NULL,'no-image.png','no-image.png','5-89',0,NULL,NULL),(108,89,'Changing Mat','changing-mat',NULL,1,1,NULL,'no-image.png','no-image.png','5-89',0,NULL,NULL),(109,89,'Feeding Bottles','fedding-bottles',NULL,1,1,NULL,'no-image.png','no-image.png','5-89',0,NULL,NULL),(110,89,'Seat Strap','seat-strap',NULL,1,1,NULL,'no-image.png','no-image.png','5-89',0,NULL,NULL),(111,6,'Storage','storage',NULL,1,1,NULL,'no-image.png','no-image.png','6',0,NULL,NULL),(112,6,'Textile Cloth','textile-cloth',NULL,1,1,NULL,'no-image.png','no-image.png','6',0,NULL,NULL),(113,6,'House Cleaning','house-cleaning',NULL,1,1,NULL,'no-image.png','no-image.png','6',0,NULL,NULL),(114,111,'Storage Box','storage-box',NULL,1,1,NULL,'no-image.png','no-image.png','6-111',0,NULL,NULL),(115,111,'Cosmetics','cosmetics',NULL,1,1,NULL,'no-image.png','no-image.png','6-111',0,NULL,NULL),(116,111,'Compression Bag','compression-bag',NULL,1,1,NULL,'no-image.png','no-image.png','6-111',0,NULL,NULL),(117,111,'Tissue Box','tissue-box',NULL,1,1,NULL,'no-image.png','no-image.png','6-111',0,NULL,NULL),(118,111,'Jewelry Storage','jewelry-storage',NULL,1,1,NULL,'no-image.png','no-image.png','6-111',0,NULL,NULL),(119,111,'Hook Shoe Box','hook-shoe-box',NULL,1,1,NULL,'no-image.png','no-image.png','6-111',0,NULL,NULL),(120,112,'Denim','textile-denim',NULL,1,1,NULL,'no-image.png','no-image.png','6-112',0,NULL,NULL),(121,112,'Sofa Cushion','sofa-cushion',NULL,1,1,NULL,'no-image.png','no-image.png','6-112',0,NULL,NULL),(122,112,'Carpet','carpet',NULL,1,1,NULL,'no-image.png','no-image.png','6-112',0,NULL,NULL),(123,112,'Sticth','textile-stitch',NULL,1,1,NULL,'no-image.png','no-image.png','6-112',0,NULL,NULL),(124,112,'Towels','towels',NULL,1,1,NULL,'no-image.png','no-image.png','6-112',0,NULL,NULL),(125,112,'Curtain Finished','curtain-finished',NULL,1,1,NULL,'no-image.png','no-image.png','6-112',0,NULL,NULL),(126,113,'Mop','mop',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(127,113,'Trash','trash',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(128,113,'Toilet','toilet',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(129,113,'Wipe Slippers','wipe-slippers',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(130,113,'Cuff','cuff',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(131,113,'Rubber Gloves','rubber-gloves',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(132,113,'Aprons','aprons',NULL,1,1,NULL,'no-image.png','no-image.png','6-113',0,NULL,NULL),(133,7,'Wedding Dress','wedding-dress',NULL,1,1,NULL,'no-image.png','no-image.png','7',0,NULL,NULL),(134,7,'Wedding Supplies','wedding-supplies-list',NULL,1,1,NULL,'no-image.png','no-image.png','7',0,NULL,NULL),(135,7,'Clothes & Accessories','clothes-accessories',NULL,1,1,NULL,'no-image.png','no-image.png','7',0,NULL,NULL),(136,133,'Trailing Wedding','trailing-wedding',NULL,1,1,NULL,'no-image.png','no-image.png','7-133',0,NULL,NULL),(137,133,'Gown','gown',NULL,1,1,NULL,'no-image.png','no-image.png','7-133',0,NULL,NULL),(138,133,'Bridesmaid Dress','bridesmaid-dress',NULL,1,1,NULL,'no-image.png','no-image.png','7-133',0,NULL,NULL),(139,133,'Toast Clothing','toast-clothing',NULL,1,1,NULL,'no-image.png','no-image.png','7-133',0,NULL,NULL),(140,133,'Suits Groom','suits-groom',NULL,1,1,NULL,'no-image.png','no-image.png','7-133',0,NULL,NULL),(141,133,'Bow Tie','bow-tie',NULL,1,1,NULL,'no-image.png','no-image.png','7-133',0,NULL,NULL),(142,134,'Invitations','invitations',NULL,1,1,NULL,'no-image.png','no-image.png','7-134',0,NULL,NULL),(143,134,'Candy Box','candy-box',NULL,1,1,NULL,'no-image.png','no-image.png','7-134',0,NULL,NULL),(144,134,'Candy Bags','candy-bags',NULL,1,1,NULL,'no-image.png','no-image.png','7-134',0,NULL,NULL),(145,134,'Kneeling Pads','kneeling-pads',NULL,1,1,NULL,'no-image.png','no-image.png','7-134',0,NULL,NULL),(146,134,'Wedding Decoration','wedding-decoration',NULL,1,1,NULL,'no-image.png','no-image.png','7-134',0,NULL,NULL),(147,134,'Ring Pillow','ring-pillow',NULL,1,1,NULL,'no-image.png','no-image.png','7-134',0,NULL,NULL),(148,135,'Viel','wedding-viel',NULL,1,1,NULL,'no-image.png','no-image.png','7-135',0,NULL,NULL),(149,135,'Headband','wedding-headband',NULL,1,1,NULL,'no-image.png','no-image.png','7-135',0,NULL,NULL),(150,135,'Gloves','wedding-gloves',NULL,1,1,NULL,'no-image.png','no-image.png','7-135',0,NULL,NULL),(151,135,'Curd','wedding-curd',NULL,1,1,NULL,'no-image.png','no-image.png','7-135',0,NULL,NULL),(152,135,'Shawl','wedding-shawl',NULL,1,1,NULL,'no-image.png','no-image.png','7-135',0,NULL,NULL),(153,135,'Belt','wedding-belt',NULL,1,1,NULL,'no-image.png','no-image.png','7-135',0,NULL,NULL),(154,8,'PC & Laptops','pc-laptops',NULL,1,1,NULL,'no-image.png','catimg14.jpg','8',0,NULL,NULL),(155,8,'Camera','camera',NULL,1,1,NULL,'no-image.png','catimg15.jpg','8',0,NULL,NULL),(156,8,'Mobile Phones','mobile-phones',NULL,1,1,NULL,'no-image.png','catimg13.jpg','8',0,NULL,NULL),(157,8,'Accessories','digital-accessories',NULL,1,1,NULL,'no-image.png','catimg16.jpg','8',0,NULL,NULL),(158,154,'PC Packages','pc-packages',NULL,1,1,NULL,'no-image.png','no-image.png','8-154',0,NULL,NULL),(159,154,'Gaming Laptops','gaming-laptops',NULL,1,1,NULL,'no-image.png','no-image.png','8-154',0,NULL,NULL),(160,155,'Camera Tripod','camera-tripod',NULL,1,1,NULL,'no-image.png','no-image.png','8-155',0,NULL,NULL),(161,155,'Camera Bag','camera-bag',NULL,1,1,NULL,'no-image.png','no-image.png','8-155',0,NULL,NULL),(162,155,'Micro Camera','micro-camera',NULL,1,1,NULL,'no-image.png','no-image.png','8-155',0,NULL,NULL),(163,155,'SLR Accessories','slr-accessories',NULL,1,1,NULL,'no-image.png','no-image.png','8-155',0,NULL,NULL),(164,156,'Smart Phones','smart-phones',NULL,1,1,NULL,'no-image.png','no-image.png','8-156',0,NULL,NULL),(165,156,'Basic Phones','basig-phones',NULL,1,1,NULL,'no-image.png','no-image.png','8-156',0,NULL,NULL),(166,156,'Android','phone-android',NULL,1,1,NULL,'no-image.png','no-image.png','8-156',0,NULL,NULL),(167,156,'Apple','phone-apple',NULL,1,1,NULL,'no-image.png','no-image.png','8-156',0,NULL,NULL),(168,157,'Mouse','mouse',NULL,1,1,NULL,'no-image.png','no-image.png','8-157',0,NULL,NULL),(169,157,'Network Tools','network-tools',NULL,1,1,NULL,'no-image.png','no-image.png','8-157',0,NULL,NULL),(170,157,'Computer Tables','computer-tables',NULL,1,1,NULL,'no-image.png','no-image.png','8-157',0,NULL,NULL),(171,8,'Storage','digital-storage',NULL,1,1,NULL,'no-image.png','no-image.png','8',0,NULL,NULL),(172,171,'Memory/SD Cards','memory-sd-cards',NULL,1,1,NULL,'no-image.png','no-image.png','8-171',0,NULL,NULL),(173,171,'External HD','external-hd',NULL,1,1,NULL,'no-image.png','no-image.png','8-171',0,NULL,NULL),(174,9,'Appliances','appliances',NULL,1,1,NULL,'no-image.png','no-image.png','9',0,NULL,NULL),(175,9,'Living Room','living-room',NULL,1,1,NULL,'no-image.png','no-image.png','9',0,NULL,NULL),(176,9,'Kitchen','kitchen',NULL,1,1,NULL,'no-image.png','no-image.png','9',0,NULL,NULL),(177,9,'Tools','house-tools',NULL,1,1,NULL,'no-image.png','no-image.png','9',0,NULL,NULL),(178,174,'Flat Screen TV','flat-screen-tv',NULL,1,1,NULL,'no-image.png','no-image.png','9-174',0,NULL,NULL),(179,174,'Washing Machine','washing-machine',NULL,1,1,NULL,'no-image.png','no-image.png','9-174',0,NULL,NULL),(180,174,'Refrigerator','refrigerator',NULL,1,1,NULL,'no-image.png','no-image.png','9-174',0,NULL,NULL),(181,174,'Gas Stoves','gas-stoves',NULL,1,1,NULL,'no-image.png','no-image.png','9-174',0,NULL,NULL),(182,174,'Gas Water Heater','gas-water-heater',NULL,1,1,NULL,'no-image.png','no-image.png','9-174',0,NULL,NULL),(183,174,'Air Conditioning','air-conditioning',NULL,1,1,NULL,'no-image.png','no-image.png','9-174',0,NULL,NULL),(184,175,'Audio Cable','audio-cable',NULL,1,1,NULL,'no-image.png','no-image.png','9-175',0,NULL,NULL),(185,175,'HDMI Cable','hdmi-cable',NULL,1,1,NULL,'no-image.png','no-image.png','9-175',0,NULL,NULL),(186,175,'Speaker','speaker',NULL,1,1,NULL,'no-image.png','no-image.png','9-175',0,NULL,NULL),(187,175,'Amplifier','amplifier',NULL,1,1,NULL,'no-image.png','no-image.png','9-175',0,NULL,NULL),(188,175,'Radio','radio',NULL,1,1,NULL,'no-image.png','no-image.png','9-175',0,NULL,NULL),(189,176,'Humidifier','humidifier',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(190,176,'Cleaner','cleaner',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(191,176,'Electric Kettle','electric-kettle',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(192,176,'Cooker','cooker',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(193,176,'Purifier','purifier',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(194,176,'Telephone','telephone',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(195,176,'Electric Boxes','electric-boxes',NULL,1,1,NULL,'no-image.png','no-image.png','9-176',0,NULL,NULL),(196,177,'Hammer','hammer',NULL,1,1,NULL,'no-image.png','no-image.png','9-177',0,NULL,NULL),(197,177,'Pliers','pliers',NULL,1,1,NULL,'no-image.png','no-image.png','9-177',0,NULL,NULL),(198,177,'Wrench','wrench',NULL,1,1,NULL,'no-image.png','no-image.png','9-177',0,NULL,NULL),(199,10,'Beauty Tools','beauty-tools',NULL,1,1,NULL,'no-image.png','no-image.png','10',0,NULL,NULL),(200,10,'Beauty Appliances','beaty-appliances',NULL,1,1,NULL,'no-image.png','no-image.png','10',0,NULL,NULL),(201,10,'Cleansing','cleansing',NULL,1,1,NULL,'no-image.png','no-image.png','10',0,NULL,NULL),(202,199,'Eyebrow Shaping Device','eyebrow-shaping-device',NULL,1,1,NULL,'no-image.png','no-image.png','10-199',0,NULL,NULL),(203,199,'Eyelash Curler','eyelash-curler',NULL,1,1,NULL,'no-image.png','no-image.png','10-199',0,NULL,NULL),(204,199,'Eyelash Brush','eyelash-brush',NULL,1,1,NULL,'no-image.png','no-image.png','10-199',0,NULL,NULL),(205,199,'False Eyelash','false-eyelash',NULL,1,1,NULL,'no-image.png','no-image.png','10-199',0,NULL,NULL),(206,199,'Cosmetic Cases','cosmetic-cases',NULL,1,1,NULL,'no-image.png','no-image.png','10-199',0,NULL,NULL),(207,200,'Beauty Products','beautyproducts',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(208,200,'Beauty Sprayer','beauty-sprayer',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(209,200,'Hair Dryer','hair-dryer',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(210,200,'Facelift Tool','facelift-yool',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(211,200,'Barber','barber',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(212,200,'Epilator','epilator',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(213,200,'Nose Hair Trimmer','nose-hairtrimmer',NULL,1,1,NULL,'no-image.png','no-image.png','10-200',0,NULL,NULL),(214,201,'Shaver','shaver',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(215,201,'Electric Toothbrush','electric-toothbrush',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(216,201,'Scales','scales',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(217,201,'Footh Bath','footh-bath',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(218,201,'Scraping Platters','scraping-platters',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(219,201,'Massage Hammer','massage-hammer',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(220,201,'Massage Pillow','massage-pillow',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(221,201,'Massage Cape','massage-cape',NULL,1,1,NULL,'no-image.png','no-image.png','10-201',0,NULL,NULL),(222,11,'Sample Inactive (parent Discounted Products)','asdfasdf-adsf',NULL,1,1,NULL,'no-image.png','no-image.png','11',0,NULL,NULL),(223,11,'This is requested by client','asdf-asdf-asd',NULL,1,1,NULL,'no-image.png','no-image.png','11',0,NULL,NULL),(226,1,'User Custom Category','user-custom-category',NULL,0,0,'','no-image.png','no-image.png','1',4,'Pending',NULL),(227,0,'Test Add Category','test-add-category',NULL,0,0,'','no-image.png','no-image.png',NULL,4,'Pending',NULL),(229,0,'asdfasdf','asdfasdf',NULL,0,0,'','no-image.png','no-image.png',NULL,4,'Rejected','asdfasdf');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `wyc2`.`tbl_category_BEFORE_INSERT`
BEFORE INSERT ON `wyc2`.`tbl_category`
FOR EACH ROW
IF NEW.parent > 0 THEN
	SET @cat_id = NEW.parent;
	SET @tree_parent = @cat_id;
	
	WHILE (@cat_id > 0) DO
		SELECT parent INTO @p FROM tbl_category 
			WHERE id = @cat_id;
			
			IF @p > 0 THEN
				SET @tree_parent = CONCAT('-', @tree_parent);
				SET @tree_parent = CONCAT( @p, @tree_parent);
			END IF;
			
		SET @cat_id = @p;
	END WHILE;
	
	SET NEW.tree_parent = @tree_parent;
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `wyc2`.`tbl_category_BEFORE_UPDATE`
BEFORE UPDATE ON `wyc2`.`tbl_category`
FOR EACH ROW

IF NEW.parent > 0 THEN
	SET @cat_id = NEW.parent;
	SET @tree_parent = @cat_id;
	
	WHILE (@cat_id > 0) DO
		SELECT parent INTO @p FROM tbl_category 
			WHERE id = @cat_id;
			
			IF @p > 0 THEN
				SET @tree_parent = CONCAT('-', @tree_parent);
				SET @tree_parent = CONCAT( @p, @tree_parent);
			END IF;
			
		SET @cat_id = @p;
	END WHILE;
	
	SET NEW.tree_parent = @tree_parent;
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_city`
--

DROP TABLE IF EXISTS `tbl_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_city` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned NOT NULL,
  `city_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_city_tbl_region1_idx` (`region_id`),
  CONSTRAINT `fk_tbl_city_tbl_region1` FOREIGN KEY (`region_id`) REFERENCES `tbl_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1637 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_city`
--

LOCK TABLES `tbl_city` WRITE;
/*!40000 ALTER TABLE `tbl_city` DISABLE KEYS */;
INSERT INTO `tbl_city` VALUES (1,1,'Bangued'),(2,1,'Boliney'),(3,1,'Bucay'),(4,1,'Bucloc'),(5,1,'Daguioman'),(6,1,'Danglas'),(7,1,'Dolores'),(8,1,'La Paz'),(9,1,'Lacub'),(10,1,'Lagangilang'),(11,1,'Lagayan'),(12,1,'Langiden'),(13,1,'Licuan-Baay'),(14,1,'Luba'),(15,1,'Malibcong'),(16,1,'Manabo'),(17,1,'Peñarrubia'),(18,1,'Pidigan'),(19,1,'Pilar'),(20,1,'Sallapadan'),(21,1,'San Isidro'),(22,1,'San Juan'),(23,1,'San Quintin'),(24,1,'Tayum'),(25,1,'Tineg'),(26,1,'Tubo'),(27,1,'Villaviciosa'),(28,2,'Butuan City'),(29,2,'Buenavista'),(30,2,'Cabadbaran'),(31,2,'Carmen'),(32,2,'Jabonga'),(33,2,'Kitcharao'),(34,2,'Las Nieves'),(35,2,'Magallanes'),(36,2,'Nasipit'),(37,2,'Remedios T. Romualdez'),(38,2,'Santiago'),(39,2,'Tubay'),(40,3,'Bayugan'),(41,3,'Bunawan'),(42,3,'Esperanza'),(43,3,'La Paz'),(44,3,'Loreto'),(45,3,'Prosperidad'),(46,3,'Rosario'),(47,3,'San Francisco'),(48,3,'San Luis'),(49,3,'Santa Josefa'),(50,3,'Sibagat'),(51,3,'Talacogon'),(52,3,'Trento'),(53,3,'Veruela'),(54,4,'Altavas'),(55,4,'Balete'),(56,4,'Banga'),(57,4,'Batan'),(58,4,'Buruanga'),(59,4,'Ibajay'),(60,4,'Kalibo'),(61,4,'Lezo'),(62,4,'Libacao'),(63,4,'Madalag'),(64,4,'Makato'),(65,4,'Malay'),(66,4,'Malinao'),(67,4,'Nabas'),(68,4,'New Washington'),(69,4,'Numancia'),(70,4,'Tangalan'),(71,5,'Legazpi City'),(72,5,'Ligao City'),(73,5,'Tabaco City'),(74,5,'Bacacay'),(75,5,'Camalig'),(76,5,'Daraga'),(77,5,'Guinobatan'),(78,5,'Jovellar'),(79,5,'Libon'),(80,5,'Malilipot'),(81,5,'Malinao'),(82,5,'Manito'),(83,5,'Oas'),(84,5,'Pio Duran'),(85,5,'Polangui'),(86,5,'Rapu-Rapu'),(87,5,'Santo Domingo'),(88,5,'Tiwi'),(89,6,'Anini-y'),(90,6,'Barbaza'),(91,6,'Belison'),(92,6,'Bugasong'),(93,6,'Caluya'),(94,6,'Culasi'),(95,6,'Hamtic'),(96,6,'Laua-an'),(97,6,'Libertad'),(98,6,'Pandan'),(99,6,'Patnongon'),(100,6,'San Jose'),(101,6,'San Remigio'),(102,6,'Sebaste'),(103,6,'Sibalom'),(104,6,'Tibiao'),(105,6,'Tobias Fornier'),(106,6,'Valderrama'),(107,7,'Calanasan'),(108,7,'Conner'),(109,7,'Flora'),(110,7,'Kabugao'),(111,7,'Luna'),(112,7,'Pudtol'),(113,7,'Santa Marcela'),(114,8,'Baler'),(115,8,'Casiguran'),(116,8,'Dilasag'),(117,8,'Dinalungan'),(118,8,'Dingalan'),(119,8,'Dipaculao'),(120,8,'Maria Aurora'),(121,8,'San Luis'),(122,9,'Isabela City'),(123,9,'Akbar'),(124,9,'Al-Barka'),(125,9,'Hadji Mohammad Ajul'),(126,9,'Hadji Muhtamad'),(127,9,'Lamitan'),(128,9,'Lantawan'),(129,9,'Maluso'),(130,9,'Sumisip'),(131,9,'Tabuan-Lasa'),(132,9,'Tipo-Tipo'),(133,9,'Tuburan'),(134,9,'Ungkaya Pukan'),(135,10,'Balanga City'),(136,10,'Abucay'),(137,10,'Bagac'),(138,10,'Dinalupihan'),(139,10,'Hermosa'),(140,10,'Limay'),(141,10,'Mariveles'),(142,10,'Morong'),(143,10,'Orani'),(144,10,'Orion'),(145,10,'Pilar'),(146,10,'Samal'),(147,11,'Basco'),(148,11,'Itbayat'),(149,11,'Ivana'),(150,11,'Mahatao'),(151,11,'Sabtang'),(152,11,'Uyugan'),(153,12,'Batangas City'),(154,12,'Lipa City'),(155,12,'Tanauan City'),(156,12,'Agoncillo'),(157,12,'Alitagtag'),(158,12,'Balayan'),(159,12,'Balete'),(160,12,'Bauan'),(161,12,'Calaca'),(162,12,'Calatagan'),(163,12,'Cuenca'),(164,12,'Ibaan'),(165,12,'Laurel'),(166,12,'Lemery'),(167,12,'Lian'),(168,12,'Lobo'),(169,12,'Mabini'),(170,12,'Malvar'),(171,12,'Mataas na Kahoy'),(172,12,'Nasugbu'),(173,12,'Padre Garcia'),(174,12,'Rosario'),(175,12,'San Jose'),(176,12,'San Juan'),(177,12,'San Luis'),(178,12,'San Nicolas'),(179,12,'San Pascual'),(180,12,'Santa Teresita'),(181,12,'Santo Tomas'),(182,12,'Taal'),(183,12,'Talisay'),(184,12,'Taysan'),(185,12,'Tingloy'),(186,12,'Tuy'),(187,13,'Baguio City'),(188,13,'Atok'),(189,13,'Bakun'),(190,13,'Bokod'),(191,13,'Buguias'),(192,13,'Itogon'),(193,13,'Kabayan'),(194,13,'Kapangan'),(195,13,'Kibungan'),(196,13,'La Trinidad'),(197,13,'Mankayan'),(198,13,'Sablan'),(199,13,'Tuba'),(200,13,'Tublay'),(201,14,'Almeria'),(202,14,'Biliran'),(203,14,'Cabucgayan'),(204,14,'Caibiran'),(205,14,'Culaba'),(206,14,'Kawayan'),(207,14,'Maripipi'),(208,14,'Naval'),(209,15,'Tagbilaran City'),(210,15,'Alburquerque'),(211,15,'Alicia'),(212,15,'Anda'),(213,15,'Antequera'),(214,15,'Baclayon'),(215,15,'Balilihan'),(216,15,'Batuan'),(217,15,'Bien Unido'),(218,15,'Bilar'),(219,15,'Buenavista'),(220,15,'Calape'),(221,15,'Candijay'),(222,15,'Carmen'),(223,15,'Catigbian'),(224,15,'Clarin'),(225,15,'Corella'),(226,15,'Cortes'),(227,15,'Dagohoy'),(228,15,'Danao'),(229,15,'Dauis'),(230,15,'Dimiao'),(231,15,'Duero'),(232,15,'Garcia Hernandez'),(233,15,'Getafe'),(234,15,'Guindulman'),(235,15,'Inabanga'),(236,15,'Jagna'),(237,15,'Lila'),(238,15,'Loay'),(239,15,'Loboc'),(240,15,'Loon'),(241,15,'Mabini'),(242,15,'Maribojoc'),(243,15,'Panglao'),(244,15,'Pilar'),(245,15,'President Carlos P. Garcia'),(246,15,'Sagbayan'),(247,15,'San Isidro'),(248,15,'San Miguel'),(249,15,'Sevilla'),(250,15,'Sierra Bullones'),(251,15,'Sikatuna'),(252,15,'Talibon'),(253,15,'Trinidad'),(254,15,'Tubigon'),(255,15,'Ubay'),(256,15,'Valencia'),(257,16,'Malaybalay City'),(258,16,'Valencia City'),(259,16,'Baungon'),(260,16,'Cabanglasan'),(261,16,'Damulog'),(262,16,'Dangcagan'),(263,16,'Don Carlos'),(264,16,'Impasug-ong'),(265,16,'Kadingilan'),(266,16,'Kalilangan'),(267,16,'Kibawe'),(268,16,'Kitaotao'),(269,16,'Lantapan'),(270,16,'Libona'),(271,16,'Malitbog'),(272,16,'Manolo Fortich'),(273,16,'Maramag'),(274,16,'Pangantucan'),(275,16,'Quezon'),(276,16,'San Fernando'),(277,16,'Sumilao'),(278,16,'Talakag'),(279,17,'Malolos City'),(280,17,'Meycauayan City'),(281,17,'San Jose del Monte City'),(282,17,'Angat'),(283,17,'Balagtas'),(284,17,'Baliuag'),(285,17,'Bocaue'),(286,17,'Bulacan'),(287,17,'Bustos'),(288,17,'Calumpit'),(289,17,'Doña Remedios Trinidad'),(290,17,'Guiguinto'),(291,17,'Hagonoy'),(292,17,'Marilao'),(293,17,'Norzagaray'),(294,17,'Obando'),(295,17,'Pandi'),(296,17,'Paombong'),(297,17,'Plaridel'),(298,17,'Pulilan'),(299,17,'San Ildefonso'),(300,17,'San Miguel'),(301,17,'San Rafael'),(302,17,'Santa Maria'),(303,18,'Tuguegarao City'),(304,18,'Abulug'),(305,18,'Alcala'),(306,18,'Allacapan'),(307,18,'Amulung'),(308,18,'Aparri'),(309,18,'Baggao'),(310,18,'Ballesteros'),(311,18,'Buguey'),(312,18,'Calayan'),(313,18,'Camalaniugan'),(314,18,'Claveria'),(315,18,'Enrile'),(316,18,'Gattaran'),(317,18,'Gonzaga'),(318,18,'Iguig'),(319,18,'Lal-lo'),(320,18,'Lasam'),(321,18,'Pamplona'),(322,18,'Peñablanca'),(323,18,'Piat'),(324,18,'Rizal'),(325,18,'Sanchez-Mira'),(326,18,'Santa Ana'),(327,18,'Santa Praxedes'),(328,18,'Santa Teresita'),(329,18,'Santo Niño'),(330,18,'Solana'),(331,18,'Tuao'),(332,19,'Basud'),(333,19,'Capalonga'),(334,19,'Daet'),(335,19,'Jose Panganiban'),(336,19,'Labo'),(337,19,'Mercedes'),(338,19,'Paracale'),(339,19,'San Lorenzo Ruiz'),(340,19,'San Vicente'),(341,19,'Santa Elena'),(342,19,'Talisay'),(343,19,'Vinzons'),(344,20,'Iriga City'),(345,20,'Naga City'),(346,20,'Baao'),(347,20,'Balatan'),(348,20,'Bato'),(349,20,'Bombon'),(350,20,'Buhi'),(351,20,'Bula'),(352,20,'Cabusao'),(353,20,'Calabanga'),(354,20,'Camaligan'),(355,20,'Canaman'),(356,20,'Caramoan'),(357,20,'Del Gallego'),(358,20,'Gainza'),(359,20,'Garchitorena'),(360,20,'Goa'),(361,20,'Lagonoy'),(362,20,'Libmanan'),(363,20,'Lupi'),(364,20,'Magarao'),(365,20,'Milaor'),(366,20,'Minalabac'),(367,20,'Nabua'),(368,20,'Ocampo'),(369,20,'Pamplona'),(370,20,'Pasacao'),(371,20,'Pili'),(372,20,'Presentacion'),(373,20,'Ragay'),(374,20,'Sagñay'),(375,20,'San Fernando'),(376,20,'San Jose'),(377,20,'Sipocot'),(378,20,'Siruma'),(379,20,'Tigaon'),(380,20,'Tinambac'),(381,21,'Catarman'),(382,21,'Guinsiliban'),(383,21,'Mahinog'),(384,21,'Mambajao'),(385,21,'Sagay'),(386,22,'Roxas City'),(387,22,'Cuartero'),(388,22,'Dao'),(389,22,'Dumalag'),(390,22,'Dumarao'),(391,22,'Ivisan'),(392,22,'Jamindan'),(393,22,'Ma-ayon'),(394,22,'Mambusao'),(395,22,'Panay'),(396,22,'Panitan'),(397,22,'Pilar'),(398,22,'Pontevedra'),(399,22,'President Roxas'),(400,22,'Sapi-an'),(401,22,'Sigma'),(402,22,'Tapaz'),(403,23,'Bagamanoc'),(404,23,'Baras'),(405,23,'Bato'),(406,23,'Caramoran'),(407,23,'Gigmoto'),(408,23,'Pandan'),(409,23,'Panganiban'),(410,23,'San Andres'),(411,23,'San Miguel'),(412,23,'Viga'),(413,23,'Virac'),(414,24,'Cavite City'),(415,24,'Dasmariñas City'),(416,24,'Tagaytay City'),(417,24,'Trece Martires City'),(418,24,'Alfonso'),(419,24,'Amadeo'),(420,24,'Bacoor'),(421,24,'Carmona'),(422,24,'General Mariano Alvarez'),(423,24,'General Emilio Aguinaldo'),(424,24,'General Trias'),(425,24,'Imus'),(426,24,'Indang'),(427,24,'Kawit'),(428,24,'Magallanes'),(429,24,'Maragondon'),(430,24,'Mendez'),(431,24,'Naic'),(432,24,'Noveleta'),(433,24,'Rosario'),(434,24,'Silang'),(435,24,'Tanza'),(436,24,'Ternate'),(437,25,'Bogo City'),(438,25,'Cebu City'),(439,25,'Carcar City'),(440,25,'Danao City'),(441,25,'Lapu-Lapu City'),(442,25,'Mandaue City'),(443,25,'Naga City'),(444,25,'Talisay City'),(445,25,'Toledo City'),(446,25,'Alcantara'),(447,25,'Alcoy'),(448,25,'Alegria'),(449,25,'Aloguinsan'),(450,25,'Argao'),(451,25,'Asturias'),(452,25,'Badian'),(453,25,'Balamban'),(454,25,'Bantayan'),(455,25,'Barili'),(456,25,'Boljoon'),(457,25,'Borbon'),(458,25,'Carmen'),(459,25,'Catmon'),(460,25,'Compostela'),(461,25,'Consolacion'),(462,25,'Cordoba'),(463,25,'Daanbantayan'),(464,25,'Dalaguete'),(465,25,'Dumanjug'),(466,25,'Ginatilan'),(467,25,'Liloan'),(468,25,'Madridejos'),(469,25,'Malabuyoc'),(470,25,'Medellin'),(471,25,'Minglanilla'),(472,25,'Moalboal'),(473,25,'Oslob'),(474,25,'Pilar'),(475,25,'Pinamungahan'),(476,25,'Poro'),(477,25,'Ronda'),(478,25,'Samboan'),(479,25,'San Fernando'),(480,25,'San Francisco'),(481,25,'San Remigio'),(482,25,'Santa Fe'),(483,25,'Santander'),(484,25,'Sibonga'),(485,25,'Sogod'),(486,25,'Tabogon'),(487,25,'Tabuelan'),(488,25,'Tuburan'),(489,25,'Tudela'),(490,26,'Compostela'),(491,26,'Laak'),(492,26,'Mabini'),(493,26,'Maco'),(494,26,'Maragusan'),(495,26,'Mawab'),(496,26,'Monkayo'),(497,26,'Montevista'),(498,26,'Nabunturan'),(499,26,'New Bataan'),(500,26,'Pantukan'),(501,27,'Kidapawan City'),(502,27,'Alamada'),(503,27,'Aleosan'),(504,27,'Antipas'),(505,27,'Arakan'),(506,27,'Banisilan'),(507,27,'Carmen'),(508,27,'Kabacan'),(509,27,'Libungan'),(510,27,'M\'lang'),(511,27,'Magpet'),(512,27,'Makilala'),(513,27,'Matalam'),(514,27,'Midsayap'),(515,27,'Pigkawayan'),(516,27,'Pikit'),(517,27,'President Roxas'),(518,27,'Tulunan'),(519,28,'Panabo City'),(520,28,'Island Garden City of Samal'),(521,28,'Tagum City'),(522,28,'Asuncion'),(523,28,'Braulio E. Dujali'),(524,28,'Carmen'),(525,28,'Kapalong'),(526,28,'New Corella'),(527,28,'San Isidro'),(528,28,'Santo Tomas'),(529,28,'Talaingod'),(530,29,'Davao City'),(531,29,'Digos City'),(532,29,'Bansalan'),(533,29,'Don Marcelino'),(534,29,'Hagonoy'),(535,29,'Jose Abad Santos'),(536,29,'Kiblawan'),(537,29,'Magsaysay'),(538,29,'Malalag'),(539,29,'Malita'),(540,29,'Matanao'),(541,29,'Padada'),(542,29,'Santa Cruz'),(543,29,'Santa Maria'),(544,29,'Sarangani'),(545,29,'Sulop'),(546,30,'Mati City'),(547,30,'Baganga'),(548,30,'Banaybanay'),(549,30,'Boston'),(550,30,'Caraga'),(551,30,'Cateel'),(552,30,'Governor Generoso'),(553,30,'Lupon'),(554,30,'Manay'),(555,30,'San Isidro'),(556,30,'Tarragona'),(557,31,'Arteche'),(558,31,'Balangiga'),(559,31,'Balangkayan'),(560,31,'Borongan'),(561,31,'Can-avid'),(562,31,'Dolores'),(563,31,'General MacArthur'),(564,31,'Giporlos'),(565,31,'Guiuan'),(566,31,'Hernani'),(567,31,'Jipapad'),(568,31,'Lawaan'),(569,31,'Llorente'),(570,31,'Maslog'),(571,31,'Maydolong'),(572,31,'Mercedes'),(573,31,'Oras'),(574,31,'Quinapondan'),(575,31,'Salcedo'),(576,31,'San Julian'),(577,31,'San Policarpo'),(578,31,'Sulat'),(579,31,'Taft'),(580,32,'Buenavista'),(581,32,'Jordan'),(582,32,'Nueva Valencia'),(583,32,'San Lorenzo'),(584,32,'Sibunag'),(585,33,'Aguinaldo'),(586,33,'Alfonso Lista'),(587,33,'Asipulo'),(588,33,'Banaue'),(589,33,'Hingyon'),(590,33,'Hungduan'),(591,33,'Kiangan'),(592,33,'Lagawe'),(593,33,'Lamut'),(594,33,'Mayoyao'),(595,33,'Tinoc'),(596,34,'Batac City'),(597,34,'Laoag City'),(598,34,'Adams'),(599,34,'Bacarra'),(600,34,'Badoc'),(601,34,'Bangui'),(602,34,'Banna'),(603,34,'Burgos'),(604,34,'Carasi'),(605,34,'Currimao'),(606,34,'Dingras'),(607,34,'Dumalneg'),(608,34,'Marcos'),(609,34,'Nueva Era'),(610,34,'Pagudpud'),(611,34,'Paoay'),(612,34,'Pasuquin'),(613,34,'Piddig'),(614,34,'Pinili'),(615,34,'San Nicolas'),(616,34,'Sarrat'),(617,34,'Solsona'),(618,34,'Vintar'),(619,35,'Candon City'),(620,35,'Vigan City'),(621,35,'Alilem'),(622,35,'Banayoyo'),(623,35,'Bantay'),(624,35,'Burgos'),(625,35,'Cabugao'),(626,35,'Caoayan'),(627,35,'Cervantes'),(628,35,'Galimuyod'),(629,35,'Gregorio Del Pilar'),(630,35,'Lidlidda'),(631,35,'Magsingal'),(632,35,'Nagbukel'),(633,35,'Narvacan'),(634,35,'Quirino'),(635,35,'Salcedo'),(636,35,'San Emilio'),(637,35,'San Esteban'),(638,35,'San Ildefonso'),(639,35,'San Juan'),(640,35,'San Vicente'),(641,35,'Santa'),(642,35,'Santa Catalina'),(643,35,'Santa Cruz'),(644,35,'Santa Lucia'),(645,35,'Santa Maria'),(646,35,'Santiago'),(647,35,'Santo Domingo'),(648,35,'Sigay'),(649,35,'Sinait'),(650,35,'Sugpon'),(651,35,'Suyo'),(652,35,'Tagudin'),(653,36,'Iloilo City'),(654,36,'Passi City'),(655,36,'Ajuy'),(656,36,'Alimodian'),(657,36,'Anilao'),(658,36,'Badiangan'),(659,36,'Balasan'),(660,36,'Banate'),(661,36,'Barotac Nuevo'),(662,36,'Barotac Viejo'),(663,36,'Batad'),(664,36,'Bingawan'),(665,36,'Cabatuan'),(666,36,'Calinog'),(667,36,'Carles'),(668,36,'Concepcion'),(669,36,'Dingle'),(670,36,'Dueñas'),(671,36,'Dumangas'),(672,36,'Estancia'),(673,36,'Guimbal'),(674,36,'Igbaras'),(675,36,'Janiuay'),(676,36,'Lambunao'),(677,36,'Leganes'),(678,36,'Lemery'),(679,36,'Leon'),(680,36,'Maasin'),(681,36,'Miagao'),(682,36,'Mina'),(683,36,'New Lucena'),(684,36,'Oton'),(685,36,'Pavia'),(686,36,'Pototan'),(687,36,'San Dionisio'),(688,36,'San Enrique'),(689,36,'San Joaquin'),(690,36,'San Miguel'),(691,36,'San Rafael'),(692,36,'Santa Barbara'),(693,36,'Sara'),(694,36,'Tigbauan'),(695,36,'Tubungan'),(696,36,'Zarraga'),(697,37,'Cauayan City'),(698,37,'Santiago City'),(699,37,'Alicia'),(700,37,'Angadanan'),(701,37,'Aurora'),(702,37,'Benito Soliven'),(703,37,'Burgos'),(704,37,'Cabagan'),(705,37,'Cabatuan'),(706,37,'Cordon'),(707,37,'Delfin Albano'),(708,37,'Dinapigue'),(709,37,'Divilacan'),(710,37,'Echague'),(711,37,'Gamu'),(712,37,'Ilagan'),(713,37,'Jones'),(714,37,'Luna'),(715,37,'Maconacon'),(716,37,'Mallig'),(717,37,'Naguilian'),(718,37,'Palanan'),(719,37,'Quezon'),(720,37,'Quirino'),(721,37,'Ramon'),(722,37,'Reina Mercedes'),(723,37,'Roxas'),(724,37,'San Agustin'),(725,37,'San Guillermo'),(726,37,'San Isidro'),(727,37,'San Manuel'),(728,37,'San Mariano'),(729,37,'San Mateo'),(730,37,'San Pablo'),(731,37,'Santa Maria'),(732,37,'Santo Tomas'),(733,37,'Tumauini'),(734,38,'Tabuk'),(735,38,'Balbalan'),(736,38,'Lubuagan'),(737,38,'Pasil'),(738,38,'Pinukpuk'),(739,38,'Rizal'),(740,38,'Tanudan'),(741,38,'Tinglayan'),(742,39,'San Fernando City'),(743,39,'Agoo'),(744,39,'Aringay'),(745,39,'Bacnotan'),(746,39,'Bagulin'),(747,39,'Balaoan'),(748,39,'Bangar'),(749,39,'Bauang'),(750,39,'Burgos'),(751,39,'Caba'),(752,39,'Luna'),(753,39,'Naguilian'),(754,39,'Pugo'),(755,39,'Rosario'),(756,39,'San Gabriel'),(757,39,'San Juan'),(758,39,'Santo Tomas'),(759,39,'Santol'),(760,39,'Sudipen'),(761,39,'Tubao'),(762,40,'Biñan City'),(763,40,'Calamba City'),(764,40,'San Pablo City'),(765,40,'Santa Rosa City'),(766,40,'Alaminos'),(767,40,'Bay'),(768,40,'Cabuyao'),(769,40,'Calauan'),(770,40,'Cavinti'),(771,40,'Famy'),(772,40,'Kalayaan'),(773,40,'Liliw'),(774,40,'Los Baños'),(775,40,'Luisiana'),(776,40,'Lumban'),(777,40,'Mabitac'),(778,40,'Magdalena'),(779,40,'Majayjay'),(780,40,'Nagcarlan'),(781,40,'Paete'),(782,40,'Pagsanjan'),(783,40,'Pakil'),(784,40,'Pangil'),(785,40,'Pila'),(786,40,'Rizal'),(787,40,'San Pedro'),(788,40,'Santa Cruz'),(789,40,'Santa Maria'),(790,40,'Siniloan'),(791,40,'Victoria'),(792,41,'Iligan City'),(793,41,'Bacolod'),(794,41,'Baloi'),(795,41,'Baroy'),(796,41,'Kapatagan'),(797,41,'Kauswagan'),(798,41,'Kolambugan'),(799,41,'Lala'),(800,41,'Linamon'),(801,41,'Magsaysay'),(802,41,'Maigo'),(803,41,'Matungao'),(804,41,'Munai'),(805,41,'Nunungan'),(806,41,'Pantao Ragat'),(807,41,'Pantar'),(808,41,'Poona Piagapo'),(809,41,'Salvador'),(810,41,'Sapad'),(811,41,'Sultan Naga Dimaporo'),(812,41,'Tagoloan'),(813,41,'Tangcal'),(814,41,'Tubod'),(815,42,'Marawi City'),(816,42,'Bacolod-Kalawi'),(817,42,'Balabagan'),(818,42,'Balindong'),(819,42,'Bayang'),(820,42,'Binidayan'),(821,42,'Buadiposo-Buntong'),(822,42,'Bubong'),(823,42,'Bumbaran'),(824,42,'Butig'),(825,42,'Calanogas'),(826,42,'Ditsaan-Ramain'),(827,42,'Ganassi'),(828,42,'Kapai'),(829,42,'Kapatagan'),(830,42,'Lumba-Bayabao'),(831,42,'Lumbaca-Unayan'),(832,42,'Lumbatan'),(833,42,'Lumbayanague'),(834,42,'Madalum'),(835,42,'Madamba'),(836,42,'Maguing'),(837,42,'Malabang'),(838,42,'Marantao'),(839,42,'Marogong'),(840,42,'Masiu'),(841,42,'Mulondo'),(842,42,'Pagayawan'),(843,42,'Piagapo'),(844,42,'Poona Bayabao'),(845,42,'Pualas'),(846,42,'Saguiaran'),(847,42,'Sultan Dumalondong'),(848,42,'Picong'),(849,42,'Tagoloan II'),(850,42,'Tamparan'),(851,42,'Taraka'),(852,42,'Tubaran'),(853,42,'Tugaya'),(854,42,'Wao'),(855,43,'Ormoc City'),(856,43,'Tacloban City'),(857,43,'Abuyog'),(858,43,'Alangalang'),(859,43,'Albuera'),(860,43,'Babatngon'),(861,43,'Barugo'),(862,43,'Bato'),(863,43,'Baybay'),(864,43,'Burauen'),(865,43,'Calubian'),(866,43,'Capoocan'),(867,43,'Carigara'),(868,43,'Dagami'),(869,43,'Dulag'),(870,43,'Hilongos'),(871,43,'Hindang'),(872,43,'Inopacan'),(873,43,'Isabel'),(874,43,'Jaro'),(875,43,'Javier'),(876,43,'Julita'),(877,43,'Kananga'),(878,43,'La Paz'),(879,43,'Leyte'),(880,43,'Liloan'),(881,43,'MacArthur'),(882,43,'Mahaplag'),(883,43,'Matag-ob'),(884,43,'Matalom'),(885,43,'Mayorga'),(886,43,'Merida'),(887,43,'Palo'),(888,43,'Palompon'),(889,43,'Pastrana'),(890,43,'San Isidro'),(891,43,'San Miguel'),(892,43,'Santa Fe'),(893,43,'Sogod'),(894,43,'Tabango'),(895,43,'Tabontabon'),(896,43,'Tanauan'),(897,43,'Tolosa'),(898,43,'Tunga'),(899,43,'Villaba'),(900,44,'Cotabato City'),(901,44,'Ampatuan'),(902,44,'Barira'),(903,44,'Buldon'),(904,44,'Buluan'),(905,44,'Datu Abdullah Sangki'),(906,44,'Datu Anggal Midtimbang'),(907,44,'Datu Blah T. Sinsuat'),(908,44,'Datu Hoffer Ampatuan'),(909,44,'Datu Montawal'),(910,44,'Datu Odin Sinsuat'),(911,44,'Datu Paglas'),(912,44,'Datu Piang'),(913,44,'Datu Salibo'),(914,44,'Datu Saudi-Ampatuan'),(915,44,'Datu Unsay'),(916,44,'General Salipada K. Pendatun'),(917,44,'Guindulungan'),(918,44,'Kabuntalan'),(919,44,'Mamasapano'),(920,44,'Mangudadatu'),(921,44,'Matanog'),(922,44,'Northern Kabuntalan'),(923,44,'Pagalungan'),(924,44,'Paglat'),(925,44,'Pandag'),(926,44,'Parang'),(927,44,'Rajah Buayan'),(928,44,'Shariff Aguak'),(929,44,'Shariff Saydona Mustapha'),(930,44,'South Upi'),(931,44,'Sultan Kudarat'),(932,44,'Sultan Mastura'),(933,44,'Sultan sa Barongis'),(934,44,'Talayan'),(935,44,'Talitay'),(936,44,'Upi'),(937,45,'Boac'),(938,45,'Buenavista'),(939,45,'Gasan'),(940,45,'Mogpog'),(941,45,'Santa Cruz'),(942,45,'Torrijos'),(943,46,'Masbate City'),(944,46,'Aroroy'),(945,46,'Baleno'),(946,46,'Balud'),(947,46,'Batuan'),(948,46,'Cataingan'),(949,46,'Cawayan'),(950,46,'Claveria'),(951,46,'Dimasalang'),(952,46,'Esperanza'),(953,46,'Mandaon'),(954,46,'Milagros'),(955,46,'Mobo'),(956,46,'Monreal'),(957,46,'Palanas'),(958,46,'Pio V. Corpuz'),(959,46,'Placer'),(960,46,'San Fernando'),(961,46,'San Jacinto'),(962,46,'San Pascual'),(963,46,'Uson'),(964,47,'Caloocan'),(965,47,'Las Piñas'),(966,47,'Makati'),(967,47,'Malabon'),(968,47,'Mandaluyong'),(969,47,'Manila'),(970,47,'Marikina'),(971,47,'Muntinlupa'),(972,47,'Navotas'),(973,47,'Parañaque'),(974,47,'Pasay'),(975,47,'Pasig'),(976,47,'Quezon City'),(977,47,'San Juan City'),(978,47,'Taguig'),(979,47,'Valenzuela City'),(980,47,'Pateros'),(981,48,'Oroquieta City'),(982,48,'Ozamiz City'),(983,48,'Tangub City'),(984,48,'Aloran'),(985,48,'Baliangao'),(986,48,'Bonifacio'),(987,48,'Calamba'),(988,48,'Clarin'),(989,48,'Concepcion'),(990,48,'Don Victoriano Chiongbian'),(991,48,'Jimenez'),(992,48,'Lopez Jaena'),(993,48,'Panaon'),(994,48,'Plaridel'),(995,48,'Sapang Dalaga'),(996,48,'Sinacaban'),(997,48,'Tudela'),(998,49,'Cagayan de Oro'),(999,49,'Gingoog City'),(1000,49,'Alubijid'),(1001,49,'Balingasag'),(1002,49,'Balingoan'),(1003,49,'Binuangan'),(1004,49,'Claveria'),(1005,49,'El Salvador'),(1006,49,'Gitagum'),(1007,49,'Initao'),(1008,49,'Jasaan'),(1009,49,'Kinoguitan'),(1010,49,'Lagonglong'),(1011,49,'Laguindingan'),(1012,49,'Libertad'),(1013,49,'Lugait'),(1014,49,'Magsaysay'),(1015,49,'Manticao'),(1016,49,'Medina'),(1017,49,'Naawan'),(1018,49,'Opol'),(1019,49,'Salay'),(1020,49,'Sugbongcogon'),(1021,49,'Tagoloan'),(1022,49,'Talisayan'),(1023,49,'Villanueva'),(1024,50,'Barlig'),(1025,50,'Bauko'),(1026,50,'Besao'),(1027,50,'Bontoc'),(1028,50,'Natonin'),(1029,50,'Paracelis'),(1030,50,'Sabangan'),(1031,50,'Sadanga'),(1032,50,'Sagada'),(1033,50,'Tadian'),(1034,51,'Bacolod City'),(1035,51,'Bago City'),(1036,51,'Cadiz City'),(1037,51,'Escalante City'),(1038,51,'Himamaylan City'),(1039,51,'Kabankalan City'),(1040,51,'La Carlota City'),(1041,51,'Sagay City'),(1042,51,'San Carlos City'),(1043,51,'Silay City'),(1044,51,'Sipalay City'),(1045,51,'Talisay City'),(1046,51,'Victorias City'),(1047,51,'Binalbagan'),(1048,51,'Calatrava'),(1049,51,'Candoni'),(1050,51,'Cauayan'),(1051,51,'Enrique B. Magalona'),(1052,51,'Hinigaran'),(1053,51,'Hinoba-an'),(1054,51,'Ilog'),(1055,51,'Isabela'),(1056,51,'La Castellana'),(1057,51,'Manapla'),(1058,51,'Moises Padilla'),(1059,51,'Murcia'),(1060,51,'Pontevedra'),(1061,51,'Pulupandan'),(1062,51,'Salvador Benedicto'),(1063,51,'San Enrique'),(1064,51,'Toboso'),(1065,51,'Valladolid'),(1066,52,'Bais City'),(1067,52,'Bayawan City'),(1068,52,'Canlaon City'),(1069,52,'Guihulngan City'),(1070,52,'Dumaguete City'),(1071,52,'Tanjay City'),(1072,52,'Amlan'),(1073,52,'Ayungon'),(1074,52,'Bacong'),(1075,52,'Basay'),(1076,52,'Bindoy'),(1077,52,'Dauin'),(1078,52,'Jimalalud'),(1079,52,'La Libertad'),(1080,52,'Mabinay'),(1081,52,'Manjuyod'),(1082,52,'Pamplona'),(1083,52,'San Jose'),(1084,52,'Santa Catalina'),(1085,52,'Siaton'),(1086,52,'Sibulan'),(1087,52,'Tayasan'),(1088,52,'Valencia'),(1089,52,'Vallehermoso'),(1090,52,'Zamboanguita'),(1091,53,'Allen'),(1092,53,'Biri'),(1093,53,'Bobon'),(1094,53,'Capul'),(1095,53,'Catarman'),(1096,53,'Catubig'),(1097,53,'Gamay'),(1098,53,'Laoang'),(1099,53,'Lapinig'),(1100,53,'Las Navas'),(1101,53,'Lavezares'),(1102,53,'Lope de Vega'),(1103,53,'Mapanas'),(1104,53,'Mondragon'),(1105,53,'Palapag'),(1106,53,'Pambujan'),(1107,53,'Rosario'),(1108,53,'San Antonio'),(1109,53,'San Isidro'),(1110,53,'San Jose'),(1111,53,'San Roque'),(1112,53,'San Vicente'),(1113,53,'Silvino Lobos'),(1114,53,'Victoria'),(1115,54,'Cabanatuan City'),(1116,54,'Gapan City'),(1117,54,'Science City of Muñoz'),(1118,54,'Palayan City'),(1119,54,'San Jose City'),(1120,54,'Aliaga'),(1121,54,'Bongabon'),(1122,54,'Cabiao'),(1123,54,'Carranglan'),(1124,54,'Cuyapo'),(1125,54,'Gabaldon'),(1126,54,'General Mamerto Natividad'),(1127,54,'General Tinio'),(1128,54,'Guimba'),(1129,54,'Jaen'),(1130,54,'Laur'),(1131,54,'Licab'),(1132,54,'Llanera'),(1133,54,'Lupao'),(1134,54,'Nampicuan'),(1135,54,'Pantabangan'),(1136,54,'Peñaranda'),(1137,54,'Quezon'),(1138,54,'Rizal'),(1139,54,'San Antonio'),(1140,54,'San Isidro'),(1141,54,'San Leonardo'),(1142,54,'Santa Rosa'),(1143,54,'Santo Domingo'),(1144,54,'Talavera'),(1145,54,'Talugtug'),(1146,54,'Zaragoza'),(1147,55,'Alfonso Castaneda'),(1148,55,'Ambaguio'),(1149,55,'Aritao'),(1150,55,'Bagabag'),(1151,55,'Bambang'),(1152,55,'Bayombong'),(1153,55,'Diadi'),(1154,55,'Dupax del Norte'),(1155,55,'Dupax del Sur'),(1156,55,'Kasibu'),(1157,55,'Kayapa'),(1158,55,'Quezon'),(1159,55,'Santa Fe'),(1160,55,'Solano'),(1161,55,'Villaverde'),(1162,56,'Abra de Ilog'),(1163,56,'Calintaan'),(1164,56,'Looc'),(1165,56,'Lubang'),(1166,56,'Magsaysay'),(1167,56,'Mamburao'),(1168,56,'Paluan'),(1169,56,'Rizal'),(1170,56,'Sablayan'),(1171,56,'San Jose'),(1172,56,'Santa Cruz'),(1173,57,'Calapan City'),(1174,57,'Baco'),(1175,57,'Bansud'),(1176,57,'Bongabong'),(1177,57,'Bulalacao'),(1178,57,'Gloria'),(1179,57,'Mansalay'),(1180,57,'Naujan'),(1181,57,'Pinamalayan'),(1182,57,'Pola'),(1183,57,'Puerto Galera'),(1184,57,'Roxas'),(1185,57,'San Teodoro'),(1186,57,'Socorro'),(1187,57,'Victoria'),(1188,58,'Puerto Princesa City'),(1189,58,'Aborlan'),(1190,58,'Agutaya'),(1191,58,'Araceli'),(1192,58,'Balabac'),(1193,58,'Bataraza'),(1194,58,'Brooke\'s Point'),(1195,58,'Busuanga'),(1196,58,'Cagayancillo'),(1197,58,'Coron'),(1198,58,'Culion'),(1199,58,'Cuyo'),(1200,58,'Dumaran'),(1201,58,'El Nido'),(1202,58,'Kalayaan'),(1203,58,'Linapacan'),(1204,58,'Magsaysay'),(1205,58,'Narra'),(1206,58,'Quezon'),(1207,58,'Rizal'),(1208,58,'Roxas'),(1209,58,'San Vicente'),(1210,58,'Sofronio Española'),(1211,58,'Taytay'),(1212,59,'Angeles City'),(1213,59,'City of San Fernando'),(1214,59,'Apalit'),(1215,59,'Arayat'),(1216,59,'Bacolor'),(1217,59,'Candaba'),(1218,59,'Floridablanca'),(1219,59,'Guagua'),(1220,59,'Lubao'),(1221,59,'Mabalacat'),(1222,59,'Macabebe'),(1223,59,'Magalang'),(1224,59,'Masantol'),(1225,59,'Mexico'),(1226,59,'Minalin'),(1227,59,'Porac'),(1228,59,'San Luis'),(1229,59,'San Simon'),(1230,59,'Santa Ana'),(1231,59,'Santa Rita'),(1232,59,'Santo Tomas'),(1233,59,'Sasmuan'),(1234,60,'Alaminos City'),(1235,60,'Dagupan City'),(1236,60,'San Carlos City'),(1237,60,'Urdaneta City'),(1238,60,'Agno'),(1239,60,'Aguilar'),(1240,60,'Alcala'),(1241,60,'Anda'),(1242,60,'Asingan'),(1243,60,'Balungao'),(1244,60,'Bani'),(1245,60,'Basista'),(1246,60,'Bautista'),(1247,60,'Bayambang'),(1248,60,'Binalonan'),(1249,60,'Binmaley'),(1250,60,'Bolinao'),(1251,60,'Bugallon'),(1252,60,'Burgos'),(1253,60,'Calasiao'),(1254,60,'Dasol'),(1255,60,'Infanta'),(1256,60,'Labrador'),(1257,60,'Laoac'),(1258,60,'Lingayen'),(1259,60,'Mabini'),(1260,60,'Malasiqui'),(1261,60,'Manaoag'),(1262,60,'Mangaldan'),(1263,60,'Mangatarem'),(1264,60,'Mapandan'),(1265,60,'Natividad'),(1266,60,'Pozzorubio'),(1267,60,'Rosales'),(1268,60,'San Fabian'),(1269,60,'San Jacinto'),(1270,60,'San Manuel'),(1271,60,'San Nicolas'),(1272,60,'San Quintin'),(1273,60,'Santa Barbara'),(1274,60,'Santa Maria'),(1275,60,'Santo Tomas'),(1276,60,'Sison'),(1277,60,'Sual'),(1278,60,'Tayug'),(1279,60,'Umingan'),(1280,60,'Urbiztondo'),(1281,60,'Villasis'),(1282,61,'Lucena City'),(1283,61,'Tayabas City'),(1284,61,'Agdangan'),(1285,61,'Alabat'),(1286,61,'Atimonan'),(1287,61,'Buenavista'),(1288,61,'Burdeos'),(1289,61,'Calauag'),(1290,61,'Candelaria'),(1291,61,'Catanauan'),(1292,61,'Dolores'),(1293,61,'General Luna'),(1294,61,'General Nakar'),(1295,61,'Guinayangan'),(1296,61,'Gumaca'),(1297,61,'Infanta'),(1298,61,'Jomalig'),(1299,61,'Lopez'),(1300,61,'Lucban'),(1301,61,'Macalelon'),(1302,61,'Mauban'),(1303,61,'Mulanay'),(1304,61,'Padre Burgos'),(1305,61,'Pagbilao'),(1306,61,'Panukulan'),(1307,61,'Patnanungan'),(1308,61,'Perez'),(1309,61,'Pitogo'),(1310,61,'Plaridel'),(1311,61,'Polillo'),(1312,61,'Quezon'),(1313,61,'Real'),(1314,61,'Sampaloc'),(1315,61,'San Andres'),(1316,61,'San Antonio'),(1317,61,'San Francisco'),(1318,61,'San Narciso'),(1319,61,'Sariaya'),(1320,61,'Tagkawayan'),(1321,61,'Tiaong'),(1322,61,'Unisan'),(1323,62,'Aglipay'),(1324,62,'Cabarroguis'),(1325,62,'Diffun'),(1326,62,'Maddela'),(1327,62,'Nagtipunan'),(1328,62,'Saguday'),(1329,63,'Antipolo City'),(1330,63,'Angono'),(1331,63,'Baras'),(1332,63,'Binangonan'),(1333,63,'Cainta'),(1334,63,'Cardona'),(1335,63,'Jalajala'),(1336,63,'Morong'),(1337,63,'Pililla'),(1338,63,'Rodriguez'),(1339,63,'San Mateo'),(1340,63,'Tanay'),(1341,63,'Taytay'),(1342,63,'Teresa'),(1343,64,'Alcantara'),(1344,64,'Banton'),(1345,64,'Cajidiocan'),(1346,64,'Calatrava'),(1347,64,'Concepcion'),(1348,64,'Corcuera'),(1349,64,'Ferrol'),(1350,64,'Looc'),(1351,64,'Magdiwang'),(1352,64,'Odiongan'),(1353,64,'Romblon'),(1354,64,'San Agustin'),(1355,64,'San Andres'),(1356,64,'San Fernando'),(1357,64,'San Jose'),(1358,64,'Santa Fe'),(1359,64,'Santa Maria'),(1360,65,'Calbayog City'),(1361,65,'Catbalogan City'),(1362,65,'Almagro'),(1363,65,'Basey'),(1364,65,'Calbiga'),(1365,65,'Daram'),(1366,65,'Gandara'),(1367,65,'Hinabangan'),(1368,65,'Jiabong'),(1369,65,'Marabut'),(1370,65,'Matuguinao'),(1371,65,'Motiong'),(1372,65,'Pagsanghan'),(1373,65,'Paranas'),(1374,65,'Pinabacdao'),(1375,65,'San Jorge'),(1376,65,'San Jose De Buan'),(1377,65,'San Sebastian'),(1378,65,'Santa Margarita'),(1379,65,'Santa Rita'),(1380,65,'Santo Niño'),(1381,65,'Tagapul-an'),(1382,65,'Talalora'),(1383,65,'Tarangnan'),(1384,65,'Villareal'),(1385,65,'Zumarraga'),(1386,66,'Alabel'),(1387,66,'Glan'),(1388,66,'Kiamba'),(1389,66,'Maasim'),(1390,66,'Maitum'),(1391,66,'Malapatan'),(1392,66,'Malungon'),(1393,67,'Enrique Villanueva'),(1394,67,'Larena'),(1395,67,'Lazi'),(1396,67,'Maria'),(1397,67,'San Juan'),(1398,67,'Siquijor'),(1399,68,'Sorsogon City'),(1400,68,'Barcelona'),(1401,68,'Bulan'),(1402,68,'Bulusan'),(1403,68,'Casiguran'),(1404,68,'Castilla'),(1405,68,'Donsol'),(1406,68,'Gubat'),(1407,68,'Irosin'),(1408,68,'Juban'),(1409,68,'Magallanes'),(1410,68,'Matnog'),(1411,68,'Pilar'),(1412,68,'Prieto Diaz'),(1413,68,'Santa Magdalena'),(1414,69,'General Santos City'),(1415,69,'Koronadal City'),(1416,69,'Banga'),(1417,69,'Lake Sebu'),(1418,69,'Norala'),(1419,69,'Polomolok'),(1420,69,'Santo Niño'),(1421,69,'Surallah'),(1422,69,'T\'boli'),(1423,69,'Tampakan'),(1424,69,'Tantangan'),(1425,69,'Tupi'),(1426,70,'Maasin City'),(1427,70,'Anahawan'),(1428,70,'Bontoc'),(1429,70,'Hinunangan'),(1430,70,'Hinundayan'),(1431,70,'Libagon'),(1432,70,'Liloan'),(1433,70,'Limasawa'),(1434,70,'Macrohon'),(1435,70,'Malitbog'),(1436,70,'Padre Burgos'),(1437,70,'Pintuyan'),(1438,70,'Saint Bernard'),(1439,70,'San Francisco'),(1440,70,'San Juan'),(1441,70,'San Ricardo'),(1442,70,'Silago'),(1443,70,'Sogod'),(1444,70,'Tomas Oppus'),(1445,71,'Tacurong City'),(1446,71,'Bagumbayan'),(1447,71,'Columbio'),(1448,71,'Esperanza'),(1449,71,'Isulan'),(1450,71,'Kalamansig'),(1451,71,'Lambayong'),(1452,71,'Lebak'),(1453,71,'Lutayan'),(1454,71,'Palimbang'),(1455,71,'President Quirino'),(1456,71,'Senator Ninoy Aquino'),(1457,72,'Banguingui'),(1458,72,'Hadji Panglima Tahil'),(1459,72,'Indanan'),(1460,72,'Jolo'),(1461,72,'Kalingalan Caluang'),(1462,72,'Lugus'),(1463,72,'Luuk'),(1464,72,'Maimbung'),(1465,72,'Old Panamao'),(1466,72,'Omar'),(1467,72,'Pandami'),(1468,72,'Panglima Estino'),(1469,72,'Pangutaran'),(1470,72,'Parang'),(1471,72,'Pata'),(1472,72,'Patikul'),(1473,72,'Siasi'),(1474,72,'Talipao'),(1475,72,'Tapul'),(1476,73,'Surigao City'),(1477,73,'Alegria'),(1478,73,'Bacuag'),(1479,73,'Basilisa'),(1480,73,'Burgos'),(1481,73,'Cagdianao'),(1482,73,'Claver'),(1483,73,'Dapa'),(1484,73,'Del Carmen'),(1485,73,'Dinagat'),(1486,73,'General Luna'),(1487,73,'Gigaquit'),(1488,73,'Libjo'),(1489,73,'Loreto'),(1490,73,'Mainit'),(1491,73,'Malimono'),(1492,73,'Pilar'),(1493,73,'Placer'),(1494,73,'San Benito'),(1495,73,'San Francisco'),(1496,73,'San Isidro'),(1497,73,'San Jose'),(1498,73,'Santa Monica'),(1499,73,'Sison'),(1500,73,'Socorro'),(1501,73,'Tagana-an'),(1502,73,'Tubajon'),(1503,73,'Tubod'),(1504,74,'Bislig City'),(1505,74,'Tandag City'),(1506,74,'Barobo'),(1507,74,'Bayabas'),(1508,74,'Cagwait'),(1509,74,'Cantilan'),(1510,74,'Carmen'),(1511,74,'Carrascal'),(1512,74,'Cortes'),(1513,74,'Hinatuan'),(1514,74,'Lanuza'),(1515,74,'Lianga'),(1516,74,'Lingig'),(1517,74,'Madrid'),(1518,74,'Marihatag'),(1519,74,'San Agustin'),(1520,74,'San Miguel'),(1521,74,'Tagbina'),(1522,74,'Tago'),(1523,75,'Tarlac City'),(1524,75,'Anao'),(1525,75,'Bamban'),(1526,75,'Camiling'),(1527,75,'Capas'),(1528,75,'Concepcion'),(1529,75,'Gerona'),(1530,75,'La Paz'),(1531,75,'Mayantoc'),(1532,75,'Moncada'),(1533,75,'Paniqui'),(1534,75,'Pura'),(1535,75,'Ramos'),(1536,75,'San Clemente'),(1537,75,'San Jose'),(1538,75,'San Manuel'),(1539,75,'Santa Ignacia'),(1540,75,'Victoria'),(1541,76,'Bongao'),(1542,76,'Languyan'),(1543,76,'Mapun'),(1544,76,'Panglima Sugala'),(1545,76,'Sapa-Sapa'),(1546,76,'Sibutu'),(1547,76,'Simunul'),(1548,76,'Sitangkai'),(1549,76,'South Ubian'),(1550,76,'Tandubas'),(1551,76,'Turtle Islands'),(1552,77,'Olongapo City'),(1553,77,'Botolan'),(1554,77,'Cabangan'),(1555,77,'Candelaria'),(1556,77,'Castillejos'),(1557,77,'Iba'),(1558,77,'Masinloc'),(1559,77,'Palauig'),(1560,77,'San Antonio'),(1561,77,'San Felipe'),(1562,77,'San Marcelino'),(1563,77,'San Narciso'),(1564,77,'Santa Cruz'),(1565,77,'Subic'),(1566,78,'Dapitan City'),(1567,78,'Dipolog City'),(1568,78,'Bacungan'),(1569,78,'Baliguian'),(1570,78,'Godod'),(1571,78,'Gutalac'),(1572,78,'Jose Dalman'),(1573,78,'Kalawit'),(1574,78,'Katipunan'),(1575,78,'La Libertad'),(1576,78,'Labason'),(1577,78,'Liloy'),(1578,78,'Manukan'),(1579,78,'Mutia'),(1580,78,'Piñan'),(1581,78,'Polanco'),(1582,78,'President Manuel A. Roxas'),(1583,78,'Rizal'),(1584,78,'Salug'),(1585,78,'Sergio Osmeña Sr.'),(1586,78,'Siayan'),(1587,78,'Sibuco'),(1588,78,'Sibutad'),(1589,78,'Sindangan'),(1590,78,'Siocon'),(1591,78,'Sirawai'),(1592,78,'Tampilisan'),(1593,79,'Pagadian City'),(1594,79,'Zamboanga City'),(1595,79,'Aurora'),(1596,79,'Bayog'),(1597,79,'Dimataling'),(1598,79,'Dinas'),(1599,79,'Dumalinao'),(1600,79,'Dumingag'),(1601,79,'Guipos'),(1602,79,'Josefina'),(1603,79,'Kumalarang'),(1604,79,'Labangan'),(1605,79,'Lakewood'),(1606,79,'Lapuyan'),(1607,79,'Mahayag'),(1608,79,'Margosatubig'),(1609,79,'Midsalip'),(1610,79,'Molave'),(1611,79,'Pitogo'),(1612,79,'Ramon Magsaysay'),(1613,79,'San Miguel'),(1614,79,'San Pablo'),(1615,79,'Sominot'),(1616,79,'Tabina'),(1617,79,'Tambulig'),(1618,79,'Tigbao'),(1619,79,'Tukuran'),(1620,79,'Vincenzo A. Sagun'),(1621,80,'Alicia'),(1622,80,'Buug'),(1623,80,'Diplahan'),(1624,80,'Imelda'),(1625,80,'Ipil'),(1626,80,'Kabasalan'),(1627,80,'Mabuhay'),(1628,80,'Malangas'),(1629,80,'Naga'),(1630,80,'Olutanga'),(1631,80,'Payao'),(1632,80,'Roseller Lim'),(1633,80,'Siay'),(1634,80,'Talusan'),(1635,80,'Titay'),(1636,80,'Tungawan');
/*!40000 ALTER TABLE `tbl_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country` (
  `code` varchar(10) NOT NULL,
  `country_name` varchar(60) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country`
--

LOCK TABLES `tbl_country` WRITE;
/*!40000 ALTER TABLE `tbl_country` DISABLE KEYS */;
INSERT INTO `tbl_country` VALUES ('ph','Philippines');
/*!40000 ALTER TABLE `tbl_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_options`
--

DROP TABLE IF EXISTS `tbl_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(10) unsigned DEFAULT NULL,
  `name_unique` varchar(45) DEFAULT NULL,
  `name_alt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_options_tbl_stores1_idx` (`store_id`),
  CONSTRAINT `fk_tbl_options_tbl_stores1` FOREIGN KEY (`store_id`) REFERENCES `tbl_stores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_options`
--

LOCK TABLES `tbl_options` WRITE;
/*!40000 ALTER TABLE `tbl_options` DISABLE KEYS */;
INSERT INTO `tbl_options` VALUES (2,4,'Acer Laptop Color (Model C-2341)','Color'),(4,1,'Casual Shirt Sizes','Size'),(5,1,'Buytra Fashion Slim','Color'),(6,1,'Mint Shirts Color','Color');
/*!40000 ALTER TABLE `tbl_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_password_resets`
--

DROP TABLE IF EXISTS `tbl_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_password_resets` (
  `email` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_password_resets`
--

LOCK TABLES `tbl_password_resets` WRITE;
/*!40000 ALTER TABLE `tbl_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_category`
--

DROP TABLE IF EXISTS `tbl_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_category` (
  `cat_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_tbl_product_category_tbl_products1_idx` (`product_id`),
  KEY `fk_tbl_product_category_tbl_category1_idx` (`cat_id`),
  CONSTRAINT `fk_tbl_product_category_tbl_category1` FOREIGN KEY (`cat_id`) REFERENCES `tbl_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_product_category_tbl_products1` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_category`
--

LOCK TABLES `tbl_product_category` WRITE;
/*!40000 ALTER TABLE `tbl_product_category` DISABLE KEYS */;
INSERT INTO `tbl_product_category` VALUES (8,2),(8,1),(2,3),(2,4),(2,5),(2,6),(2,7);
/*!40000 ALTER TABLE `tbl_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_images`
--

DROP TABLE IF EXISTS `tbl_product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_images` (
  `product_id` int(10) unsigned DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  KEY `fk_tbl_product_images_tbl_products1_idx` (`product_id`),
  CONSTRAINT `fk_tbl_product_images_tbl_products1` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_images`
--

LOCK TABLES `tbl_product_images` WRITE;
/*!40000 ALTER TABLE `tbl_product_images` DISABLE KEYS */;
INSERT INTO `tbl_product_images` VALUES (1,'nikon-af-50mm-f18-lens-black.jpg'),(2,'phottix-varos-ii-bg-multi-function-flash-shoe-umbrella-holder.jpg'),(3,'buytra-fashion-slim-men-short-sleeve-casual-shirt.jpg'),(4,'mint-washed-chambray-short-sleeves-shirt-1.jpg'),(4,'mint-washed-chambray-short-sleeves-shirt-2.jpg'),(4,'mint-washed-chambray-short-sleeves-shirt-3.jpg'),(5,'mint-aztec-print-short-sleeves-with-side-pocket-1.jpg'),(5,'mint-aztec-print-short-sleeves-with-side-pocket-2.jpg'),(5,'mint-aztec-print-short-sleeves-with-side-pocket-3.jpg'),(6,'mint-fatigue-short-sleeves-fatigue-3.jpg'),(6,'mint-fatigue-short-sleeves-fatigue-2.jpg'),(6,'mint-fatigue-short-sleeves-fatigue-1.jpg'),(7,'mint-plain-short-sleeves-with-left-pocket-light-blue-1.jpg'),(7,'mint-plain-short-sleeves-with-left-pocket-light-blue-2.jpg'),(7,'mint-plain-short-sleeves-with-left-pocket-light-blue-3.jpg');
/*!40000 ALTER TABLE `tbl_product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `sku` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `desc_short` varchar(255) DEFAULT NULL,
  `desc_long` text,
  `stock` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_sale` double DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `min_order` int(11) DEFAULT NULL,
  `max_order` int(11) DEFAULT NULL,
  `sold_count` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `featured` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `image` varchar(100) DEFAULT NULL,
  `tax_class` varchar(45) DEFAULT NULL,
  `availability` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_products_tbl_stores1_idx` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_products`
--

LOCK TABLES `tbl_products` WRITE;
/*!40000 ALTER TABLE `tbl_products` DISABLE KEYS */;
INSERT INTO `tbl_products` VALUES (1,4,NULL,'NI643ELAJY9JANPH-605703','Nikon AF 50mm F1.8 Lens (Black) ','nikon-af-50mm-f18-lens-black','Nikon AF 50mm F1.8 Lens (Black) ','<ul><li>Lens type: F Mount Lens</li><li>Compatibility: Nikon</li><li>Filter size: 52mm</li><li>Lens construction: 6 elements, 5 groups</li><li>Number of diaphragm blades: 7</li><li>Minimum aperture: f/22</li><li>Minimum focusing distance: 45.72 cm</li><li>Maximum magnifications: 0.15x</li></ul>',20,6999,5588,20,'php',0.5,1,10,NULL,NULL,1,1,'nikon-af-50mm-f18-lens-black.png',NULL,NULL,NULL,NULL),(2,4,NULL,'PH559ELALD96ANPH-684147','Phottix Varos II BG Multi-Function Flash Shoe Umbrella Holder','phottix-varos-ii-bg-multi-function-flash-shoe-umbrella-holder','Phottix Varos II BG Multi-Function Flash Shoe Umbrella Holder','<ul><li>Great choice for the larger and heavier Phottix Para-Pro Umbrellas.</li><li>Adjustable cold shoe to mount flashes or flash triggers. </li><li>The cold shoe is removable. </li><li>Allowing accesses to a reversible 3/8\" / 1/4\" threaded mount. </li><li>Mounts to a light stand or tripod with a 3/4\" female lug or a reversible 3/8\" / 1/4\" threaded insert</li></ul>',10,1499,NULL,0,'php',0.2,1,10,NULL,NULL,1,1,'phottix-varos-ii-bg-multi-function-flash-shoe-umbrella-holder.png',NULL,NULL,NULL,NULL),(3,1,NULL,'BU663FAAPQJ4ANPH-920674','Buytra Fashion Slim Men Short Sleeve Casual Shirt','buytra-fashion-slim-men-short-sleeve-casual-shirt','Condition:100% brand new ','<ul><li>Package included: One Shirt Only</li><li>size:Asia size M,L,XL,XXL,XXXL,XXXXL</li><li>Color:Blue,Grey,DarkBlue</li></ul>',50,2848,1566,45,NULL,0.1,NULL,NULL,NULL,NULL,1,1,'buytra-fashion-slim-men-short-sleeve-casual-shirt.png',NULL,NULL,NULL,NULL),(4,1,NULL,'MI595FAAJODXANPH-577056','Mint Washed Chambray Short Sleeves Shirt','mint-washed-chambray-short-sleeves-shirt','This acid wash chambray with details like basic collar, chest patch pocket, and a buttoned front is great worn open with a tee and shorts ','<ul><li>Woven fabric</li><li>Short sleeves</li><li>Machine wash cold</li><li>Single pocket</li><li>Measured from extra small</li></ul>',50,1050,NULL,0,NULL,0.1,NULL,NULL,NULL,NULL,1,1,'mint-washed-chambray-short-sleeves-shirt.png',NULL,NULL,NULL,NULL),(5,1,NULL,'MI595FAALLTGANPH-697682','Mint Aztec Print Short Sleeves with Side Pocket','mint-aztec-print-short-sleeves-with-side-pocket','This shirt will keep you cool and comfortable on a sunny days!','<ul><li>Woven fabric</li><li>Short sleeves</li><li>Machine wash cold</li><li>Single pocket</li><li>Measured from extra small</li></ul>',50,1050,650,38,NULL,0.1,NULL,NULL,NULL,NULL,1,1,'mint-aztec-print-short-sleeves-with-side-pocket.png',NULL,NULL,NULL,NULL),(6,1,NULL,'MI595FAALJ67ANPH-693051','Mint Fatigue Short Sleeves','mint-fatigue-short-sleeves-fatigue','Easy approach with jeans!','<ul><li>Woven fabric</li><li>Short sleeves</li><li>Machine wash cold</li><li>Single pocket</li><li>Measured from extra small</li></ul>',50,950,NULL,0,NULL,0.1,NULL,NULL,NULL,NULL,1,1,'mint-fatigue-short-sleeves-fatigue.png',NULL,NULL,NULL,NULL),(7,1,NULL,'MI595FAALLTLANPH-697695','Mint Plain Short Sleeves with Left Pocket','mint-plain-short-sleeves-with-left-pocket-light-blue','This shirt is a quick choice for casual fridays!','<ul><li>Woven fabric</li><li>Short sleeves</li><li>Machine wash cold</li><li>Single pocket</li><li>Measured from extra small</li></ul>',50,950,NULL,0,NULL,0.1,NULL,NULL,NULL,NULL,1,1,'mint-plain-short-sleeves-with-left-pocket-light-blue.png',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `wyc2`.`tbl_products_BEFORE_INSERT`
BEFORE INSERT ON `wyc2`.`tbl_products`
FOR EACH ROW
IF NEW.price_sale > 0 THEN
		SET NEW.discount = ROUND(((1 - (NEW.price_sale / NEW.price)) * 100), 0);
	ELSE 
		SET NEW.discount = 0;
    END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `wyc2`.`tbl_products_BEFORE_UPDATE`
BEFORE UPDATE ON `wyc2`.`tbl_products`
FOR EACH ROW
IF NEW.price_sale > 0 THEN
		SET NEW.discount = ROUND(((1 - (NEW.price_sale / NEW.price)) * 100), 0);
	ELSE 
		SET NEW.discount = 0;
    END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_region`
--

DROP TABLE IF EXISTS `tbl_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_region` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` varchar(10) NOT NULL,
  `region_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_region_tbl_country1_idx` (`country_code`),
  CONSTRAINT `fk_tbl_region_tbl_country1` FOREIGN KEY (`country_code`) REFERENCES `tbl_country` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_region`
--

LOCK TABLES `tbl_region` WRITE;
/*!40000 ALTER TABLE `tbl_region` DISABLE KEYS */;
INSERT INTO `tbl_region` VALUES (1,'ph','Abra'),(2,'ph','Agusan del Norte'),(3,'ph','Agusan del Sur'),(4,'ph','Aklan'),(5,'ph','Albay'),(6,'ph','Antique'),(7,'ph','Apayao'),(8,'ph','Aurora'),(9,'ph','Basilan'),(10,'ph','Bataan'),(11,'ph','Batanes'),(12,'ph','Batangas'),(13,'ph','Benguet'),(14,'ph','Biliran'),(15,'ph','Bohol'),(16,'ph','Bukidnon'),(17,'ph','Bulacan'),(18,'ph','Cagayan'),(19,'ph','Camarines Norte'),(20,'ph','Camarines Sur'),(21,'ph','Camiguin'),(22,'ph','Capiz'),(23,'ph','Catanduanes'),(24,'ph','Cavite'),(25,'ph','Cebu'),(26,'ph','Compostela Valley'),(27,'ph','Cotabato'),(28,'ph','Davao del Norte'),(29,'ph','Davao del Sur'),(30,'ph','Davao Oriental'),(31,'ph','Eastern Samar'),(32,'ph','Guimaras'),(33,'ph','Ifugao'),(34,'ph','Ilocos Norte'),(35,'ph','Ilocos Sur'),(36,'ph','Iloilo'),(37,'ph','Isabela'),(38,'ph','Kalinga'),(39,'ph','La Union'),(40,'ph','Laguna'),(41,'ph','Lanao del Norte'),(42,'ph','Lanao del Sur'),(43,'ph','Leyte'),(44,'ph','Maguindanao'),(45,'ph','Marinduque'),(46,'ph','Masbate'),(47,'ph','Metro Manila'),(48,'ph','Misamis Occidental'),(49,'ph','Misamis Oriental'),(50,'ph','Mountain Province'),(51,'ph','Negros Occidental'),(52,'ph','Negros Oriental'),(53,'ph','Northern Samar'),(54,'ph','Nueva Ecija'),(55,'ph','Nueva Vizcaya'),(56,'ph','Occidental Mindoro'),(57,'ph','Oriental Mindoro'),(58,'ph','Palawan'),(59,'ph','Pampanga'),(60,'ph','Pangasinan'),(61,'ph','Quezon'),(62,'ph','Quirino'),(63,'ph','Rizal'),(64,'ph','Romblon'),(65,'ph','Samar'),(66,'ph','Sarangani'),(67,'ph','Siquijor'),(68,'ph','Sorsogon'),(69,'ph','South Cotabato'),(70,'ph','Southern Leyte'),(71,'ph','Sultan Kudarat'),(72,'ph','Sulu'),(73,'ph','Surigao del Norte'),(74,'ph','Surigao del Sur'),(75,'ph','Tarlac'),(76,'ph','Tawi-Tawi'),(77,'ph','Zambales'),(78,'ph','Zamboanga del Norte'),(79,'ph','Zamboanga del Sur'),(80,'ph','Zamboanga Sibugay');
/*!40000 ALTER TABLE `tbl_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_store_category`
--

DROP TABLE IF EXISTS `tbl_store_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_category` (
  `cat_id` int(10) unsigned DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_tbl_store_category_tbl_stores1_idx` (`store_id`),
  KEY `fk_tbl_store_category_tbl_category1_idx` (`cat_id`),
  CONSTRAINT `fk_tbl_store_category_tbl_category1` FOREIGN KEY (`cat_id`) REFERENCES `tbl_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_store_category_tbl_stores1` FOREIGN KEY (`store_id`) REFERENCES `tbl_stores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_store_category`
--

LOCK TABLES `tbl_store_category` WRITE;
/*!40000 ALTER TABLE `tbl_store_category` DISABLE KEYS */;
INSERT INTO `tbl_store_category` VALUES (4,1),(5,1),(3,2),(4,2),(5,2),(1,3),(2,3),(8,4),(8,6),(2,5),(4,5),(7,5);
/*!40000 ALTER TABLE `tbl_store_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_store_disabled_category`
--

DROP TABLE IF EXISTS `tbl_store_disabled_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_disabled_category` (
  `cat_id` int(10) unsigned DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_tbl_store_disabled_category_tbl_category1_idx` (`cat_id`),
  KEY `fk_tbl_store_disabled_category_tbl_stores1_idx` (`store_id`),
  CONSTRAINT `fk_tbl_store_disabled_category_tbl_category1` FOREIGN KEY (`cat_id`) REFERENCES `tbl_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_store_disabled_category_tbl_stores1` FOREIGN KEY (`store_id`) REFERENCES `tbl_stores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_store_disabled_category`
--

LOCK TABLES `tbl_store_disabled_category` WRITE;
/*!40000 ALTER TABLE `tbl_store_disabled_category` DISABLE KEYS */;
INSERT INTO `tbl_store_disabled_category` VALUES (222,4);
/*!40000 ALTER TABLE `tbl_store_disabled_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stores`
--

DROP TABLE IF EXISTS `tbl_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `rate` int(11) DEFAULT '0',
  `slug` varchar(60) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `desc` text,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `status` enum('Pending','Active','Declined','Banned') DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `banner` varchar(45) DEFAULT NULL,
  `doc_nbi` varchar(45) DEFAULT NULL,
  `doc_valid_id` varchar(45) DEFAULT NULL,
  `decline_reason` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_stores_tbl_users1_idx` (`user_id`),
  CONSTRAINT `fk_tbl_stores_tbl_users1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stores`
--

LOCK TABLES `tbl_stores` WRITE;
/*!40000 ALTER TABLE `tbl_stores` DISABLE KEYS */;
INSERT INTO `tbl_stores` VALUES (1,3,0,'fuxing-daza','Fuxing Daza','asdfsdafsadf \r\nsdaf ','Bulan Sorsogon','(123) 123-2222','Active','56517ddc0d70f.jpg','56517de0b7e27.jpg','56517de2b8376.docx','','sdfasdf','2015-11-22 00:33:53','2015-11-22 00:51:37'),(2,3,0,'fuxing-daza-irosin','Fuxing Daza Irosin','asdfasdf','Irosin Sorsogon','(222) 222-2222','Pending','56519be2946d2.jpg','56519be4b7abd.jpg','56519be71a233.docx','56519be8dc2a5.docx',NULL,'2015-11-22 02:41:59','2015-11-22 02:41:59'),(3,5,0,'mabuhay-supplies','Mabuhay Supplies','test desc','Gen Santons Ermita Manila','(222) 222-2222','Active','56519f4f6a4c9.jpg','56519f5387bc4.jpg','56519f57bc253.docx','56519f5956c72.docx',NULL,'2015-11-22 02:57:14','2015-11-22 02:57:35'),(4,6,0,'johns-digital','Johns Digital','asdfasdf asdf asdf ','Area 72 Occultus Creator Mars','(222) 222-2222','Active','5651a55501e3b.jpg','5651a556aecd5.jpg','5651a55982b94.docx','5651a55b68d65.docx',NULL,'2015-11-22 03:22:20','2015-11-22 03:22:41'),(5,4,0,'test-bandecline','Test Ban/Decline','asdfsadf','test address','(333) 333-3333','Banned','5651a6080ba20.jpg','5651a609a5ca4.jpg','5651a60bae956.docx','5651a60da610f.docx','asdfsadf','2015-11-22 03:25:06','2015-11-24 21:25:51'),(6,6,0,'johns-digital-2','Johns Digital 2','asdf asdf sadf asdf sadf asdf ','Sample Address','(222) 222-2222','Pending','5651a700b7b22.jpg','5651a7027c954.jpg','5651a70623b7b.docx','5651a708107ca.docx',NULL,'2015-11-22 03:29:26','2015-11-22 03:29:26');
/*!40000 ALTER TABLE `tbl_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('admin','support','vendor','shopper') DEFAULT 'shopper',
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `fname` varchar(60) DEFAULT NULL,
  `lname` varchar(60) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `birthdate` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `avatar` varchar(45) DEFAULT 'user.jpg',
  `active` tinyint(4) DEFAULT '1',
  `doc_sss` varchar(45) DEFAULT NULL,
  `doc_tin` varchar(45) DEFAULT NULL,
  `doc_pagibig` varchar(45) DEFAULT NULL,
  `doc_philhealth` varchar(45) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'admin','moonlightsro@gmail.com','$2y$10$x38/UyQ9CHi4F58CvXmP3eAWtj9Y5.69OPkGUSGWCbUKWY0pTbjWa','Moony','Lighty','Female','11/11/2011','+633242342342','user.jpg',1,NULL,NULL,NULL,NULL,'ntdVCzF6Oz2rUFW1qhu9FIEZSPSvGKLIMixwfm1kKziZrDLggxpx6o9aHGef','2015-11-14 07:20:43','2015-11-27 10:48:41'),(2,'support','support@test.com','$2y$10$j4bHmjoQw/lWZu3mXGT4SuOo.BVXDmiwzYdoD6SXLP9ITbY7pAJke','Support','Sporty','Male','02/22/2022','+639999999999','user.jpg',1,NULL,NULL,NULL,NULL,'FpDez9MAG2fajdzCaLWeLKi7RsdERgDlWfl0Rzjh8YnSskizu1XlQrVxVa4m','2015-11-17 07:31:59','2015-11-25 08:13:34'),(3,'vendor','fuxing.daza@test.com','$2y$10$hAQpnpX/5KYAktRMiUvQKuqrygdl/JRMKhQyZaXZgnHhHKbQyUq3e','Ben','Chan','Male','03/31/2011','+632222222222','user.jpg',1,NULL,NULL,NULL,NULL,'o0ZV1xSqujlb4DWBytNZvESPW42f1xL7LmhYlJ9jcf83Lu55ZOgqKIbvABtf','2015-11-18 22:15:19','2015-11-22 00:35:03'),(4,'vendor','dummy@test.com','$2y$10$Qq/cnzgiEVZ4hKRca4.Xg.gGEiGahAL5IYBGI6S8yabO.S1BE/lBC','Scare','Crow','Male','11/11/2011','+632222222222','user.jpg',1,NULL,NULL,NULL,NULL,'QrdLwvGZACy9RHUOIA4SpkDbBnUbPbRY5oozUYP9IrY647wr2VoQ7xsbkvbH','2015-11-18 22:54:28','2015-11-24 14:55:29'),(5,'vendor','mabuhay.supplies@test.com','$2y$10$rryCK5b9VOslRy58EQCkpOiiwg7jmxZ8Ql9YXsnvyWoIWq4aZcQFu','Pan','de Lama','Male','03/31/2011','+631111111111','user.jpg',1,NULL,NULL,NULL,NULL,'5HnLiY4YuvJjhXskWxWJlfo0BgiygnKADnXUKCeCSGkk284ZZcRKl17yRn6z','2015-11-18 23:23:27','2015-11-22 03:17:47'),(6,'vendor','johns.digital@test.com','$2y$10$KLzYwt79fXv9zfjAz/iZP.vWndzckmYThpsyogDxU4kwybXXNSPZm','Johnny','Macho','Male','12/23/2032','+632323222222','user.jpg',1,NULL,NULL,NULL,NULL,'TOQkZddM5gGmaAWS9hl26g1PmO4ZuhxJVAB7MCvZaW1voiGAy9v1AHLDJcAJ','2015-11-18 23:31:22','2015-11-25 20:10:32'),(7,'shopper','captcha@test.com','$2y$10$jHTGmRJrPaUk25GDiayHOu0Gg8RD.ffrlNf3yPCSof1w7JtxvEWri','sdfasd','asdfX','Male','02/22/2022','+633333333333','user.jpg',0,NULL,NULL,NULL,NULL,'nWE2Kb8TFFN7TdGkaXFZ5sgtOCNLqQ43gDjsiNOxxf4EiBmfpGovB9rGaD5v','2015-11-24 18:01:27','2015-11-25 02:13:18'),(8,'shopper','shopper1@test.com','$2y$10$47Dq/9ax/2O1nfRilni6WODAE/TXjWoiyxbRPUQTNdSiHgq3FLIX2','Barbara','Collins','Female','02/22/2022','+632222222221','user.jpg',1,NULL,NULL,NULL,NULL,'WehXOGlkD8d63KVgQegvY4HK4fSShlEURC3FugkSnSYhTvqcFKhmcK2VsDOA','2015-11-24 20:40:00','2015-11-25 02:13:16');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_variants`
--

DROP TABLE IF EXISTS `tbl_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_variants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_variants_tbl_options1_idx` (`option_id`),
  CONSTRAINT `fk_tbl_variants_tbl_options1` FOREIGN KEY (`option_id`) REFERENCES `tbl_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_variants`
--

LOCK TABLES `tbl_variants` WRITE;
/*!40000 ALTER TABLE `tbl_variants` DISABLE KEYS */;
INSERT INTO `tbl_variants` VALUES (8,2,'Green',1),(9,2,'Black',1),(10,2,'Red',1),(11,4,'S',0.1),(12,4,'M',0.1),(13,4,'X',0.1),(14,4,'XL',0.1),(15,4,'XXL',0.1),(16,5,'Blue',0.1),(17,6,'Gray',0),(18,6,'Green',0),(19,6,'Light Blue',0),(20,6,'Dark Blue',0);
/*!40000 ALTER TABLE `tbl_variants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-28  3:00:09
