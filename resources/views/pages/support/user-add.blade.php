@extends('layouts.master')
@section('title', 'User Add - Support')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User
      <small>Control Panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('support.stores')}}">Users</a></li>
      <li class="active">Add</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
  			@if($errors->any())
	        <div class="alert alert-danger"> <!-- php triggered -->
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif

	      {!! Form::open(['route' => 'support.user.save.p', 'method' => 'POST', 'id' => 'store-form']) !!}
	      	<div class="box box-info">
		        <div class="box-header with-border">
		          <h3 class="box-title">User Details</h3>
		        </div>

		        <div class="box-body">
		        	
              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Email</h5>
                  {!! Form::email('email', '', ['id'=>'signup-email', 'class' => 'form-control text', 'placeholder' => 'Enter Email', 'required'=>'', 'autofocus'=>'']) !!}
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Gender</h5>
                  {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], '', ['id' => 'gender', 'class' => 'form-control']) !!}
                </div>
              </div>
          	
              <div class="col-xs-6">
                <div class="form-group">
                  <h5>First Name</h5>
                  {!! Form::text('fname', '', ['class' => 'form-control', 'placeholder' => 'Enter First Name', 'required'=>'']) !!}
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Last Name</h5>
                  {!! Form::text('lname', '', ['class' => 'form-control', 'placeholder' => 'Enter Last Name', 'required'=>'']) !!}
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Birth Date</h5>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    {!! Form::text('birthdate', '', ['id' => 'birthdate', 'class' => 'form-control', 'data-mask' => '', 'data-inputmask' => "'alias': 'mm/dd/yyyy'", 'placeholder'=>'Enter Birth Date']) !!}
                  </div>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Mobile:</h5>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                    {!! Form::text('phone', '', ['id' => 'phone', 'class' => 'form-control', 'data-mask' => '', 'data-inputmask' => "'mask': '+639999999999'", 'placeholder'=>'Enter Mobile', 'required'=>'']) !!}
                  </div>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Password</h5>
                  {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Please enter the password', 'required'=>''] ) !!}
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <h5>Confirm Password</h5>
                  {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Please re-enter the password', 'required'=>'', 'value' => '*******']) !!}
                </div>
              </div>
	            
						{!!Form::hidden('user-id', '', ['id' => 'user-id'])!!}
						
	       		</div><!-- /.box-body -->
	       	
		        <div class="box-footer clearfix">
		        	<div class="pull-right">

			          <button type="submit" class="btn btn-sm btn-info btn-flat">Save</button>
			        </div>
		        </div><!-- /.box-footer -->

	      	</div><!-- /.box -->
	      {!!Form::close()!!}

	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->


@stop