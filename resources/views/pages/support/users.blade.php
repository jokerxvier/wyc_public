@extends('layouts.master')
@section('title', 'Users - Support')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Users<small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Users</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

	 	<div class="row">
	    <div class="col-md-8">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Visitors Report</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-9 col-sm-8">
                <div class="pad">
                  <!-- Map will be created here -->
                  <div id="world-map-markers" style="height: 325px;"></div>
                </div>
              </div><!-- /.col -->
              <div class="col-md-3 col-sm-4">
                <div class="pad box-pane-right bg-green" style="min-height: 280px">
                  <div class="description-block margin-bottom">
                    <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                    <h5 class="description-header">8390</h5>
                    <span class="description-text">Visits</span>
                  </div><!-- /.description-block -->
                  <div class="description-block margin-bottom">
                    <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                    <h5 class="description-header">30%</h5>
                    <span class="description-text">Referrals</span>
                  </div><!-- /.description-block -->
                  <div class="description-block">
                    <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                    <h5 class="description-header">70%</h5>
                    <span class="description-text">Organic</span>
                  </div><!-- /.description-block -->
                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->

      @if(isset($latest_users))
  	    <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Users</h3>
              <div class="box-tools pull-right">
                <span class="label label-danger">{{count($latest_users)}} New Users</span>
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="users-list clearfix">
                @foreach($latest_users as $new_user)
                <li>
                	<a class="" href="#">
                    {!! Html::image('images/'.$new_user->avatar, 'User Image') !!}
                  </a>
                  <a class="users-list-name" href="#">{{$new_user->fname}}</a>
                  <span class="users-list-date">{{$new_user->time_elapsed}}</span>
                </li>
                @endforeach

              </ul><!-- /.users-list -->
            </div><!-- /.box-body -->
          </div><!--/.box -->
        </div> <!-- ./col -->
      @endif
    </div> <!-- ./row -->

    <div class="row">
    	<div class="col-md-12">
	    	<!-- TABLE: LATEST ORDERS -->
	      <div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">User List</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table" id="dtbl-users">
	              <thead>
	                <tr>
                    <th>Name</th>
	                  <th>Email</th>
                    <th>Birth Date</th>
                    <th>Mobile</th>
                    <th>Since</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	          <a href="{{URL::route('support.user.add')}}" class="btn btn-sm btn-info pull-right">Add New User</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->

  <script type="text/javascript">
    'use strict';
    $(function () {
      $('#world-map-markers').vectorMap({
        map: 'world_mill_en',
        normalizeFunction: 'polynomial',
        hoverOpacity: 0.7,
        hoverColor: false,
        backgroundColor: 'transparent',
        regionStyle: {
          initial: {
            fill: 'rgba(210, 214, 222, 1)',
            "fill-opacity": 1,
            stroke: 'none',
            "stroke-width": 0,
            "stroke-opacity": 1
          },
          hover: {
            "fill-opacity": 0.7,
            cursor: 'pointer'
          },
          selected: {
            fill: 'yellow'
          },
          selectedHover: {
          }
        },
        markerStyle: {
          initial: {
            fill: '#00a65a',
            stroke: '#111'
          }
        },
        markers: [
          {latLng: [41.90, 12.45], name: 'Vatican City'},
          {latLng: [43.73, 7.41], name: 'Monaco'},
          {latLng: [-0.52, 166.93], name: 'Nauru'},
          {latLng: [-8.51, 179.21], name: 'Tuvalu'},
          {latLng: [43.93, 12.46], name: 'San Marino'},
          {latLng: [47.14, 9.52], name: 'Liechtenstein'},
          {latLng: [7.11, 171.06], name: 'Marshall Islands'},
          {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
          {latLng: [3.2, 73.22], name: 'Maldives'},
          {latLng: [35.88, 14.5], name: 'Malta'},
          {latLng: [12.05, -61.75], name: 'Grenada'},
          {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
          {latLng: [13.16, -59.55], name: 'Barbados'},
          {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
          {latLng: [-4.61, 55.45], name: 'Seychelles'},
          {latLng: [7.35, 134.46], name: 'Palau'},
          {latLng: [42.5, 1.51], name: 'Andorra'},
          {latLng: [14.01, -60.98], name: 'Saint Lucia'},
          {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
          {latLng: [1.3, 103.8], name: 'Singapore'},
          {latLng: [1.46, 173.03], name: 'Kiribati'},
          {latLng: [-21.13, -175.2], name: 'Tonga'},
          {latLng: [15.3, -61.38], name: 'Dominica'},
          {latLng: [-20.2, 57.5], name: 'Mauritius'},
          {latLng: [26.02, 50.55], name: 'Bahrain'},
          {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
        ]
      });

    });
  </script>

  <form id="user-popup" class="white-popup-block mfp-hide">
    <div class="box-header with-border">
      <h3 class="box-title user">Add New User</h3>
    </div><!-- /.box-header -->
    <fieldset style="border:0;">
      <div class="box-body">
        <div class="alert alert-danger">
          <ul id="user-add-alert">
            <li>test</li>
          </ul>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputEmail1">User Type</label>
              <select class="form-control" id="type">
                <option>Shopper</option>
                <option>Vendor</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="email" placeholder="Enter email">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputPassword1">First Name</label>
              <input type="text" class="form-control" id="first_name" placeholder="Enter First Name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Last Name</label>
              <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Gender</label>
              {!! 
                Form::select('gender', 
                  ['Male' => 'Male', 'Female' => 'Female'], $user->gender,
                  ['id' => 'gender', 'class' => 'form-control'])
              !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="password" placeholder="Enter Password">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Birth Date:</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="birthdate" data-inputmask='"alias": "mm/dd/yyyy"' data-mask="" placeholder="Enter Birth Date">
              </div><!-- /.input group -->
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Mobile:</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-mobile"></i>
                </div>
                <input type="text" class="form-control" id="mobile" data-inputmask='"mask": "+639999999999"' data-mask="" placeholder="Enter Mobile">
              </div><!-- /.input group -->
            </div>
          </div>
        </div>


      </div>
      <div class="box-footer clearfix">
        <div class="pull-right">
          {{ csrf_field() }}
          <button class="btn btn-info btn-flat" id="user-popup-submit" user_id="0">Save</button>
          <button class="btn btn-default btn-flat popup-modal-dismiss">Cancel</button>
        </div>
      </div>
    </fieldset>
  </form>

@stop
