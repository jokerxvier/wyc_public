@extends('layouts.master')
@section('title', 'Stores - Support')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Stores
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Stores</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  	<div class="row">

  		<div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-aqua"><i class="fa fa-thumbs-o-up"></i></span>
	        <div class="info-box-content">
	          <span class="info-box-text">For Approval</span>
	          <span class="info-box-number">23</span>
	        </div><!-- /.info-box-content -->
	      </div><!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-yellow"><i class="fa fa-thumbs-o-down"></i></span>
	        <div class="info-box-content">
	          <span class="info-box-text">Declined</span>
	          <span class="info-box-number">23</span>
	        </div><!-- /.info-box-content -->
	      </div><!-- /.info-box -->
	    </div>

  		<div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-green"><i class="fa fa-sun-o"></i></span>
	        <div class="info-box-content">
	          <span class="info-box-text">Active</span>
	          <span class="info-box-number">233</span>
	        </div><!-- /.info-box-content -->
	      </div><!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-red"><i class="fa fa-lock"></i></span>
	        <div class="info-box-content">
	          <span class="info-box-text">Banned</span>
	          <span class="info-box-number">21</span>
	        </div><!-- /.info-box-content -->
	      </div><!-- /.info-box -->
	    </div>
    </div> <!-- ./row -->

    <div class="row">
    	<div class="col-md-12">
	    	<!-- TABLE: LATEST ORDERS -->
	      <div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">For Approval</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table table-hover" id="dtbl-stores-for-aprvl">
	              <thead>
	                <tr>
	                  <th>Logo</th>
	                  <th>Store</th>
	                  <th>Owner</th>
	                  <th>Email</th>
	                  <th>Address</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              		@if ($pending_store) 
			              	@foreach($pending_store as $pending)
			              		<tr>

			              			<td> 
			              				{!! Html::image('uploads/' . $pending->logo, 'a picture', array('class' => 'user-image', 'style' => 'border-radius: 50%')) !!}
			              			</td>
			              			<td>{{ $pending->name}}</td>
			              			<td>{{$pending->user->fname.' '.$pending->user->lname}}</td>
			              			<td>{{$pending->user->email}}</td>
			              			<td>{{$pending->address}}</td>
			              			<td>

                						<a href="#" store-id="{{$pending->id}}" class="approve-store">Approve</a>
			              			</td>
			              	
		              			</tr>

	              			@endforeach

			           @endif
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	          <!-- <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->

    </div> <!-- ./row -->

    <div class="row">
    	<div class="col-md-12">
	    	<!-- TABLE: LATEST ORDERS -->
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Active</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table table-hover" id="dtbl-stores-active">
	              <thead>
	                <tr>
	                  <th>Logo</th>
	                  <th>Store</th>
	                  <th>Vendor</th>
	                  <th>Email</th>
	                  <th>Address</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	          <!-- <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->

    </div> <!-- ./row -->

    <div class="row">
    	<div class="col-md-12">
	      <div class="box box-warning">
	        <div class="box-header with-border">
	          <h3 class="box-title">Declined</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table table-hover" id="dtbl-stores-declined">
	              <thead>
	                <tr>
	                  <th>Logo</th>
	                  <th>Store</th>
	                  <th>Vendor</th>
	                  <th>Email</th>
	                  <th>Address</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	          <!-- <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

    <div class="row">
    	<div class="col-md-12">
	      <div class="box box-danger">
	        <div class="box-header with-border">
	          <h3 class="box-title">Banned</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table table-hover" id="dtbl-stores-banned">
	              <thead>
	                <tr>
	                  <th>Logo</th>
	                  <th>Store</th>
	                  <th>Vendor</th>
	                  <th>Email</th>
	                  <th>Address</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	          <!-- <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->



  <div class="box box-info mfp-hide" id="decline-store">
    <div class="box-header with-border">
      <h3 class="box-title">Decline Store</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<div class="col-md-12">
	    	<p>Reason(s) why this store should be declined. This will be displayed in store owners profile.</p>
        {{ csrf_field() }}
        {!!Form::textarea('reason', '', ['class' => 'form-control', 'id' => 'decline-reason', 'placeholder' => 'Why declined?', 'required' => ''])!!}

			</div>
    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right" id="decline-submit">Submit</a>
    </div>
  </div>

@stop