@extends('layouts.master')
@section('title', 'Store Profile - Support')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$store->name}}
      <small>Store</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('support.stores')}}">Stores</a></li>
      <li class="active">Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">

    	<div class="col-md-12">

    		@if($errors->any())
	        <div class="alert alert-danger"> <!-- php triggered -->
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif

	      <div class="alert alert-danger hidden"> <!-- js triggered -->
	        <ul></ul>
	      </div>
	    	
	      <div class="box box-info">
	      	{!! Form::open(['route' => 'general.store.save.p', 'method' => 'POST', 'id' => 'store-form']) !!}

	        <!-- Store Details -->

	        <div class="box-header with-border">
	          <h3 class="box-title">Store Details</h3>
	        </div>

	        <div class="box-body">
	        	<div class="col-xs-6">
	        		<div class="form-group">
									{!! Form::text('name', $store->name, ['class' => 'form-control', 'placeholder' => 'Enter Store Name', 'id' => 'store-name', 'required' => '']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('address', $store->address, ['class' => 'form-control', 'placeholder' => 'Enter Store Address', 'id' => 'store-address', 'required' => '']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('phone', $store->phone, ['class' => 'form-control', 'placeholder' => 'Enter Store Phone Number', 'id' => 'store-phone', 'data-mask' => '','data-inputmask' => "'mask': '(999) 999-9999'", 'required' => '']) !!}
								</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
							 	{!!Form::textarea('desc', $store->desc, ['class' => 'form-control', 'id' => 'store-desc', 'placeholder' => 'Enter Store Description', 'required' => ''])!!}
							</div>
						</div>

						<div class="col-xs-12">
						  <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                  Add Category <i class="fa fa-plus"></i>
                </a>
                <ul class="list">
                  @if(Session::has('pps-category'))
                    @foreach(Session::get('pps-category') as $id)
                      <?php $category = $categories->where('id', (int)$id)->first() ?>
                      <li class="option" pps-id="{{$category->id}}">
                        {{$category->name}}<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                      </li>
                    @endforeach
                  @else
                    @foreach($store_cat as $id => $name)
                      <li class="option" pps-id="{{$id}}">
                        {{$name}}<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="{{$id}}">
                      </li>
                    @endforeach
                  @endif
                </ul>

						</div>

						{!!Form::hidden('store-id', $store['id'], ['id' => 'store-id'])!!}
						
						@if(Session::has('file-logo'))
              {!!Form::hidden('file-logo', Session::get('file-logo'), ['id' => 'file-logo'])!!}
            @else
              {!!Form::hidden('file-logo', $store['logo'], ['id' => 'file-logo'])!!}
            @endif

            @if(Session::has('file-banner'))
              {!!Form::hidden('file-banner', Session::get('file-banner'), ['id' => 'file-banner'])!!}
            @else
              {!!Form::hidden('file-banner', $store['banner'], ['id' => 'file-banner'])!!}
            @endif

            @if(Session::has('file-nbi'))
              {!!Form::hidden('file-nbi', Session::get('file-nbi'), ['id' => 'file-nbi'])!!}
            @else
              {!!Form::hidden('file-nbi', $store['doc_nbi'], ['id' => 'file-nbi'])!!}
            @endif

						@if(Session::has('file-valid-id'))
              {!!Form::hidden('file-valid-id', Session::get('file-valid-id'), ['id' => 'file-valid-id'])!!}
            @else
              {!!Form::hidden('file-valid-id', $store['doc_valid_id'], ['id' => 'file-valid-id'])!!}
            @endif
						
	        </div><!-- /.box-body -->
	        {!!Form::close()!!}


	        <div class="box-body">
	        	<div class="col-xs-6">
							<h4>Store Logo</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-logo',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 40, 40],
                'note' => 'Preferred size 40 x 40.',
                'default' => $store->logo
              ])
						</div>

						<div class="col-xs-6">
							<h4>Store Banner</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-banner',
                'type' => 'images',
                'style' => 'width: 70%',
                'action' => ['fit', 847, 277],
                'note' => 'Preferred size 847 x 277.',
                'default' => $store->banner
              ])
						</div>
					</div><!-- /.box -->

					<div class="box-body">
						<div class="col-xs-6">
							<h4>NBI Clearance</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-nbi',
                'type' => 'any',
                'style' => 'width: 70%',
                'action' => ['move', 0, 0],
                'note' => '',
                'default' => $store->doc_nbi,
                'preview' => '0'
              ])
						</div>

						<div class="col-xs-6">
								<h4>Valid Identification Card</h4>
								@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-valid-id',
                'type' => 'any',
                'style' => 'width: 70%',
                'action' => ['move', 0, 0],
                'note' => '',
                'default' => $store->doc_valid_id,
                'preview' => '0'
              ])
						</div>
	        </div><!-- /.box -->

	        <div class="box-footer clearfix">
	        	<div class="pull-right">
		          <a href="#" class="btn btn-sm btn-info btn-flat" id="store-submit">Save Store</a>
		          @if($store['status'] == 'Pending')
		          	<a href="#" class="btn btn-sm btn-success btn-flat approve-store" store-id="{{$store['id'] }}">Approve</a>
			          <a href="#" class="btn btn-sm btn-danger btn-flat decline-store" store-id="{{$store['id'] }}">Decline</a>
			        @elseif($store['status'] == 'Declined')
			        	<a href="#" class="btn btn-sm btn-success btn-flat approve-store" store-id="{{$store['id'] }}">Approve</a>
			        @endif
		        </div>
	        </div><!-- /.box-footer -->

	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->

  <!-- popup -->
  <div class="box box-info mfp-hide" id="decline-store">
    <div class="box-header with-border">
      <h3 class="box-title">Decline Store</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<div class="col-md-12">
	    	<p>Reason(s) why this store should be declined. This will be displayed in store owners profile.</p>
        {{ csrf_field() }}
        {!!Form::textarea('reason', '', ['class' => 'form-control', 'id' => 'decline-reason', 'placeholder' => 'Why declined?', 'required' => ''])!!}
			</div>
    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right" id="decline-submit">Submit</a>
    </div>
  </div>

@include('pages.vendor.modal._category')
@stop