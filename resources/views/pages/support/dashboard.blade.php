@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>0</h3>
            <p>Total Orders</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{$prod_count}}</h3>
            <p>Total Products</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>{{$user_count}}</h3>
            <p>Total Users</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>{{$vendor_count}}</h3>
            <p>Vendor Registrations</p>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
          <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
        </div>
      </div><!-- ./col -->
    </div><!-- /.row -->



    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-7 connectedSortable">
          
				<!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Latest Orders</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Item</th>
                    <th>Status</th>
                    <th>Popularity</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="5">Coming Soon</td>
                  </tr>
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="{{URL::route('support.dashboard')}}" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
          </div><!-- /.box-footer -->
        </div><!-- /.box -->

      	<!-- Vendor Requests -->
        <div class="connectedSortable ui-sortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#shoppers" data-toggle="tab">Shoppers</a></li>
                  <li><a href="#vendors" data-toggle="tab">Vendors</a></li>
                  <li class="pull-left header"><i class="fa fa-inbox"></i> Latest Users</li>
                </ul>
                <div class="tab-content no-padding">
                  <div class="tab-pane" id="vendors">
                    @if($vendor_info)
                        <ul class="users-list clearfix">
                          @foreach($vendor_info as $vendor)
                            <li>
                              {!! Html::image('images/user.jpg', 'User Image') !!}
                              <a class="users-list-name" href="{{URL::route('support.user.view',  array('id'=> $vendor->id))}}">{{ucfirst($vendor->lname)}}</a>
                              <span class="users-list-date">{{$vendor->created_at->diffForHumans()}}</span>
                            </li>
                          @endforeach

                         
                        </ul><!-- /.users-list -->
                    @endif

                    @if(!$vendor_info) <center>No User Data</center> @endif

                  </div> 

                  <div class="chart tab-pane active" id="shoppers">
                    @if($shoppers_info)
                      <ul class="users-list clearfix">
                         @foreach($shoppers_info as $shopper)
                            <li>
                              {!! Html::image('images/user.jpg', 'User Image') !!}
                              <a class="users-list-name" href="{{URL::route('support.user.view',  array('id'=> $vendor->id))}}">{{ucfirst($shopper->lname)}}</a>
                              <span class="users-list-date">{{$shopper->created_at->diffForHumans()}}</span>
                            </li>
                          @endforeach
                      </ul><!-- /.users-list -->
                     @endif

                      @if(!$shoppers_info) <center>No User Data</center> @endif
                  </div>
                </div>

                <div class="box-footer text-center">
                    <a href="{{URL::route('support.users')}}" class="uppercase">View All Users</a>
                </div>

              </div><!-- /.nav-tabs-custom -->
        </div><!--/.box -->

      </section><!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
      <section class="col-lg-5 connectedSortable">
          <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#storeApproved" data-toggle="tab">Approved</a></li>
                  <li><a href="#storePending" data-toggle="tab">Pending</a></li>
                  <li class="pull-left header"><i class="fa fa-inbox"></i> Latest Stores</li>
              </ul>

              <div class="tab-content">
                  <div class="chart tab-pane active" id="storeApproved">
                    @if ($store_active)
                      <ul class="products-list product-list-in-box">
                        
                          @foreach($store_active as $store)
                              <li class="item">
                                <div class="product-img">
                                  {!! Html::image('uploads/' . $store->logo, 'Product Image') !!}
                                </div>
                                <div class="product-info">
                                  <a href="{{URL::route('support.store.view',  array('id'=> $store->id))}}" class="product-title">{{$store->name}} <span class="label label-warning pull-right">{{$store->created_at->diffForHumans()}}</span></a>
                                  <span class="product-description">
                             
                                    {{ str_limit($store->desc, $limit = 50, $end = '...') }}
                                  </span>
                                </div>
                              </li><!-- /.item -->
                            @endforeach
                        </ul>
                      @endif

                      @if(!$store_active) <center>No  Data</center> @endif

                  </div><!--end storeApproved-->

                  <div class="chart tab-pane" id="storePending">
                      @if ($store_pending)
                         <ul class="products-list product-list-in-box">
                            
                              @foreach($store_pending as $store)
                                  <li class="item">
                                    <div class="product-img">
                                      {!! Html::image('uploads/' . $store->logo, 'Product Image') !!}
                                    </div>
                                    <div class="product-info">
                                      <a href="{{URL::route('support.store.view',  array('id'=> $store->id))}}" class="product-title">{{$store->name}} <span class="label label-warning pull-right">{{$store->created_at->diffForHumans()}}</span></a>
                                      <span class="product-description">
                                 
                                        {{ str_limit($store->desc, $limit = 50, $end = '...') }}
                                      </span>
                                    </div>
                                  </li><!-- /.item -->
                                @endforeach
                        </ul>
                      @endif

                      @if(!$store_pending) <center>No  Data</center> @endif

                  </div><!--end storePending-->

                  
              </div>

              <div class="box-footer text-center">
                    <a href="{{URL::route('support.stores')}}" class="uppercase">View All Products</a>
              </div>

          </div><!--end nav-tabs-custom-->

      </section><!-- right col -->
    </div><!-- /.row (main row) -->

  </section><!-- /.content -->

@stop