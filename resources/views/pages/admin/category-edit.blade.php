@extends('layouts.master')
@section('title', 'Category Edit - Admin')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category Manager
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('admin.categories')}}"><i class="fa fa-dashboard"></i> Categories</a></li>
      <li class="active">Edit</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    	@if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @else
        	<div class="alert alert-danger hidden"> <!-- js triggered -->
            <ul></ul>
          </div>
        @endif
        
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Edit Category
	          	<a href="#cat-list-choose" class="btn btn-sm btn-default popup-modal pull-right cat-pop-up-trigger"><i class="fa fa-plus"></i> Add Parent Category</a>
	          </h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          
	          {!! Form::open(['route' => ['admin.category.save.p'], 'method' => 'POST', 'id' => 'category-form']) !!}
		        	<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('category-name', $category->name, ['class' => 'form-control', 'placeholder' => 'Enter Category Name', 'id' => 'category-name', 'required' => '']) !!}
								</div>
							</div>
							<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('category-icon', $category->icon, ['class' => 'form-control', 'placeholder' => 'Enter Category Icon Class', 'id' => 'category-icon', 'required' => '']) !!}
								</div>
							</div>

							{!!Form::hidden('cat-id', $category->id, ['id' => 'cat-id'])!!}
							{!!Form::hidden('file-image', '', ['id' => 'file-image'])!!}
							{!!Form::hidden('file-image-alt', '', ['id' => 'file-image-alt'])!!}

							<div class="col-xs-12">
								<ul id="cat-list" style="display:block;">
									@if($parent)
										<li>{{$parent->name}} <i class="fa fa-times"></i><input type="hidden" name="categories[]" value="{{$parent->id}}" <="" li=""></li>
									@else
										<li>Root Category <i class="fa fa-times"></i><input type="hidden" name="categories[]" value="0" <="" li=""></li>
									@endif
							  </ul>
							</div>
						{!!Form::close()!!}

						<div class="col-xs-6">
							<h4>Primary Image</h4>
							{!! Form::open(array('class' => 'img-form', "files" => true)) !!}
								<div class="form-group">
									<div class="file-input file-input-ajax-new">
										<div class="input-group ">
											<div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
											<div class="file-caption-name" id="file-image"></div>
											</div>

											<div class="input-group-btn">
												<button type="button" title="Clear selected files" class="btn btn-default img-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
												<button type="button" title="Abort ongoing upload" class="btn btn-default hide fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
												<div tabindex="500" class="btn btn-primary btn-file disabled">
													<i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse …
													{!! Form::file('image', array('class' => 'img-file', 'name' => 'img-file')) !!}
													{!!Form::hidden('img-type', 'file-image', ['class' => 'img-type'])!!}
												</div>
											</div>
										</div>
									</div>
									<small class="upload-msg">Preferred size 130 x 48 with transparent background.</small>
								</div>
								<div class="preview-img">
								</div>
							{!! Form::close() !!}
						</div>

						<div class="col-xs-6">
							<h4>Alternate Image</h4>
							{!! Form::open(array('class' => 'img-form', "files" => true)) !!}
								<div class="form-group">
									<div class="file-input file-input-ajax-new">
										<div class="input-group ">
											<div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
											<div class="file-caption-name" id="file-image"></div>
											</div>

											<div class="input-group-btn">
												<button type="button" title="Clear selected files" class="btn btn-default img-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
												<button type="button" title="Abort ongoing upload" class="btn btn-default hide fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
												<div tabindex="500" class="btn btn-primary btn-file disabled">
													<i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse …
													{!! Form::file('image', array('class' => 'img-file', 'name' => 'img-file')) !!}
													{!!Form::hidden('img-type', 'file-image-alt', ['class' => 'img-type'])!!}
												</div>
											</div>
										</div>
									</div>
									<small class="upload-msg">Preferred size 100 x 26 with #181818 background.</small>
								</div>
								<div class="preview-img">
								</div>
							{!! Form::close() !!}
						</div>

						

	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="{{URL::route('admin.categories')}}" class="btn btn-sm btn-default pull-right">Back</a>
	          <a href="#" class="btn btn-sm btn-success pull-right btn-right-pad approve-cat" cat-id="{{$category->id}}">Approve</a>
	          <a href="#" class="btn btn-sm btn-info pull-right btn-right-pad" id="category-submit">Submit</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  <!-- popup -->
	<div class="box box-info mfp-hide" id="cat-list-choose">
    <div class="box-header with-border">
      <h3 class="box-title">Select Parent Category</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="col-xs-12">

      	<table  id="treetable-cats">
					<tbody>
						@if(isset($categories))
							@foreach($categories as $category)
								<?php 
									$dtp = '';
									$dti = $category->id;
									if($category->parent > 0) {
										$dtp = $category->tree_parent;
										$dti = $dtp .'-'. $category->id;
									}	
								?>
								<tr data-tt-id="{{ $dti }}" {{ $dtp != '' ? 'data-tt-parent-id='. $dtp .'' : '' }}>
									<td> 
										<!-- <label for="cat-{{$category->id}}"> -->
											<input type="checkbox" class="minimal selected-cat" cat-id="{{$category->id}}" id="cat-{{$category->id}}" {{(isset($store_cat[$category->id]) ? 'checked' : '')}}/> {{ $category->name }} 
										<!-- </label> -->
									</td>
								</tr>
							@endforeach
						@endif
					
				  </tbody>
				</table>

			</div>
    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad add-parent-category" id="add-categories">Select Category</a>
    </div>
  </div>

@stop