@extends('layouts.master')
@section('title', 'Store Edit - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$store->name}}
      <small>Store</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li class="active">Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    	@if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>

        @else

		    	<div class="alert alert-danger {{($store['status'] == 'Declined' || $store['status'] == 'Banned' ? '' : 'hidden')}}"> <!-- js triggered -->
	          <ul>
	          	@if($store['status'] == 'Declined')
	          		<li>
	          			<b>Your Store Creation Request has been declined/banned!</b>
	          			<br> REASON: {{$store->decline_reason}}
	          		</li>
              @elseif($store['status'] == 'Banned')
                <li>
                  Your Store Page has been banned! Please contact our support for info.
                </li>
	          	@endif
	          </ul>
	        </div>
	      @endif

	      <div class="box box-info">
	      	{!! Form::open(['route' => 'general.store.save.p', 'method' => 'POST', 'id' => 'store-form']) !!}

	      	<!-- User Account Information -->
	        <div class="box-header with-border">
	          <h3 class="box-title">Store Details</h3>
	        </div><!-- /.box-header -->

	        <div class="box-body">
	        	<div class="col-xs-6">
	        		<div class="form-group">
									{!! Form::text('name', $store->name, ['class' => 'form-control', 'placeholder' => 'Enter Store Name', 'id' => 'store-name', 'required' => '']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('address', $store->address, ['class' => 'form-control', 'placeholder' => 'Enter Store Address', 'id' => 'store-address', 'required' => '']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('phone', $store->phone, ['class' => 'form-control', 'placeholder' => 'Enter Store Phone Number', 'id' => 'store-phone', 'data-mask' => '','data-inputmask' => "'mask': '(999) 999-9999'", 'required' => '']) !!}
								</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
							 	{!!Form::textarea('desc', $store->desc, ['class' => 'form-control', 'id' => 'store-desc', 'placeholder' => 'Enter Store Description', 'required' => ''])!!}
							</div>
						</div>

						<div class="col-xs-12">
						  <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                Add Category <i class="fa fa-plus"></i>
              </a>
              <ul class="list">
                @if(Session::has('pps-category'))
                  @foreach(Session::get('pps-category') as $id)
                    <?php $category = $categories->where('id', (int)$id)->first() ?>
                    <li class="option" pps-id="{{$category->id}}">
                      {{$category->name}}<i class="fa fa-times pps-del"></i>
                      <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                    </li>
                  @endforeach
                @else
                  @foreach($store_cat as $id => $name)
                    <li class="option" pps-id="{{$id}}">
                      {{$name}}<i class="fa fa-times pps-del"></i>
                      <input type="hidden" name="pps-category[]" value="{{$id}}">
                    </li>
                  @endforeach
                @endif
              </ul>

						</div>

						{!!Form::hidden('store-id', $store['id'], ['id' => 'store-id'])!!}
						
						@if(Session::has('file-logo'))
              {!!Form::hidden('file-logo', Session::get('file-logo'), ['id' => 'file-logo'])!!}
            @else
              {!!Form::hidden('file-logo', $store['logo'], ['id' => 'file-logo'])!!}
            @endif

            @if(Session::has('file-banner'))
              {!!Form::hidden('file-banner', Session::get('file-banner'), ['id' => 'file-banner'])!!}
            @else
              {!!Form::hidden('file-banner', $store['banner'], ['id' => 'file-banner'])!!}
            @endif

            @if(Session::has('file-nbi'))
              {!!Form::hidden('file-nbi', Session::get('file-nbi'), ['id' => 'file-nbi'])!!}
            @else
              {!!Form::hidden('file-nbi', $store['doc_nbi'], ['id' => 'file-nbi'])!!}
            @endif

						@if(Session::has('file-valid-id'))
              {!!Form::hidden('file-valid-id', Session::get('file-valid-id'), ['id' => 'file-valid-id'])!!}
            @else
              {!!Form::hidden('file-valid-id', $store['doc_valid_id'], ['id' => 'file-valid-id'])!!}
            @endif

	        </div><!-- /.box-body -->
	        {!!Form::close()!!}


	        <div class="box-body">
	        	<div class="col-xs-6">
							<h4>Store Logo</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-logo',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 40, 40],
                'note' => 'Preferred size 40 x 40.',
                'default' => $store->logo
              ])
						</div>

						<div class="col-xs-6">
							<h4>Store Banner</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-banner',
                'type' => 'images',
                'style' => 'width: 70%',
                'action' => ['fit', 847, 277],
                'note' => 'Preferred size 847 x 277.',
                'default' => $store->banner
              ])
						</div>
					</div><!-- /.box -->

					<div class="box-body">
						<div class="col-xs-6">
							<h4>NBI Clearance</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-nbi',
                'type' => 'any',
                'style' => 'width: 70%',
                'action' => ['move', 0, 0],
                'note' => '',
                'default' => $store->doc_nbi,
                'preview' => '0'
              ])
						</div>

						<div class="col-xs-6">
							<h4>Valid Identification Card</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-valid-id',
                'type' => 'any',
                'style' => 'width: 70%',
                'action' => ['move', 0, 0],
                'note' => '',
                'default' => $store->doc_valid_id,
                'preview' => '0'
              ])
						</div>

	        </div><!-- /.box -->

	        <div class="box-footer clearfix">
	        	<div class="pull-right">
		          <a href="#" class="btn btn-sm btn-info btn-flat" id="store-submit">Save Store</a>
		        </div>
	        </div><!-- /.box-footer -->

	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->

  <!-- popup -->
	<div class="box box-info mfp-hide" id="cat-list-choose">
    <div class="box-header with-border">
      <h3 class="box-title">Select Store Categories</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="col-xs-12">

      	<table  id="treetable-cats">
					<tbody>
						@if(isset($categories))
							@foreach($categories as $category)
								<?php 
									$dtp = '';
									$dti = $category->cat_id;
									if($category->parent > 0) {
										$dtp = $category->tree_parent;
										$dti = $dtp .'-'. $category->cat_id;
									}	
								?>
								<tr data-tt-id="{{ $dti }}" {{ $dtp != '' ? 'data-tt-parent-id='. $dtp .'' : '' }}>
									<td> 
										<label for="cat-{{$category->id}}">
											<input type="checkbox" class="minimal selected-cat" cat-id="{{$category->id}}" id="cat-{{$category->id}}" {{(isset($store_cat[$category->id]) ? 'checked' : '')}}/> {{ $category->name }} 
											
										</label>
									</td>
								</tr>
							@endforeach
						@endif
					
				  </tbody>
				</table>
			</div>
    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad" id="add-categories">Add Category</a>
    </div>
  </div>

@include('pages.vendor.modal._category')
@stop