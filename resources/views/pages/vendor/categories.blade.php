@extends('layouts.master')
@section('title', 'Categories - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category Manager
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li><a href="{{URL::route('vendor.store.dashboard', $active_store_id)}}">Store Dashboard</a></li>
      <li class="active">Categories</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  	{{csrf_field()}}
    <div class="row">
    	<div class="col-md-6">
        
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Active Categories</h3>
	          <div class="box-tools pull-right">
		          <div class="btn-group pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>

            </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
						@if(isset($root_categories))
							<ol class="sortable ui-sortable">
								@foreach($root_categories as $d0)
									<?php $depth1 = $filtered_categories->where('parent', $d0->id) ?>
									<li id="list_{{$d0->id}}" class="{{($depth1->count() > 0 ? 'mjs-nestedSortable-branch mjs-nestedSortable-collapsed' : '')}}">
										<div class="item-content">
											<span class="disclose"><span></span></span>
											<span class="{{$d0->icon}}"></span> {{$d0->name}} <small><code>({{$d0->slug}})</code></small>	
											@if($d0->store_id == 0)
												<i class="fa fa-fw fa-chain pull-right unlink-cat" cat-id="{{$d0->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Active and already linked to your store. Click to Deactivate/unlink from your store category list."></i>
											@else
												<span class="label label-info">User Category</span>
											@endif
											<!-- <i class="fa fa-fw fa-unlink pull-right" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive and hidden from your store category list. Click to link/display it from your store category list."></i> -->
																						

										</div>

										@if($depth1->count() > 0)
											<ol>
												@foreach($depth1 as $d1)
													<?php $depth2 = $filtered_categories->where('parent', $d1->id) ?>
													<li id="list_{{$d1->id}}" class="{{($depth2->count() > 0 ? 'mjs-nestedSortable-branch mjs-nestedSortable-collapsed' : '')}}">
														<div class="item-content">
															<span class="disclose"><span></span></span>
															{{$d1->name}} <small><code>({{$d1->slug}})</code></small>
															@if($d1->store_id == 0)
																<i class="fa fa-fw fa-chain pull-right unlink-cat" cat-id="{{$d1->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Active and already linked to your store. Click to Deactivate/unlink from your store category list."></i>
															@endif
														</div>
														
														<ol>
															@foreach($depth2 as $d2)
																<li id="list_{{$d2->id}}" class="mjs-nestedSortable-branch">
																	<div class="item-content">
																		<span class="disclose"><span></span></span>
																		{{$d2->name}} <small><code>({{$d2->slug}})</code></small>
																		@if($d2->store_id == 0)
																			<i class="fa fa-fw fa-chain pull-right unlink-cat" cat-id="{{$d2->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Active and already linked to your store. Click to Deactivate/unlink from your store category list."></i>
																		@endif
																	</div>
																</li>
															@endforeach
														</ol>
														
													</li>
												@endforeach
											</ol>
										@endif

									</li>
								@endforeach
							</ol>
						@endif

	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="#" class="btn btn-sm btn-default pull-right disabled" id="">Reset / Cancel Changes</a>
	          <a href="#" class="btn btn-sm btn-success pull-right btn-right-pad disabled" id="">Save Changes</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    	
    	<!-- ==============  -->

    	<div class="col-md-6">
        
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Inactive/For Approval Categories</h3>
	          <div class="box-tools pull-right">
		          <div class="btn-group pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>

            </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">

						@if(isset($disabled_categories) && count($disabled_categories) > 0)
							<ol class="sortable ui-sortable">
								@foreach($disabled_categories as $d0)
									<?php $cat_id = $d0->id; $depth1 = $categories->where('parent', $cat_id); ?>
									<li id="list_{{$cat_id}}" class="{{($depth1->count() > 0 ? 'mjs-nestedSortable-branch mjs-nestedSortable-collapsed' : '')}}">
										<div class="item-content">
											<span class="disclose"><span></span></span>
											<span class="{{$d0->icon}}"></span> {{$d0->name}} <small><code>({{$d0->slug}})</code></small>	
											@if($d0->store_id == 0)
												<i class="fa fa-fw fa-unlink pull-right link-cat" cat-id="{{$cat_id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive and hidden from your store category list. Click to Activate/Link again."></i>
											@else
												<i class="fa fa-times pull-right delete-cat" cat-id="{{$cat_id}}" data-toggle="tooltip" title="Delete"></i>
												@if($d0->status == 'Rejected')
													<span class="label label-danger" data-toggle="tooltip" title="Reason: {{$d0->reject_reason}}">Rejected</span>
												@else
													<a href="{{URL::route('vendor.store.category.edit', [$active_store_id, $cat_id])}}"><i class="fa fa-pencil pull-right" cat-id="{{$d0->id}}" data-toggle="tooltip" title="Edit"></i></a>
													<span class="label label-success">For Approval</span>
												@endif
											@endif

										</div>

										@if($depth1->count() > 0)
											<ol>
												@foreach($depth1 as $d1)
													<?php $cat_id = $d1->id; $depth2 = $categories->where('parent', $cat_id); ?>
													<li id="list_{{$cat_id}}" class="{{($depth2->count() > 0 ? 'mjs-nestedSortable-branch mjs-nestedSortable-collapsed' : '')}}">
														<div class="item-content">
															<span class="disclose"><span></span></span>
															{{$d1->name}} <small><code>({{$d1->slug}})</code></small>
															<i class="fa fa-fw fa-unlink pull-right link-cat" cat-id="{{$cat_id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive and hidden from your store category list. Click to Activate/Link again."></i>
														</div>
														
														<ol>
															@foreach($depth2 as $d2)
																<?php $cat_id = $d2->id ?>
																<li id="list_{{$d2->id}}" class="mjs-nestedSortable-branch">
																	<div class="item-content">
																		<span class="disclose"><span></span></span>
																		{{$d2->name}} <small><code>({{$d2->slug}})</code></small>
																		<i class="fa fa-fw fa-unlink pull-right link-cat" cat-id="{{$d2->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive and hidden from your store category list. Click to Activate/Link again."></i>
																	</div>
																</li>
															@endforeach
														</ol>
														
													</li>
												@endforeach
											</ol>
										@endif

									</li>
								@endforeach
							</ol>
						
						@else
							<center><code>-- No entries found --</code></center>
						@endif

	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="#" class="btn btn-sm btn-default pull-right disabled" id="">Delete all For Approvals</a>
	        	<a href="{{URL::route('vendor.store.category.add', $active_store_id)}}" class="btn btn-sm btn-info pull-right btn-right-pad" id="">Add New Category</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->

    </div> <!-- ./row -->

    <script type="text/javascript">
			$(document).ready(function(){

				$('.sortable').nestedSortable({
					forcePlaceholderSize: true,
					handle: 'div',
					helper:	'clone',
					items: 'li',
					opacity: .6,
					placeholder: 'placeholder',
					revert: 250,
					tabSize: 25,
					tolerance: 'pointer',
					toleranceElement: '> div',
					maxLevels: 3,
					
					// listType: 'ul',
					protectRoot: true,

					isTree: true,
					expandOnHover: 700,
					startCollapsed: true
				});

				$('.disclose').on('click', function() {
					$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
				})

			});
		</script>
@stop