@extends('layouts.master')
@section('title', 'Store Products - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$store->name}}
      <small>Products</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li><a href="{{URL::route('vendor.store.dashboard', $store->id)}}">Store Dashboard</a></li>
      <li class="active">Products</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	      <div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Store Products</h3>
	        </div><!-- /.box-header -->

	        <div class="box-body">
	        	<div class="table-responsive">
              <table class="table table-hover" id="dtbl-products">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Sale</th>
                    <th>Stock</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div><!-- /.table-responsive -->

            {!! csrf_field() !!}

            <script>
              // see notes in Vendor.js file about this global var
              var ACTIVE_STORE_ID = {{$active_store_id}};
            </script>

          </div><!-- /.box-body -->

	        <div class="box-footer clearfix">
	        	<div class="pull-right">
		          <a href="{{URL::route('vendor.store.product.add', $active_store_id)}}" class="btn btn-sm btn-info btn-flat" id="">Add New Product</a>
		        </div>
	        </div><!-- /.box-footer -->

	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->

@stop