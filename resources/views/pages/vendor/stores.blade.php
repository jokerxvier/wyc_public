@extends('layouts.master')
@section('title', 'Stores - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Stores
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Stores</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  	<div class="row">
  		<div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>150</h3>
            <p>New Orders</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-5 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>$31,430.63</h3>
            <p>Sales</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>$340.63</h3>
            <p>Taxes</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
    </div><!-- ./row -->

    <div class="row">
    	<div class="col-md-12">
	    	<!-- TABLE: LATEST ORDERS -->
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Store List</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table table-hover" id="dtbl-stores-active">
	              <thead>
	                <tr>
	                  <th>Logo</th>
	                  <th>Store</th>
	                  <th>Vendor</th>
	                  <th>Email</th>
	                  <th>Status</th>
	                  <th>Address</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	          <a href="{{URL::route('vendor.store.add')}}" class="btn btn-sm btn-info pull-right">Add New Store</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

@stop