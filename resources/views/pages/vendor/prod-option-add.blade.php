@extends('layouts.master')
@section('title', 'Product Option Add - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product Options
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Product Options</li>
      <li class="active">Add</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    	@if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @else
        	<div class="alert alert-danger hidden"> <!-- js triggered -->
            <ul></ul>
          </div>
        @endif
        
        {!! Form::open(['route' => ['vendor.store.prod.option.save.p', $active_store_id], 'method' => 'POST', 'id' => 'option-form']) !!}
		      <div class="box box-success">
		        <div class="box-header with-border">
		          <h3 class="box-title">Add Product Option</h3>
		        </div>
		        
		        <div class="box-body">
		        	<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('option-name', '', ['class' => 'form-control', 'placeholder' => 'Enter Unique Option Name', 'id' => 'option-name', '' => '']) !!}
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									{!! Form::text('display-name', '', ['class' => 'form-control', 'placeholder' => 'Enter Display Name', 'id' => 'option-name', 'required' => '']) !!}
								</div>
							</div>
		        </div>

		        <div class="box-body">
		          <div class="table-responsive">
		            <table class="table table-hover" id="tbl-variants">
		              <thead>
		                <tr>
		                  <th>Variant Name</th>
		                  <th>Weight</th>
		                  <th>Action</th>
		                </tr>
		              </thead>
		              <tbody>
										@if(Session::has('variant-name'))
											@foreach(Session::get('variant-name') as $key => $value)
												<tr class="variant-entry">
													<td>{!! Form::text('variant-name[]', $value, ['class' => 'form-control', 'placeholder' => 'Enter Variant Name']) !!}</td>
													<td>{!! Form::text('variant-weight[]', Session::get('variant-weight')[$key], ['class' => 'form-control', 'placeholder' => 'Enter Variant Weight']) !!}</td>
													<td class="action">
														{!!Form::hidden('variant-id[]', null, [])!!}
														<a href="#" class="variant-entry-remove">Remove</a>
													</td>
												</tr>
											@endforeach
										@else
											<tr class="variant-entry">
												<td>{!! Form::text('variant-name[]', null, ['class' => 'form-control', 'placeholder' => 'Enter Variant Name']) !!}</td>
												<td>{!! Form::text('variant-weight[]', null, ['class' => 'form-control', 'placeholder' => 'Enter Variant Weight']) !!}</td>
												<td class="action">
														{!!Form::hidden('variant-id[]', null, [])!!}
														<a href="#" class="variant-entry-remove">Remove</a>
													</td>
											</tr>
										@endif
		              </tbody>
		            </table>
		          </div>
		        </div>

		        {!!Form::hidden('option-id', '', ['id' => 'option-id'])!!}

		        <div class="box-footer clearfix">
		        	<a href="{{URL::route('vendor.store.prod.options', $active_store_id)}}" class="btn btn-sm btn-default pull-right">Back</a>
		          <a href="#" class="btn btn-sm btn-default pull-right btn-right-pad" id="variant-entry-add">Add Variant Entry</a>
		          <button type="submit" class="btn btn-sm btn-info pull-right btn-right-pad" id="pro-option-submit">Submit</button>
		        </div>
		      </div><!-- /.box -->
	      {!!Form::close()!!}

	    </div><!-- ./col -->
    </div> <!-- ./row -->

@stop