@extends('layouts.master')
@section('title', 'Product Edit - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$store->name}}
      <small>Store</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li><a href="{{URL::route('vendor.store.dashboard', $store->id)}}">Store Dashboard</a></li>
      <li><a href="{{URL::route('vendor.store.products', $store->id)}}">Products</a></li>
      <li class="active">Edit</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-52">
	      <div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Edit Product</h3>
	        </div><!-- /.box-header -->

	        <div class="box-body addproduct">
            <!-- Nav tabs -->
            <!-- <ul class="nav nav-tabs" role="tablist"> -->
              <!-- <li role="presentation" class="active"><a href="#general" aria-controls="home" role="tab" data-toggle="tab" id="tab-general">General</a></li> -->
              <!-- <li role="presentation"><a href="#images" aria-controls="images" role="tab" data-toggle="tab">Images</a></li> -->
              <!-- <li role="presentation"><a href="#qd" aria-controls="qd" role="tab" data-toggle="tab">Quantity discounts</a></li> -->
            <!-- </ul> -->

            @if($errors->any())
              <div class="alert alert-danger"> <!-- php triggered -->
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @else
              <div class="alert alert-danger hidden"> <!-- js triggered -->
                <ul></ul>
              </div>
            @endif

            <!-- Tab panes -->
            <div class="tab-content">

              <div role="tabpanel" class="tab-pane active" id="general">
                <div class="addprod_form">
                  {!! Form::open(['route' => ['vendor.store.product.post', $active_store_id], 'method' => 'POST', 'id' => 'product-form']) !!}
                    <h3>Product Information
                      <a href="#" class="btn btn-sm btn-info btn-flat pull-right product-submit">Submit Product</a>
                    </h3>

                    <div class="row">
                      <div class="col-xs-3 labelname">Vendor<small>*</small>:</div>
                      <div class="col-xs-9">{{$store->name}}</div>
                    </div>
                    <div class="row">
                      <div class="col-xs-3 labelname">Name <small>*</small>:</div>
                      <div class="col-xs-9">
                        {!! Form::text('product-name', $product->name, ['class' => 'form-control', 'required' => '']) !!}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Brands <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Brands of product if any. Select one only."></span> <small>*</small></div>
                      <div class="col-xs-9">
                        <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-brand-choose" id="pps-brand">
                          Add Brands <i class="fa fa-plus"></i>
                        </a>
                        <ul class="list">
                          @if(Session::has('pps-brand'))
                            @foreach(Session::get('pps-brand') as $id)
                              <?php $brand = $brands->where('id', (int)$id)->first() ?>
                              <li class="option" pps-id="{{$brand->id}}">
                                {{$brand->name}}<i class="fa fa-times pps-del"></i>
                                <input type="hidden" name="pps-brand[]" value="{{$brand->id}}">
                              </li>
                            @endforeach
                          @else
                            <?php $brand = $brands->where('id', $product->brand_id)->first() ?>
                            <li class="option" pps-id="{{$brand->id}}">
                              {{$brand->name}}<i class="fa fa-times pps-del"></i>
                              <input type="hidden" name="pps-brand[]" value="{{$brand->id}}">
                            </li>
                          @endif
                        </ul>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Categories:</div>
                      <div class="col-xs-9">
                        <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                          Add Category <i class="fa fa-plus"></i>
                        </a>
                        <ul class="list">
                          @if(Session::has('pps-category'))
                            @foreach(Session::get('pps-category') as $id)
                              <?php $category = $categories->where('id', (int)$id)->first() ?>
                              <li class="option" pps-id="{{$category->id}}">
                                {{$category->name}}<i class="fa fa-times pps-del"></i>
                                <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                              </li>
                            @endforeach
                          @else
                            @foreach($product_categories as $category)
                              <li class="option" pps-id="{{$category->id}}">
                                {{$category->name}}<i class="fa fa-times pps-del"></i>
                                <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                              </li>
                            @endforeach
                          @endif
                        </ul>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Options:</div>
                      <div class="col-xs-9">
                        <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-option-choose" id="pps-option">
                          Add Option <i class="fa fa-plus"></i>
                        </a>

                        <ul class="list">
                          @if(Session::has('pps-option'))
                            @foreach(Session::get('pps-option') as $id)
                              <?php $option = $options->where('id', (int)$id)->first() ?>
                              <li class="option" pps-id="{{$option->id}}">
                                {{$option->name_unique}}<i class="fa fa-times pps-del"></i>
                                <input type="hidden" name="pps-option[]" value="{{$option->id}}">
                              </li>
                            @endforeach
                          @else
                            @foreach($product_options as $option)
                              <li class="option" pps-id="{{$option->id}}">
                                {{$option->name_unique}}<i class="fa fa-times pps-del"></i>
                                <input type="hidden" name="pps-option[]" value="{{$option->id}}">
                              </li>
                            @endforeach
                          @endif
                        </ul>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Price (₱) <small>*</small>:</div>
                      <div class="col-xs-9">
                        {!! Form::text('product-price', $product->price, ['class' => 'form-control', 'required' => '', 'placeholder' => '0.00']) !!}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Sale Price (₱) <small>*</small>:</div>
                      <div class="col-xs-9">
                        {!! Form::text('product-price-sale', $product->price_sale, ['class' => 'form-control', 'required' => '', 'placeholder' => '0.00']) !!}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Short Description <small>*</small>:</div>
                      <div class="col-xs-9">
                        {!! Form::textarea('product-desc-short', $product->desc_short, ['class' => 'form-control', 'required' => '', 'rows' => '3']) !!}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Full description<small>*</small>:</div>
                      <div class="col-xs-9">
                        {!! Form::textarea('product-desc-long', $product->desc_long, ['class' => 'form-control', 'required' => '', 'id' => 'product-desc-long']) !!}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-3 labelname">Status<small>*</small>:</div>
                      <div class="col-xs-9" id="radio-active">
                        <label class="checkbox-inline" style="padding-left:0;">
                          {!!Form::radio('active', '1', true)!!} Active
                        </label>
                        <label class="checkbox-inline">
                          {!!Form::radio('active', '0')!!} Disabled
                        </label>
                      </div>
                    </div>
                    
                    <h3>Inventory</h3>
                    <div class="row">
                        <div class="col-xs-3 labelname">SKU:</div>
                        <div class="col-xs-9">
                          {!! Form::text('product-sku', $product->sku, ['class' => 'form-control', 'required' => '']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 labelname">In Stock<small></small>:</div>
                        <div class="col-xs-9">
                          {!! Form::text('product-stock', $product->stock, ['class' => 'form-control', 'required' => '']) !!}
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-3 labelname">Minimum order quantity:</div>
                      <div class="col-xs-9">
                        {!! Form::text('product-min-qty', $product->min_qty, ['class' => 'form-control', 'placeholder' => '0']) !!}
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-3 labelname">Maximum order quantity:</div>
                      <div class="col-xs-9">
                        {!! Form::text('product-max-qty', $product->max_qty, ['class' => 'form-control', 'placeholder' => '0']) !!}
                      </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-3 labelname">Taxes:</div>
                        <div class="col-xs-9">
                          <label class="checkbox-inline" style="padding-left:0;">
                            {!!Form::radio('product-tax', '1', true)!!} VAT
                          </label>
                        </div>
                    </div>

                    
                    
                    {!!Form::hidden('product-id', $product->id)!!}
                    {!!Form::hidden('store-id', $store->id)!!}

                    @if(Session::has('featured-image'))
                      {!!Form::hidden('featured-image', Session::get('featured-image'))!!}
                    @else
                      {!!Form::hidden('featured-image', $product->image)!!}
                    @endif

                    <!-- ugly as hell madafaka -->
                    
                    @if(Session::has('product-image-1'))
                      {!!Form::hidden('product-image-1', Session::get('product-image-1'))!!}
                    @else
                      {!!Form::hidden('product-image-1', $product_image_1) !!}
                    @endif

                    @if(Session::has('product-image-2'))
                      {!!Form::hidden('product-image-2', Session::get('product-image-2'))!!}
                    @else
                      {!!Form::hidden('product-image-2', $product_image_2)!!}
                    @endif

                    @if(Session::has('product-image-3'))
                      {!!Form::hidden('product-image-3', Session::get('product-image-3'))!!}
                    @else
                      {!!Form::hidden('product-image-3', $product_image_3)!!}
                    @endif

                    @if(Session::has('product-image-4'))
                      {!!Form::hidden('product-image-4', Session::get('product-image-4'))!!}
                    @else
                      {!!Form::hidden('product-image-4', $product_image_4)!!}
                    @endif

                    @if(Session::has('product-image-5'))
                      {!!Form::hidden('product-image-5', Session::get('product-image-5'))!!}
                    @else
                      {!!Form::hidden('product-image-5', $product_image_5)!!}
                    @endif

                  {!!Form::close()!!}

                  <h3>Images</h3>
                  <div class="row">
                    <div class="col-xs-3 labelname">Featured Image:</div>
                    <div class="col-xs-5">
                      @include('pages.general.etc.upload-form', [
                        'trigger_id' => 'featured-image',
                        'type' => 'images',
                        'style' => 'width: inherit',
                        'action' => ['fit', 132, 129],
                        'note' => 'Preferably png image file with transparent BG and with dimensions of 132 x 129.',
                        'default' => $product->image
                      ])
                    </div>
                  </div>

                  <h3>&nbsp;</h3>
                  <div class="row">
                    <div class="col-xs-3 labelname"></div>
                    <div class="col-xs-5">
                      <h5>We only allow up to 5 product images to reduce sever resource consumption</h5>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-xs-3 labelname">Product Images:</div>
                    <div class="col-xs-5">
                      @include('pages.general.etc.upload-form', [
                        'trigger_id' => 'product-image-5',
                        'type' => 'images',
                        'style' => 'width: inherit',
                        'action' => ['fit', 340, 340],
                        'note' => 'Any image file, prefered white BG and with dimensions of 340 x 340.',
                        'default' => (isset($product_image_1) ? $product_image_1 : '')
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-3 labelname"></div>
                    <div class="col-xs-5">
                      @include('pages.general.etc.upload-form', [
                        'trigger_id' => 'product-image-2',
                        'type' => 'images',
                        'style' => 'width: inherit',
                        'action' => ['fit', 340, 340],
                        'note' => '',
                        'default' => (isset($product_image_2) ? $product_image_2 : '')
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-3 labelname"></div>
                    <div class="col-xs-5">
                      @include('pages.general.etc.upload-form', [
                        'trigger_id' => 'product-image-3',
                        'type' => 'images',
                        'style' => 'width: inherit',
                        'action' => ['fit', 340, 340],
                        'note' => '',
                        'default' => (isset($product_image_3) ? $product_image_3 : '')
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-3 labelname"></div>
                    <div class="col-xs-5">
                      @include('pages.general.etc.upload-form', [
                        'trigger_id' => 'product-image-4',
                        'type' => 'images',
                        'style' => 'width: inherit',
                        'action' => ['fit', 340, 340],
                        'note' => '',
                        'default' => (isset($product_image_4) ? $product_image_4 : '')
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-3 labelname"></div>
                    <div class="col-xs-5">
                      @include('pages.general.etc.upload-form', [
                        'trigger_id' => 'product-image-5',
                        'type' => 'images',
                        'style' => 'width: inherit',
                        'action' => ['fit', 340, 340],
                        'note' => '',
                        'default' => (isset($product_image_5) ? $product_image_5 : '')
                      ])
                    </div>
                  </div>

                </div>
              </div><!--end tabpanel-->

              <div role="tabpanel" class="tab-pane" id="images">
                <div class="aploadentrylist">
                  <div class="row entry">
                    <div class="col-xs-9">
                      <div class="col-xs-5">Images:</div>
                      <div class="col-xs-52">
                        <p><small style="color:#808080">Any image file, prefered white BG and with dimensions of 340 x 340.</small></p>
                        <div class="fileinput fileinput-new" data-provides="fileinput" id="product-images">

                          {!! Form::open(array('class' => 'image-upd-form', "files" => true,)) !!}
                            <span class="btn btn-default btn-file">
                              <span class="fileinput-new">Select image</span>
                              {!! Form::file('image', array('id' => 'image-upd-btn', 'name' => 'image-file')) !!}
                            </span>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!--end tabpanel-->

              <div role="tabpanel" class="tab-pane" id="qd">
                <div class="box-body no-padding prodchart">
                  <table class="table table-hover">
                    <tbody>
                      <tr>
                        <th width="10">Quantity</th>
                        <th width="150">Value</th>
                        <th width="200 ">Type</th>
                        <th width="200 ">Type</th>
                        <th width="200 ">&nbsp;</th>
                      </tr>
                      <tr>
                        <td width="50">
                          <input type="text" class="form-control" style="width:50px">
                        </td>
                        <td>
                          <input type="text" class="form-control" >
                        </td>
                        <td>
                          <select class="form-control">
                            <option value="A" selected="selected">Absolute ($)</option>
                            <option value="P">Percent (%)</option>
                          </select>
                        </td>
                        <td>
                          <select class="form-control">
                            <option value="0">All</option>
                            <option value="1">Guest</option>
                            <option value="2">Registered user</option>
                          </select>
                        </td>
                        <td>
                          <div class="optionedit">
                            <a href="#"><i class="fa fa-plus"></i></a>
                            <a href="#"><i class="fa fa-clone"></i></a>
                            <a href="#"><i class="fa fa-times"></i></a>
                          </div>
                        </td>
                      </tr>
                  </table>
                </div>
              </div><!--end tabpanel-->

            </div> <!-- tab-content end -->

	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<div class="pull-right">
		          <a href="#" class="btn btn-sm btn-info btn-flat product-submit">Submit Product</a>
		        </div>
	        </div><!-- /.box-footer -->

	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->

  <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
  <script type="text/javascript">
    $(function () {
      CKEDITOR.replace('product-desc-long');
      $(".textarea").wysihtml5();
    });

  </script>


  @include('pages.vendor.modal._category')
  @include('pages.vendor.modal._brands')
  @include('pages.vendor.modal._options')
@stop