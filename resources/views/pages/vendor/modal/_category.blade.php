
<!-- popup -->
<div class="box box-info mfp-hide" id="pps-category-choose">
<div class="box-header with-border">
  <h3 class="box-title">Select Product Categories</h3>
  <div class="box-tools pull-right">
    <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
  </div>
</div>
<div class="box-body">
  <div class="col-xs-12">

    <table  id="treetable-cats" class="pps-treetable">
      <tbody>
        @if(isset($categories))
          @foreach($categories as $category)
            <?php 
              $dtp = '';
              $dti = $category->id;
              if($category->parent > 0) {
                $dtp = $category->tree_parent;
                $dti = $dtp .'-'. $category->id;
              } 
            ?>
            <tr data-tt-id="{{ $dti }}" {{ $dtp != '' ? 'data-tt-parent-id='. $dtp .'' : '' }}>
              <td> 
                <label>
                  <input type="checkbox" class="minimal pps-selected" pps-id="{{$category->id}}"> {{$category->name}}
                </label>
              </td>
            </tr>
          @endforeach
        @endif
      
      </tbody>
    </table>
  </div>
</div>
<div class="box-footer clearfix">
  <a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
  <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad" id="pps-category-insert">Add Category</a>
</div>
</div>