
<!-- popup -->
<div class="box box-info mfp-hide" id="pps-option-choose">
<div class="box-header with-border">
  <h3 class="box-title">Select Product Options</h3>
  <div class="box-tools pull-right">
    <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
  </div>
</div>
<div class="box-body">
  <div class="col-xs-12">

    <table class="pps-treetable">
      <tbody>
        @if ($options)
          @foreach($options as $option)
            <tr data-tt-id="">
              <td> 
                <label>
                  <input type="checkbox" class="minimal pps-selected" pps-id="{{$option->id}}"> {{$option->name_unique}}
                </label>
              </td>
            </tr>
          @endforeach
        @endif

        @if(!$options) <center> No  Brand Available</center> @endif
      </tbody>
    </table>
  </div>
</div>
<div class="box-footer clearfix">
  <a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
  <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad" id="pps-option-insert">Add Option</a>
</div>
</div>