@extends('layouts.master')
@section('title', 'Store Dashboard - Support')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$store->name}}
      <small>Dashboard</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li class="active">Store Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{($prod_count) ? $prod_count : 0}}</h3>
            <p>Total Products</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>{{($cat_count) ? $cat_count : 0  }}</h3>
            <p>Total Categories</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>0</h3>
            <p>Total Orders</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>0</h3>
            <p>Total Customer</p>
          </div>
          <div class="icon">
            <i class="ion ion-ios-people-outline"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
    </div><!-- /.row -->


    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-7 connectedSortable">
          
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Latest Orders</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Item</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="3"> Coming Soon</td>
                  
                  </tr>
                 
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="{{URL::to('coming-soon')}}" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
          </div><!-- /.box-footer -->
        </div><!-- /.box -->

      </section><!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
      <section class="col-lg-5 connectedSortable">


        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Recently Added Products</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              @foreach($prods as $prod)
                <li class="item">
                  <div class="product-img">
                    @if($prod->image) 
                      {!! Html::image('uploads/' . $prod->image, 'Product Image') !!}
                    @else
                        {!! Html::image('images/img/default-50x50.gif', 'Product Image') !!}
                    @endif
                  
                  </div>
                  <div class="product-info">
                    <a href="{{URL::route('vendor.store.products')}}" class="product-title"> {{$prod->name}} <span class="label label-warning pull-right">{{($prod->currency) ? $prod->currency : 'php'}} {{number_format($prod->price, 2)}}</span></a>
                    <span class="product-description">
                   
                      {{str_limit($prod->name, $limit = 20, $end = '...')}}
                    </span>
                  </div>
                </li><!-- /.item -->
              @endforeach
            </ul>
          </div><!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="{{URL::route('vendor.store.products')}}" class="uppercase">View All Products</a>
          </div><!-- /.box-footer -->
        </div><!-- /.box -->

        

      </section><!-- right col -->
    </div><!-- /.row (main row) -->

  </section><!-- /.content -->

@stop