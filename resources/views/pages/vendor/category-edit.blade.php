@extends('layouts.master')
@section('title', 'Category Edit - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category Manager
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li><a href="{{URL::route('vendor.store.dashboard', $active_store_id)}}">Store Dashboard</a></li>
      <li><a href="{{URL::route('vendor.store.categories', $active_store_id)}}">Categories</a></li>
      <li class="active">Edit New</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    	@if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @else
        	<div class="alert alert-danger hidden"> <!-- js triggered -->
            <ul></ul>
          </div>
        @endif
        
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Edit Category</h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          
	          {!! Form::open(['route' => ['vendor.store.category.save.p', $active_store_id], 'method' => 'POST', 'id' => 'category-form']) !!}
		        	<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('category-name', $category->name, ['class' => 'form-control', 'placeholder' => 'Enter Category Name', 'id' => 'category-name', 'required' => '']) !!}
								</div>
							</div>
							<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('category-icon', $category->icon, ['class' => 'form-control', 'placeholder' => 'Enter Category Icon Class', 'id' => 'category-icon', 'required' => '']) !!}
								</div>
							</div>

							{!!Form::hidden('cat-id', $category->id, ['id' => 'cat-id'])!!}

							@if(Session::has('file-image'))
                {!!Form::hidden('file-image', Session::get('file-image'), ['id' => 'file-image'])!!}
              @else
                {!!Form::hidden('file-image', $category->img_primary, ['id' => 'file-image'])!!}
              @endif

              @if(Session::has('file-image-alt'))
                {!!Form::hidden('file-image-alt', Session::get('file-image-alt'), ['id' => 'file-image-alt'])!!}
              @else
                {!!Form::hidden('file-image-alt', $category->img_home, ['id' => 'file-image-alt'])!!}
              @endif


							<div class="col-xs-12">
							  <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                  Add Category <i class="fa fa-plus"></i>
                </a>
                <ul class="list">
                  @if(Session::has('pps-category'))
                    @foreach(Session::get('pps-category') as $id)
                      <?php $category = $categories->where('id', (int)$id)->first() ?>
                      <li class="option" pps-id="{{$category->id}}">
                        {{$category->name}}<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                      </li>
                    @endforeach
                  @else
                    @if($parent)
											<li class="option" pps-id="{{$parent->id}}">
                        {{$parent->name}}<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="{{$parent->id}}">
                      </li>
										@else
											<li class="option" pps-id="0">
                        Root Category<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="0">
                      </li>
										@endif
                  @endif
                </ul>

							</div>
						{!!Form::close()!!}

						<div class="col-xs-6">
							<h4>Primary Image</h4>
							@include('pages.general.etc.upload-form', [
	              'trigger_id' => 'file-image',
	              'type' => 'images',
	              'style' => 'width: 70%',
	              'action' => ['fit', 847, 277],
								'note' => 'Preferred size 847 x 277.',
								'default' => $category->img_primary
	            ])
						</div>

						<div class="col-xs-6">
							<h4>Alternate Image</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-image-alt',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 163, 106],
								'note' => 'Preferred size 163 x 106.',
								'default' => $category->img_home
	             ])
						</div>


	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="{{URL::route('vendor.store.categories', $active_store_id)}}" class="btn btn-sm btn-default pull-right">Back</a>
	          <a href="#" class="btn btn-sm btn-info pull-right btn-right-pad" id="category-submit">Submit</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  <!-- popup -->
	<div class="box box-info mfp-hide" id="cat-list-choose">
    <div class="box-header with-border">
      <h3 class="box-title">Select Parent Category</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="col-xs-12">

      	<table  id="treetable-cats">
					<tbody>
						@if(isset($categories))
							@foreach($categories as $category)
								<?php 
									$dtp = '';
									$dti = $category->id;
									if($category->parent > 0) {
										$dtp = $category->tree_parent;
										$dti = $dtp .'-'. $category->id;
									}	
								?>
								<tr data-tt-id="{{ $dti }}" {{ $dtp != '' ? 'data-tt-parent-id='. $dtp .'' : '' }}>
									<td> 
										<!-- <label for="cat-{{$category->id}}"> -->
											<input type="checkbox" class="minimal selected-cat" cat-id="{{$category->id}}" id="cat-{{$category->id}}" {{(isset($store_cat[$category->id]) ? 'checked' : '')}}/> {{ $category->name }} 
										<!-- </label> -->
									</td>
								</tr>
							@endforeach
						@endif
					
				  </tbody>
				</table>

			</div>
    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad add-parent-category" id="add-categories">Select Category</a>
    </div>
  </div>

@stop