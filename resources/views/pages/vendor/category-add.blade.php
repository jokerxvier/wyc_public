@extends('layouts.master')
@section('title', 'Category Add - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category Manager
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li><a href="{{URL::route('vendor.store.dashboard', $active_store_id)}}">Store Dashboard</a></li>
      <li><a href="{{URL::route('vendor.store.categories', $active_store_id)}}">Categories</a></li>
      <li class="active">Add New</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    	@if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @else
        	<div class="alert alert-danger hidden"> <!-- js triggered -->
            <ul></ul>
          </div>
        @endif
        
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Add Category</h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          
	          {!! Form::open(['route' => ['vendor.store.category.save.p', $active_store_id], 'method' => 'POST', 'id' => 'category-form']) !!}
		        	<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('category-name', '', ['class' => 'form-control', 'placeholder' => 'Enter Category Name', 'id' => 'category-name', 'required' => '']) !!}
								</div>
							</div>
							<div class="col-xs-6">
		        		<div class="form-group">
									{!! Form::text('category-icon', '', ['class' => 'form-control', 'placeholder' => 'Enter Category Icon Class', 'id' => 'category-icon', 'required' => '']) !!}
								</div>
							</div>

							{!!Form::hidden('cat-id', '', ['id' => 'cat-id'])!!}
							
							@if(Session::has('file-image'))
                {!!Form::hidden('file-image', Session::get('file-image'), ['id' => 'file-image'])!!}
              @else
                {!!Form::hidden('file-image', '', ['id' => 'file-image'])!!}
              @endif

              @if(Session::has('file-image-alt'))
                {!!Form::hidden('file-image-alt', Session::get('file-image-alt'), ['id' => 'file-image-alt'])!!}
              @else
                {!!Form::hidden('file-image-alt', '', ['id' => 'file-image-alt'])!!}
              @endif

							<div class="col-xs-12">

							  <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                Add Category <i class="fa fa-plus"></i>
              </a>
              @if(Session::has('pps-category'))
                <ul class="list">
                	<li class="option" pps-id="0">
                    Root Category<i class="fa fa-times pps-del"></i>
                    <input type="hidden" name="pps-category[]" value="0">
                  </li>

                  @foreach(Session::get('pps-category') as $id)
                    <?php 
                    	$category = $categories->where('id', (int)$id)->first() 
                    ?>
                    <li class="option" pps-id="{{$category->id}}">
                      {{$category->name}}<i class="fa fa-times pps-del"></i>
                      <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                    </li>
                  @endforeach
                </ul>
              @endif

							</div>

						{!!Form::close()!!}

						<div class="col-xs-6">
							<h4>Primary Image</h4>
							@include('pages.general.etc.upload-form', [
	              'trigger_id' => 'file-image',
	              'type' => 'images',
	              'style' => 'width: 70%',
	              'action' => ['fit', 847, 277],
								'note' => 'Preferred size 847 x 277.',
	            ])
						</div>

						<div class="col-xs-6">
							<h4>Alternate Image</h4>
								@include('pages.general.etc.upload-form', [
	                'trigger_id' => 'file-image-alt',
	                'type' => 'images',
	                'style' => 'width: inherit',
	                'action' => ['fit', 163, 106],
								'note' => 'Preferred size 163 x 106.',
	              ])
						</div>

	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="{{URL::route('vendor.store.categories', $active_store_id)}}" class="btn btn-sm btn-default pull-right">Back</a>
	          <a href="#" class="btn btn-sm btn-info pull-right btn-right-pad" id="category-submit">Submit</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

@include('pages.vendor.modal._category')
@stop