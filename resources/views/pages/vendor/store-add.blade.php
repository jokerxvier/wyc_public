@extends('layouts.master')
@section('title', 'Store Add - Support')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Store
      <small>Details</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li class="active">Store Add</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    	@if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <div class="alert alert-danger hidden"> <!-- js triggered -->
          <ul></ul>
        </div>

	      <div class="box box-info">
	      	{!! Form::open(['route' => 'general.store.save.p', 'method' => 'POST', 'id' => 'store-form']) !!}

	      	<!-- User Account Information -->
	        <div class="box-header with-border">
	          <h3 class="box-title">Store Details</h3>
	        </div><!-- /.box-header -->

	        <div class="box-body">
	        	<div class="col-xs-6">
	        		<div class="form-group">
								{!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter Store Name', 'id' => 'store-name', 'required' => '']) !!}
							</div>
							<div class="form-group">
								{!! Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'Enter Store Address', 'id' => 'store-address', 'required' => '']) !!}
							</div>
							<div class="form-group">
								{!! Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Enter Store Phone Number', 'id' => 'store-phone', 'data-mask' => '','data-inputmask' => "'mask': '(999) 999-9999'", 'required' => '']) !!}
							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
							 	{!!Form::textarea('desc', '', ['class' => 'form-control', 'id' => 'store-desc', 'placeholder' => 'Enter Store Description', 'required' => ''])!!}
							</div>
						</div>

						<div class="col-xs-12">
							<a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                Add Category <i class="fa fa-plus"></i>
              </a>
              @if(Session::has('pps-category'))
                <ul class="list">
                  @foreach(Session::get('pps-category') as $id)
                    <?php $category = $categories->where('id', (int)$id)->first() ?>
                    <li class="option" pps-id="{{$category->id}}">
                      {{$category->name}}<i class="fa fa-times pps-del"></i>
                      <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                    </li>
                  @endforeach
                </ul>
              @endif
						</div>

						{!!Form::hidden('store-id', '', ['id' => 'store-id'])!!}

						@if(Session::has('file-logo'))
              {!!Form::hidden('file-logo', Session::get('file-logo'), ['id' => 'file-logo'])!!}
            @else
              {!!Form::hidden('file-logo', '', ['id' => 'file-logo'])!!}
            @endif

            @if(Session::has('file-banner'))
              {!!Form::hidden('file-banner', Session::get('file-banner'), ['id' => 'file-banner'])!!}
            @else
              {!!Form::hidden('file-banner', '', ['id' => 'file-banner'])!!}
            @endif

            @if(Session::has('file-nbi'))
              {!!Form::hidden('file-nbi', Session::get('file-nbi'), ['id' => 'file-nbi'])!!}
            @else
              {!!Form::hidden('file-nbi', '', ['id' => 'file-nbi'])!!}
            @endif

						@if(Session::has('file-valid-id'))
              {!!Form::hidden('file-valid-id', Session::get('file-valid-id'), ['id' => 'file-valid-id'])!!}
            @else
              {!!Form::hidden('file-valid-id', '', ['id' => 'file-valid-id'])!!}
            @endif
						
	        </div><!-- /.box-body -->
	        {!!Form::close()!!}

	        <div class="box-body">
	        	<div class="col-xs-6">
							<h4>Store Logo</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-logo',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 40, 40],
                'note' => 'Preferred size 40 x 40.',
              ])
						</div>

						<div class="col-xs-6">
							<h4>Store Banner</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-banner',
                'type' => 'images',
                'style' => 'width: 70%',
                'action' => ['fit', 847, 277],
                'note' => 'Preferred size 847 x 277.',
              ])
						</div>
					</div><!-- /.box -->

					<div class="box-body">
						<div class="col-xs-6">
							<h4>NBI Clearance</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-nbi',
                'type' => 'any',
                'style' => 'width: 70%',
                'action' => ['move', 0, 0],
                'note' => '',
                'default' => '',
                'preview' => '0'
              ])
						</div>

						<div class="col-xs-6">
							<h4>Valid Identification Card</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-valid-id',
                'type' => 'any',
                'style' => 'width: 70%',
                'action' => ['move', 0, 0],
                'note' => '',
                'preview' => '0'
              ])
						</div>
	        </div><!-- /.box -->

	        <div class="box-footer clearfix">
	        	<div class="pull-right">
		          <a href="#" class="btn btn-sm btn-info btn-flat" id="store-submit">Save Store</a>
		        </div>
	        </div><!-- /.box-footer -->

	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

  </section><!-- /.content -->

@include('pages.vendor.modal._category')
@stop