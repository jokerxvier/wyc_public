@extends('layouts.master')
@section('title', 'Brand Edit - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Brands
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{URL::route('vendor.stores')}}">Stores</a></li>
      <li><a href="{{URL::route('vendor.store.dashboard', $active_store_id)}}">Store Dashboard</a></li>
      <li><a href="{{URL::route('vendor.store.brands', $active_store_id)}}">Brands</a></li>
      <li class="active">Edit</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	
	    @if($errors->any())
          <div class="alert alert-danger"> <!-- php triggered -->
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Add Brand</h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          
	          {!! Form::open(['route' => ['vendor.store.brand.save.p', $active_store_id], 'method' => 'POST', 'id' => 'brand-form']) !!}
		        	<div class="col-xs-12">
		        		<div class="form-group">
									{!! Form::text('name', $brand->name, ['class' => 'form-control', 'placeholder' => 'Enter Brand Name', 'id' => 'brand-name', 'required' => '']) !!}
									{!!Form::hidden('brand-id', $brand->id, ['id' => 'brand-id'])!!}

									@if(Session::has('file-image'))
                    {!!Form::hidden('file-image', Session::get('file-image'), ['id' => 'file-image'])!!}
                  @else
                    {!!Form::hidden('file-image', $brand->image, ['id' => 'file-image'])!!}
                  @endif

                  @if(Session::has('file-image-alt'))
                    {!!Form::hidden('file-image-alt', Session::get('file-image-alt'), ['id' => 'file-image-alt'])!!}
                  @else
                    {!!Form::hidden('file-image-alt', $brand->image_alt, ['id' => 'file-image-alt'])!!}
                  @endif

								</div>
							</div>

							<div class="col-xs-12">
							  <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
                  Add Category <i class="fa fa-plus"></i>
                </a>
                <ul class="list">
                  @if(Session::has('pps-category'))
                    @foreach(Session::get('pps-category') as $id)
                      <?php $category = $categories->where('id', (int)$id)->first() ?>
                      <li class="option" pps-id="{{$category->id}}">
                        {{$category->name}}<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="{{$category->id}}">
                      </li>
                    @endforeach
                  @else
                    @foreach($brand_cat as $id => $name)
                      <li class="option" pps-id="{{$id}}">
                        {{$name}}<i class="fa fa-times pps-del"></i>
                        <input type="hidden" name="pps-category[]" value="{{$id}}">
                      </li>
                    @endforeach
                  @endif
                </ul>
							</div>
						{!!Form::close()!!}

						<div class="col-xs-6">
							<h4>Primary Image</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-image',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 130, 48],
                'note' => 'Preferred size 130 x 48 with transparent background.',
                'default' => $brand->image
              ])
						</div>

						<div class="col-xs-6">
							<h4>Alternate Image</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-image-alt',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 100, 26],
                'note' => 'Preferred size 100 x 26 with #181818 background.',
                'default' => $brand->image_alt
              ])
						</div>
	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="{{URL::route('vendor.store.brands', $active_store_id)}}" class="btn btn-sm btn-default pull-right">Back</a>
	          <a href="#" class="btn btn-sm btn-info pull-right btn-right-pad" id="brand-submit">Submit</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->


 @include('pages.vendor.modal._category')
@stop