@extends('layouts.master')
@section('title', 'Product Options - Vendor')
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product Options
      <small>Control panel</small>
    </h1>
                    
    <ol class="breadcrumb">
      <li><a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Product Options</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
    	<div class="col-md-12">
	    	<!-- TABLE: LATEST ORDERS -->
	      <div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Product Option List</h3>
	          <div class="box-tools pull-right">
	            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	            <table class="table table-hover" id="dtbl-prod-options">
	              <thead>
	                <tr>
	                  <th>Option Name</th>
	                  <th>Display Name</th>
	                  <th>Variant Count</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div><!-- /.table-responsive -->
	          {!! csrf_field() !!}

	          <script>
	          	// see notes in Vendor.js file about this global var
	          	var ACTIVE_STORE_ID = {{$active_store_id}};
	          </script>

	        </div><!-- /.box-body -->
	        <div class="box-footer clearfix">
	        	<a href="{{URL::route('vendor.stores')}}" class="btn btn-sm btn-default pull-right">Back</a>
	          <a href="{{URL::route('vendor.store.prod.option.add', $active_store_id)}}" class="btn btn-sm btn-info pull-right btn-right-pad">Add New Product Option</a>
	        </div><!-- /.box-footer -->
	      </div><!-- /.box -->
	    </div><!-- ./col -->
    </div> <!-- ./row -->

@stop