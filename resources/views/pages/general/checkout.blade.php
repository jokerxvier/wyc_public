@extends('layouts.default')
@section('title', 'Checkout Page')
@section('content')

<div class="body">
  <div class="main">
		<div class="container">
			
			<div class="checkout">

				<h3 class="title">Check out</h3>
				<div class="steptab">
					<ul  class="list-unstyled list-inline">
						<li class="active">Review your order</li>
						<li>Payment</li>
						<li >Review your order</li>
					</ul>
				</div>

				<div class="checkform">

					
					{!! Form::open(['id'=>'place-order-form', 'method'=>'POST']); !!}
						<div class="checktitle">
							<span>1</span>
							<h3>Verify your shipping information</h3>
							<small>Check your shipping info to ensure that your item will be recieved</small>
						</div>

						<div class="row field">
							<div class="shipping-info col-xs-10">
								
								@if($shipping == '')
									<div class="alert alert-danger">
				            <ul>
				              <li>We cant find your shipping or billing address. Please provide these information from your <a href="{{URL::route('user.profile')}}">Profile Page</a>.</li>
				            </ul>
				          </div>
				        @endif

								<dl class="dl-horizontal">
                  <dt>Name:</dt>
                  <dd><code>{{$name}}</code></dd>
                  <dt>Phone:</dt>
                  <dd><code>{{$phone}}</code></dd>
                  <dt>Email:</dt>
                  <dd><code>{{$email}}</code></dd>
                  <dt>Shipping Address:</dt>
                  <dd><code>{{$shipping}}</code></dd>
                  <dt>Billing Address:</dt>
                  <dd><code>{{$billing}}</code></dd>
                </dl>
							</div>
						</div>

						<div class="shopping_cart">
							<div class="checktitle">
								<span>2</span>
								<h3>Review your ordered items</h3>
								<small>Please verify the quantity per item</small>
							</div>

							<table class="table">
								<tr>
									<th colspan="2">Product Name &amp; Details </th>
									<th>Quantity</th>
									<th>Price</th>
									<th class="th-subtotal">Subtotal</th>
									<th>&nbsp;</th>
								</tr>

								<tr class="no-cart-item {{ $cart_item_count == 0 ? '' : 'hide' }}">
									<td><h4>No items in cart!</h4></td>
									<td>&nbsp;</td><td>&nbsp;</td>
									<td>&nbsp;</td><td>&nbsp;</td>
								</tr>

								@if(isset($cart) && count($cart) > 0)
									@foreach($cart as $item)
										<tr id="{{ $item->rowid }}">
											<td class="imgshop">
												<a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{!! Html::image('uploads/'.$item->productmodel->image, '' ) !!}</a>
											</td>
											<td class="shortdesc">
												<h4><a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{{ str_limit($item->name, 50, '...') }}</a></h4>
												<ul class="list-unstyled list-inline">
													@foreach($item->options as $option)
														<li>
															{{ $option['name'] }}
															{!!Form::hidden('options[]', $option['id'], ['id' => 'opt-'. $option['id'] ])!!}
														</li> 
													@endforeach
													<li>In stock</li>
													<!-- <li><a href="#" prod-id="{{$item->productmodel->id}}" class="add-to-wishlist">Move to wishlist</a></li> -->
													<li><a href="{{URL::route('general.store.detail', $item->productmodel->store->slug)}}">Contact seller</a></li>
												</ul>
											</td>
											<td class="qty" width="150">
												<input type="number" size="2" class="prod-qty" min="1" max="99" value="{{ $item->qty }}">
											</td>
											<td class="checkprice" width="160">
												@if($item->productmodel->discount > 0)
													<span class="disc_tag">{{ $item->productmodel->discount }}% <br/>OFF</span>
													<span>
														<em>{{ number_format($item->productmodel->price, 2) }}</em><br/>
														₱ {{ number_format($item->productmodel->sale_price, 2) }}
													</span>
												@else
													₱ {{ number_format($item->price, 2) }}
												@endif
											</td>
											<td class="subtotal">
												₱ {{ number_format($item->subtotal, 2) }}
											</td>
											<td>
												<a href="#" class="deletebtn cart-item-del" row-id="{{ $item->rowid }}">delete</a>
											</td>
										</tr>
									@endforeach
								@endif
							</table>

							<div class="row subtotalbreakdown">
								<div class="col-xs-7 ">
									<textarea name="message" class="textarea" cols="30" rows="10" placeholder="Leave a message for this seller"></textarea>
									<label class="check">
										<input type="checkbox" class="minimal" id="terms"/>
										<span>I agree to disclose my email address to this member</span>
									</label>
								</div>
								<div class="col-xs-5">
									<table>
										<tr>
											<td class="breakdownsub" width="190">SubTotal</td>
											<td class="total">₱ {{ number_format($grand_subtotal, 2) }}</td>
										</tr>
										<tr>
											<td class="breakdownsub" width="190">Shipping Cost</td>
											<td clas="shipping">₱ {{ number_format($grand_shipping, 2) }}</td>
										</tr>
										<tr>
											<td  class="breakdownsub" width="190">Total</td>
											<td class="totalbreakdown">₱ {{ number_format($grand_total, 2) }}<br/><small>VAT incl.</small></td>
										</tr>
									</table>
									<div class="btnset">
										<a href="{{URL::route('general.cart.payout')}}" class="btnproceed {{ $cart_item_count == 0 || $shipping == '' ? 'disabled' :'' }}" id="place-order" type="submit">Place Order</a>
										<a href="{{URL::route('general.cart') }}" class="continue">Return to cart</a>
									</div>
								</div>
							</div>

						</div>	<!-- end shopping_cart -->
					<!-- </form> -->
					{!! Form::close() !!}
				</div> <!-- end checkform -->
			
			</div><!--end check out-->	

		</div> <!-- end container -->
	</div> <!-- end main -->
</div><!--end body-->

@stop
