@extends('layouts.default')
@section('title', 'Order Review')
@section('content')

<div class="body">
  <div class="main">
		<div class="container">
			
			<div class="checkout">
					<h3 class="title">Order Summary</h3>
				<div class="steptab">
					<ul  class="list-unstyled list-inline">
						<li >Review your order</li>
						<li>Payment</li>
						<li class="lastli active">Review your order</li>
					</ul>
				</div>
				<div class="thankyou">
					<h4>Order Number - OR{{$order_num}}</h4>
					<h5>An email was sent to your email address for confirmation.<br>Please review your order summary below.</h5>
					<h5>Thank You!</h5>
				</div>
			</div><!--end check out-->	

			<div class="shopping_cart">
					<table class="table confirm-list">
						<tr>
							<th colspan="2">Product Name &amp; Details </th>
							<th>Quantity</th>
							<th>Price</th>
							<th class="th-subtotal">Subtotal</th>
						</tr>

						@if(isset($items) && count($items) > 0)
							@foreach($items as $item)
								<tr id="{{ $item->rowid }}">
									<td class="imgshop">
										<a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{!! Html::image('uploads/'.$item->productmodel->image, '' ) !!}</a>
									</td>
									<td class="shortdesc">
										<h4><a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{{ str_limit($item->name, 50, '...') }}</a></h4>
										<ul class="list-unstyled list-inline">
											@foreach($item->options as $option)
												<li><a href="#">{{ $option['name'] }}</a></li> 
											@endforeach
											<li><a href="#">In stock</a></li>     
											<li><a href="{{URL::route('general.store.detail', $item->productmodel->store->slug)}}">Contact seller</a></li>
										</ul>
									</td>
									<td class="qty" width="130">
										<h4>x {{ $item->qty }}</h4>
									</td>
									<td class="checkprice" width="130">
										@if($item->discount > 0)
											<span class="disc_tag">{{ $item->discount }}% <br/>OFF</span>
											<span>
												<em>{{ number_format($item->price, 2) }}</em><br/>
												₱ {{ number_format($item->price_sale, 2) }}
											</span>
										@else
											₱ {{ number_format($item->price, 2) }}
										@endif
									</td>
									<td class="subtotal">
										₱ {{ number_format($item->subtotal, 2) }}
									</td>
								</tr>
							@endforeach
						@endif
					</table>

					<div class="subtotalbreakdown confirm">
						<table>
							<tr>
								<td class="breakdownsub" width="190">SubTotal</td>
								<td class="total">₱ {{ number_format($grand_subtotal, 2) }}</td>
							</tr>
							<tr>
								<td class="breakdownsub" width="190">Shipping Cost</td>
								<td clas="shipping">₱ {{ number_format($grand_shipping, 2) }}</td>
							</tr>
							<tr>
								<td class="breakdownsub" width="190">Total</td>
								<td class="totalbreakdown">₱ {{ number_format($grand_total, 2) }}<br/><small>VAT incl.</small></td>
							</tr>
						</table>
					</div>
					
				</div> <!-- end shopping_cart -->

		</div> <!-- end container -->
	</div> <!-- end main -->
</div><!--end body-->

@stop

