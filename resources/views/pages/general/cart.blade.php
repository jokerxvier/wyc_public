@extends('layouts.default')
@section('title', 'Shopping Cart')
@section('content')

  <div class="body">
    <div class="main">
			<div class="container">
			
				<div class="shopping_cart">
					<h3 class="title">Shopping Cart</h3>
					<table class="table">
						<tr>
							<th colspan="2">Product Name &amp; Details </th>
							<th>Quantity</th>
							<th>Price</th>
							<th class="th-subtotal">Subtotal</th>
							<th>&nbsp;</th>
						</tr>

						<tr class="no-cart-item {{ $cart_item_count == 0 ? '' : 'hide' }}">
							<td><h4>No items in cart!</h4></td>
							<td>&nbsp;</td><td>&nbsp;</td>
							<td>&nbsp;</td><td>&nbsp;</td>
						</tr>
						{{csrf_field()}}
						@if(isset($cart) && count($cart) > 0)
							@foreach($cart as $item)
								<tr id="{{ $item->rowid }}">
									<td class="imgshop">
										<a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{!! Html::image('uploads/'.$item->productmodel->image, '' ) !!}</a>
									</td>
									<td class="shortdesc">
										<h4><a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{{ str_limit($item->name, 50, '...') }}</a></h4>
										<ul class="list-unstyled list-inline">
											@foreach($item->options as $option)
												<li>
													{{ $option['name'] }}
													{!!Form::hidden('options[]', $option['id'], ['id' => 'opt-'. $option['id'] ])!!}
												</li> 
											@endforeach
											<li>In stock</li>
											<li><a href="#" prod-id="{{$item->productmodel->id}}" row-id="{{ $item->rowid }}" class="cart-to-wish">Move to wishlist</a></li>
											<li>Seller: <a href="{{URL::route('general.store.detail', $item->productmodel->store->slug)}}">{{ $item->productmodel->store->name }}</a></li> 
										</ul>
									</td>
									<td class="qty" width="150">
										<input type="number" size="2" class="prod-qty" min="1" max="99" value="{{ $item->qty }}">
									</td>
									<td class="checkprice" width="160">
										@if($item->productmodel->discount > 0)
											<span class="disc_tag">{{ $item->productmodel->discount }}% <br/>OFF</span>
											<span>
												<em>{{ number_format($item->productmodel->price, 2) }}</em><br/>
												₱ {{ number_format($item->productmodel->sale_price, 2) }}
											</span>
										@else
											₱ {{ number_format($item->price, 2) }}
										@endif
									</td>
									<td class="subtotal">
										₱ {{ number_format($item->subtotal, 2) }}
									</td>
									<td>
										<a href="#" class="deletebtn cart-item-del" row-id="{{ $item->rowid }}">delete</a>
									</td>
								</tr>
							@endforeach
						@endif
					</table>

					<div class="subtotalbreakdown">
						<table>
							<tr>
								<td class="breakdownsub" width="190">SubTotal</td>
								<td class="total">₱ {{ number_format($grand_subtotal, 2) }}</td>
							</tr>
							<tr>
								<td class="breakdownsub" width="190">Shipping Cost</td>
								<td clas="shipping">₱ {{ number_format($grand_shipping, 2) }}</td>
							</tr>
							<tr>
								<td class="breakdownsub" width="190">Total</td>
								<td class="totalbreakdown">₱ {{ number_format($grand_total, 2) }}<br/><small>VAT incl.</small></td>
							</tr>
						</table>
						<div class="btnset">
							<!--<a href="{{ URL::to('cart/checkout') }}" class="btnproceed {{ $cart_item_count == 0 ? 'disabled' :'' }}">Proceed to checkout</a>-->
							<a href="{{ URL::to('/') }}" class="continue">Continue shopping</a>
						</div>
					</div>
					
					@if(isset($featured_product) && count($featured_product) > 0)
						<div class="prodcat popularprod">
							<h4>Featured Products</h4>

							@foreach(array_chunk($featured_product, 6) as $prods)
								<div class="row">
									@foreach($prods as $prod)
										<div class="col-xs-2">
											<div class="box">
												<div class="illustrate">
													<a href="{{ URL::to('product/detail', $prod->slug) }}">
														{!! Html::image('images/'. $prod->image, '') !!}
													</a>
													@if( $prod->discount > 0)
														<div class="discount_prize">
															<h1>{{ $prod->discount }}% OFF</h1>
															<span>{{ number_format($prod->price, 2 )}}</span>
														</div>
													@endif
												</div>
												<div class="catdesc">
													<h5>
														<a href="{{ URL::to('product/detail', $prod->slug) }}">
															{{ str_limit($prod->name, $limit = 26, $end = '...') }}
														</a>
													</h5>
													<span>{{ str_limit($prod->short_desc, $limit = 35, $end = '...') }}</span>
													<h3>
														<small>&#8369;</small> 
														@if( $prod->discount > 0)
															{{ number_format($prod->sale_price, 2) }}
														@else
															{{ number_format($prod->price, 2) }}
														@endif
													</h3>
												</div>
											</div>
										</div>
									@endforeach
								</div>
							@endforeach
						</div>
					@endif
				</div>
				
			</div> <!-- end container -->
		</div> <!-- end main -->
	</div><!--end body-->
@stop

