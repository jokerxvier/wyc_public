
@extends('layouts.default')
@section('title', 'Product Detail')
@section('content')
    <div class="body">
		<div class="main">
			<div class="container">
					<h3 class="title">Product Details</h3>
				<div class="details">
			
					<div class="prodname">
						<h4>{{ $product->name }}</h4>

						<span><strong>Seller:</strong> <a href="{{URL::route('general.store.detail', $product->store->slug)}}">{{ $product->store->name }}</a></span>
						@if($product->brand_name)
							<span><strong>Brand:</strong> {{ $product->brand_name }}</span>
						@endif
					</div>

					@if($product->stock < 1)	
						<div class="alert alert-danger">
	            <ul>
	              <li>
	              	Currently, this item is out of stock. The seller is working on to replenish the said item. You may also contact the seller from thier 
	              	<a href="{{URL::route('general.store.detail', $product->store->slug)}}">Store Page</a>.
	              </li>
	            </ul>
	          </div>
	         @endif
			    

					<div class="row">
						<div class="col-xs-6">
							<div class="view_large">
								<div class="center">
									{!! Html::image('uploads/'. $product->images[0]->image, $product->name) !!}
								</div>
							</div>

							@if($product->images)
								<div class="thumbs">
									<ul class="list-unstyled">
										@foreach($product->images as $img)
											{!! Html::image('uploads/'. $img->image, '') !!}
										@endforeach
									</ul>
									<div class="clear"></div>
								</div>
							@endif

							<!-- <div class="additional_link">
								<a href="#">Share</a>
								<a href="#">Favorite</a>
								<a href="#" class="add-to-wishlist" prod_id="{{ $product->product_id }}">Add to wishlist</a>
							</div> -->
						</div><!--end colxs-6-->
						
						<div class="col-xs-6">
							<div class="price">
								
								@if($product->discount > 0)
									<h3>₱ {{ number_format($product->price_sale, 2) }}</h3>
									<div class="prevprice">
										<small>Before</small>
										<span>₱ {{ number_format($product->price, 2) }}</span>
									</div>
								@else
									<h3>₱ {{ number_format($product->price, 2) }}</h3>
								@endif

								<div class="clear"></div>
							</div><!--end price-->
							<div class="prod_desc">
								<h5>Description</h5>
								<div class="row">
									<div class="col-xs-12">
										<p>{{ $product->desc_short }}</p>
										<a href="#" class="warrantylink">Warranty information</a>
									</div>	
									<!-- <div class="col-xs-6">
										<p style="color: #aaa;">8MP Rear Camera, 1.2MP Front Camera Wi-Fi 802.11 b/g/n, Bluetooth 4.0 Nano-SIM Non-removable Li-Po 2915mAh battery (11.1 Wh)</p>
									</div>	 -->
								</div>
							</div>

							<div class="additional_option">
								<div class="row inneradditional">
									<div class="col-xs-3">
										<!-- <h5>Stocks</h5> -->
										<!-- <span>[{{$product->stock}}]</span> -->
										<h5>Quantity</h5>
										<!-- <p>Items on stocks</p> -->
										<input type="number" size="2" class="prod-qty" min="1" max="99" value="1">
										<input type="hidden" name="prod-id" id="prod-id" value="{{$product->id}}">
									</div>
									
									@if(isset($options) && count($options) > 0)
										@foreach($options as $option)
											<div class="col-xs-3">
												<h5>{{ $option->name_alt }}</h5>
												<select name="variant[]" class="variants pad-5">
													@foreach($option->variants as $variant)
														<option value="{{ $variant->id }}">{{ $variant->name }}</option>
													@endforeach
												</select>
											</div>	
										@endforeach
									@endif
									
								</div>
								
								<div class="btnset">
									{!! csrf_field() !!}
									<!-- <a href="#" class="buynow" id="item-buy-now">buy now</a> -->
									<a href="#" class="addcart {{($product->stock < 1 ? 'disabled' : '')}}" id="add-to-cart">Add to cart</a>
									<a href="#" class="add-to-wishlist" prod-id="{{$product->id}}">Add to Wishlist</a>
									<!-- instructed to hide by mick-->
								</div>
							</div>
						</div>
					</div>
				</div><!--end details-->
				<div class="product_details">
					<div class="row">
						<div class="col-xs-9	">
							<div class="nav-tabs-custom">
                <!-- <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Product Details</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Specification</a></li>
                  <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Review</a></li>
                </ul> -->

                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <p>
                    	<b>{{ $product->name }}</b>
                    </p>
                    {!! $product->desc_long !!}
                  </div>

                  <div class="tab-pane" id="tab_2">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                  <div class="tab-pane" id="tab_3">
                    <!-- xxx -->
                  </div>
                </div>
              </div>

              <div>
              	<div id="disqus_thread"></div>
								<script type="text/javascript">
							    /* * * CONFIGURATION VARIABLES * * */
							    var disqus_shortname = 'wyc-ph';
							    
							    /* * * DON'T EDIT BELOW THIS LINE * * */
							    (function() {
						        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
						        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
						        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							    })();
								</script>
								<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
              </div>

						</div>

						<div class="col-xs-3 rightside">
							<h5>Recently viewed</h5>
							@foreach($recent as $prod)
								<div class="box">
									<div class="illustrate">
										<a href="{{ URL::to('product/detail', $prod->slug) }}">
											{!! Html::image('uploads/'.$prod->image, '', ['class' => 'recent-image']) !!}
										</a>
									</div>
									<div class="catdesc">
										<h5><a href="{{ URL::to('product/detail', $prod->slug) }}">{{ $prod->name }}</a></h5>
										<span>{{ $prod->desc_short }}</span>
										<h3><small>₱</small> {{ number_format($prod->price, 2) }}</h3>
									</div>	
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--end body-->
@stop

