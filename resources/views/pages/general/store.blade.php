@extends('layouts.default')
@section('title', 'Store Page')
@section('content')

  <div class="body">
  	
		<div class="container">
			
			<section class="mainsection">
			<div class="row">
				<div class="col-xs-3 sidebar">
					<div class="boxlist primary">
            <h4>Categories</h4>
            <div class="sidenav">
              <ul class="list-unstyled" id="sidenav">
                 @include('includes.sidebar-shopper')
              </ul> <!-- ul sidenav -->
            </div> <!-- div sidenav -->
          </div> <!--end boxlist-->
				</div><!--end sidebar-->

				<div class="col-xs-9 content">
					<div class="featuredbanner prodsec">
						<h3>{{ $store->name }}</h3>
						<div class="categorybanner">
							@if($store->banner)
								{!! Html::image('uploads/'. $store->banner, '') !!}
							@else
								{!! Html::image('uploads/sample-image.jpg', '') !!}
							@endif
						</div>
					</div>
					
          <div class="shopbrandslides prodsec">
					  @if(isset($brands))
							<h4>Shop by brands</h4>
							<ul class="list-unstyled">
								
								@foreach(array_chunk($brands, 6) as $brands_chunk)
									<li>
										<div class="brandshow">
											<div class="brandlist">
												@foreach($brands_chunk as $brand)
													{!! Html::image("uploads/{$brand->image}", '') !!}
												@endforeach
											</div>
										</div>
									</li>
								@endforeach
								
							</ul>
            @else
              <code>
                <h2>
                  No brands or products for this store, it is either a new store or an abandoned store (please report to our support to check this one)
                </h2>
              </code>
					  @endif
          </div>



				</div><!--end content-->
			</div><!--end row-->

      @if(isset($brands) || isset($products))
  			<div class="main">
  				@if(isset($sections) && count($sections) > 0)
            @foreach($sections as $section)
              <section >
                <div class="row">
                  <div class="col-xs-3  ">
                    <div class="sidemenu">
                      <div class="inner">
                        <div class="title">
                          <h5>{{ $section['root_cat']['name'] }}</h5>
                          <span>Recently Popular</span>
                        </div>
                        <div class="widget">
                          <ul class="links">
                            @foreach($section['popular_cats'] as $cat)
                              <li><a href="{{ URL::to('category',  $cat['slug']) }}">{{ $cat['name'] }}</a></li>
                            @endforeach
                        </div>
                      </div>
                      <div class="shoplist">
                        <ul>
                          @foreach($section['stores'] as $store)
                            <li>
                              <a href="{{ URL::to('store', $store->slug) }}" class="directlink">
                                <img class="lazy" data-original="{{asset('uploads/'. $store->logo)}}" width="41" height="41" />
                                <span>
                                  <h4>{{ $store->name }}</h4>
                                  <small>{{ $store->desc }}</small>
                                </span>
                              </a>
                            </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-xs-9">
                    <div class="product_post">
                        <ul class="slideshow slideshow01 list-unstyled" >
                          
                            <li><img class="lazy" data-original="{{asset('images/slidesample4.jpg')}}" width="652" height="296" /></li>
                          @foreach($section['banner_ads'] as $ad)
                          @endforeach
                        </ul>
                        <div class="categoryprod">
                          <div class="centerwrap">
                            <ul class="list-unstyled">
                              @foreach($section['brands'] as $brand)
                                <li>
                                	<a href="{{URL::to('brand', $brand->slug)}}">
                                		<img src="{{asset('uploads/'. $brand->image_alt) }}" />
                                	</a>
                                </li>
                              @endforeach
                            </ul>
                          </div>
                        </div>
                        <div class="othercategory hovereffect">
                          @foreach($section['sub_cats'] as $cat)
                            <div class="boxcell">
                              <a href="{{ URL::to('category',  $cat['slug']) }}">
                                <img src="{{asset('uploads/'. $cat['img_home'])}}" width="163" height="106" />
                                <span>{{ $cat['name'] }}</span>
                              </a>
                            </div>
                          @endforeach
                        </div>
                    </div>

                    @if(isset($section['best_seller']))
                      <div class="promoproduct bestseller">
                        <div class="showblock blockstyle ">
                            <div class="blocktitle">
                              <h3>Best seller</h3>
                            </div>
                            <div class="prodlist ">
                              @foreach($section['best_seller'] as $prod)
                                @if($prod->discount > 0) 
                                  <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                    <div class="entry discount">
                                      <div class="discount_tag">
                                        <h4>{{ $prod->discount }}<span>% OFF</span></h4>
                                      </div>
                                      <div class="desc">
                                        <span>{{ $prod->brand_name }}</span>
                                        <h5>{{ $prod->name }}</h5>
                                        <small class="prevprice">{{$prod->currency}} {{ number_format($prod->price, 2) }}</small>
                                        <p><strong>{{$prod->currency}} {{ number_format($prod->price_sale, 2) }}</strong></p>
                                      </div>
                                      <div class="showimage">
                                        <img class="lazy" data-original="{{asset('uploads/'. $prod->image)}}" />
                                      </div>
                                    </div>
                                  </a>
                                @else
                                  <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                  <div class="entry">
                                      <div class="desc">
                                        <span>{{ $prod->brand_name }}</span>
                                        <h5>{{ $prod->name }}</h5>
                                        <p><strong>{{$prod->currency}} {{ number_format($prod->price, 2) }}</strong></p>
                                      </div>
                                      <div class="showimage">
                                        <img class="lazy" data-original="{{asset('uploads/'. $prod->image)}}" />
                                      </div>
                                    </div>
                                  </a>
                                @endif
                              @endforeach
                            </div>
                        </div>
                      </div>
                    @endif

                    @if(isset($section['recommended_products']))

                      <div class="promoproduct bestseller">
                        <div class="showblock blockstyle ">
                            <div class="blocktitle">
                              <h3>Recommended</h3>
                            </div>
                            <div class="prodlist ">
                              @foreach($section['recommended_products'] as $prod)
                                @if($prod->discount > 0) 
                                  <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                    <div class="entry discount">
                                      <div class="discount_tag">
                                        <h4>{{ $prod->discount }}<span>% OFF</span></h4>
                                      </div>
                                      <div class="desc">
                                        <span>{{ $prod->brand_name }}</span>
                                        <h5>{{ $prod->name }}</h5>
                                        <small class="prevprice">{{$prod->currency}} {{ number_format($prod->price, 2) }}</small>
                                        <p><strong>{{$prod->currency}} {{ number_format($prod->price_sale, 2) }}</strong></p>
                                      </div>
                                      <div class="showimage">
                                        <img class="lazy" data-original="{{asset('uploads/'. $prod->image)}}" />
                                      </div>
                                    </div>
                                  </a>
                                @else
                                  <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                    <div class="entry">
                                      <div class="desc">
                                        <span>{{ $prod->brand_name }}</span>
                                        <h5>{{ $prod->name }}</h5>
                                        <p><strong>{{$prod->currency}} {{ number_format($prod->price, 2) }}</strong></p>
                                      </div>
                                      <div class="showimage">
                                        <img class="lazy" data-original="{{asset('uploads/'. $prod->image)}}" />
                                      </div>
                                    </div>
                                  </a>
                                @endif
                              @endforeach
                            </div>
                        </div>
                      </div>
                    @endif 

                  </div>
                </div>
              </section><!--end section-->
            @endforeach
          @endif
        </div>
      @endif
      
    </div> <!-- end container -->
    
	</div><!--end body-->
@stop