@extends('layouts.default')
@section('title', 'Checkout Page')
@section('content')

<div class="body">
  <div class="main">
		<div class="container">
			
			<div class="checkout">

				<h3 class="title">Guest Check out</h3>
				<div class="steptab">
					<ul  class="list-unstyled list-inline">
						<li class="active">Review your order</li>
						<li>Payment</li>
						<li >Review your order</li>
					</ul>
				</div>

				<div class="checkform">
						<div class="checktitle">
							<span>1</span>
							<h3>We need your shipping information</h3>
							<small>Please login or register to continue shopping</small>
						</div>

						<div class="row field guest-checkout">
							<div class="col-xs-5 guest-login">
								<!-- <div class=""> -->
									<h4>Login with your existing account</h4>
									{!! Form::open(['route' => 'user.login.p', 'method'=>'POST']); !!}
          					{!!Form::hidden('from','checkout')!!}
				            <div class="input-group pad-bottom-5">
				              <span class="input-group-addon"><span class="icon-logiconemail19"></span></span>
				              {!! Form::text('email', '', ['class' => 'form-control text', 'placeholder' => 'Username / Email', 'required'=>'', 'autofocus'=>'']) !!}
				            </div>
				            <div class="input-group">
				              <span class="input-group-addon"><span class="icon-logiconpassword19"></span></span>
				              {!! Form::password('password', ['class' => 'form-control text', 'placeholder' => 'Password']) !!}
				            </div>

				            <label class="remember-me">
				              {!! Form::checkbox('remember', 1, null, ['class' => 'minimal']) !!} Remember Me
				            </label>
				             | <a href="{{URL::route('pass.forgot')}}">Forgot Password</a>

				            <button class="btn bg-maroon btn-flat margin" type="submit">Sign In</button>

				          {!! Form::close() !!}
							</div>

							<div class="col-xs-4 guest-signup">
							<span class="or">OR</span>
								<h4>Sign Up to Continue Shopping</h4>
								<a  href="{{URL::route('user.signup')}}" class="btn bg-maroon btn-flat signup">Create a New Account</a>

								<h5>You can also signup with your</h5>
								<ul class="list-unstyled">
		              <li style="margin-bottom: 3px;">
		                <a href="#">
		                	{!! Html::image('images/fblink.png', '' ) !!}
		                </a>
		              </li>
		              <li>
		                <a href="#">
		                	{!! Html::image('images/googleplus.png', '' ) !!}
		                </a>
		              </li>
		            </ul>
							</div>

							<div class="col-xs-3">
								<h4>Signing Up!</h4>
									<p>
            				It is very easy! Register yourself as a normal user and if you wish to be a retailer/vendor just click the 'Open Store' link in your profile page.
            			</p>
							</div>
						</div>

						{!! Form::open(['id'=>'place-order-form', 'method'=>'POST']); !!}
						<div class="shopping_cart">
							<div class="checktitle">
								<span>2</span>
								<h3>Review your ordered items</h3>
								<small>Please verify the quantity per item</small>
							</div>

							<table class="table">
								<tr>
									<th colspan="2">Product Name &amp; Details </th>
									<th>Quantity</th>
									<th>Price</th>
									<th class="th-subtotal">Subtotal</th>
									<th>&nbsp;</th>
								</tr>

								<tr class="no-cart-item {{ $cart_item_count == 0 ? '' : 'hide' }}">
									<td><h4>No items in cart!</h4></td>
									<td>&nbsp;</td><td>&nbsp;</td>
									<td>&nbsp;</td><td>&nbsp;</td>
								</tr>

								@if(isset($cart) && count($cart) > 0)
									@foreach($cart as $item)
										<tr id="{{ $item->rowid }}">
											<td class="imgshop">
												<a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{!! Html::image('uploads/'.$item->productmodel->image, '' ) !!}</a>
											</td>
											<td class="shortdesc">
												<h4><a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{{ str_limit($item->name, 50, '...') }}</a></h4>
												<ul class="list-unstyled list-inline">
													@foreach($item->options as $option)
														<li>
															<a href="#">{{ $option['name'] }}</a>
															{!!Form::hidden('options[]', $option['id'], ['id' => 'opt-'. $option['id'] ])!!}
														</li> 
													@endforeach
													<li><a href="#">In stock</a></li>     
													<li><a href="#" prod-id="{{$item->productmodel->id}}" class="add-to-wishlist">Move to wishlist</a></li>
													<li><a href="{{URL::route('general.store.detail', $item->productmodel->store->slug)}}">Contact seller</a></li>
												</ul>
											</td>
											<td class="qty" width="150">
												<input type="number" size="2" class="prod-qty" min="1" max="99" value="{{ $item->qty }}">
											</td>
											<td class="checkprice" width="160">
												@if($item->productmodel->discount > 0)
													<span class="disc_tag">{{ $item->productmodel->discount }}% <br/>OFF</span>
													<span>
														<em>{{ number_format($item->productmodel->price, 2) }}</em><br/>
														₱ {{ number_format($item->productmodel->price_sale, 2) }}
													</span>
												@else
													₱ {{ number_format($item->price, 2) }}
												@endif
											</td>
											<td class="subtotal">
												₱ {{ number_format($item->subtotal, 2) }}
											</td>
											<td>
												<a href="#" class="deletebtn cart-item-del" row_id="{{ $item->rowid }}">delete</a>
											</td>
										</tr>
									@endforeach
								@endif
							</table>

							<div class="row subtotalbreakdown">
								<div class="col-xs-7 ">
									<textarea name="message" class="textarea" cols="30" rows="10" placeholder="Leave a message for this seller"></textarea>
									<label class="check">
										<input type="checkbox" class="minimal" id="terms"/>
										<span>I agree to disclose my email address to this member</span>
									</label>
								</div>
								<div class="col-xs-5">
									<table>
										<tr>
											<td class="breakdownsub" width="190">SubTotal</td>
											<td class="total">₱ {{ number_format($grand_subtotal, 2) }}</td>
										</tr>
										<tr>
											<td class="breakdownsub" width="190">Shipping Cost</td>
											<td clas="shipping">₱ {{ number_format($grand_shipping, 2) }}</td>
										</tr>
										<tr>
											<td class="breakdownsub" width="190">Total</td>
											<td class="totalbreakdown">₱ {{ number_format($grand_total, 2) }}<br/><small>VAT incl.</small></td>
										</tr>
									</table>
									<div class="btnset">
										<a href="#" class="btnproceed disabled" id="place-order" type="submit">Place Order</a>
										<a href="{{ URL::route('general.cart') }}" class="continue">Return to cart</a>
									</div>
								</div>
							</div>

						</div>	<!-- end shopping_cart -->
					<!-- </form> -->
					{!! Form::close() !!}
				</div> <!-- end checkform -->
			
			</div><!--end check out-->	

		</div> <!-- end container -->
	</div> <!-- end main -->
</div><!--end body-->

@stop
