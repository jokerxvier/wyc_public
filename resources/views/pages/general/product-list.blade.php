@extends('layouts.default')
@section('title', 'Category Page')
@section('content')

    <div class="body categorypage">
		<div class="container">
			
			<div class="row">
					<div class="col-xs-3 sidebar">
						<div class="boxlist primary">
			              <h4>Categories</h4>
			              <div class="sidenav">
			                <ul class="list-unstyled" id="sidenav">
			             		@include('includes.sidebar-shopper')
			                </ul> <!-- ul sidenav -->
			              </div> <!-- div sidenav -->
			            </div> <!--end boxlist-->
					</div><!--end sidebar-->

					<div class="col-xs-9 content">

						<!--start Product-->

							@if(isset($products) && count($products) > 0)
								<div class="prodcat popularprod">
									<h4>Product List</h4>

									@foreach (array_chunk($products->getCollection()->all(), 4) as $prods)
										<div class="row">
											@foreach($prods as $prod)
												<div class="col-xs-3">
													<div class="box">
														<div class="illustrate">
															<a href="{{ URL::to('product/detail', $prod['slug']) }}">
																{!! Html::image('uploads/'. $prod['image'], '') !!}
															</a>

															@if($prod['discount'] > 0)
																<div class="discount_prize">
																	<h1>{{ $prod['discount'] }}% OFF</h1>
																	<span>{{ number_format($prod['price'], 2 )}}</span>
																</div>
															@endif
														</div>
															<div class="catdesc">
																<h5><a href="{{ URL::to('product/detail', $prod['slug']) }}">{{$prod['name']}}</a></h5>
																<span>{{$prod['short_desc']}}</span>
																<h3>
																	<small>&#8369;</small> 
																	@if($prod['discount'] > 0)
																		{{ number_format($prod['sale_price'], 2) }}
																	@else
																		{{ number_format($prod['price'], 2) }}
																	@endif
																</h3>
														</div>
													</div>
											
												</div><!--end col-xs-3-->
											@endforeach
										</div><!--end row-->
									@endforeach
									{!! $products->render() !!}
								</div>
						@else 
								<center><h1>No Available Product</h1><center>
						@endif

						<!--end Product-->

	
					</div><!--end content-->
		
			</div><!--end product-group-->
		</div> <!-- end container -->
	</div><!--end body-->
@stop

