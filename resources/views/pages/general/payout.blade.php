@extends('layouts.default')
@section('title', 'Payment Page')
@section('content')

<div class="body">
  <div class="main">
		<div class="container">
			
			<div class="checkout">
				<h3 class="title">Check out</h3>
				<div class="steptab">
					<ul  class="list-unstyled list-inline">
						<li >Review your order</li>
						<li class="active">Payment</li>
						<li >Review your order</li>
					</ul>
				</div>

				@if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

						<div class="checkpayment">
							  <div class="row ">
									<div class="col-xs-6 courier">
										<ul class="nav nav-tabs">
										  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{!! Html::image('images/visamastercard.png') !!}</a></li>
										  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">{!! Html::image('images/paypal.png') !!}</a></li>
										  <li class="last"><a href="#tab_3" data-toggle="tab" aria-expanded="false">{!! Html::image('images/bank-transper.png') !!}</a></li>
										</ul>
										  <div class="tab-content">
											<div class="tab-pane active" id="tab_1">
												{!! Form::open( ['route'=> 'general.cart.payout.p', 'method'=>'POST']); !!}
													<div class="row">
													
														<div class="col-xs-6 border-right card-type">
															<div class="form-group">
																<label for="exampleInputEmail1">Card Type</label>
																<ul class="list-unstyled list-inline cards">
																	<li>{!! Html::image('images/visaicon.png') !!}</li>
																	<li>{!! Html::image('images/mastericon.png') !!}</li>
																	<li>{!! Html::image('images/maestro.png') !!}</li>
																</ul>
															</div>
																<div class="form-group">
																<label for="exampleInputEmail1">Card Number</label>
																{!! Form::text('card-number', Input::old('card-number'), ['class' => 'form-control', 'id' => 'cnumber', 'required'=>'']) !!}
															</div>
														</div>
														<div class="col-xs-6 card-info">
															<div class="form-inline exp">
													
																<div class="form-group">
																	<label>Expiration Date</label>
																	{!! Form::text('card-month', Input::old('card-month'), ['class' => 'form-control', 'id' => 'mm', 'required'=>'', 'placeholder'=>'MM' ]) !!}
																	{!! Form::text('card-year', Input::old('card-year'), ['class' => 'form-control', 'id' => 'yy', 'required'=>'', 'placeholder'=>'YY' ]) !!}
																</div>	

																<div class="form-group securitygroup">
																	<label for="">Security Code</label>
																	{!! Form::text('card-sec', Input::old('card-sec'), ['class' => 'form-control', 'id' => 'yy', 'required'=>'', 'placeholder'=>'Code' ]) !!}
																</div>
															</div>

															<div class="form-inline cardhold">
																<label>CardHolder Name</label>
																<div class="form-group">
																	{!! Form::text('fname', Input::old('fname'), ['class' => 'form-control', 'id' => 'yy', 'required'=>'', 'placeholder'=>'First Name' ]) !!}
																</div>	
																<div class="form-group">
																	{!! Form::text('lname', Input::old('lname'), ['class' => 'form-control', 'id' => 'yy', 'required'=>'', 'placeholder'=>'Last Name' ]) !!}
																</div>
															</div>
														</div>
													
													</div>
													<a href="#" class="paynow reddefault" onclick="$(this).closest('form').submit(); return false;">Pay Out</a>
												{!! Form::close() !!}
											</div>
											<div class="tab-pane " id="tab_2">
												<p><strong>Pay using your PayPal account.</strong></p>
												<p>You will be redirected to the PayPal system to complete the payment.</p>
												<p><strong>Please choose your option</strong></p>
												<div class="radio">
												  <label>
													<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
													Using your PayPal account (You need to log into PayPal)
												  </label>
												</div>	
												<div class="radio">
												  <label>
													<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
														Using your credit card (PayPal guest checkout)
												  </label>
												</div>
													{!! Form::open(['route'=> 'general.cart.payout.p','method'=>'POST']); !!}
														<a href="#" class="paynow reddefault" onclick="$(this).closest('form').submit(); return false;">Pay Out</a>
														<p>You will be notified regarding your order status via email and sms.</p>
													{!! Form::close() !!}
											</div>
											<div class="tab-pane " id="tab_3">
												{!! Form::open(array('route'=> 'general.cart.payout.p', 'class' => 'receipt-form', "files" => true,)) !!}
												<div class="form-group">
													<label class="bank-pay-msg">Bank or any money transfer receipt are subject for review by our support team.</label>
													<div class="file-input file-input-ajax-new">
														<div class="input-group ">
															<div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
															<div class="file-caption-name receipt-name" file=""></div>
															</div>

															<div class="input-group-btn">
																<button type="button" tabindex="500" title="Clear selected files" class="btn btn-default receipt-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
																<button type="button" tabindex="500" title="Abort ongoing upload" class="btn btn-default hide fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
																<div tabindex="500" class="btn btn-primary btn-file">
																	<i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse …
																	{!! Form::file('image', array('id' => 'receipt-file', 'name' => 'receipt-file')) !!}
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="receipt-preview"></div>

												<a href="#" class="paynow reddefault" onclick="$(this).closest('form').submit(); return false;">Submit</a>
											{!! Form::close() !!}
											</div>
										</div>
						
							
									</div>
									<div class="col-xs-5 totalorder">
										<div class="total_price_order">
											<span>Total Order</span>
											<h4>₱ {{ number_format($grand_total, 2) }}</h4>
										</div>
										<ul class="totalbreakdown list-unstyled">
											<li class="first">
												<p><strong>Order summary</strong></p>
											</li>
											
											@foreach($items as $item)
												<li>
													<span class="imgshop">
														<a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{!! Html::image('uploads/'.$item->productmodel->image, '' ) !!}</a>
													</span>
													<div class="summary-item">
														<p>{{ $item->qty }} x <a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{{ str_limit($item->name, 50) }}</a></p>
														<h5>₱ {{ number_format($item->subtotal, 2) }}</h5>
													</div>
												</li>
											@endforeach

										</ul>
										<div class="total">
											<p>Total</p>
											<h2>₱ {{ number_format($grand_total, 2) }}</h2>
										</div>
									</div>
							  </div>		
					</div><!--end check out-->	
			
		</div> <!-- end container -->
	</div> <!-- end main -->
</div><!--end body-->

@stop

