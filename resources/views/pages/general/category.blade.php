@extends('layouts.default')
@section('title', 'Category Page')
@section('content')

    <div class="body categorypage">
		<div class="container">
			
			<div class="row">
					<div class="col-xs-3 sidebar">
						<div class="boxlist primary">
			              <h4>Categories</h4>
			              <div class="sidenav">
			                <ul class="list-unstyled" id="sidenav">
			             		@include('includes.sidebar-shopper')
			                </ul> <!-- ul sidenav -->
			              </div> <!-- div sidenav -->
			            </div> <!--end boxlist-->
					</div><!--end sidebar-->

					<div class="col-xs-9 content">

						<!--start Product-->	





							@if(isset($cat) && count($cat) > 0)
								<div class="prodcat prodsec">
									<h4>Product Categories</h4>
									@foreach(array_chunk($cat->getCollection()->all(), 4) as $cats)
										<div class="row">
											@foreach($cats as $cat)
												<div class="col-xs-3">
											 		<div class="imgview">
											 			<a href="{{ URL::to('category', $cat['slug']) }}">
															{!! Html::image('uploads/'. $cat['image'], '') !!}
														</a>
													</div>
													<div class="catname">
														<p><a href="{{ URL::to('category', $cat['slug']) }}">{{ $cat['name']}}</a></p>
													</div>
												</div>
											@endforeach
										</div>
									@endforeach
								</div>
							@else 
								
							@endif

						<!--end Product-->

	
					</div><!--end content-->
		
			</div><!--end product-group-->
		</div> <!-- end container -->
	</div><!--end body-->
@stop

