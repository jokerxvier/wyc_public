@extends('layouts.default')
@section('title', 'Wish List Page')
@section('content')
	<div class="main">
		<div class="container">
			<div class="title">
				<h3>Wishlist</h3>
			</div>

			<div class="tablelist row-eq-height">
				<div class="tablecontent" id="wishlist">
					{!! csrf_field() !!}
					<table id="tabledata-wishlist" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
								<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Image</th>
								<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="" aria-sort="ascending">Name/Details</th>
								<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Qty</th>
								<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Price</th>
								<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Subtotal</th>
								<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Action</th>
								
							</tr>
						</thead>
						<tbody>
							@if(isset($wishlist))
								@foreach($wishlist as $item)
									<tr role="row" class="odd">
										<td width="62">
											<a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">
												{!! Html::image('uploads/'. $item->productmodel->image, $item->name, ['class' => 'thumb']) !!}
											</a>
										</td>
										<td>
											<h5><a href="{{ URL::to('product/detail', $item->productmodel->slug) }}">{{ str_limit($item->name, 45, '...') }}</a></h5>
											<span class="articleid">
												<ul class="list-unstyled list-inline wish-options">
													@foreach($item->options as $option)
														<li>{{ $option['name'] }} |</li> 
													@endforeach
													<li>{{ $item->productmodel->stock > 0 ? 'In Stock' : 'Out of Stock' }} |</li> 
													<li>Seller: <a href="{{URL::route('general.store.detail', $item->productmodel->store->slug)}}">{{ $item->productmodel->store->name }}</a></li> 
												</ul>
											</span>
											<button class="btn bg-maroon addcartbtn wish-to-cart {{ $item->productmodel->stock > 0 ? '' : 'disabled' }}" row-id="{{ $item->rowid }}">Add to Cart</button>
										</td>
										<td>x{{ $item->qty }}</td>
										<td class="pricewhist">₱ {{ number_format($item->price, 2) }}</td>
										<td class="pricewhist">₱ {{ number_format($item->subtotal, 2) }}</td>
										<td class="delete">
											<a href="#" row-id="{{ $item->rowid }}" class="wish-item-del"><i class="fa fa-times-circle"></i> Delete</a>
										</td>
										
									</tr>
								@endforeach
							@endif
						</tbody>  
					</table>
				</div>

				<div class="vendorsidebar" id="wishlistsidebar">
					<h3>My Account</h3>
					<div class="recentreports">
						<a href="{{ URL::route('general.wishlist') }}" class="active">My Wishlist <i class="fa fa-angle-right pull-right"></i></a>
						@if(Auth::check())
							<a href="{{ URL::route('user.orders') }}" class="">Orders <i class="fa fa-angle-right pull-right"></i></a>
							<a href="{{ URL::route('user.profile') }}" class="">Profile Details <i class="fa fa-angle-right pull-right"></i></a>
							@if(Auth::user()->type == 'shopper')
								<a href="{{ URL::route('user.store.new')}}">Open Store <i class="fa fa-angle-right pull-right"></i></a>
							@endif
						@endif
					</div>
				</div>
			</div>
		</div><!--end container-->
	</div><!--end main-->

@stop
