<!-- 
	expirement reusable universal file uploader
	an adhoc to form upload since we cannot nest Form elem
	the js driver is located at _DefaultGlobal.js
-->
{!! Form::open(["files" => true]) !!}
	<div class="form-group">
		<div class="file-input file-input-ajax-new">
			<div class="input-group ">
				<div tabindex="500" class="form-control file-caption kv-fileinput-caption">

					@if(isset($trigger_id))
						@if(Session::has($trigger_id))
							<div class="file-caption-name">{{Session::get($trigger_id)}}</div>
						@else
							<div class="file-caption-name">{{isset($default) ? $default : ''}}</div>
						@endif
					@endif

				</div>

				<div class="input-group-btn">
					<button type="button" title="Clear selected files" class="btn btn-default fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
					<div tabindex="500" class="btn btn-primary btn-file"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse …
						{!! Form::file('file', ['id' => isset($trigger_id) ? $trigger_id : '', 'name' => 'file']) !!}
						
						{!!Form::hidden('type', isset($type) ? $type : '')!!}
						{!!Form::hidden('style', isset($style) ? $style : '')!!}
						{!!Form::hidden('link', isset($trigger_id) ? $trigger_id : '')!!}

						@if(isset($action))
							@foreach($action as $value)
								{!!Form::hidden('action[]', $value)!!}
							@endforeach
						@endif

					</div>
				</div>
			</div>
		</div>
		<small class="upload-note">{{isset($note) ? $note : ''}}</small>
	</div>

	@if(isset($trigger_id))
		@if(Session::has($trigger_id) && Session::get($trigger_id) != '')
			<div class="upload-preview">
				@if(isset($preview) && $preview == '0')
					<a href="{{asset('uploads/'.Session::get($trigger_id) )}}">Preview/Download File</a>
				@else
					{!!Html::image('uploads/'.Session::get($trigger_id), '', ['style' => isset($style) ? $style : ''])!!}
				@endif
			</div>
		@else
			<div class="upload-preview">
				@if(isset($default) && $default != '')
					@if(isset($preview) && $preview == '0')
						<a href="{{asset('uploads/'.$default )}}">Preview/Download File</a>
					@else
						{!!Html::image('uploads/'.$default, '', ['style' => isset($style) ? $style : ''])!!}
					@endif
				@endif
				
			</div>
		@endif
	@endif
	
{!! Form::close() !!}