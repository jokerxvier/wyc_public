@extends('layouts.default')
@section('title', 'Upgrade')
@section('content')
	<div class="main">
		<div class="container">
			<div class="title">
				<h3>Open a Store</h3>
			</div>

			<div class="tablelist row-eq-height">
				<div class="tablecontent">

					<div class="row profile-row bottom">	

						@if($errors->any())
              <div class="alert alert-danger"> <!-- php triggered -->
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @else

            	@if($store['status'] == 'Pending')
            		<div class="alert alert-success"> <!-- js triggered -->
		              <ul>
		              	<li>
		              		Application Submitted! We will notify you via email once it is approved.
		              	</li>
		            	</ul>
		            </div>
								<!-- <label class="store-pending"></label> -->
							@else

	            	<div class="alert alert-danger {{($store['status'] == 'Declined' ? '' : 'hidden')}}"> <!-- js triggered -->
		              <ul>
		              	@if($store['status'] == 'Declined')
		              		<li>
		              			<b>Your Store Creation Request has been declined!</b>
		              			<br> REASON: {{$store['decline_reason']}}
		              		</li>
		              	@endif
		              </ul>
		            </div>
		           @endif
            @endif

						<h4>Store Information</h4>

							{!! Form::open(['route' => 'general.store.save.p', 'method' => 'POST', 'id' => 'store-form']) !!}
							<div class="col-xs-6">
								<div class="form-group">
									{!! Form::text('name', $store['name'], ['class' => 'form-control', 'placeholder' => 'Enter Store Name', 'id' => 'store-name', 'required' => '']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('address', $store['address'], ['class' => 'form-control', 'placeholder' => 'Enter Store Address', 'id' => 'store-address', 'required' => '']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('phone', $store['phone'], ['class' => 'form-control', 'placeholder' => 'Enter Store Phone Number', 'id' => 'store-phone', 'data-mask' => '','data-inputmask' => "'mask': '(999) 999-9999'", 'required' => '']) !!}
								</div>
							</div>
							<div class="col-xs-6">
								 <div class="form-group">
								 	{!!Form::textarea('desc', $store['desc'], ['class' => 'form-control', 'id' => 'store-desc', 'placeholder' => 'Enter Store Description', 'required' => ''])!!}
								</div>
							</div>
							<div class="col-xs-12">
							  <a class="popup-modal btn btn-default btn-flat btn-sm" href="#pps-category-choose" id="pps-category">
	                Add Category <i class="fa fa-plus"></i>
	              </a>
	              <ul class="list">
	                @if(Session::has('pps-category'))
	                  @foreach(Session::get('pps-category') as $id)
	                    <?php $category = $categories->where('id', (int)$id)->first() ?>
	                    <li class="option" pps-id="{{$category->id}}">
	                      {{$category->name}}<i class="fa fa-times pps-del"></i>
	                      <input type="hidden" name="pps-category[]" value="{{$category->id}}">
	                    </li>
	                  @endforeach
	                @else
	                  @foreach($store_cat as $id => $name)
	                    <li class="option" pps-id="{{$id}}">
	                      {{$name}}<i class="fa fa-times pps-del"></i>
	                      <input type="hidden" name="pps-category[]" value="{{$id}}">
	                    </li>
	                  @endforeach
	                @endif
	              </ul>

							</div>

							{!!Form::hidden('store-id', $store['id'], ['id' => 'store-id'])!!}

							@if(Session::has('file-logo'))
	              {!!Form::hidden('file-logo', Session::get('file-logo'), ['id' => 'file-logo'])!!}
	            @else
	              {!!Form::hidden('file-logo', $store['logo'], ['id' => 'file-logo'])!!}
	            @endif

	            @if(Session::has('file-banner'))
	              {!!Form::hidden('file-banner', Session::get('file-banner'), ['id' => 'file-banner'])!!}
	            @else
	              {!!Form::hidden('file-banner', $store['banner'], ['id' => 'file-banner'])!!}
	            @endif

	            @if(Session::has('file-nbi'))
	              {!!Form::hidden('file-nbi', Session::get('file-nbi'), ['id' => 'file-nbi'])!!}
	            @else
	              {!!Form::hidden('file-nbi', $store['doc_nbi'], ['id' => 'file-nbi'])!!}
	            @endif

							@if(Session::has('file-valid-id'))
	              {!!Form::hidden('file-valid-id', Session::get('file-valid-id'), ['id' => 'file-valid-id'])!!}
	            @else
	              {!!Form::hidden('file-valid-id', $store['doc_valid_id'], ['id' => 'file-valid-id'])!!}
	            @endif

						{!!Form::close()!!}

						<div class="col-xs-6">
							<h4>Store Logo</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-logo',
                'type' => 'images',
                'style' => 'width: inherit',
                'action' => ['fit', 40, 40],
                'note' => 'Preferred size 40 x 40.',
                'default' => $store['logo']
              ])
						</div>

						<div class="col-xs-6">
							<h4>Store Banner</h4>
							@include('pages.general.etc.upload-form', [
                'trigger_id' => 'file-banner',
                'type' => 'images',
                'style' => 'width: 70%',
                'action' => ['fit', 847, 277],
                'note' => 'Preferred size 847 x 277.',
                'default' => $store['banner']
              ])
						</div>
					</div>
					
					<div class="row profile-row bottom">	
						<h4>Required Documents</h4>
						<div class="box-body docs-personal">
		        	<div class="col-xs-6">
								<h4>NBI Clearance</h4>
								@include('pages.general.etc.upload-form', [
	                'trigger_id' => 'file-nbi',
	                'type' => 'any',
	                'style' => 'width: 70%',
	                'action' => ['move', 0, 0],
	                'note' => '',
	                'default' => $store['doc_nbi'],
	                'preview' => '0'
	              ])
							</div>

							<div class="col-xs-6">
								<h4>Valid Identification Card</h4>
								@include('pages.general.etc.upload-form', [
	                'trigger_id' => 'file-valid-id',
	                'type' => 'any',
	                'style' => 'width: 70%',
	                'action' => ['move', 0, 0],
	                'default' => $store['doc_valid_id'],
	                'preview' => '0'
	              ])
							</div>
		        </div>
					</div>

					<button type="submit" class="btn btn-info" id="store-submit">Save</button>

				</div>

				<div class="vendorsidebar" id="wishlistsidebar">
					<h3>My Account</h3>
					<div class="recentreports">
						<a href="{{ URL::route('general.wishlist') }}">My Wishlist <i class="fa fa-angle-right pull-right"></i></a>
						<a href="{{ URL::route('user.orders') }}" class="">Orders <i class="fa fa-angle-right pull-right"></i></a>
						<a href="{{ URL::route('user.profile') }}">Profile Details <i class="fa fa-angle-right pull-right"></i></a>
						<a href="{{ URL::route('user.store.new')}}" class="active">Open Store <i class="fa fa-angle-right pull-right"></i></a>
					</div>
				</div>
			</div>
		</div><!--end container-->
	</div><!--end main-->

	@include('pages.vendor.modal._category')
@stop
