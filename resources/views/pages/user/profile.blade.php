@extends('layouts.default')
@section('title', 'Profile Page')
@section('content')
	<div class="main">
		<div class="container">
			<div class="title">
				<h3>Profile Details</h3>
			</div>

			<div class="tablelist row-eq-height">
				<div class="tablecontent">

					{!! Form::open(['route' => 'user.profile.p', 'method' => 'POST']) !!}
						<div class="row profile-row bottom">	
							<h4>User account information</h4>

							@if(Session::has('status'))
								<div class="alert alert-success" style="display:block;">
	                <ul><li>Profile detail has been saved.</li></ul>
	              </div>
            	@endif

							<div class="col-xs-6">
								<div class="form-group">
									<input type="email" class="form-control" id="email" placeholder="Email" value="{{ $user->email }}" disabled>
								</div>
								<div class="form-group">
									{!! Form::text('fname', $user->fname, ['class' => 'form-control', 'placeholder' => 'First Name', 'required'=>'']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('lname', $user->lname, ['class' => 'form-control', 'placeholder' => 'Last Name', 'required'=>'']) !!}
								</div>
							</div>

							<div class="col-xs-6">
								<div class="form-group">
									{!! 
	                  Form::select('gender', 
	                    ['Male' => 'Male', 'Female' => 'Female'], $user->gender,
	                    ['id' => 'gender', 'class' => 'form-control'])
	                !!}
								</div>
								<div class="form-group">
									{!! Form::text('phone', $user->phone, ['id' => 'phone', 'class' => 'form-control', 'data-mask' => '','data-inputmask' => "'mask': '+639999999999'", 'placeholder'=>'Mobile']) !!}
								</div>
								<div class="form-group">
									<!-- <input type="text" name="landline" class="form-control" id="landline" placeholder="Landline" value="{{ $user->landline }}"> -->
									<div class="input-group">
	                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
	                  {!! Form::text('birthdate', $user->birthdate, ['id' => 'birthdate', 'class' => 'form-control', 'data-mask' => '','data-inputmask' => "'alias': 'mm/dd/yyyy'", 'placeholder'=>'Birth Date']) !!}
	                </div>
								</div>
							</div>

							<a class="popup-modal" href="#change-password" id="change-pass-link">Change Password</a>
							
							<button class="btn btn-info" type="submit" id="save-profile" user_id="{{ $user->user_id }}">Save Profile</button>
							<a class="btn btn-default popup-modal" href="#new-address" id="address-add">Add New Address</a>
							<!-- <a class="popup-with-form btn btn-default" href="#new-address" id="add-new-address">Add New Address</a> -->
						</div>
					{!!Form::close()!!}

					<div class="row profile-row">
						<table id="tbl-user-address" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
							<thead>
							  <tr role="row">
									<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Address Book</th>
									<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="" aria-sort="ascending">Type</th>
									<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="" style="">Action</th>									
								</tr>
							</thead>
							<tbody>	
								@if(isset($addresses) && count($addresses) > 0)
									@foreach($addresses as $addr)
										<tr role="row" id="addr-{{$addr->id}}">
											<td>{{$addr->street .', '. $addr->city .', '. $addr->region .', '. strtoupper($addr->country) .' '. $addr->zip }}</td>
											<td>{{$addr->type}}</td>
											<td class="delete">
												<a href="#" class="address-edit" addr_id="{{$addr->id}}"><i class="fa fa-pencil"></i> Edit</a> | 
												<a href="#" class="address-del" addr_id="{{$addr->id}}"><i class="fa fa-times-circle"></i> Delete</a>
											</td>
										</tr>
									@endforeach
								@else
									<tr role="row" class="no-record">
										<td>No records found.</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								@endif
							</tbody>  
						</table>
					</div>
				</div>

				<div class="vendorsidebar" id="wishlistsidebar">
					<h3>My Account</h3>
					<div class="recentreports">
						<a href="{{ URL::route('general.wishlist') }}">My Wishlist <i class="fa fa-angle-right pull-right"></i></a>
						<a href="{{ URL::route('user.orders') }}" class="">Orders <i class="fa fa-angle-right pull-right"></i></a>
						<a href="{{ URL::route('user.profile') }}" class="active">Profile Details <i class="fa fa-angle-right pull-right"></i></a>
						@if($user->type == 'shopper')
							<a href="{{ URL::route('user.store.new')}}">Open Store <i class="fa fa-angle-right pull-right"></i></a>
						@endif
					</div>
				</div>
			</div>
		</div><!--end container-->
	</div><!--end main-->



	<div class="box box-info mfp-hide" id="change-password">
    <div class="box-header with-border">
      <h3 class="box-title">Change Password</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="col-xs-12">
      	<small class="cp-msg">this is a test message</small>
				<div class="form-group">
					<h5>Old Password:</h5>
					<input type="password" class="form-control" id="cp-old-pass" placeholder="Enter your Old Password">
				</div>
				<div class="form-group">
					<h5>New Password:</h5>
					<input type="password" class="form-control" id="cp-pass1" placeholder="Enter your New Password">
				</div>
				<div class="form-group">
					<h5>Repeat Password:</h5>
					<input type="password" class="form-control" id="cp-pass2" placeholder="Repeat your New Password">
				</div>
			</div>

    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad" id="btn-change-pass" user_id="{{$user->id}}">Submit</a>
    </div>
  </div>

	<div class="box box-info mfp-hide" id="new-address">
    <div class="box-header with-border">
      <h3 class="box-title">Add New Address</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool popup-modal-dismiss" ><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<div class="col-md-12">
	    	<p>Please provide the accurate address to ensure that your package will be recieved.</p>
	    	<div class="alert alert-danger hidden">
          <ul></ul>
        </div>

	      <div class="col-xs-6">
					<div class="form-group">
						<input type="text" class="form-control" id="street" placeholder="Street Address">
					</div>
					<div class="form-group">
						<select id="country" name="country" class="form-control">
						  <!-- <option value="">Select Country</option> -->
						  @if(isset($country))
						  	@foreach($country as $c)
						  		<option value="{{ $c->code }}">{{$c->country_name}}</option>
						  	@endforeach
						  @endif
						</select>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="zip" placeholder="Postal Code">
					</div>
				</div>

				<div class="col-xs-6">
					<div class="form-group">
						{!! Form::select('type', $types, '', ['id' => 'type', 'class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						<select id="region" name="region" class="form-control">
						  <option value="">Select Region</option>
						  @if(isset($regions))
						  	@foreach($regions as $region)
						  		<option value="{{ $region->id }}">{{$region->region_name}}</option>
						  	@endforeach
						  @endif
						</select>
					</div>
					<div class="form-group">
						<select id="city" name="city" class="form-control">
						  <option value="">Select City/Municipality</option>
						  @if(isset($cities))
						  	@foreach($cities as $city)
						  		<option value="{{ $city->id }}" class="{{ $city->region_id }}">{{$city->city_name}}</option>
						  	@endforeach
						  @endif
						</select>
					</div>
				</div>
			</div>
    </div>
    <div class="box-footer clearfix">
    	<a href="#" class="btn btn-sm btn-default pull-right popup-modal-dismiss">Cancel</a> 
      <a href="#" class="btn btn-sm btn-info pull-right btn-left-pad" id="save-address" user_id="{{$user->id}}" useraddr_id="">Save</a>
    </div>
  </div>

@stop
