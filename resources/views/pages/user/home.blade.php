@extends('layouts.default')
@section('title', 'Home Page')
@section('content')
  <div class="body">
    <div class="main">
      <div class="container">
        <section class="mainsection">
            <div class="row">
              <div class="col-xs-3 sidebar ">
                <div class="boxlist primary">
                  <h4>Categories</h4>
                  <div class="sidenav">
                    <ul class="list-unstyled" id="sidenav">
                       @include('includes.sidebar-shopper')
                    </ul> <!-- ul sidenav -->
                  </div> <!-- div sidenav -->
                </div> <!--end boxlist-->
              </div><!--end sidebar-->
              
              <div class="col-xs-9">
                <div class="showcase">
                  <div class="featured">
                    <ul class="slides list-unstyled">
                      @foreach($ads_main_slide as $ad)
                        <li><img src="{{'uploads/'. $ad->image }}" width="457" height="518" /></li>
                      @endforeach
                    </ul>
                    <div class="carousel">
                    </div>
                  </div>

                  <div class="latest">
                    <h3>Latest Product</h3>
                    @foreach($ads_latest_prod as $ad)
                      <img src="{{'uploads/'. $ad->image }}" width="195" height="152" />
                    @endforeach
                  </div>

                  <div class="promoproduct">
                    <div class=" blockstyle">
                        <div class="blocktitle">
                          <h3>Newsletter</h3>
                        </div>
                        <div class="newsletterform">
                          <span>Nemo enim ipsam voluptat voluptas sit aspernatur</span>
                          <form>
                            <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                            </div>
                            <button type="submit" class="btn btn-default">Subscribe</button>
                          </form>
                        </div>
                    </div>
                    @foreach($ads_feature_shop as $ad)
                      <img src="{{'uploads/'. $ad->image }}" width="195" height="300" />
                    @endforeach
                  </div>
                </div>
                
              </div>
            </div>
        </section><!--end section-->

        @if(isset($sections) && count($sections) > 0)
          @foreach($sections as $section)
            <section >
              <div class="row">
                <div class="col-xs-3  ">
                  <div class="sidemenu">
                    <div class="inner">
                      <div class="title">
                        <h5>{{ $section['root_cat']['name'] }}</h5>
                        <span>Recently Popular</span>
                      </div>
                      <div class="widget">
                        <ul class="links">
                          @foreach($section['popular_cats'] as $cat)
                            <li><a href="{{ URL::to('category',  $cat['slug']) }}">{{ $cat['name'] }}</a></li>
                          @endforeach
                      </div>
                    </div>
                    <div class="shoplist">
                      <ul>
                        @foreach($section['stores'] as $store)
                          <li>
                            <a href="{{ URL::to('store', $store->slug) }}" class="directlink">
                              <img class="lazy" data-original="{{'images/'. $store->logo }}" width="41" height="41" />
                              <span>
                                <h4>{{ $store->name }}</h4>
                                <small>{{ $store->desc }}</small>
                              </span>
                            </a>
                          </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="col-xs-9">
                  <div class="product_post">
                      <ul class="slideshow slideshow01 list-unstyled" >
                        @foreach($section['banner_ads'] as $ad)
                          <li><img class="lazy" data-original="{{'images/'. $ad->image }}" width="652" height="296" /></li>
                        @endforeach
                      </ul>
                      <div class="categoryprod">
                        <div class="centerwrap">
                          <ul class="list-unstyled">
                            @foreach($section['brands'] as $brand)
                              <li><a href="{{URL::to('brand', $brand->slug)}}"><img class="lazy" data-original="{{'images/'. $brand->home_slide_img }}" /></a></li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                      <div class="othercategory hovereffect">
                        @foreach($section['sub_cats'] as $cat)
                          <div class="boxcell">
                            <a href="{{ URL::to('category',  $cat['slug']) }}">
                              <img class="lazy" data-original="{{'images/'. $cat['home_img'] }}" width="163" height="106" />
                              <span>{{ $cat['name'] }}</span>
                            </a>
                          </div>
                        @endforeach
                      </div>
                  </div>

                  @if(isset($section['sale_ads']))
                    <div class="promoproduct">
                      <div class="showblock blockstyle">
                          <div class="blocktitle">
                            <h3>Sale Item</h3>
                            <span>Enjoy up to 50% off</span>
                          </div>
                          <div class="prodlist">
                            @foreach($section['sale_ads'] as $ad)
                              <a href="#"><img class="lazy" data-original="{{'images/'. $ad->image }}" width="180" height="200" /></a>
                            @endforeach
                          </div>
                      </div>
                    </div>
                  @endif

                  @if(isset($section['best_seller']))
                    <div class="promoproduct bestseller">
                      <div class="showblock blockstyle ">
                          <div class="blocktitle">
                            <h3>Best seller</h3>
                          </div>
                          <div class="prodlist ">
                            @foreach($section['best_seller'] as $prod)
                              @if($prod->discount > 0) 
                                <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                  <div class="entry discount">
                                    <div class="discount_tag">
                                      <h4>{{ $prod->discount }}<span>% OFF</span></h4>
                                    </div>
                                    <div class="desc">
                                      <span>{{ $prod->brand_name }}</span>
                                      <h5>{{ $prod->name }}</h5>
                                      <small class="prevprice">{{$prod->currency}} {{ number_format($prod->price, 2) }}</small>
                                      <p><strong>{{$prod->currency}} {{ number_format($prod->sale_price, 2) }}</strong></p>
                                    </div>
                                    <div class="showimage">
                                      <img class="lazy" data-original="{{'images/'. $prod->image }}" />
                                    </div>
                                  </div>
                                </a>
                              @else
                                <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                <div class="entry">
                                    <div class="desc">
                                      <span>{{ $prod->brand_name }}</span>
                                      <h5>{{ $prod->name }}</h5>
                                      <p><strong>{{$prod->currency}} {{ number_format($prod->price, 2) }}</strong></p>
                                    </div>
                                    <div class="showimage">
                                      <img class="lazy" data-original="{{'images/'. $prod->image }}" />
                                    </div>
                                  </div>
                                </a>
                              @endif
                            @endforeach
                          </div>
                      </div>
                    </div>
                  @endif

                  @if(isset($section['good_stores']))
                    <div class="promoproduct">
                      <div class="showblock blockstyle">
                          <div class="blocktitle">
                            <h3>Find a good shop</h3>
                          </div>
                          <div class=" blocklist">
                            <ul class="list-unstyled">
                              @foreach($section['good_stores'] as $store)
                                <li>
                                  <a href="{{ URL::to('store', $store->slug) }}" class="directlink">
                                    <div class="icon">
                                      <img class="lazy" data-original="{{'images/'. $store->logo }}" width="41" height="41" />
                                    </div>
                                    <div class="desc">
                                      <h4>{{str_limit($store->name, $limit = 12, $end = '...')}}</h4>
                                      <small>{{str_limit($store->desc, $limit = 17, $end = '...')}}</small>
                                    </div>
                                  </a>
                                </li>
                              @endforeach
                            </ul>
                          </div>
                      </div>
                    </div>
                  @endif

                  @if(isset($section['recommended_products']))

                    <div class="promoproduct bestseller">
                      <div class="showblock blockstyle ">
                          <div class="blocktitle">
                            <h3>Recommended</h3>
                          </div>
                          <div class="prodlist ">
                            @foreach($section['recommended_products'] as $prod)
                              @if($prod->discount > 0) 
                                <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                  <div class="entry discount">
                                    <div class="discount_tag">
                                      <h4>{{ $prod->discount }}<span>% OFF</span></h4>
                                    </div>
                                    <div class="desc">
                                      <span>{{ $prod->brand_name }}</span>
                                      <h5>{{ $prod->name }}</h5>
                                      <small class="prevprice">{{$prod->currency}} {{ number_format($prod->price, 2) }}</small>
                                      <p><strong>{{$prod->currency}} {{ number_format($prod->sale_price, 2) }}</strong></p>
                                    </div>
                                    <div class="showimage">
                                      <img class="lazy" data-original="{{'images/'. $prod->image }}" />
                                    </div>
                                  </div>
                                </a>
                              @else
                                <a href="{{ URL::to('product/detail',  $prod->slug)  }}">
                                  <div class="entry">
                                    <div class="desc">
                                      <span>{{ $prod->brand_name }}</span>
                                      <h5>{{ $prod->name }}</h5>
                                      <p><strong>{{$prod->currency}} {{ number_format($prod->price, 2) }}</strong></p>
                                    </div>
                                    <div class="showimage">
                                      <img class="lazy" data-original="{{'images/'. $prod->image }}" />
                                    </div>
                                  </div>
                                </a>
                              @endif
                            @endforeach
                          </div>
                      </div>
                    </div>
                  @endif 

                  @if(isset($section['recommended_ads']))
                    <div class="promoproduct bestseller">
                      <div class="showblock blockstyle ">
                          <div class="blocktitle">
                            <h3>Recommended</h3>
                          </div>
                          <div class="prodlist ">
                            <div class="prodlist">
                            @foreach($section['recommended_ads'] as $ad)
                              <a href="#"><img class="lazy" data-original="{{'images/'. $ad->image }}" width="180" height="200" /></a>
                            @endforeach
                            </div>
                          </div>
                      </div>
                    </div>
                  @endif

                </div>
              </div>
            </section><!--end section-->
          @endforeach
        @endif
      </div>
    </div>
  </div><!--end body-->
@stop