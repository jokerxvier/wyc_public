@extends('layouts.default')
@section('title', 'OR'.$order->mask.' Detail')
@section('content')

<div class="body">
  <div class="main">
		<div class="container">
			
			<div class="checkout">
					<h3 class="title">OR{{ $order->mask }} Details</h3>
			</div><!--end check out-->	

			<div class="shopping_cart">
					<table class="table confirm-list">
						<tr>
							<th colspan="2">Product Name &amp; Details </th>
							<th>Quantity</th>
							<th>Price</th>
							<th class="th-subtotal">Subtotal</th>
						</tr>

						@if(isset($items) && count($items) > 0)
							@foreach($items as $item)
								<tr>
									<td class="imgshop">
										<a href="{{ URL::route('user.order.detail', $item->product->slug) }}">{!! Html::image('uploads/'.$item->product->image, '' ) !!}</a>
									</td>
									<td class="shortdesc">
										<h4><a href="{{ URL::route('user.order.detail', $item->product->slug) }}">{{ str_limit($item->product->name, 50, '...') }}</a></h4>
										{{$item->variants}}
									</td>
									<td class="qty" width="130">
										<h4>x {{ $item->quantity }}</h4>
									</td>
									<td class="checkprice" width="130">
										@if($item->product->discount > 0 && $item->product->sale_price == $item->price_per_item)
											<span class="disc_tag">{{ $item->product->discount }}% <br/>OFF</span>
											<span>
												<em>{{ number_format($item->product->price, 2) }}</em><br/>
												₱ {{ number_format($item->product->price_sale, 2) }}
											</span>
										@else
											₱ {{ number_format($item->price_per_item, 2) }}
										@endif
									</td>
									<td class="subtotal">
										₱ {{ number_format($item->price_per_item * $item->quantity, 2) }}
									</td>
								</tr>
							@endforeach
						@endif

					</table>

					<div class="subtotalbreakdown confirm">
						<table>
							<tr>
								<td class="breakdownsub" width="190">SubTotal</td>
								<td class="total">₱ {{ number_format($order->total, 2) }}</td>
							</tr>
							<tr>
								<td class="breakdownsub" width="190">Shipping Cost</td>
								<td clas="shipping">₱ {{ number_format($order->ship_fee, 2) }}</td>
							</tr>
							<tr>
								<td class="breakdownsub" width="190">Total</td>
								<td class="totalbreakdown">₱ {{ number_format($order->total, 2) }}<br/><small>VAT incl.</small></td>
							</tr>
						</table>
					</div>
					
				</div> <!-- end shopping_cart -->

		</div> <!-- end container -->
	</div> <!-- end main -->
</div><!--end body-->

@stop

