@extends('layouts.default')
@section('title', 'Order List Page')
@section('content')
	<div class="main">
		<div class="container">
			<div class="title">
				<h3>Orders</h3>
			</div>

			<div class="tablelist row-eq-height">
				<div class="tablecontent" id="wishlist">
					{!! csrf_field() !!}
					
					<table id="dtbl-orders" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
						  	<th>Store</th>
								<th>Order</th>
								<th>Total</th>
								<th>Status</th>
								<th>Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>  
					</table>

				</div>

				<div class="vendorsidebar" id="wishlistsidebar">
					<h3>My Account</h3>
					<div class="recentreports">
						<a href="{{ URL::route('general.wishlist') }}" class="">My Wishlist <i class="fa fa-angle-right pull-right"></i></a>
						@if(Auth::check())
							<a href="{{ URL::route('user.orders') }}" class="active">Orders <i class="fa fa-angle-right pull-right"></i></a>
							<a href="{{ URL::route('user.profile') }}" class="">Profile Details <i class="fa fa-angle-right pull-right"></i></a>
							@if(Auth::user()->type == 'shopper')
								<a href="{{ URL::route('user.store.new')}}">Open Store <i class="fa fa-angle-right pull-right"></i></a>
							@endif
						@endif
					</div>
				</div>
			</div>
		</div><!--end container-->
	</div><!--end main-->

@stop
