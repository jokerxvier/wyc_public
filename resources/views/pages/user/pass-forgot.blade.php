@extends('layouts.default')
@section('title', 'Forgot Password')
@section('content')
  <div class="body">
    <div class="container">
      <div class="col-xs-7">
        <div class="loginslides">
          <ul>
            <li>
              <div class="illustrate">
                {!! Html::image('images/iphone.png', '') !!}
              </div>
              <div class="logindesc">
                <p><strong>iPhone 6 &amp; 6+</strong><p>
                <h3>Upgrade to iphone</h3>
                <span><small>It’s easy to make the switch</small></span>
                <a href="#" class="shop">Shop Now</a>
              </div>
            </li>
            <li>
              <div class="illustrate">
                {!! Html::image('images/iphone.png', '') !!}
              </div>
              <div class="logindesc">
                <p><strong>iPhone 6 &amp; 6+</strong><p>
                <h3>Upgrade to iphone</h3>
                <span><small>It’s easy to make the switch</small></span>
                <a href="#" class="shop">Shop Now</a>
              </div>
            </li>
          </ul>
        </div>
      </div>  
      <div class="col-xs-5">
        <div class="loginform pull-right">
          <h4>Password Reset</h4>

          
          <!-- <form method="POST" action="/login"> -->
          {!! Form::open(['route'=> 'pass.forgot.p', 'id'=>'su-post', 'method'=>'POST']); !!}
            @if(Session::has('status'))
              <div class="alert alert-success">
                <ul><li>Pasword reset link has been sent.</li></ul>
              </div>
            @endif
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif

            <div class="input-group">
              <span class="input-group-addon"><span class="icon-logiconemail19"></span></span>
              {!! Form::text('email', '', ['class' => 'form-control text', 'placeholder' => 'Email', 'required'=>'', 'autofocus'=>'']) !!}
              <!-- <input type="email" class="" placeholder=""> -->
            </div>

            <button class="btn bg-maroon btn-flat margin" type="submit">Send Reset Link</button>
            <p>Not a member? <a href="{{ URL::route('user.signup') }}">Sign up now</a></p>
          {!! Form::close() !!}
        </div>
      </div>
    </div><!--end container-->
  </div><!--end body-->
@stop