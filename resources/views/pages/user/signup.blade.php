@extends('layouts.default')
@section('title', 'Sign Up Page')
@section('content')
  <div class="body">
    <div class="container">
      <div class="row">
        <div class="col-xs-9  border-right">
            
            <h2 class="new-account">Create an Account</h2>
            
            <div class="su alert alert-danger" {{ (count($errors) > 0 ? 'style=display:block;' : '') }}>
              <ul>
                @if (count($errors) > 0)
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                @endif
              </ul>
            </div>

            {!! Form::open([ 'route' => 'user.signup.p', 'id'=>'su-post', 'method'=>'POST']); !!}
              
              <div class="tcontent" id="su-tab1">
              	<div class="row formreg">
                  <div class="col-xs-12">
                    <div class="info_form">

                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <h5>First Name</h5>
                            {!! Form::text('fname', '', ['class' => 'form-control', 'placeholder' => 'Enter your First Name', 'required'=>'']) !!}
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <div class="form-group">
                            <h5>Last Name</h5>
                            {!! Form::text('lname', '', ['class' => 'form-control', 'placeholder' => 'Enter your Last Name', 'required'=>'']) !!}
                          </div>
                        </div>
                      </div>

                      <div class="row">

                          <div class="col-xs-6">
                            <div class="form-group">
                              <h5>Gender</h5>
                              {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], '', ['id' => 'gender', 'class' => 'form-control']) !!}
                            </div>
                          </div>

                          <div class="col-xs-6">
                          <div class="form-group">
                            <h5>Birth Date</h5>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                              {!! Form::text('birthdate', '', ['id' => 'birthdate', 'class' => 'form-control', 'data-mask' => '', 'data-inputmask' => "'alias': 'mm/dd/yyyy'", 'placeholder'=>'Enter your Birth Date']) !!}
                            </div>
                          </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <h5>Mobile:</h5>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                              {!! Form::text('phone', '', ['id' => 'phone', 'class' => 'form-control', 'data-mask' => '', 'data-inputmask' => "'mask': '+639999999999'", 'placeholder'=>'Enter your Mobile', 'required'=>'']) !!}
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                              <h5>Email</h5>
                              {!! Form::email('email', '', ['id'=>'signup-email', 'class' => 'form-control text', 'placeholder' => 'Your Email', 'required'=>'', 'autofocus'=>'']) !!}
                            </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <h5>Password</h5>
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Please enter the password', 'required'=>''] ) !!}
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <div class="form-group">
                            <h5>Confirm Password</h5>
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Please re-enter the password', 'required'=>'']) !!}
                          </div>
                        </div>
                      </div>


                      

                    </div>
                  </div>
                </div>

              </div><!--end su-tab1-->
              
              <div class="tcontent" id="su-tab2x">
                <div class="row formreg">
                	<div class="col-xs-6">
                   <!-- <h5>Captcha</h5>
                    {!! Recaptcha::render() !!}
                    -->
                  </div>
                  <div class="col-xs-6">
                    <h5>Terms &amp; Conditions</h5>
                    <label class="check">
                      {!! Form::checkbox('terms', 1, null, ['class' => 'minimal', 'id'=>'signup-terms']) !!}
                      <span>Please read and accept our <a href="#">Terms & Condition</a>.</span>
                    </label>
                  </div>
                </div>
                <button class="btn btn-info btn-lg pull-right" id="new-account-submit">Submit</button>
              </div>

            {!! Form::close() !!}

        </div>  
        <div class="col-xs-3 sidesocial">
          <div class="socialsignup">

            <h5>Signing Up!</h5>
            You are just a few steps from registering your account.
            <br><br>
            It is very easy! Register yourself as a normal user and if you wish to be a retailer/vendor just click the 'Open Store' link in your profile page.

            <br><br>
            <h5>Signup with your</h5>
           
            <ul  class="list-unstyled">
              <li>
                <a href="#"><img src="images/fblink.png" alt=""/></a>
              </li>
              <li>
                <a href="#"><img src="images/googleplus.png" alt=""/></a>
              </li>
            </ul>

          </div>
        </div>
      </div>
    </div><!--end container-->
  </div><!--end body-->
@stop