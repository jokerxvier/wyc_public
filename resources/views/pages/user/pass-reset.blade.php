@extends('layouts.default')
@section('title', 'Password Reset')
@section('content')
  <div class="body">
    <div class="container">
      <div class="col-xs-7">
        <div class="loginslides">
          <ul>
            <li>
              <div class="illustrate">
                {!! Html::image('images/iphone.png', '') !!}
              </div>
              <div class="logindesc">
                <p><strong>iPhone 6 &amp; 6+</strong><p>
                <h3>Upgrade to iphone</h3>
                <span><small>It’s easy to make the switch</small></span>
                <a href="#" class="shop">Shop Now</a>
              </div>
            </li>
            <li>
              <div class="illustrate">
                {!! Html::image('images/iphone.png', '') !!}
              </div>
              <div class="logindesc">
                <p><strong>iPhone 6 &amp; 6+</strong><p>
                <h3>Upgrade to iphone</h3>
                <span><small>It’s easy to make the switch</small></span>
                <a href="#" class="shop">Shop Now</a>
              </div>
            </li>
          </ul>
        </div>
      </div>  
      <div class="col-xs-5">
        <div class="loginform pull-right">
          <h4>Password Reset</h4>

          
          <!-- <form method="POST" action="/login"> -->
          {!! Form::open(['route'=> ['pass.reset.p'], 'id'=>'su-post', 'method'=>'POST']); !!}
          
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="input-group">
              <span class="input-group-addon"><span class="icon-logiconemail19"></span></span>
              {!! Form::text('email', '', ['class' => 'form-control text', 'placeholder' => 'Email', 'required'=>'', 'autofocus'=>'']) !!}
            </div>
            <div class="input-group">
              <span class="input-group-addon"><span class="icon-logiconpassword19"></span></span>
              {!! Form::password('password', ['class' => 'form-control text', 'placeholder' => 'Password']) !!}
            </div>
            <div class="input-group">
              <span class="input-group-addon"><span class="icon-logiconpassword19"></span></span>
              {!! Form::password('password_confirmation', ['class' => 'form-control text', 'placeholder' => 'Repeat Password']) !!}
            </div>

            <button class="btn bg-maroon btn-flat margin" type="submit">Reset</button>
          {!! Form::close() !!}
        </div>
      </div>
    </div><!--end container-->
  </div><!--end body-->
@stop
