@extends('layouts.default')
@section('title', 'Login Page')
@section('content')
  <div class="body">
    <div class="container">
      <div class="col-xs-7">
        <div class="loginslides">
          <ul>
            <li>
              <div class="illustrate">
                <img src="images/iphone.png" alt=""/>
              </div>
              <div class="logindesc">
                <p><strong>iPhone 6 &amp; 6+</strong><p>
                <h3>Upgrade to iphone</h3>
                <span><small>It’s easy to make the switch</small></span>
                <a href="#" class="shop">Shop Now</a>
              </div>
            </li>
            <li>
              <div class="illustrate">
                <img src="images/iphone.png" alt=""/>
              </div>
              <div class="logindesc">
                <p><strong>iPhone 6 &amp; 6+</strong><p>
                <h3>Upgrade to iphone</h3>
                <span><small>It’s easy to make the switch</small></span>
                <a href="#" class="shop">Shop Now</a>
              </div>
            </li>
          </ul>
        </div>
      </div>  
      <div class="col-xs-5">
        <div class="loginform pull-right">
          <h4>Sign In</h4>

          {!! Form::open(['route' => 'user.login.p', 'method'=>'POST']); !!}
            
            {!!Form::hidden('from','login')!!}
            
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif

            <div class="input-group">
              <span class="input-group-addon"><span class="icon-logiconemail19"></span></span>
              {!! Form::text('email', '', ['class' => 'form-control text', 'placeholder' => 'Username / Email', 'required'=>'', 'autofocus'=>'']) !!}
              <!-- <input type="email" class="" placeholder=""> -->
            </div>
            <div class="input-group">
              <span class="input-group-addon"><span class="icon-logiconpassword19"></span></span>
              
              {!! Form::password('password', ['class' => 'form-control text', 'placeholder' => 'Password']) !!}
              <!-- <input type="password" class="" placeholder="Password"> -->
            </div>

            <label class="remember-me">
              {!! Form::checkbox('remember', 1, null, ['class' => 'minimal']) !!} Remember Me
            </label>
             | <a href="{{URL::route('pass.forgot')}}">Forgot Password</a>

            <button class="btn bg-maroon btn-flat margin" type="submit">Sign In</button>
            <p>Not a member? <a href="{{URL::route('user.signup')}}">Sign up now</a></p>

          {!! Form::close() !!}

        </div>
      </div>
    </div><!--end container-->
  </div><!--end body-->
@stop