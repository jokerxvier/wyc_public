@if(isset($categories) && isset($tree_order))
  @foreach($tree_order as $k => $lvl_1)
    <?php $category = $categories[$k] ?>
    
    <li cat="{{ $category['id'] }}" loaded="0">
      <a href="{{ URL::to('category', $category['slug']) }}"> 
        <span class="{{ $category['icon'] }}"></span>{{ $category['name'] }}
      </a>

      <div class="morecategories">
        <section class="drawer-data1 data">
          <!--  -->
            <div class="leftdrawer">
            <h5>Weekly Features</h5>
              
              <div class="categorieslink col-md-4">
                @foreach($lvl_1 as $k => $lvl_2)
                  <div class="col-3">
                    <?php $category = $categories[$k] ?>
                    <h5><a href="{{ URL::to('category', $category['slug']) }}">{{ $category['name'] }}</a></h5>
                    @foreach($lvl_2 as $k => $lvl_3)
                      <?php $category = $categories[$k] ?>
                      <a href="{{ URL::to('category', $category['slug']) }}">{{ $category['name'] }}</a>
                    @endforeach
                  </div>
                @endforeach
              </div>

              <div class="thumbfeatures col-md-8">
                <ul class="list-unstyled">
                  @foreach($popup_ads->random(6) as $sample)
                    <li><a href="#">{!! Html::image("uploads/{$sample}", '') !!}</a></li>
                  @endforeach
                </ul>
              </div>

          </div>
            
          <div class="rightdrawer">
            {!! Html::image('images/shopnow.jpg', '') !!}
          </div>
            
          <!--  -->
        </section>
      </div>

    </li>
  @endforeach
@endif

<li class="more">
  <a href="{{ URL::to('category') }}">
    <span class="icon-moreadd1392"></span>More Categories <small>Pet, Art materials...</small>
  </a>
</li>
