<header>

  @if(!isset($body_class) || $body_class != 'account')
    <div class="topnav">
        <div class="container">
          <ul class="topnav list-unstyled list-inline pull-right">
            <!-- <li><a href="#">Tracking Order</a></li> -->
            <!-- <li><a href="#">Wishlist</a></li> -->
            <!-- <li><a href="#">Seller Center</a></li> -->
            <!-- <li><a href="#">Customer Care</a></li> -->

            @if(Auth::check())
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ title_case(Auth::user()->lname) }}, {{ title_case(Auth::user()->fname) }}<span class="icon-headerusers81"></span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <!-- <li><a href="#">Orders</a></li> -->

                  @if(Auth::user()->type != 'shopper')
                    <?php
                      $type = Auth::user()->type;
                      if( $type == 'admin') {
                        $url = URL::route('admin.dashboard');
                      } else if ($type == 'support') {
                        $url = URL::route('support.dashboard');  
                      } else if( $type == 'vendor') {
                        $url = URL::route('vendor.dashboard');
                      } else {
                        $url = '#';
                      }
                    ?>
                    <li><a href="{{$url}}">Admin Panel</a></li>
                    <li class="divider"></li>
                  @endif

                  <li><a href="{{URL::route('user.profile')}}">Profile</a></li>
                  <li><a href="{{URL::route('user.orders')}}">Orders</a></li>
                  <li><a href="{{URL::route('general.wishlist')}}">My Wishlist</a></li>
                  <li><a href="{{URL::route('user.logout')}}">Sign Out</a></li>
                </ul>
              </li>
            @else
              <li><a href="{{URL::route('general.wishlist')}}">My Wishlist</a></li>
              <li><a href="{{URL::route('user.signup')}}">Sign Up</a></li>
              <li><a href="{{URL::route('user.login')}}">Login</a></li>
            @endif

          </ul>
        </div>
    </div><!--end topnav-->
  @endif

  <div class="main_head" id="sticky">
    <div class="container">
      <div class="row ">
          <div class="col-xs-3 lefthead" >
            <a href="{{URL::route('user.home')}}" >WYC</a>
            <button class="btn bg-maroon btn-flat  catbtn">Categories</button>
            <div class="boxlist primary" id="dropnav">
              <h4>Categories</h4>
              <div class="sidenav">
                <ul class="list-unstyled">
                  <li>@include('includes.sidebar-shopper')</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="col-xs-9 search_group" >
            <div class="search">
              {!! Form::open(['route' => 'general.search', 'class'=>'input-group']); !!}
                <input type="search" name="q" class="form-control searchbox" placeholder="Search">
                <span class="input-group-btn">
                <button class="searchbtn btn  btn-warning" type="submit"><i class="fa fa-search"></i></button>
                </span>
              {!! Form::close() !!}
              <!-- hide the popular search
              <ul class="list-unstyled list-inline recent">
                <li><span>Popular Search:</span></li>

                 <li><a href="#"> Gopro accessories</a></li>
                 <li><a href="#"> Hero 4 black </a></li>
                 <li><a href="#">Clae Shoes</a></li>
                 <li><a href="#">laptop</a></li>
                   
              </ul>
              -->
            </div><!--end search-->

            
            <div class="cart">
              <a href="{{URL::route('general.cart')}}" class="btn" ><span>{!! Html::image('images/carticon.jpg', '') !!}</span><i id="cart-item-count">{{ isset($cart_item_count) ? $cart_item_count : 0 }}</i> Item</a> 
            </div>
            <div class="wyc-cart-tooltip cart-tooltip b-right" style="right: -2px; top: 45px;">
              <span class="wyc-cart-tooltip-close"><i class="fa fa-close"></i></span>
              <div class="wyc-cart-tooltip-content">
                <div class="image"></div>
                <span class="title"></span>
                <div class="shopping">
                  <a href="{{URL::route('general.cart')}}" class="btn btn-xs btn-info pull-left btn-right-pad">View Cart</a>
                  <!--<a href="{{URL::route('general.cart.checkout')}}" class="btn btn-xs btn-info pull-left btn-right-pad">Checkout</a>-->
                </div>
                <div class="wishlist">
                  <a href="{{URL::route('general.wishlist')}}" class="btn btn-xs btn-info pull-left btn-right-pad">View Wishlist</a>
                </div>
              </div>
            </div>
            
            <div class="social">
              <!-- <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
              <a href="#" class="tweet"><i class="fa fa-twitter"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a> -->
            </div> 
          </div><!--end search group-->
      </div>
    </div>
  </div><!--end main head-->

</header><!--end header-->