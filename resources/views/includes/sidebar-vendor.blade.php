
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              {!! Html::image('images/'.$user['avatar'], 'User Image', ['class' => 'img-circle']) !!}
              <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" /> -->
            </div>
            <div class="pull-left info">
              <p>{{$user['fname'].' '.$user['lname']}}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Store NAVIGATION</li>
            <li class="treeview">
              <a href="{{URL::route('vendor.dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></i></a>  
            </li>
            @if(isset($stores))
                <li class="active treeview">
                  <a href="{{URL::route('vendor.stores')}}">
                    <i class="fa fa-institution"></i>
                    <span>Stores</span>
                    <span class="label pull-right bg-yellow">{{count($stores)}}</span>
                  </a>
                
                       <ul class="treeview-menu">
                        @foreach($stores as $id => $name)
                          <li class="treeview {{($active_store_id == $id || count($stores) == 1 ? 'active' : '')}}">
                            <a href="#"><i class="fa fa-suitcase"></i> {{$name}}</a>
                            <ul class="treeview-menu">
                              <li class="{{($active_menu == 'dashboard' ? 'active' : '')}}">
                                <a href="{{URL::route('vendor.store.dashboard', $id)}}"><i class="fa fa-dashboard"></i> <span>Store Dashboard</span></i></a>  
                              </li>

                              <li class="treeview">
                                <a href="{{URL::to('/coming-soon')}}">
                                  <i class="fa fa-users"></i>
                                  <span>Customers</span>
                                  <span class="label label-primary pull-right ">123</span>
                                </a>
                              </li>

                              <li class="treeview">
                                <a href="{{URL::to('/coming-soon')}}">
                                  <i class="fa fa-shopping-cart"></i>
                                  <span>Orders</span>
                                  <span class="label label-primary pull-right bg-green">123</span>
                                </a>
                              </li>

                              <li class="treeview {{( (in_array($active_menu, ['product','products','brand','brands','categories','prod-options','prod-option','categories','category'])) ? 'active' : '')}}">
                                <a href="#">
                                  <i class="fa fa-cubes"></i>
                                  <span>Products</span>
                                  <i class="fa fa-angle-left pull-right"></i>
                                </a>

                                <ul class="treeview-menu">
                                  <li class="{{( (in_array($active_menu, ['product','products'])) ? 'active' : '')}}">
                                    <a href="{{URL::route('vendor.store.products', $id)}}">
                                      <i class="fa fa-list-alt"></i> Product List
                                    </a>
                                  </li>

                                  <li class="{{( (in_array($active_menu, ['brand','brands','prod-options','prod-option','categories','category'])) ? 'active' : '')}}">
                                    <a href="#"><i class="fa fa-file-text-o "></i> Properties<i class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu">
                                      <li class="{{( (in_array($active_menu, ['brand','brands'])) ? 'active' : '')}}">
                                        <a href="{{URL::route('vendor.store.brands', $id)}}"><i class="fa fa-tag"></i> Brands</a>
                                      </li>
                                      <li class="{{( (in_array($active_menu, ['categories','category'])) ? 'active' : '')}}">
                                        <a href="{{URL::route('vendor.store.categories', $id)}}"><i class="fa fa-tags"></i> Categories</a>
                                      </li>
                                      <li class="{{( (in_array($active_menu, ['prod-options','prod-option'])) ? 'active' : '')}}">
                                        <a href="{{URL::route('vendor.store.prod.options', $id)}}"><i class="fa fa-ticket"></i> Product Options</a>
                                      </li>
                                    </ul>
                                  </li>
                                  
                                  <li><a href="{{URL::to('/coming-soon')}}"><i class="fa fa-table"></i> Reports</a></li>
                                </ul>
                              </li>

                              <li class="treeview">
                                <a href="{{URL::to('/coming-soon')}}">
                                  <i class="fa fa-pie-chart"></i>
                                  <span>Sales Reports</span>
                                  <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                  <li><a href="{{URL::to('/coming-soon')}}"><i class="fa fa-shopping-cart"></i> Orders</a></li>
                                  <li><a href="{{URL::to('/coming-soon')}}"><i class="fa fa-truck"></i> Shipping</a></li>
                                  <li><a href="{{URL::to('/coming-soon')}}"><i class="fa fa-money"></i> Refunds</a></li>
                                </ul>
                              </li>

                            </ul>
                          </li>
                        @endforeach
                      
                  </ul>
                </li> 
              @endif
            @if(Session::has('active_store'))          
            
            @endif

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      