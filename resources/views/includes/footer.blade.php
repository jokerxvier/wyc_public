<footer>
	@if (isset($popular_products))
		<div class="popular">
			<div class="outside"><span id="slider-prev"></span>  <span id="slider-next"></span></div>	
				<div class="container">
					<h4>Popular products</h4>

					<ul class="list-unstyled ">
						@foreach($popular_products as $prod)
							<li>
								<div class="items">
									<div class="view">

										@if($prod->discount > 0)
											<div class="discount_tag">
												<h4>{{ $prod->discount }} <span>%OFF</span></h4>
											</div>
										@endif
										<a href="{{ URL::to('product/detail', $prod->slug ) }}">
											{!! Html::image("uploads/{$prod->image}", '', ['class' => 'centered']) !!}
										</a>
									</div>

									<div class="iteminfo">
										<h5>
											<a href="{{ URL::to('product/detail', $prod->slug ) }}">
												{{str_limit($prod->name, $limit = 50, $end = '...')}}
											</a>
											<br/>
											<span>{{str_limit($prod->desc_short, $limit = 40, $end = '...')}} </span>
										</h5>

										@if($prod->discount > 0)
											<em>{{ number_format($prod->price, 2) }}</em>
											<h3>₱ {{ number_format($prod->price_sale, 2) }}</h3>
										@else
											<h3>₱ {{ number_format($prod->price, 2) }}</h3>
										@endif
										
									</div>

								</div>
							</li>
						@endforeach
					</ul>

				</div>
		</div>
	@endif

		<div class="footer_info">
			<div class="container">
				<div class="row">
					<div class="col-xs-3">
						<div class="media">
						  <div class="media-left">
							 <span class="icon-footeruser7"></span>
						  </div>
						  <div class="media-body">
							<h4 class="media-heading"><strong>Beginners Guide</strong></h4>
							<p>User registration process <br/>Tutorial</p>
						  </div>
						</div>
						
						<div class="media">
						  <div class="media-left">
							  <span class="icon-footerdelivery52"></span>
						  </div>
						  <div class="media-body">
							<h4 class="media-heading"><strong>Shipping Method</strong></h4>
							<p>Cargo - substantial savings <br/>in shipping <br/><br/>Official Tutorial</p>
						  </div>
						</div>
					</div>
					
					<div class="col-xs-3">
						<div class="media">
						  <div class="media-left">
							 <span class="icon-footercredit-card5"></span>
						  </div>
						  <div class="media-body">
							<h4 class="media-heading"><strong>Payment Method</strong></h4>
							<p>Sed ut perspiciatis unde <br/>omnis </p>
						  </div>
						</div>
						
						<div class="media">
						  <div class="media-left">
							<span class="icon-footeroperator"></span>
						  </div>
						  <div class="media-body">
							<h4 class="media-heading"><strong>Customer Service</strong></h4>
							<p>Online customer service<br/></br>FAQ  </p>
						  </div>
						</div>
					</div><!--end col-->
					<div class="col-xs-3 newsletter">
							<h4 ><strong>NewsLetter</strong></h4>
							<p>Nemo enim ipsam voluptatem quia <br/>voluptas sit aspernatur</p>
							<input type="text" class="form-control newsletter_input" placeholder="Text input">
							<p>
							  <button type="button" class="btn btn-primary">Man</button>
							  <button type="button" class="btn btn-primary pink">Woman</button>
							</p>
					</div>
						<div class="col-xs-3 newsletter">
							<h4 ><strong>verified Services</strong></h4>
								<div>
									<ul class="list-unstyled list-inline ">
										<li>{!! Html::image("images/apr.png", '') !!}</li>
										<li>{!! Html::image("images/norton.png", '') !!}</li>
									</ul>
								</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottomfooter">
			<div class="container">
				<div class="row">
					<div class="col-md-10">
						<ul class="list-unstyled list-inline">
							<li><a href="#">About</a></li>
							<li><a href="#">Help Center</a></li>
							<li><a href="#">Payment</a></li>
							<li><a href="#">How to Buy</a></li>
							<li><a href="#">Shipping & Delivery</a></li>
							<li><a href="#">International Product Policy</a></li>
							<li><a href="#">How to Return</a></li>	
						</ul>
					</div>
					<div class="col-md-2 pull-right">
						<a href="#">&copy; Copyright 2015</a>
					</div>
				</div>
			</div>
		</div>
</footer>