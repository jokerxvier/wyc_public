
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              {!! Html::image('images/'.$user['avatar'], 'User Image', ['class' => 'img-circle']) !!}
              <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" /> -->
            </div>
            <div class="pull-left info">
              <p>{{$user['fname'].' '.$user['lname']}}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="{{URL::route('support.dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></i></a>  
            </li>            
            <li class="treeview">
              <a href="{{URL::route('support.users')}}">
                <i class="fa fa-users"></i>
                <span>Users</span>
                <span class="label label-primary pull-right ">0</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{URL::route('support.stores')}}">
                <i class="fa fa-suitcase"></i>
                <span>Stores</span>
                <span class="label pull-right bg-yellow">0</span>
              </a>
            </li>
            <!-- <li class="treeview">
              <a href="{{URL::route('support.dashboard')}}">
                <i class="fa fa-shopping-cart"></i>
                <span>Orders</span>
                <span class="label label-primary pull-right bg-green">0</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Tickets</span>
                <span class="label pull-right bg-red">4</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-buysellads"></i>
                <span>Ads Manager</span>
                <span class="label pull-right bg-purple">0</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cubes"></i>
                <span>Products</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-file-text-o "></i> Properties<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-tags"></i> Categories</a></li>
                    <li><a href="#"><i class="fa fa-tag"></i> Brands</a></li>
                    <li><a href="#"><i class="fa fa-ticket"></i> Variations</a></li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-list-alt"></i> List</a></li>
                <li><a href="#"><i class="fa fa-table"></i> Reports</a></li>
              </ul>
            </li>
 -->
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Sales Reports</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-shopping-cart"></i> Orders</a></li>
                <li><a href="#"><i class="fa fa-truck"></i> Shipping</a></li>
                <li><a href="#"><i class="fa fa-money"></i> Refunds</a></li>
              </ul>
            </li> -->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      