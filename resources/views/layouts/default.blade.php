<!DOCTYPE html>
<html>
  <head>
    
    <meta charset="UTF-8">
    <title>@yield('title')</title>
 
    <!-- Bootstrap 3.3.4 -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/bootstrap/css/glype.css') !!}

    <!-- Font Awesome Icons -->
    {!! Html::style('http://fonts.googleapis.com/css?family=Roboto:400,700,900,300,300italic,400italic,700italic') !!}
    {!! Html::style('http://fonts.googleapis.com/css?family=Montserrat:400,700') !!}

    {!! Html::style('http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic,600italic,700italic') !!}
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
    {!! Html::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') !!}

    <!-- Theme style -->
    {!! Html::style('assets/dist/css/AdminLTE.min.css') !!}
    {!! Html::style('assets/dist/css/skins/skin-blue.min.css') !!}
    
    <!-- plugins -->
    {!! Html::style('assets/plugins/iCheck/all.css') !!}
    {!! Html::style('assets/plugins/datatables/dataTables.bootstrap.css') !!}
    {!! Html::style('assets/plugins/magnific-popup/magnific-popup.css') !!}
    {!! Html::style('assets/plugins/treetable/jquery.treetable.css') !!}
    {!! Html::style('assets/plugins/treetable/jquery.treetable.theme.default.css') !!}


    <!-- jQuery 2.1.4 -->
    {!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name='viewport' content='width=1190'>

    {!! Html::style('assets/default.css') !!}
    {!! Html::style('assets/default-extra.css') !!}
  </head>

  <body class="{{ isset($body_class) ? $body_class : 'page' }}">
    <div class="maskbg"></div>
    
    <!-- Main Content -->
    @include('includes.header')
      @yield('content')
    @include('includes.footer')
    
    <!-- Bootstrap 3.3.2 JS -->
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}

    <!-- AdminLTE App -->
    {!! Html::script('assets/dist/js/app.min.js') !!}
    {!! Html::script('assets/plugins/iCheck/icheck.min.js') !!}
    {!! Html::script('assets/bootstrap/js/scripts.js') !!}

    <!-- plugins -->
    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/bxslider/jquery.bxslider.min.js') !!}
    {!! Html::script('assets/plugins/lazyload/jquery.lazyload.min.js') !!}
    {!! Html::script('assets/plugins/maskinput/jquery.maskedinput.min.js') !!}
    {!! Html::script('assets/plugins/form-validate/jquery.validate.min.js') !!}
    {!! Html::script('assets/plugins/magnific-popup/jquery.magnific-popup.min.js') !!}
    {!! Html::script('assets/plugins/chained/jquery.chained.min.js') !!}
    {!! Html::script('assets/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('assets/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('assets/plugins/slimscroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('assets/plugins/treetable/jquery.treetable.js') !!}

    <script>var base_url = "{{url('/')}}";</script>

    <!-- wyc required js -->
    {!! Html::script('assets/js/_DefaultGlobal.js') !!}
    {!! Html::script('assets/js/_StoreGlobal.js') !!}
    {!! Html::script('assets/js/_ShoppingGlobal.js') !!}
    {!! Html::script('assets/js/User.js') !!}
    

  </body>

  
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/560ba1a5c358d2941996d1b7/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
</html>

  