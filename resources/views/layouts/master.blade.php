<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.4 -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/bootstrap/css/glype.css') !!}

    <!-- FontAwesome 4.3.0 -->
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css')!!}
    <!-- Ionicons 2.0.0 -->
    {!! Html::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')!!}
    
    <!-- Theme style -->
    {!! Html::style('assets/dist/css/AdminLTE.min.css') !!}
    {!! Html::style('assets/dist/css/skins/_all-skins.min.css') !!}
    
    <!-- plugins -->
    {!! Html::style('assets/plugins/iCheck/all.css') !!}
    {!! Html::style('assets/plugins/morris/morris.css') !!}
    {!! Html::style('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    {!! Html::style('assets/plugins/datepicker/datepicker3.css') !!}
    {!! Html::style('assets/plugins/daterangepicker/daterangepicker-bs3.css') !!}
    {!! Html::style('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
    {!! Html::style('assets/plugins/datatables/dataTables.bootstrap.css') !!}
    {!! Html::style('assets/plugins/magnific-popup/magnific-popup.css') !!}
    {!! Html::style('assets/plugins/treetable/jquery.treetable.css') !!}
    {!! Html::style('assets/plugins/treetable/jquery.treetable.theme.default.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! Html::style('assets/support.css') !!}

    <!-- We need to put these js on top because charts called on page and data populated via php -->
    {!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
    {!! Html::script('assets/plugins/chartjs/Chart.min.js') !!}

    <!-- jvectormap -->
    {!! Html::script('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}

    <!-- input-mask -->
    {!! Html::script('assets/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('assets/plugins/input-mask/jquery.inputmask.extensions.js') !!}

  </head>

  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- HEADER -->
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>w</b>yc</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg wyc-logo-sml"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                {!! Html::image('images/'.$user['avatar'], 'User Image', ['class' => 'user-image']) !!}
                  <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/> -->
                  <span class="hidden-xs">{{$user['fname'].' '.$user['lname']}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    {!! Html::image('images/'.$user['avatar'], 'User Image', ['class' => 'img-circle']) !!}
                    <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" /> -->
                    <p>
                      {{$user['fname'].' '.$user['lname']}} - {{ucfirst($user['type'])}}
                      <small>Member since Nov</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!-- <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li> -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{URL::route('user.profile')}}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ URL::route('user.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- ./HEADER -->

      <!-- SIDEBAR -->
      @include('includes.sidebar-'. $user['type'])
      <!-- ./SIDEBAR -->
      

      <!-- MAIN CONTECT -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          @yield('content')
        </div><!-- /.content-wrapper -->
      <!-- ./MAIN CONTECT -->

      <!-- FOOTER -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2015 All rights reserved.
      </footer>
      <!-- ./FOOTER -->

      <!-- RIGHT SIDEBAR -->
      <!-- Control Sidebar -->      
      <aside class="control-sidebar control-sidebar-dark">                
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class='control-sidebar-menu'>
              <li>
                <a href='javascript::;'>
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
              <li>
                <a href='javascript::;'>
                  <i class="menu-icon fa fa-user bg-yellow"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                    <p>New phone +1(800)555-1234</p>
                  </div>
                </a>
              </li>
              <li>
                <a href='javascript::;'>
                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                    <p>nora@example.com</p>
                  </div>
                </a>
              </li>
              <li>
                <a href='javascript::;'>
                  <i class="menu-icon fa fa-file-code-o bg-green"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                    <p>Execution time 5 seconds</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3> 
            <ul class='control-sidebar-menu'>
              <li>
                <a href='javascript::;'>               
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>                                    
                </a>
              </li> 
              <li>
                <a href='javascript::;'>               
                  <h4 class="control-sidebar-subheading">
                    Update Resume
                    <span class="label label-success pull-right">95%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                  </div>                                    
                </a>
              </li> 
              <li>
                <a href='javascript::;'>               
                  <h4 class="control-sidebar-subheading">
                    Laravel Integration
                    <span class="label label-waring pull-right">50%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                  </div>                                    
                </a>
              </li> 
              <li>
                <a href='javascript::;'>               
                  <h4 class="control-sidebar-subheading">
                    Back End Framework
                    <span class="label label-primary pull-right">68%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                  </div>                                    
                </a>
              </li>               
            </ul><!-- /.control-sidebar-menu -->         

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">            
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked />
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Allow mail redirect
                  <input type="checkbox" class="pull-right" checked />
                </label>
                <p>
                  Other sets of options are available
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Expose author name in posts
                  <input type="checkbox" class="pull-right" checked />
                </label>
                <p>
                  Allow the user to show his name in blog posts
                </p>
              </div><!-- /.form-group -->

              <h3 class="control-sidebar-heading">Chat Settings</h3>

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Show me as online
                  <input type="checkbox" class="pull-right" checked />
                </label>                
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Turn off notifications
                  <input type="checkbox" class="pull-right" />
                </label>                
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Delete chat history
                  <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                </label>                
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
      <!-- ./RIGHT SIDEBAR -->
    </div><!-- ./wrapper -->

    <!-- jQuery UI 1.11.2 -->
    {!! Html::script('http://code.jquery.com/ui/1.11.2/jquery-ui.min.js') !!}
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>$.widget.bridge('uibutton', $.ui.button);</script>

    <!-- Bootstrap 3.3.2 JS -->
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    <!-- Morris.js charts -->
    {!! Html::script('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    {!! Html::script('assets/plugins/morris/morris.min.js') !!}
    <!-- jQuery Knob Chart -->
    {!! Html::script('assets/plugins/knob/jquery.knob.js') !!}
    <!-- Sparkline -->
    {!! Html::script('assets/plugins/sparkline/jquery.sparkline.min.js') !!}
    <!-- daterangepicker -->
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js') !!}
    {!! Html::script('assets/plugins/daterangepicker/daterangepicker.js') !!}
    <!-- datepicker -->
    {!! Html::script('assets/plugins/datepicker/bootstrap-datepicker.js') !!}
    <!-- Bootstrap WYSIHTML5 -->
    {!! Html::script('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
    <!-- Slimscroll -->
    {!! Html::script('assets/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('assets/plugins/fastclick/fastclick.min.js') !!}
    <!-- datatables -->
    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js') !!}
    <!-- AdminLTE App -->
    {!! Html::script('assets/dist/js/app.min.js') !!}

    {!! Html::script('assets/plugins/magnific-popup/jquery.magnific-popup.min.js') !!}
    {!! Html::script('assets/plugins/chained/jquery.chained.min.js') !!}
    {!! Html::script('assets/plugins/lazyload/jquery.lazyload.min.js') !!}
    {!! Html::script('assets/plugins/nestedSortable/jquery.mjs.nestedSortable.js') !!}
    {!! Html::script('assets/plugins/treetable/jquery.treetable.js') !!}

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    {!! Html::script('assets/dist/js/pages/dashboard.js') !!}
    {!! Html::script('assets/dist/js/pages/dashboard2.js') !!}
    
    <!-- AdminLTE for demo purposes -->
    <!-- // <script src="dist/js/demo.js" type="text/javascript"></script> -->
    {!! Html::script('assets/dist/js/demo.js') !!}
    
    <!-- WYC required js -->
    
    <script type="text/javascript">
      
      var base_url = "{{url('/')}}";

      @if(isset($active_store_id))
        // see notes in Vendor.js file about this global var
        var ACTIVE_STORE_ID = isNaN('{{$active_store_id}}') ? 0 : '{{$active_store_id}}';
      @endif

    </script>
    
    {!! Html::script('assets/js/_DefaultGlobal.js') !!}
    {!! Html::script('assets/js/_StoreGlobal.js') !!}

    @if($user->type == 'admin')
      {!! Html::script('assets/js/Admin.js') !!}
    @elseif($user->type == 'support')
      {!! Html::script('assets/js/Support.js') !!}
    @elseif($user->type == 'vendor')
      {!! Html::script('assets/js/Vendor.js') !!}
    @endif


  </body>

  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/560ba1a5c358d2941996d1b7/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
</html>