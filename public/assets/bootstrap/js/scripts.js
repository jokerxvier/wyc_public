$(document).ready(function(){
	$(".steptab li").append("<div class='edge'></div>")
	$(".nav-tabs li").append("<div class='edge'></div>")
$('.tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
$(".catbtn").click(function(){
	$(".maskbg").hide()
	var elem = $("#dropnav");
		if(elem.is(":visible")){
			elem.addClass("close")
			elem.removeClass("open");
			
		}else if(elem.is(":hidden")){
			elem.addClass("open")
			elem.removeClass("close")
				$(".maskbg").show()
		}
		return false;
	})
	$(".maskbg").click(function(){
		$(this).hide();
		 $("#dropnav").addClass("close")
		 $("#dropnav").removeClass("open")
	})
	
	//iCheck for checkbox and radio inputs
    // $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    $('#terms, #signup-terms').iCheck({
		checkboxClass: 'icheckbox_square-red',
        radioClass: 'iradio_square-red'
    });

    $('.slides').bxSlider({
		pager : false,
		auto:true,
		controls:false,
	});   
	$(".shopbrandslides ul").bxSlider({
		pager : true,
		auto:true,
		controls:false,
	});   
	$('.loginslides ul').bxSlider({
		pager : true,
		auto:true,
		controls:false,
		auto:true
	});
	var  sliderdetail =$(".slideshow").bxSlider({
		pager : false,
		auto:true,
		controls:false,
		autoDelay: 1000,
		mode:'fade'
	});
	
	$(".categoryprod ul li").each(function(){
		$(this).click(function(){
		var indexcount = $(".categoryprod ul li").index(this);
		sliderdetail.goToSlide(indexcount)
		return false;
		})
	})
	$(".details .thumbs ul").bxSlider({
	  minSlides: 4,
	  maxSlides:4,
	  slideWidth: 106,
	  auto:false,
	  infiniteLoop:false,
	  responsive:true,
  	  adaptiveHeight:true,
	pager : false,
	 hideControlOnEnd: true,
	 	slideMargin: 20,
	})
	$('.categoryprod ul').each(function(){
	$(this).bxSlider({
	  minSlides: 6,
	  maxSlides: 6,
	  slideWidth: 300,
	  auto:false,
	  infiniteLoop:false,
	  responsive:true,
  	  adaptiveHeight:true,
	pager : false,

	})
	});
	$(".hovereffect .boxcell a").each(function(){
		$(this).append("<div class='mask'></div>")
	})
	
$(".popular ul").bxSlider({
	auto:true,
	minSlides: 5,
	maxSlides: 5,
	slideWidth: 230,
	slideMargin: 10,
  	 infiniteLoop:false,
	 nextSelector: '#slider-next',
	prevSelector: '#slider-prev',
	pager:false,
	speed: 500,
			 hideControlOnEnd: true
	})
	
});
$(window).scroll(function() {

if ($(this).scrollTop() > 2){  
    $('#sticky').addClass("sticky");
  }
  else{
    $('#sticky').removeClass("sticky");
	  $('#dropnav').removeClass("sticky");
    $('#dropnav').removeClass("open");
    $('#dropnav').removeClass("close");
	$(".maskbg").hide()
  }
});