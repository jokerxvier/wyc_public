var StoreJS = (function (parent, $) {

    var dom = {
        alert: $('.alert'),
        alert_ul: $('.alert ul'),
        token: $("input[name=_token]"),

        store_form: $('#store-form'),
        store_submit: $('#store-submit'),
        store_name: $('#store-name'),
        store_desc: $('#store-desc'),
        store_address: $('#store-address'),
        store_phone: $('#store-phone'),
        store_id: $('#store-id'),

        doc_form: $('.doc-form'),
        doc_file: $('.file-doc'),
        doc_remove: $('.remove-doc'),

        treetable: $("#treetable-cats"),
        add_categories: $('#add-categories'),
        add_parent_category: $('.add-parent-category'),
        cat_list: $('#cat-list'),
        cat_list_li: $('#cat-list li'),
        selected_cat: $('.selected-cat'),
    }

    dom.store_submit.click(function(e){
        
        e.preventDefault();

        // if( dom.store_name.val() == '' || dom.store_desc.val() == '' || dom.store_address.val() == '' || dom.store_phone.val() == '') {
        //     dom.alert_ul.empty().append('<li>'+ERR_EMPTY_FIELD+'</li>')
        //     dom.alert.removeClass('hidden');
        //     return false;
        // }
        
        // var count = 0;
        // $('input[name="categories[]"]').each(function(){
        //     count++;
        // });

        // if(count == 0) {
        //     dom.alert_ul.empty().append('<li>Please select/add store category</li>')
        //     dom.alert.removeClass('hidden');
        //     return false;
        // }
        
        dom.store_form.submit();
        
    });

    // dom.doc_remove.click(function(){
    //     var form = $(this).closest('form'),
    //         doc_type = form.find('.doc-type'),
    //         doc_name = form.find('.doc-name'),
    //         doc_preview = form.find('.preview-doc');

    //     if(typeof doc_type.val() == 'undefined') {    
    //         return false;
    //     }

    //     $.ajax({
    //         url: base_url + '/store-doc-delete',
    //         data: { 
    //             store_id: dom.store_id.val(),
    //             type: doc_type.val(),
    //             file: $('#'+doc_type.val()).val(), 
    //             _token: dom.token.val()
    //         },
    //         type: 'POST',
    //         beforeSend: function() {CursorWait();}, 
    //         success: function(ret) {
    //             CursorDefault();
    //             if(ret == '1') {
    //                 doc_name.text('');
    //                 doc_preview.empty();
    //                 $('#'+doc_type.val()).val('');
    //             }
    //         }
    //     });

    // });

    // dom.doc_file.change(function(){
    //     $(this).closest('form').submit();
    // });

    // dom.doc_form.submit(function() {
    //     var that = $(this),
    //         formData = new FormData(that[0]);

    //     $.ajax({
    //         url: base_url + '/store-doc-updload',
    //         type: 'post',
    //         cache: false,
    //         data: formData,
    //         processData: false,
    //         contentType: false,
    //         beforeSend: function() {CursorWait();},
    //         success: function(data) {
    //             CursorDefault();
    //             try {
    //                 jn = JSON.parse(data);
    //                 that.find('.preview-doc').empty();

    //                 if(jn.status == '1' && jn.allowed == '1') {
                        
    //                     $('#'+jn.type).val(jn.file_name); // store form hidden field (for normal post !ajax)
    //                     that.find('.doc-name').text(jn.file_name).attr('type', jn.type);
    //                     that.find('.preview-doc').append(jn.img);

    //                 } else if(jn.status == '2' && jn.allowed == '1') {

    //                     $('#'+jn.type).val(jn.file_name); // store form hidden field (for normal post !ajax)
    //                     that.find('.doc-name').text(jn.file_name).attr('type', jn.type);
    //                     that.find('.preview-doc').append('<a href="'+jn.path+'">Preview/Download File</a>');
    //                     return true;
    //                 }

    //                 if(jn.allowed != '1') {
    //                     alert(jn.msg);
    //                 }

    //             } catch(e) {
    //                 console.log(ERR_DEFAULT);
    //             }
    //         },
    //         error: function(xhr, textStatus, thrownError) {
    //             console.log(xhr);
    //             console.log(textStatus);
    //             console.log(thrownError);
    //         }
    //     });
    //     return false;
    // });

    dom.add_parent_category.click(function(e){
        var ctr = 0, depth_exceed = false, parents_id;

        $.each(dom.selected_cat, function(){
            if($(this).is(':checked')) {
                ctr++;
                parents_id = $(this).closest('tr').attr('data-tt-parent-id');
                if(parents_id){
                    if (parents_id.indexOf("-") >= 0) {
                        depth_exceed = true;
                        return false;
                    }
                }
            }
        });

        if(depth_exceed) {
            alert('We only allow up to three (3) depths level. Choosing a  3rd depth level parent category will result to 4th depth level to your new category. Please select categories from 1st to 2nd depth level only.')
            e.stopImmediatePropagation(); // stop dom.add_categories.click() from beeing called
            return false;
        }

        if(ctr > 1) {
            alert('You can only select 1 parent category.');
            e.stopImmediatePropagation(); // stop dom.add_categories.click() from beeing called
            return false;
        }
    });

    dom.add_categories.click(function(e){
        e.preventDefault();
        dom.cat_list.empty();
        $.each(dom.selected_cat, function(){
            if($(this).is(':checked')) {
                dom.cat_list.append('<li class="option">'
                    + $(this).parent().text().trim() +' <i class="fa fa-times"></i>'
                    +'<input type="hidden" name="categories[]" value="'+ $(this).attr('cat-id') +'"'
                    +'</li>');
            }
        });

        dom.cat_list.show();
        $.magnificPopup.close();
    });

    $(document).on('click', '#cat-list .fa-times', function (e) {
        var parent_ul = $(this).parent();
        
        parent_ul.remove();
        $('#cat-'+parent_ul.find('input').val()).removeAttr('checked');
        if($('#cat-list li').length == 0) { // do not use dom.cat_list_li it will not work
            dom.cat_list.hide();            // since it is dynamic filled dom
        }
    });

    return parent;
}( StoreJS || {}, jQuery));