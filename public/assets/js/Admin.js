var AdminJS = (function (parent, $) {
	
	var dom = {
		alert: $('.alert'),
        alert_ul: $('.alert ul'),
        token: $("input[name=_token]"),

		category_delete: $('.delete-cat'),
		category_approve: $('.approve-cat'),
		category_reject: $('.reject-cat'),
		category_reject_submit: $('#reject-cat-submit'),
		category_reject_reason: $('#reject-reason'),

		category_submit: $('#category-submit'),
        category_form: $('#category-form'),
	};

	$('#treetable-cats').treetable({ expandable: true });

	dom.category_submit.click(function(e){
        e.preventDefault();
        dom.category_form.submit();
    });

	dom.category_delete.click(function(e){
        e.preventDefault()

        var that = $(this),
            cat_id = $(this).attr('cat-id');

        if(confirm('Are you sure you want to delete this category?') == true) {
            $.ajax({
                url: base_url +'/admin/category/delete',
                data: { 
                    cat_id: cat_id, 
                    _token: dom.token.val()
                },
                type: 'POST',
                beforeSend: function() {
                    CursorWait(); }, 
                success: function(ret) {
                    CursorDefault();
                    if(ret == '1') {
                        that.closest('li').remove();
                    }
                }
            });
        }
    });

    dom.category_approve.click(function(e){
        e.preventDefault()

        var cat_id = $(this).attr('cat-id');
        
        $.ajax({
            url: base_url +'/admin/category/approve',
            data: { 
                cat_id: cat_id, 
                _token: dom.token.val()
            },
            type: 'POST',
            beforeSend: function() {
                CursorWait(); }, 
            success: function(ret) {
                CursorDefault();
                if(ret == '1') {
                    window.location.href = base_url + '/admin/categories'
                }
            }
        });
    });

    dom.category_reject.click(function(e){
    	e.preventDefault()

    	var cat_id = $(this).attr('cat-id');

    	dom.category_reject_submit.attr('cat-id', cat_id);
    	$.magnificPopup.open({
			items: { src: $('#category-reject') },
			type: 'inline'
		});

    });

    dom.category_reject_submit.click(function(e){
        e.preventDefault()

        var cat_id = $(this).attr('cat-id'),
        	reason = dom.category_reject_reason.val();
        
        if(reason == '') {
        	alert('Please indicate the reason why this category should be rejected.')
        	return false;
        }

        $.ajax({
            url: base_url +'/admin/category/reject',
            data: { 
                cat_id: cat_id, 
                reason: reason, 
                _token: dom.token.val()
            },
            type: 'POST',
            beforeSend: function() {
                CursorWait(); }, 
            success: function(ret) {
                CursorDefault();
                if(ret == '1') {
                    window.location.href = base_url + '/admin/categories'
                }
            }
        });
    });

    

	return parent;
}( AdminJS || {}, jQuery));
