var SupportJS = (function (parent, $) {
	
	var dom = {
		alert: $('.alert'),
        alert_ul: $('.alert ul'),
        token: $("input[name=_token]"),

        dtbl_store_for_approval: $('#dtbl-stores-for-aprvl'),
        dtbl_store_active: $('#dtbl-stores-active'),
        dtbl_store_declined: $('#dtbl-stores-declined'),
        dtbl_store_banned: $('#dtbl-stores-banned'),
        dtbl_users: $('#dtbl-users'),
        dtbl_user_stores: $('#dtbl-user-stores'),

        decline_submit: $('#decline-submit'),
        decline_reason: $('#decline-reason'),
    }

    dom.dtbl_store_for_approval.dataTable();

	dom.dtbl_store_active.dataTable({
		'bServerSide': true,
		'bProcessing': true,
		'bAutoWidth': false,
    	'sAjaxSource': base_url + '/support/store/page/Active',
	});

	dom.dtbl_store_declined.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/support/store/page/Declined',
    });

    dom.dtbl_store_banned.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/support/store/page/Banned',
    });

    dom.dtbl_users.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/support/user/page',
    });

    // dom.dtbl_user_stores.dataTable({
    //     'bServerSide': true,
    //     'bProcessing': true,
    //     'bAutoWidth': false,
    //     'sAjaxSource': base_url + '/support/user/stores',
    // });
    
    $('#treetable-cats').treetable({ expandable: true });

    $('#file-logo, #file-banner, #file-nbi, #file-valid-id').change(function(){
        GenericUpload($(this).closest('form'));
    });
    $('.fileinput-remove-button').click(function(){
        ClearGenericUpload($(this).closest('form'));
    });

    $(document).on('click', '.approve-store', function(e){
        e.preventDefault();
        var store_id = $(this).attr('store-id');

        $.ajax({
            type: 'GET', 
            url: base_url + '/support/store/approve',
            data: {store_id: store_id},
            beforeSend: function() { 
            	CursorWait();},
            success: function(ret) {
                CursorDefault();
               if(ret == '1') {
                    window.location.href = base_url + '/support/stores';
               }
            }
        });
    });

    $(document).on('click', '.ban-store', function(e){
        e.preventDefault();
        var store_id = $(this).attr('store-id');

        $.ajax({
            type: 'GET', 
            url: base_url + '/support/store/ban',
            data: {store_id: store_id},
            beforeSend: function() { 
                CursorWait();},
            success: function(ret) {
                CursorDefault();
               if(ret == '1') {
                    window.location.href = base_url + '/support/stores';
               }
            }
        });
    });

    $(document).on('click', '.decline-store', function(e){
        var store_id = $(this).attr('store-id');

        dom.decline_submit.attr('store-id', store_id);
    	$.magnificPopup.open({
			items: { src: $('#decline-store') },
			type: 'inline'
		});

    });

    dom.decline_submit.click(function(e){
    	e.preventDefault();

    	var store_id = $(this).attr('store-id'),
            reason = dom.decline_reason.val();
    	
        if(reason == '') {
            alert('Please indicate the reason why this store should be declined.')
            return false;
        }

    	$.ajax({
            type: 'POST', 
            url: base_url + '/support/store/decline',
            data: {
            	store_id: store_id,
            	_token: dom.token.val(),
            	reason: reason
            }, beforeSend: function() {
                CursorWait();
            }, success: function(ret) {
                CursorDefault();
               if(ret == '1') {
                    window.location.href = base_url + '/support/stores';
               }
            }
        });
    });

    $(document).on('click', '.ban-toggle', function(e) {
        e.preventDefault();
        var user_id = $(this).attr('user-id');

        $.ajax({
            type: 'POST',
            url: base_url +'/support/user/ban-toggle',
            data: {
                _token: dom.token.val(), 
                user_id: user_id
            }, beforeSend: function() {
                CursorWait();
            }, success: function(ret) {
                CursorDefault();
                if(ret == '1') {
                    dom.dtbl_users.dataTable().fnDraw(true);
                }
            }
        });
    });

    return parent;
}( SupportJS || {}, jQuery));
