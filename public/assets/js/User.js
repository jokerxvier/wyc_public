var UserJS = (function (parent, $) {

	var dom = {
        alert: $('.alert'),
        alert_ul: $('.alert ul'),
		token: $("input[name=_token]"),
        
        cp_msg: $('.cp-msg'),
        cp_old_pass: $('#cp-old-pass'),
        cp_pass1: $('#cp-pass1'),
        cp_pass2: $('#cp-pass2'),
        btn_changpass: $('#btn-change-pass'),

        tbl_user_addr_body: $('#tbl-user-address tbody'),
        save_address: $('#save-address'),
        address_add: $('#address-add'),
        new_address_title: $('#new-address h3'),
        street: $('#street'),
        city: $('#city'),
        region: $('#region'),
        country: $('#country'),
        type: $('#type'),
        zip: $('#zip'),

        dtbl_orders: $('#dtbl-orders'),
        
	};

    $('#treetable-cats').treetable({ expandable: true });
    $('#file-logo, #file-banner, #file-nbi, #file-valid-id').change(function(){
        GenericUpload($(this).closest('form'));
    });
    
    dom.dtbl_orders.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/user/order/page',
    });


    dom.btn_changpass.click(function(e){
        e.preventDefault();

        var data = {
            _token: dom.token.val(),
            user_id: $(this).attr('user_id'),
            cp_old_pass: dom.cp_old_pass.val(),
            cp_pass1: dom.cp_pass1.val(),
            cp_pass2: dom.cp_pass2.val(),
        };

        if(data.cp_old_pass == '' || data.cp_pass1 == '' || data.cp_pass1 == '') {
            dom.cp_msg.text(ERR_EMPTY_FIELD).show();
            return false;
        }

        if(data.cp_pass1 != data.cp_pass2) {
            dom.cp_msg.text('Password didn\'t match').show();
            return false;   
        }

        $.ajax({
            type: 'POST', data: data,
            url: base_url + '/user/change-pass',
            beforeSend: function() {CursorWait();}, 
            success: function(ret) {
                try {
                    jn = JSON.parse(ret);
                    if(jn.status == '0') {
                        dom.cp_msg.text(jn.msg).show();
                    } else {
                        alert('Your password has been successfully updated.');
                        $.magnificPopup.close();
                    }
                
                } catch(e) {
                    console.log(ERR_DEFAULT);
                }

                CursorDefault();
            }
        });
    });

    dom.save_address.click(function(e){
        e.preventDefault();

        var url = '',
            that = $(this),
            user_id = $(this).attr('user_id'),
            addr_id = $(this).attr('addr_id');

        var data = {
            user_id: user_id,
            addr_id: addr_id,
            type: dom.type.find('option:selected').text(),
            region: dom.region.find('option:selected').text(),
            city: dom.city.find('option:selected').text(),
            street: dom.street.val(),
            country: dom.country.val(),
            zip: dom.zip.val(),
            _token: dom.token.val(),
        };

        if(data.street == '' || data.zip == '' || dom.region.val() == '' || dom.city.val() == '') {
            dom.alert_ul.empty().append('<li>'+ERR_EMPTY_FIELD+'</li>')
            dom.alert.removeClass('hidden');
            return false;
        } else {
            dom.alert_ul.empty();
            dom.alert.hide();
        }

        if(parseInt(addr_id) > 0) {
            url = base_url + '/user/addr-update';
        } else {
            url = base_url + '/user/addr-insert';
        }

        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            beforeSend: function() {CursorWait();}, 
            success: function(ret) {
                CursorDefault();
                try {
                    jn = JSON.parse(ret);
                    if(parseInt(addr_id) > 0) {
                        $('#addr-'+ addr_id +' td:first').text(jn.str_address);
                        $('#addr-'+ addr_id +' td:nth-child(2)').text(jn.type);
                    } else {
                        dom.tbl_user_addr_body.append(jn.row);
                        $('.no-record').remove();
                    }

                    $.magnificPopup.close();
                } catch(e) {
                    console.log(ERR_DEFAULT);
                }
            }
        });
    });

    dom.address_add.click(function(){
        dom.zip.val('');
        dom.street.val('');
        dom.region.val(0);
        dom.region.change();
        dom.new_address_title.text('Add New Address');
    });

    $(document).on('click', '.address-edit', function (e) {
        e.preventDefault();

        var addr_id = $(this).attr('addr_id');

        $.ajax({
            url: base_url + '/user/addr-get',
            data: { addr_id: addr_id},
            type: 'GET',
            beforeSend: function() {CursorWait();}, 
            success: function(ret) {
                CursorDefault();
                try {
                    jn = JSON.parse(ret);
                    
                    dom.address_add.click();
                    dom.save_address.attr('addr_id', jn.addr_id);
                    dom.new_address_title.text('Update Address');
                    dom.street.val(jn.street);
                    dom.zip.val(jn.zip);

                    $('#region option').filter(function() {
                        return this.text == jn.region; 
                    }).attr('selected', true);
                    dom.region.change();

                    $('#city option').filter(function() {
                        return this.text == jn.city; 
                    }).attr('selected', true);

                    $('#type option').filter(function() {
                        return this.text == jn.type; 
                    }).attr('selected', true);

                } catch(e) {
                    console.log(ERR_DEFAULT)
                }
            }
        });

    });

    $(document).on('click', '.address-del', function (e) {
        e.preventDefault();

        var tr = $(this).closest('tr'),
            addr_id = $(this).attr('addr_id');

        if(confirm('Are you sure you want to delete this address?') == true) {
            $.ajax({
                url: base_url + '/user/addr-delete',
                data: { addr_id: addr_id, _token: dom.token.val()},
                type: 'POST',
                beforeSend: function() {CursorWait();}, 
                success: function(ret) {
                    CursorDefault();
                    if(ret == '1') {
                        tr.remove();
                    }
                }
            });
        }
    });

    $(document).on('click', '.cancel-order', function (e) {
        e.preventDefault();

        var order_id = $(this).attr('order-id');

        if(confirm('Are you sure you want to cancel this order?') == true) {
            $.ajax({
                url: base_url + '/user/order/cancel',
                data: { order_id: order_id, _token: dom.token.val()},
                type: 'POST',
                beforeSend: function() {
                    CursorWait();
                }, complete: function() {
                    CursorDefault();
                }, success: function(ret) {
                    if(ret == '1') {
                        dom.dtbl_orders.dataTable().fnDraw(true);
                    }
                }
            });
        }
    });

	return parent;
}( UserJS || {}, jQuery));

