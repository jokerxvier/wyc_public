var VendorJS = (function (parent, $) {
	
    // [NOTE] ACTIVE_STORE_ID is a global variable filled in php/view since
    // $active_store_id is dynamic and php/blade or echo can't parse within this .js file

	var dom = {
		alert: $('.alert'),
        alert_ul: $('.alert ul'),
        token: $("input[name=_token]"),

        dtbl_brands: $('#dtbl-brands'),
        dtbl_products: $('#dtbl-products'),
        dtbl_store_active: $('#dtbl-stores-active'),
        dtbl_prod_options: $('#dtbl-prod-options'),

        img_form: $('.img-form'),
        img_file: $('.img-file'),
        img_remove: $('.img-remove'),

        brand_submit: $('#brand-submit'),
        brand_form: $('#brand-form'),

        category_submit: $('#category-submit'),
        category_form: $('#category-form'),

        variant_tbl_body: $('#tbl-variants tbody'),
        variant_entry_add: $('#variant-entry-add'),

        vendor_cat_link: $('.link-cat'),
        vendor_cat_unlink: $('.unlink-cat'),
        vendor_cat_delete: $('.delete-cat'),

        product_submit: $('.product-submit'),
        product_form: $('#product-form'),
        
    }
    
	dom.dtbl_store_active.dataTable({
		'bServerSide': true,
		'bProcessing': true,
		'bAutoWidth': false,
    	'sAjaxSource': base_url + '/vendor/store/page',
	});



    dom.dtbl_brands.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/vendor/store/'+ ACTIVE_STORE_ID +'/brand/page',
    });

    dom.dtbl_products.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/vendor/store/'+ ACTIVE_STORE_ID +'/product/page',
    });

    dom.dtbl_prod_options.dataTable({
        'bServerSide': true,
        'bProcessing': true,
        'bAutoWidth': false,
        'sAjaxSource': base_url + '/vendor/store/'+ ACTIVE_STORE_ID +'/prod-option/page',
    });

    $('#treetable-cats').treetable({ expandable: true });

    $('#file-image, #file-image-alt, #featured-image, #file-logo, #file-banner, #file-nbi, #file-valid-id').change(function(){
        GenericUpload($(this).closest('form'));
    });

    $('[id^=product-image-]').change(function(){
        GenericUpload($(this).closest('form'));
    });

    $('.fileinput-remove-button').click(function(){
        ClearGenericUpload($(this).closest('form'));
    });

	dom.img_file.change(function(){
        $(this).closest('form').submit();
    });


 //    dom.img_form.submit(function() {
 //        var that = $(this),
 //            formData = new FormData(that[0]);

 //        $.ajax({
 //            url: base_url + '/brand-img-updload',
 //            type: 'post',
 //            cache: false,
 //            data: formData,
 //            processData: false,
 //            contentType: false,
 //            beforeSend: function() {CursorWait();},
 //            success: function(data) {
 //                CursorDefault();
 //                try {
 //                    jn = JSON.parse(data);
                    
 //                    that.find('.preview-img').empty();

 //                    if(jn.allowed == '1') {
                        
 //                        $('#'+jn.type).val(jn.file_name); // store form hidden field (for normal post !ajax)
 //                        that.find('.file-caption-name').text(jn.file_name);
 //                        that.find('.preview-img').append(jn.img);

 //                    } else {
 //                        alert('Selected file is not allowed.');
 //                    }

 //                } catch(e) {
 //                    console.log(ERR_DEFAULT);
 //                }
 //            },
 //            error: function(xhr, textStatus, thrownError) {
 //                console.log(xhr);
 //                console.log(textStatus);
 //                console.log(thrownError);
 //            }
 //        });
 //        return false;
 //    });

	// dom.img_remove.click(function(){
 //        var form = $(this).closest('form'),
 //            img_type = form.find('.img-type'),
 //            img_name = form.find('.file-caption-name'),
 //            img_preview = form.find('.preview-img');

 //        if(typeof img_type.val() == 'undefined') {    
 //            return false;
 //        }

 //        $.ajax({
 //            url: base_url + '/brand-img-delete',
 //            data: { 
 //                file: $('#'+img_type.val()).val(), 
 //                _token: dom.token.val()
 //            },
 //            type: 'POST',
 //            beforeSend: function() {CursorWait();}, 
 //            success: function(ret) {
 //                CursorDefault();
 //                if(ret == '1') {
 //                    img_name.text('');
 //                    img_preview.empty();
 //                    $('#'+img_type.val()).val('');
 //                }
 //            }
 //        });

 //    });

    dom.brand_submit.click(function(e){
    	e.preventDefault();
    	dom.brand_form.submit();
    });

    $(document).on('click', '.brand-delete', function (e) {
        e.preventDefault();
        var brand_id = $(this).attr('brand-id');

        if(confirm('Are you sure you want to delete this brand?') == true) {
            $.ajax({
                url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/brand/delete',
                data: { 
                    brand_id: brand_id, 
                    _token: dom.token.val()
                },
                type: 'POST',
                beforeSend: function() {CursorWait();}, 
                success: function(ret) {
                    CursorDefault();
                    if(ret == '1') {
                        dom.dtbl_brands.dataTable().fnDraw(true);
                    }
                }
            });
        }

    });

    dom.variant_entry_add.click(function(e){
        e.preventDefault();
        var tr = $('#tbl-variants tbody tr:first-child').clone()
        tr.find('input').val('');
        tr.appendTo(dom.variant_tbl_body);
    });

    $(document).on('click', '.variant-entry-remove', function (e) {
        e.preventDefault();
        if($('#tbl-variants tbody tr').length > 1) {
            var variant_id = $(this).prev('input').val(),
                that = $(this);

            if(confirm('Are you sure you want to delete this product variant?') == true) {
                $.ajax({
                    url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/variant/delete',
                    data: { 
                        variant_id: variant_id, 
                        _token: dom.token.val()
                    },
                    type: 'POST',
                    beforeSend: function() {
                        CursorWait(); }, 
                    success: function(ret) {
                        CursorDefault();
                        if(ret == '1') {
                            that.closest('tr').remove();
                            // dom.dtbl_brands.dataTable().fnDraw(true);
                        }
                    }
                });
            }
        }
    });

    $(document).on('click', '.option-delete', function (e) {
        e.preventDefault();
        var option_id = $(this).attr('option-id');

        if(confirm('Are you sure you want to delete this product option and its variants?') == true) {
            $.ajax({
                url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/prod-option/delete',
                data: { 
                    option_id: option_id, 
                    _token: dom.token.val()
                },
                type: 'POST',
                beforeSend: function() {
                    CursorWait(); }, 
                success: function(ret) {
                    CursorDefault();
                    if(ret == '1') {
                        dom.dtbl_prod_options.dataTable().fnDraw(true);
                    }
                }
            });
        }
        
    });

    dom.vendor_cat_link.click(function(e){
        e.preventDefault();
        var cat_id = $(this).attr('cat-id');

        $.ajax({
            url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/category/link',
            data: { 
                cat_id: cat_id, 
                _token: dom.token.val()
            },
            type: 'POST',
            beforeSend: function() {
                CursorWait(); }, 
            success: function(ret) {
                CursorDefault();
                if(ret == '1') {
                    window.location.reload();
                }
            }
        });
    });

    dom.vendor_cat_unlink.click(function(e){
        e.preventDefault();
        var cat_id = $(this).attr('cat-id');

        $.ajax({
            url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/category/unlink',
            data: { 
                cat_id: cat_id, 
                _token: dom.token.val()
            },
            type: 'POST',
            beforeSend: function() {
                CursorWait(); }, 
            success: function(ret) {
                CursorDefault();
                if(ret == '1') {
                    window.location.reload();
                }
            }
        });
    });

    dom.vendor_cat_delete.click(function(e){
        e.preventDefault()

        var that = $(this),
            cat_id = $(this).attr('cat-id');

        if(confirm('Are you sure you want to delete this category?') == true) {
            $.ajax({
                url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/category/delete',
                data: { 
                    cat_id: cat_id, 
                    _token: dom.token.val()
                },
                type: 'POST',
                beforeSend: function() {
                    CursorWait(); }, 
                success: function(ret) {
                    CursorDefault();
                    if(ret == '1') {
                        that.closest('li').remove();
                        // window.location.reload();
                    }
                }
            });
        }
    });

    dom.category_submit.click(function(e){
        e.preventDefault();
        dom.category_form.submit();
    });

    dom.product_submit.click(function(e){
        e.preventDefault();

        dom.product_form.submit();
    });




    $(document).on('click', '.product-delete', function (e) {
        e.preventDefault();
        var product_id = $(this).attr('product-id');

        if(confirm('Are you sure you want to delete this product?') == true) {
            $.ajax({
                url: base_url +'/vendor/store/'+ ACTIVE_STORE_ID +'/product/delete',
                data: { 
                    product_id: product_id, 
                    _token: dom.token.val()
                },
                type: 'POST',
                beforeSend: function() {
                    CursorWait(); }, 
                success: function(ret) {
                    CursorDefault();
                    if(ret == '1') {
                        dom.dtbl_products.dataTable().fnDraw(true);
                    }
                }
            });
        }
        
    });

    return parent;
}( VendorJS || {}, jQuery));
