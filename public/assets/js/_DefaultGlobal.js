var ERR_DEFAULT = 'An error occured while processing the request.';
var ERR_EMPTY_FIELD = 'Please populate all required fields.';

var ACTIVE_STORE_ID;

var CursorWait = function() {
	$('html, body').css('cursor', 'wait');
};

var CursorDefault = function() {
	$('html, body').css('cursor', 'default');
};

// upload-form.blade.php driver
var GenericUpload = function(form) {
	var formData = new FormData(form[0]);

    $.ajax({
        url: base_url + '/generic-updload',
        type: 'post',
        cache: false,
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: function() {
        	CursorWait();
        }, complete: function() {
            CursorDefault();
        }, success: function(data) {
            try {
                jn = JSON.parse(data);
               	form.find('.upload-preview').empty();

                if(jn.allowed == '1') {
                    $("input[name="+ jn.link +"]").val(jn.file_name); // store form hidden field (for normal post !ajax)
                    form.find('.file-caption-name').text(jn.file_name);
                    if(jn.preview == '1') {
                        form.find('.upload-preview').append('<img style="'+ jn.style +'" src="'+ jn.img_src +'" />');
                    } else {
                        form.find('.upload-preview').append('<a href="'+jn.img_src+'">Preview/Download File</a>');
                    }
                } else {
                    alert('Selected file is not allowed.');
                }
            
            } catch(e) {
                console.log(ERR_DEFAULT);
            }
        },
        error: function(xhr, textStatus, thrownError) {
            console.log(xhr);
            console.log(textStatus);
            console.log(thrownError);
        }
    });
    return false;
};

var ClearGenericUpload = function(form, clear_note) {
	var 
        file_link = form.find("input[name=link]"),
        file_caption = form.find('.file-caption-name'),
        upload_preview = form.find('.upload-preview');
        upload_note = form.find('.upload-note');

    if(typeof file_link.val() == 'undefined') {    
        return false;
    }

    $.ajax({
        url: base_url + '/generic-updload-delete',
        data: { 
            file: file_caption.text(), 
            _token: $("input[name=_token]").val()
        },
        type: 'POST', 
        beforeSend: function() {
        	CursorWait();
        }, complete: function() {
            CursorDefault();
        }, success: function(ret) {
            if(ret == '1') {
                file_caption.text('');
                upload_preview.empty();
                $('input[name='+file_link.val()+']').val('');

                if(clear_note) {
                	upload_note.text();
                }
            }
        }
    });
};

// -- personal plugin for this project only

(function($){
    $.fn.popupSelect = function(options) {
        var settings = $.extend({
                allowedDepth: 0
            }, options);

        var container = this.parent(),
            wrapper = $('<div />').attr('class', 'pps'),
            wrap_options = $('<ul />').attr('class', 'list'),
            id = this.attr('id'),
            insertTrigger = $('#'+id+'-insert');
            hiddenAttr = { type: 'hidden', name: id+'[]', value: 0 };
        
        
        // once we already have the ul
        if(this.next().is('ul')) {
            wrap_options = this.next();
            $.each(wrap_options.find('li'), function(){
                $('#'+id+'-choose input[pps-id="'+ $(this).attr('pps-id') +'"').attr('checked','checked');
            });
        }

        // do not rearrange
        wrapper.appendTo(container);
        this.appendTo(wrapper);
        wrap_options.appendTo(wrapper);

        // popup insert button
        insertTrigger.click(function(e){

            var ctr = 0, allow = true;
            e.preventDefault();
            wrap_options.empty();

            $('#'+id+'-choose .pps-selected').each(function(e)
            {
                var that = $(this);
                if(that.is(':checked')) {

                    if(settings.allowedDepth > 0) {
                        ctr++;
                        if(ctr > settings.allowedDepth) {
                            alert('You can only select 1 item from the list.');
                            allow = false;
                            return false;
                        }
                    }

                    hiddenAttr.value = that.attr('pps-id');
                    hiddenAttr.name = id+'[]';

                    wrap_options.append(
                        $('<li />')
                            .attr('class', 'option')
                            .attr('pps-id', hiddenAttr.value)
                            .html(that.parent().text().trim())
                            .append($('<i />').attr('class', 'fa fa-times pps-del'))
                            .append($('<input />').attr(hiddenAttr))
                    );
                }
            });
        
            if(allow)
                $.magnificPopup.close();
        });

        // on del select item
        $(document).on('click', '.pps-del', function(e){
            var ppsItem = $(this).parent()
                ppsItem.remove();

            $('#'+id+'-choose input[pps-id="'+ ppsItem.attr('pps-id') +'"').removeAttr('checked');
        });

        return this;
    };
}(jQuery));

$(document).ready(function(){
	
    // personal plugin for this project only
    $('#pps-brand').popupSelect({allowedDepth:1});
    $('#pps-category').popupSelect();
    $('#pps-option').popupSelect();

	$("[data-mask]").inputmask();

	$("#city").chained("#region");
	
	$("img.lazy").lazyload({
	    effect : "fadeIn",
	    threshold : 100
	});

	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
	});

	$('.popup-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		// focus: '#username',
		modal: true
	});

	$('.categorieslink').slimScroll({
		height: '465px',
		width: '120px'
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});