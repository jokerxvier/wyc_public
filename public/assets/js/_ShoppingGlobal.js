var ShoppingJS = (function (parent, $) {

    var dom = {
        token: $("input[name=_token]"),

        prod_id: $('#prod-id'),
        prod_qty: $('.prod-qty'),
        prod_variants: $('.variants'),

        cart_add: $('#add-to-cart'),
        cart_tooltip: $('.cart-tooltip'),
        cart_tooltip_img: $('.cart-tooltip .image'),
        cart_tooltip_title: $('.cart-tooltip .title'),
        cart_tooltip_btn_shoppping: $('.cart-tooltip .shopping'),
        cart_tooltip_btn_wishlist: $('.cart-tooltip .wishlist'),

        cart_item_count: $('#cart-item-count'),

        td_total: $('td.total'),
        td_shipping: $('td.shipping'),
        td_breakdown: $('td.totalbreakdown'),

        no_cart_item: $('.no-cart-item'),
        btn_proceed: $('.btnproceed'),

        dtbl_wishlist: $('#tabledata-wishlist'),
        wishlist_add: $('.add-to-wishlist'),
        // prod_options: $("[id^=opt-]"),
    }

    dom.dtbl_wishlist.dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "pageLength": 8
    });

    dom.cart_add.click(function(e){
        e.preventDefault();
        var prod_variants = [];

        if($(this).hasClass('disabled')) {
            return false;
        }

        dom.prod_variants.each(function(){
            prod_variants.push($(this).val());
        });

        $.ajax({
            url: base_url + '/cart/add',
            data: { 
                prod_id: dom.prod_id.val(),
                qty: dom.prod_qty.val(),
                variants: prod_variants,
                _token: dom.token.val()
            }, 
            type: 'POST', 
            beforeSend: function() {
                CursorWait();
            }, complete: function() {
                CursorDefault();
            }, success: function(data) {
                try {
                    jn = JSON.parse(data);
                    if(jn.status == '1') {
                        dom.cart_tooltip_btn_wishlist.hide();
                        dom.cart_tooltip_btn_shoppping.show();
                        dom.cart_item_count.text(jn.cart_item_count);
                        dom.cart_tooltip_title.html(jn.title);
                        dom.cart_tooltip_img.empty().append(jn.image);
                        dom.cart_tooltip.fadeIn().delay(6000).fadeOut();
                    }

                } catch(e) {
                    console.log(ERR_DEFAULT);
                }
            }
        });
    });

    dom.prod_qty.change(function() {
        var that = $(this), 
            qty = that.val(),
            parent_tr = that.parent().parent();
            row_id = parent_tr.attr('id');

        if(row_id == undefined)
            return false;

        $.ajax({
            type: 'GET',
            url: base_url + '/cart/update',
            data: { row_id: row_id, qty: qty },
            beforeSend: function() {
                CursorWait();
            }, complete: function() {
                CursorDefault();
            }, success: function(ret) {
                try {
                    jn = JSON.parse(ret);

                    dom.td_total.text(jn.grand_subtotal);
                    dom.td_shipping.text(jn.shipping);
                    dom.td_breakdown.html(jn.grand_total);
                    parent_tr.find('td.subtotal').text(jn.item_subtotal);

                } catch(e) {
                    console.log(Messages.error.default);
                }
            }
        });
    });

    $(document).on('click', '.cart-item-del', function(e){
        e.preventDefault();

        var row_id = $(this).attr('row-id');

        $.ajax({
            type: 'GET',
            url: base_url + '/cart/delete',
            data: { row_id: row_id },
            beforeSend: function() {
                CursorWait();
            }, complete: function() {
                CursorDefault();
            }, success: function(ret) {
                try {
                    jn = JSON.parse(ret);
                    $('#'+ row_id).remove();
                    dom.td_total.text(jn.grand_subtotal);
                    dom.td_shipping.text(jn.shipping);
                    dom.td_breakdown.html(jn.grand_total);
                    dom.cart_item_count.text(jn.cart_item_count);

                    if(jn.cart_item_count == 0) {
                        dom.btn_proceed.addClass('disabled');
                        dom.no_cart_item.removeClass('hide');
                    }

                } catch(e) {
                    console.log(Messages.error.default)
                }
            }
        });
    });

    dom.btn_proceed.click(function(e){
        if($(this).hasClass('disabled')) {
            e.preventDefault();
            return false;
        }
    });

    dom.wishlist_add.click(function(e) {
        e.preventDefault;

        var variants = [],
            qty = dom.prod_qty.val(),
            prod_id = $(this).attr('prod-id');

        $.each(dom.prod_variants, function(){
            variants.push($(this).val());
        });

        var data = {
            qty: qty,
            prod_id: prod_id,
            variants: variants,
            _token: dom.token.val()
        };

        $.ajax({
            type: 'POST', data: data,
            url: base_url + '/wishlist/add',
            beforeSend: function() {
                CursorWait();
            }, complete: function() {
                CursorDefault();
            }, success: function(ret) {
                try {
                    jn = JSON.parse(ret);
                    if(jn.status == '1') {
                        dom.cart_tooltip_btn_wishlist.show();
                        dom.cart_tooltip_btn_shoppping.hide();
                        dom.cart_tooltip_title.html(jn.title);
                        dom.cart_tooltip_img.empty().append(jn.image);
                        dom.cart_tooltip.fadeIn().delay(6000).fadeOut();
                    }
                } catch(e) {
                    console.log(Messages.error.default);
                }
            }
        });

        return false;
    });

    $(document).on('click', '.wish-item-del', function(e){
        e.preventDefault;

        var table = dom.dtbl_wishlist.DataTable(),
            row_id = $(this).attr('row-id'),
            tr = $(this).closest('tr');

        $.ajax({
            type: 'GET',
            url: base_url + '/wishlist/remove',
            data: { row_id: row_id },
            beforeSend: function() {
                CursorWait();
            }, success: function(ret) {
                if(ret == '1') {
                    table.row(tr).remove();
                    tr.remove();
                }
                CursorDefault();
            }
        });

        return false;
    });

    $(document).on('click', '.wish-to-cart', function(e){
        e.preventDefault();

        var tr = $(this).closest('tr'),
            row_id = $(this).attr('row-id'),
            table = dom.dtbl_wishlist.DataTable();

        $.ajax({
            type: 'POST',
            url: base_url +'/wishlist/to-cart',
            data: { row_id: row_id, _token: dom.token.val() },
            beforeSend: function() {
                CursorWait();
            }, complete: function() {
                CursorDefault();
            }, success: function(ret) {
                try {
                    jn = JSON.parse(ret);
                    if(jn.status == '1') {
                        dom.cart_tooltip_btn_wishlist.hide();
                        dom.cart_tooltip_btn_shoppping.show();
                        dom.cart_tooltip_title.html(jn.title);
                        dom.cart_item_count.text(jn.cart_item_count);
                        dom.cart_tooltip_img.empty().append(jn.image);
                        dom.cart_tooltip.fadeIn().delay(6000).fadeOut();

                        table.row(tr).remove();
                        tr.remove();
                    }
                } catch(e) {
                    console.log(Messages.error.default);
                }
            }
        });
    });

    $(document).on('click', '.cart-to-wish', function(e){
        e.preventDefault();

        var tr = $(this).closest('tr'),
            row_id = $(this).attr('row-id'),
            table = dom.dtbl_wishlist.DataTable();

        $.ajax({
            type: 'POST',
            url: base_url +'/cart/to-wish',
            data: { row_id: row_id, _token: dom.token.val() },
            beforeSend: function() {
                CursorWait();
            }, complete: function() {
                CursorDefault();
            }, success: function(ret) {
                try {
                    jn = JSON.parse(ret);
                    if(jn.status == '1') {
                        dom.cart_tooltip_btn_wishlist.show();
                        dom.cart_tooltip_btn_shoppping.hide();
                        dom.cart_tooltip_title.html(jn.title);
                        dom.cart_item_count.text(jn.cart_item_count);
                        dom.cart_tooltip_img.empty().append(jn.image);
                        dom.cart_tooltip.fadeIn().delay(6000).fadeOut();

                        table.row(tr).remove();
                        tr.remove();
                    }
                } catch(e) {
                    console.log(Messages.error.default);
                }
            }
        });
    });

    return parent;
}( ShoppingJS || {}, jQuery));