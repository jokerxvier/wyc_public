<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
    protected $table = 'tbl_brands';
    public $timestamps = false;

    protected $fillable = [
        'store_id', 
        'name', 
        'slug',
        'image',
        'image_alt',
        'active',
    ];

    public static function getStoreBrands($store_id)
    {
        $data = self::where('store_id', '=', $store_id)->get();

        return ($data && count($data)) ? $data : false;
    }
}