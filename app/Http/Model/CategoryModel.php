<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = 'tbl_category';
    public $timestamps = false;

    protected $fillable = [
        'parent', 
        'name',
        'slug',
        'desc',
        'active',
        'icon',
        'img_primary',
        'img_home',
        'tree_parent',
        'store_id',
        'status',
    ];

    // sidebar cats are visible in almost all pages (like in product details, scroll down then cat popup shows)
    // so, doing the algo/logic inside this model instead of pasting these lines of codes per Controller

    public static function getSidebarCategoryLookups(&$out_tree, &$out_cats)
    {
        $arranged = [];
        $cats = CategoryModel::where('active', '=', 1)
            ->where('featured', '=', 1)
            ->get();
            
        self::arrangeTreetably(0, $cats, $arranged);
    
        foreach ($arranged as $category) {
            $cat_id = $category->id;

            $out_cats[$cat_id] = [
                'id' => $category->id,
                'slug' => $category->slug,
                'icon' => $category->icon,
                'name' => $category->name,
                'parent' => $category->parent,
                'img_home' => $category->img_home,
                'img_primary' => $category->img_primary,
                'tree_parent' => $category->tree_parent,
            ];
            
            if(!is_null($category->tree_parent)) {
                $tp = explode('-', $category->tree_parent);
                switch (count($tp)) {
                    case 1: $out_tree[$tp[0]][$cat_id] = []; break;
                    case 2: $out_tree[$tp[0]][$tp[1]][$cat_id] = []; break;
                    case 3: $out_tree[$tp[0]][$tp[1]][$tp[2]][$cat_id] = []; break; 
                    // current cats is up to 3dimentions only, extend if needed
                    // update: as per boss, we will limit cats at 3rd level deep only
                }
            }
        }
    }

    // -- getSidebarCategoryLookups helper / also used in category selection that needs tree (e.g. cat add page)

    public static function arrangeTreetably($parent_id, &$main_src, &$arranged_categories)
    {
        $new_src = [];
        foreach ($main_src as $k => $src) {

            if($src->parent == $parent_id) {
                $new_src[$k] = $src;
            }
        }
        foreach ($new_src as $k => $src) {
            $arranged_categories[$k] = $src; // push to dest
            unset($main_src[$k]); // pop  from src, less data = fast execution

            // call self, but this is in another mem space 
            self::arrangeTreetably($src->id, $main_src, $arranged_categories);
        }
    }

    // -- fetch active system category + user defined store category

    public static function getStoreCategory($store_id)
    {
        $query = CategoryModel::query();
        $query->where('active','=',1);

        // laravel where parenthesis
        $query->where(function($q) use($store_id) {
            $q->where('active','=', 1);
            $q->orWhere('store_id','=', $store_id);
        });

        return $query->get();
    }

}