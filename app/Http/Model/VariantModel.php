<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class VariantModel extends Model
{
    protected $table = 'tbl_variants';
    public $timestamps = false;

    protected $fillable = [
        'option_id', 
        'name',
        'weight',
        'active',
    ];

    public function option()
    {
        return $this->belongsTo('App\Http\Model\OptionModel', 'option_id');
    }
}