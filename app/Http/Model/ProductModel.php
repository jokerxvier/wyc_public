<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProductModel extends Model
{
    protected $table = 'tbl_products';
    public $timestamps = false;

    protected $fillable = [
        'store_id', 
        'brand_id', 
        'sku',
        'name', 
        'slug',
        'desc_short',
        'desc_long',
        'stock',
        'price',
        'price_sale',
        'discount',
        'currency',
        'weight',
        'min_order',
        'max_order',
        'sold_count',
        'views',
        'featured',
        'active',
        'image',
        'tax_class',
        'availability',
    ];

    public function images()
    {
        return $this->hasMany('App\Http\Model\ProductImageModel', 'product_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Http\Model\StoreModel', 'store_id');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\Http\Model\OrderDetailModel', 'product_id');
    }

    public static function getProductsCount()
    {
        $count = self::where('active', '=', 1)->count();

        return $count;
    }

    public static function getCategoryProduct($cat_id) 
    {
        $data = DB::table('tbl_products as prod')
                ->join('tbl_product_category as prod_cat', 'prod.id', '=', 'prod_cat.product_id')
                ->select('prod.*', 'prod_cat.*')
                ->where('prod_cat.cat_id', '=', $cat_id)
                ->get();

        return $data;
    }
}