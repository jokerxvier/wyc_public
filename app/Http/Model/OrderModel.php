<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    protected $table = 'tbl_orders';

    protected $fillable = [
        'mask',
        'group',
        'store_id',
        'invoice_id',
        'user_id',
        'total',
        'tax',
        'ship_fee',
        'status',
        'ship_date',
        'recieve_date',
        'tracking_num',
        'ship_address',
        'message',
    ];

    public function store()
    {
        return $this->belongsTo('App\Http\Model\StoreModel', 'store_id');
    }

    public function details()
    {
        return $this->hasMany('App\Http\Model\OrderDetailModel', 'order_id');
    }
}