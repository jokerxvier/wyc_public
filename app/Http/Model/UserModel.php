<?php

namespace App\Http\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class UserModel extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'tbl_users';

    protected $fillable = [
        'type',
        'email', 
        'password',
        'fname',
        'lname',
        'gender',
        'birthdate',
        'phone',
        'avatar',
        'active',
        'doc_sss',
        'doc_tin',
        'doc_pagibig',
        'doc_philhealth',
    ];

    protected $hidden = [
        'password', 
        'remember_token', 
        'created_at', 
        'updated_at'
    ];

    public function stores()
    {
        return $this->hasMany('App\Http\Model\StoreModel', 'id');
    }

    public function address()
    {
        return $this->hasMany('App\Http\Model\AddressBookModel', 'user_id');
    }

    public static function getUserCount($type = null)
    {
        $data = self::where('active', '=', 1);
        $data->where('type', '!=', 'admin');
        $data->where('type', '!=', 'support');

        if ($type) {
          $data->where('type', '=', $type); 
        }

        $data->count();

        return $data->count();
    }


    public static function getUserinfo($type = null, $limit = null) 
    {
        $data = self::where('active', '=', 1);

        if ($type) {
            $data->where('type', '=', $type); 
        }

        $data->orderBy('created_at', 'desc'); 

        if($limit){
            $data->take($limit); 
        }

        return $data->get();
    }

}
