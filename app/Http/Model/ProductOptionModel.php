<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class ProductOptionModel extends Model
{
    protected $table = 'tbl_product_options';
    public $timestamps = false;

    protected $fillable = [
        'product_id', 
        'option_id', 
    ];

    public function product()
    {
        return $this->belongsTo('App\Http\Model\ProductModel', 'product_id');
    }
}