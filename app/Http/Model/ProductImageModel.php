<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class ProductImageModel extends Model
{
    protected $table = 'tbl_product_images';
    public $timestamps = false;

    protected $fillable = [
        'product_id', 
        'image', 
    ];

    public function product()
    {
        return $this->belongsTo('App\Http\Model\ProductModel', 'product_id');
    }
}