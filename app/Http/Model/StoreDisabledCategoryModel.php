<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class StoreDisabledCategoryModel extends Model
{
    protected $table = 'tbl_store_disabled_category';
    public $timestamps = false;

    protected $fillable = [
        'cat_id',
        'store_id',
    ];
}