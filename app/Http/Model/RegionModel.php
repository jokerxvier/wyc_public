<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class RegionModel extends Model
{
    protected $table = 'tbl_region';

    protected $fillable = [
        'country_code', 
        'region_name'
    ];
}