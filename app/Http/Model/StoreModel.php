<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

use App\Http\Model\CategoryModel;
use App\Http\Model\StoreCategoryModel;

use DB;

class StoreModel extends Model
{
    protected $table = 'tbl_stores';

    protected $fillable = [
        'user_id', 
        'rate',
        'slug',
        'name',
        'desc',
        'address',
        'phone',
        'logo',
        'banner',
        'status',
        'doc_nbi',
        'doc_valid_id',
        'decline_reason',
    ];

    public function user()
    {
        return $this->belongsTo('App\Http\Model\UserModel', 'user_id');
    }

    public function products()
    {
        return $this->hasMany('App\Http\Model\ProductModel', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Http\Model\OrderModel', 'id');
    }

    public static function categoryCount($store_id) 
    {
        $store_cat = DB::table('tbl_category as cat')
                    ->join('tbl_store_category as store_cat', 'store_cat.cat_id', '=', 'cat.id')
                    ->select(DB::raw('SUM(store_cat.store_id) as total_count')) 
                    ->where('store_cat.store_id', '=', $store_id)
                    ->lists('total_count');

        return ($store_cat) ? $store_cat : false;
    }

    public static function productCount($store_id) {

        $data = DB::table('tbl_products as prod')
                ->select(DB::raw('SUM(prod.id) as total_count')) 
                ->where('prod.store_id', '=', $store_id)
                ->lists('total_count');

        return ($data) ? $data : false;
    }

    public static function product($store_id, $limit)
    {
        $data = DB::table('tbl_products as prod')
                ->select(DB::raw('prod.*'))
                ->orderBy('created_at', 'desc')
                ->where('store_id', '=', $store_id)
                ->take($limit)
                ->get();

        return $data;

    }


    public static function stores($status, $limit = null)
    {
        $data = self::where('status', '=', $status);
        $data = $data->orderBy('created_at', 'desc');

        if ($limit) {
            $data = $data->take($limit);
        }

        return $data->get();
    }

    public static function getStore($store_id)
    {
        $data = self::where('id', '=', $store_id)->where('status', '=', 'Active');

        return $data->first();
    }

    public static function getUserStore($store_id, $user_id)
    {
        $data = self::where('id', '=', $store_id)
                ->where('user_id', '=', $user_id)
                ->where('status', '=', 'Active');

        return $data->first();
    }




}