<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class ProductCategoryModel extends Model
{
    protected $table = 'tbl_product_category';
    public $timestamps = false;

    protected $fillable = [
        'product_id', 
        'cat_id', 
    ];

    public function product()
    {
        return $this->belongsTo('App\Http\Model\ProductModel', 'product_id');
    }
}