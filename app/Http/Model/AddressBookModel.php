<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class AddressBookModel extends Model
{
    protected $table = 'tbl_address_book';

    protected $fillable = [
        'user_id', 
        'type', 
        'street',
        'city',
        'region',
        'country',
        'zip',
    ];

    public function user()
    {
        return $this->belongsTo('App\Http\Model\UserModel', 'id');
    }
}