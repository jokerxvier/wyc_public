<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class OrderDetailModel extends Model
{
    protected $table = 'tbl_order_details';

    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'quantity',
        'variants',
        'price_per_item',
        'discount',
    ];

    public function order()
    {
        return $this->belongsTo('App\Http\Model\OrderModel', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Http\Model\ProductModel', 'product_id');
    }
}