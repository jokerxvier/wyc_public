<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class CountryModel extends Model
{
    protected $table = 'tbl_country';

    protected $fillable = [
    	'code',
        'country_name', 
    ];
}