<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;


class OptionModel extends Model
{
    protected $table = 'tbl_options';
    public $timestamps = false;

    protected $fillable = [
        'store_id', 
        'name_unique',
        'name_alt'
    ];

    public function variants()
    {
        return $this->hasMany('App\Http\Model\VariantModel', 'option_id');
    }

    public static function getstoreOption($store_id) 
    {
        $data = self::where('store_id', '=', $store_id)->get();

        return ($data && count($data) > 0) ? $data : false;
    }
}