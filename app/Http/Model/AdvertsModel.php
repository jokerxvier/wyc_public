<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class AdvertsModel extends Model
{
    protected $table = 'tbl_adverts';
    
    protected $fillable = [
        'cat_id', 
        'type_id', 
        'title', 
        'desc', 
        'image', 
        'onclick_url', 
        'start_at', 
        'end_at', 
        'approved_by',        
    ];
}