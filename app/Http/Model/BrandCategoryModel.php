<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class BrandCategoryModel extends Model
{
    protected $table = 'tbl_brand_category';
    public $timestamps = false;
    
    protected $fillable = [
        'brand_id', 
        'cat_id',
    ];
}