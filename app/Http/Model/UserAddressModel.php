<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class UserAddressModel extends Model
{
    protected $table = 'tbl_user_address';

    protected $fillable = [
        'user_id', 
        'addr_id',
    ];
}