<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class StoreCategoryModel extends Model
{
    protected $table = 'tbl_store_category';
    public $timestamps = false;
    
    protected $fillable = [
        'store_id', 
        'cat_id',
    ];
}