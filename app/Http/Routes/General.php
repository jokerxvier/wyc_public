<?php

Route::group(['namespace' => 'General'], function()
{
	Route::group(['prefix' => '/'], function()
	{	
		Route::get('/', ['as' => 'user.home', 'uses' => 'GeneralController@getHome']);
		Route::get('/coming-soon', function() {
			return view('pages.general.comingsoon');
		});

		Route::get('signup', ['as' => 'user.signup', 'uses' => 'AuthController@getSignUp']);
		Route::post('signup', ['as' => 'user.signup.p', 'uses' => 'AuthController@postSignUp']);
		Route::get('login', ['as' => 'user.login', 'uses' => 'AuthController@getLogin']);
		Route::post('login', ['as' => 'user.login.p', 'uses' => 'AuthController@postLogin']);
		Route::get('logout', ['as' => 'user.logout', 'uses' => 'AuthController@getLogout']);

		Route::get('pass-reset/{token}', ['as' => 'pass.reset', 'uses' => 'PasswordController@getReset']);
		Route::post('pass-reset', ['as' => 'pass.reset.p', 'uses' => 'PasswordController@postReset']);
		Route::get('pass-forgot', ['as' => 'pass.forgot', 'uses' => 'PasswordController@getEmail']);
		Route::post('pass-forgot', ['as' => 'pass.forgot.p', 'uses' => 'PasswordController@postEmail']);

		Route::post('store-doc-updload', ['as' => 'general.store.doc.upload', 'uses' => 'GeneralController@documentUpload']);
    	Route::post('store-doc-delete', ['as' => 'general.store.doc.delete', 'uses' => 'GeneralController@documentDelete']);
    	Route::post('brand-img-updload', ['as' => 'general.brand.img.upload', 'uses' => 'GeneralController@brandImageUpload']);
    	Route::post('brand-img-delete', ['as' => 'general.brand.img.delete', 'uses' => 'GeneralController@brandImageDelete']);

    	Route::post('generic-updload', ['as' => 'general.generic.upload', 'uses' => 'GeneralController@genericUpload']);
    	Route::post('generic-updload-delete', ['as' => 'general.generic.upload.delete', 'uses' => 'GeneralController@genericUploadDelete']);

    	Route::get('wishlist', ['as' => 'general.wishlist', 'uses' => 'WishlistController@index']);
    	Route::group(['prefix' => 'wishlist'], function()
		{
			Route::post('add', ['as' => 'general.wishlist.add', 'uses' => 'WishlistController@add']);
			Route::get('remove', ['as' => 'general.wishlist.remove', 'uses' => 'WishlistController@remove']);
			Route::POST('to-cart', ['as' => 'general.wishlist.to.cart', 'uses' => 'WishlistController@wishToCart']);
		});

	});
	
	// this is accessible by all user types

	Route::group(['prefix' => 'store'], function()
	{
		Route::post('/save', ['as' => 'general.store.save.p', 'uses' => 'GeneralController@postStore']);
		Route::get('/{slug}', ['as' => 'general.store.detail', 'uses' => 'StoreController@detail']);
	});

	Route::group(['prefix' => 'search'], function()
	{
		Route::post('/', ['as' => 'general.search', 'uses' => 'CategoryController@search']);
	});


	Route::group(['prefix' => 'category'], function()
	{
        Route::get('/', ['as' => 'category.slug','uses' => 'CategoryController@index']);
	    Route::get('/{cat_slug}', ['as' => 'category.slug','uses' => 'CategoryController@displayProducts']);
	});

	Route::group(['prefix' => 'product'], function()
	{
		Route::get('/detail/{slug}', ['as' => 'general.product.detail', 'uses' => 'ProductController@detail']);
	});

	Route::group(['prefix' => 'cart'], function()
	{
		Route::get('/', ['as' => 'general.cart', 'uses' => 'CartController@cart']);
		Route::post('/add', ['as' => 'general.cart.add', 'uses' => 'CartController@add']);
		Route::get('/update', ['as' => 'general.cart.update', 'uses' => 'CartController@update']);
		Route::get('/delete', ['as' => 'general.cart.delete', 'uses' => 'CartController@delete']);
		Route::post('/to-wish', ['as' => 'general.cart.to.wish', 'uses' => 'CartController@cartToWish']);
		
		Route::get('/checkout', ['as' => 'general.cart.checkout', 'uses' => 'CartController@checkout']);
		Route::get('/payout', ['as' => 'general.cart.payout', 'uses' => 'CartController@payout']);
		Route::post('/payout', ['as' => 'general.cart.payout.p', 'uses' => 'CartController@postPayout']);
		
		Route::get('/debug', ['as' => 'general.cart.debug', 'uses' => 'CartController@debug']);
	});

	

});