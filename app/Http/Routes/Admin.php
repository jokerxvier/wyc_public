<?php

Route::group(['namespace' => 'Admin'], function()
{
	Route::group(['prefix' => 'admin'], function()
	{
        Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);    
        Route::get('/categories', ['as' => 'admin.categories', 'uses' => 'CategoryController@index']);

        Route::group(['prefix' => 'category'], function()
		{
			Route::get('/add', ['as' => 'admin.category.add', 'uses' => 'CategoryController@add']);
			Route::get('/edit/{id}', ['as' => 'admin.category.edit', 'uses' => 'CategoryController@edit']);
			Route::post('/delete', ['as' => 'admin.category.delete', 'uses' => 'CategoryController@delete']);
			Route::post('/approve', ['as' => 'admin.category.approve', 'uses' => 'CategoryController@approve']);
			Route::post('/reject', ['as' => 'admin.category.reject', 'uses' => 'CategoryController@reject']);
			Route::post('/save', ['as' => 'admin.category.save.p', 'uses' => 'CategoryController@postCategory']);
	    });

    });
});