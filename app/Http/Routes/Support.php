<?php

Route::group(['namespace' => 'Support'], function()
{
	Route::group(['prefix' => 'support'], function()
	{
        Route::get('/', ['as' => 'support.dashboard', 'uses' => 'DashboardController@index']);
        Route::get('/users', ['as' => 'support.users', 'uses' => 'UserController@index']);
        Route::get('/stores', ['as' => 'support.stores', 'uses' => 'StoreController@index']);

        Route::group(['prefix' => 'store'], function()
	    {
		    Route::get('/view/{id}', ['as' => 'support.store.view', 'uses' => 'StoreController@view']);
		    Route::get('/page/{status}', ['as' => 'support.store.page', 'uses' => 'StoreController@page']);
		    Route::get('/ban', ['as' => 'support.store.ban.', 'uses' => 'StoreController@ban']);
		    Route::get('/approve', ['as' => 'support.store.approve.', 'uses' => 'StoreController@approve']);
		    Route::post('/decline', ['as' => 'support.store.decline.', 'uses' => 'StoreController@decline']);
		});

		Route::group(['prefix' => 'user'], function()
	    {
	    	Route::get('/view/{id}', ['as' => 'support.user.view', 'uses' => 'UserController@view']);
	    	Route::get('/page', ['as' => 'support.user.page', 'uses' => 'UserController@page']);
	    	Route::get('/add', ['as' => 'support.user.add', 'uses' => 'UserController@add']);
	    	Route::post('/ban-toggle', ['as' => 'support.user.ban.toggle', 'uses' => 'UserController@banToggle']);
	    	Route::post('/save', ['as' => 'support.user.save.p', 'uses' => 'UserController@postUser']);
	    	Route::get('/stores/{user_id}', ['as' => 'support.user.stores', 'uses' => 'UserController@userStores']);
	    });
    });
});