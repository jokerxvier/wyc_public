<?php

Route::group(['namespace' => 'User'], function()
{
	Route::group(['prefix' => 'user'], function()
	{
		Route::get('/profile', ['as' => 'user.profile', 'uses' => 'UserController@getProfile']);
		Route::post('/profile', ['as' => 'user.profile.p', 'uses' => 'UserController@postProfile']);
		Route::post('/change-pass', ['as' => 'user.changepass', 'uses' => 'UserController@changePass']);
		Route::get('/orders', ['as' => 'user.orders', 'uses' => 'UserController@orders']);

		Route::get('/addr-get', ['as' => 'user.addr.get', 'uses' => 'AddressController@get']);
        Route::post('/addr-insert', ['as' => 'user.addr.insert', 'uses' => 'AddressController@insert']);
        Route::post('/addr-delete', ['as' => 'user.addr.delete', 'uses' => 'AddressController@delete']);
        Route::post('/addr-update', ['as' => 'user.addr.update', 'uses' => 'AddressController@update']);

        Route::group(['prefix' => 'store'], function()
		{
			Route::get('/open', ['as' => 'user.store.new', 'uses' => 'StoreController@getOpenStore']);
		});

		Route::group(['prefix' => 'order'], function()
		{
			Route::get('/page', ['as' => 'user.order.page', 'uses' => 'OrderController@page']);
			Route::get('/detail/{mask}', ['as' => 'user.order.detail', 'uses' => 'OrderController@detail']);
			Route::post('/cancel', ['as' => 'user.order.cancel', 'uses' => 'OrderController@cancel']);
		});
	});

});