<?php

Route::group(['namespace' => 'Vendor'], function()
{
	Route::group(['prefix' => 'vendor'], function()
	{
        Route::get('/', ['as' => 'vendor.dashboard', 'uses' => 'DashboardController@index']);
        Route::get('/stores', ['as' => 'vendor.stores', 'uses' => 'StoreController@index']);

        Route::group(['prefix' => 'store'], function()
	    {
		    
		    Route::get('/page', ['as' => 'vendor.store.page', 'uses' => 'StoreController@page']);
		    Route::get('/add', ['as' => 'vendor.store.add', 'uses' => 'StoreController@add']);
		    Route::get('/edit/{id}', ['as' => 'vendor.store.edit', 'uses' => 'StoreController@edit']);

		    Route::group(['prefix' => '{store_id}'], function()
		    {	
		    	Route::get('/products', ['as' => 'vendor.store.products', 'uses' => 'ProductController@index']);
		    	Route::get('/dashboard', ['as' => 'vendor.store.dashboard', 'uses' => 'StoreController@dashboard']);
		    	Route::get('/brands', ['as' => 'vendor.store.brands', 'uses' => 'BrandController@index']);
		    	Route::get('/categories', ['as' => 'vendor.store.categories', 'uses' => 'CategoryController@index']);
		    	Route::get('/prod-options', ['as' => 'vendor.store.prod.options', 'uses' => 'ProductOptionController@index']);

			    Route::group(['prefix' => 'brand'], function()
			    {
			    	Route::get('/page', ['as' => 'vendor.store.brand.page', 'uses' => 'BrandController@page']);
			    	Route::get('/add', ['as' => 'vendor.store.brand.add', 'uses' => 'BrandController@add']);
			    	Route::get('/edit/{id}', ['as' => 'vendor.store.brand.edit', 'uses' => 'BrandController@edit']);
			    	Route::post('/save', ['as' => 'vendor.store.brand.save.p', 'uses' => 'BrandController@postBrand']);
			    	Route::post('/delete', ['as' => 'vendor.store.brand.delete', 'uses' => 'BrandController@delete']);
			    });

			    Route::group(['prefix' => 'product'], function()
			    {
			    	Route::get('/add', ['as' => 'vendor.store.product.add', 'uses' => 'ProductController@add']);
			    	Route::get('/page', ['as' => 'vendor.store.product.page', 'uses' => 'ProductController@page']);
			    	Route::post('/save', ['as' => 'vendor.store.product.post', 'uses' => 'ProductController@postProduct']);
			    	Route::post('/delete', ['as' => 'vendor.store.product.delete', 'uses' => 'ProductController@delete']);
			    	Route::get('/edit/{id}', ['as' => 'vendor.store.product.edit', 'uses' => 'ProductController@edit']);
			    });

			    Route::group(['prefix' => 'prod-option'], function()
			    {
			    	Route::get('/add', ['as' => 'vendor.store.prod.option.add', 'uses' => 'ProductOptionController@add']);
			    	Route::get('/page', ['as' => 'vendor.store.product.page', 'uses' => 'ProductOptionController@page']);
			    	Route::get('/edit/{id}', ['as' => 'vendor.store.prod.option.edit', 'uses' => 'ProductOptionController@edit']);
			    	Route::post('/save', ['as' => 'vendor.store.prod.option.save.p', 'uses' => 'ProductOptionController@postProductOption']);
			    	Route::post('/delete', ['as' => 'vendor.store.prod.option.delete.p', 'uses' => 'ProductOptionController@deleteOption']);
			    });

			    Route::post('/variant/delete', ['as' => 'vendor.store.prod.variant.delete.p', 'uses' => 'ProductOptionController@deleteVariant']);


			    Route::group(['prefix' => 'category'], function()
			    {
			    	Route::get('/add', ['as' => 'vendor.store.category.add', 'uses' => 'CategoryController@add']);
			    	Route::get('/edit/{id}', ['as' => 'vendor.store.category.edit', 'uses' => 'CategoryController@edit']);
			    	Route::post('/link', ['as' => 'vendor.store.category.link', 'uses' => 'CategoryController@link']);
			    	Route::post('/unlink', ['as' => 'vendor.store.category.unlink', 'uses' => 'CategoryController@unlink']);
			    	Route::post('/delete', ['as' => 'vendor.store.category.delete', 'uses' => 'CategoryController@delete']);
			    	Route::post('/save', ['as' => 'vendor.store.category.save.p', 'uses' => 'CategoryController@postCategory']);
			    });

		    });
		});
        
    });
});