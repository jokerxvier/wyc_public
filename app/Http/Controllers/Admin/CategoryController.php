<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\StoreDisabledCategoryModel;

use Auth, Request, Input;
use Validator;

class CategoryController  extends Controller
{
	protected $res = [];
    protected $user;
    protected $store_id;

    public function __construct()
    {
        if(!Common::filterUser(['admin']))
            return redirect('/login')->send();
        
        $this->user = Auth::user();

        $store = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();

        $this->res = [
            'user' => $this->user,
            'active_menu' => Request::segment(2),
        ];
    }

    public function index()
    {
    	$categories = CategoryModel::where('active', '=', 1)->get();
        $root_categories = $categories->where('parent', 0);


        // add for approval cats 
        $disabled_categories = CategoryModel::where('status','=','Pending')
            ->get();

        $this->res['categories'] = $categories;
        $this->res['root_categories'] = $root_categories;         // root cats onlu
        $this->res['disabled_categories'] = $disabled_categories; // list of inactive cats

        return view('pages.admin.categories', $this->res);
    }

    public function add()
    {
        $arranged_categories = [];
        $categories = CategoryModel::where('active', '=', 1)->get();
        CategoryModel::arrangeTreetably(0, $categories, $arranged_categories);

        $this->res['categories'] = $arranged_categories;
        return view('pages.admin.category-add', $this->res);
    }

    public function edit($cat_id)
    {
        $category = CategoryModel::find($cat_id);
        if(!$category)
            abort(404);

        $arranged_categories = [];
        $categories = CategoryModel::where('active', '=', 1)->get();
        CategoryModel::arrangeTreetably(0, $categories, $arranged_categories);

        $parent = CategoryModel::find($category->parent);

        $this->res['parent'] = $parent;
        $this->res['category'] = $category;
        $this->res['categories'] = $arranged_categories;
        return view('pages.admin.category-edit', $this->res);
    }

    public function postCategory()
    {
        $input = Input::all();

        $input['slug'] = str_slug($input['category-name'],'-');

        $rules = [
            'category-name' => 'required'
        ];

        if($input['cat-id'] > 0 ) {
            $rules['slug'] = 'unique:tbl_category,slug,'.$input['cat-id'];
        } else {
            $rules['slug'] = 'unique:tbl_category,slug';
        }

        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }

        if($input['cat-id'] > 0 ) {
            $category = CategoryModel::find($input['cat-id']);
            if(!$category)
                abor(404);

            $category->parent = $input['categories'][0];
            $category->name = $input['category-name'];
            $category->icon = $input['category-icon'];
            $category->slug = $input['slug'];
            $category->save();

        } else {
            CategoryModel::create([
                'parent' => $input['categories'][0], // we only allow 1 parent
                'name' => $input['category-name'],
                'icon' => $input['category-icon'],
                'slug' => $input['slug'],
                // 'img_primary' => $input['slug'],
                // 'img_home' => $input['slug'],
                'store_id' => 0,
            ]);
        }

        return redirect('admin/categories');
    }

    public function approve()
    {
        if(Request::ajax()){
            $input = Input::all();

            $category = CategoryModel::find($input['cat_id']);
            if($category) {
                $category->status = 'Approved';
                $category->active = 1;
                $category->save();
                return 1;
            }
        }

        return 0;
    }

    public function reject()
    {
        if(Request::ajax()){
            $input = Input::all();

            $category = CategoryModel::find($input['cat_id']);
            if($category) {
                $category->status = 'Rejected';
                $category->reject_reason = $input['reason'];
                $category->save();
                return 1;
            }
        }

        return 0;
    }

    public function delete()
    {
        if(Request::ajax()){
            $input = Input::all();

            $category = CategoryModel::find($input['cat_id']);
            if($category) {
                $category->delete();
                return 1;
            }
        }

        return 0;
    }
}