<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
// use App\Http\Model\BrandModel;
// use App\Http\Model\StoreCategoryModel;;
// use App\Http\Model\BrandCategoryModel;;
// use App\Http\Model\ProductModel;

use Auth, Request;


class DashboardController extends Controller
{
	protected $res = [];
    protected $user;

    public function __construct()
    {
        if(!Common::filterUser(['admin']))
            return redirect('/login')->send();
        
        $this->user = Auth::user();

        $store = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();

        $this->res = [
            'active_menu' => '',
            'active_store_id' => 0,
            'user' => $this->user,
            'stores' => $store->pluck('name','id'),
        ];
    }

    public function index()
    {
    	return view('pages.admin.dashboard', $this->res);
    }
}