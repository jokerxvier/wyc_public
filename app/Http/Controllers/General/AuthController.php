<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\ProductModel;

use Input, Validator, Auth;
use DB;

class AuthController extends Controller
{
	protected $res = [];

	public function __construct()
	{
		Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
	}

	public function getLogin()
	{
		if(Auth::check()) {
			$type = Auth::user()->type;
	        if( $type == 'admin') {
	            return redirect('/admin');
	        } else if ($type == 'support') {
	            return redirect('/support');
	        } else if( $type == 'vendor') {
	            return redirect('/vendor');
	        } else {
	        	return redirect('/user/profile');
	        }
		}
		
		return view('pages.user.login', $this->res);
	}

	public function getSignup()
	{
		return view('pages.user.signup', $this->res);
	}

	public function postSignup()
	{

		$input = Input::all();

		$validator =  Validator::make($input, [
            'email' => 'required|email|max:255|unique:tbl_users',
            'password' => 'required|confirmed|min:6',
            'fname' => 'required',
            'lname' => 'required',
            'gender' => 'required',
            'phone' => 'required',
            'terms' => 'required',
            
           // 'recaptcha_response_field' => 'required|recaptcha', // reCAPTCHA v1
            // 'g-recaptcha-response' => 'required', // reCAPTCHA v2
        ]);

		if($validator->fails()) {
			return redirect('/signup')
                ->withInput()
                ->withErrors($validator);
		}

		UserModel::create([
			'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'fname' => $input['fname'],
            'lname' => $input['lname'],
            'gender' => $input['gender'],
            'phone' => $input['phone'],
            'birthdate' => $input['birthdate'],            
        ]);

		if (Auth::attempt(['email' => $input['email'], 'password' => $input['password'], 'active' => 1])) {
			
			$type = Auth::user()->type;
	        if( $type == 'admin') {
	            return redirect('/admin');
	        } else if ($type == 'support') {
	            return redirect('/support');
	        } else if( $type == 'vendor') {
	            return redirect('/vendor');
	        } else {
	        	return redirect('/');
	        }

		} else {
			return redirect('/login')
				->withInput()
                ->withErrors([ 'These credentials do not match our records.']);
		}

        // return redirect('/login');

	}

	public function postLogin()
	{
		$input = Input::all();		
		$remember = (isset($input['remember']) ? $input['remember'] : 0);
		if (Auth::attempt(['email' => $input['email'], 'password' => $input['password'], 'active' => 1], $remember)) {
			
			if($input['from'] == 'checkout') {
				return redirect('cart/checkout');
			}

			$type = Auth::user()->type;
	        if( $type == 'admin') {
	            return redirect('/admin');
	        } else if ($type == 'support') {
	            return redirect('/support');
	        } else if( $type == 'vendor') {
	            return redirect('/vendor');
	        } else {
	        	return redirect('/');
	        }

		} else {
			return redirect('/login')
				->withInput()
                ->withErrors([ 'These credentials do not match our records.']);
		}
	}

	public function getLogout()
	{
		Auth::logout();
		return redirect('/login');
	}
}