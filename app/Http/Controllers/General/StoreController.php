<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
use App\Http\Model\BrandModel;
use App\Http\Model\StoreCategoryModel;;
use App\Http\Model\BrandCategoryModel;;
use App\Http\Model\ProductModel;

// use Auth, Input, Request, Validator, Image;


class StoreController extends Controller
{
	protected $res = [];

    public function __construct()
    {
        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }

    public function detail($slug)
    {
    	$store = StoreModel::where('slug','=', $slug)->get();

    	if(!$store)
    		abort(404);

    	// -- store
    	$store = $store->first();
		$this->res['store'] = $store;

        $products = [];
    	$store_products = ProductModel::where('store_id', '=', $store->id)->get();
        foreach ($store_products as $prod){
            $products[$prod->id] = $prod;
        }


        // -- brands lookup

        $brands_all = BrandModel::where('store_id','=', $store->id)
        	->where('active', '=', 1)
        	->get();

        $brands = [];
        foreach ($brands_all as $brand)
            $brands[$brand->id] = $brand;

        $parents = collect(StoreCategoryModel::where('store_id', '=', $store->id)->get())->pluck('cat_id');
        $brand_cats_all = BrandCategoryModel::whereIN('cat_id', $parents)->get();
        foreach ($brand_cats_all as $v) {
            if(isset($brands[$v->brand_id]))
                $this->res['brands'][] = $brands[$v->brand_id];
        }

        // --

        $cat_ids = $parents;  // cat ids to display per section
        // dd($cat_ids);

        $brands = [];
        $stores = [];
        $sections = [];
        $brand_cats = [];
        $store_cats = [];

        $brands_all = BrandModel::where('active', '=', 1)
        	->where('store_id','=', $store->id)
        	->get();

        $stores_all = StoreModel::where('status', '=', 'Active')
        	->get();

        $brand_cats_all = BrandCategoryModel::whereIn('cat_id', $cat_ids)->get();
        $store_cats_all = StoreCategoryModel::whereIn('cat_id', $cat_ids)->get();

        foreach ($brands_all as $brand)
            $brands[$brand->id] = $brand;
        foreach ($stores_all as $store)
            $stores[$store->id] = $store;
        
        foreach ($brand_cats_all as $v) {
            if(isset($brands[$v->brand_id]))
                $brand_cats[$v->cat_id][] = $brands[$v->brand_id];
        }
        
        foreach ($store_cats_all as $v) {
        	if(isset($stores[$v->store_id]))
            	$store_cats[$v->cat_id][] = $stores[$v->store_id];
        }

        foreach ($cat_ids as $index => $cat_id)
        {
            $section = [];
            $section['root_cat'] = $this->res['categories'][$cat_id];

            if(isset($brand_cats[$cat_id]))
                $section['brands'] = $brand_cats[$cat_id];
            else 
                $section['brands'] = [];
            
            $section['banner_ads'] = []; //array_slice($this->adverts[1][$cat_id], 0, 10);

            // limit stores to display to 2
            $ctr = 0;

            if(isset($store_cats[$cat_id])) {
                foreach ($store_cats[$cat_id] as $store) {
                    $ctr++;
                    if($ctr > 2)
                        break;

                    $section['stores'][] = $store;
                }
            }
            
            $cat_cats = [];
            $sub_cats = [];
            $sub_cats_ix = $this->res['tree_order'][$cat_id];
            foreach ($sub_cats_ix as $k => $v) {
                $sub_cats[] = $this->res['categories'][$k];
                $cat_cats += $v;
            }

            $popular_cats = [];
            foreach ($cat_cats as $k => $v)
                $popular_cats[] = $this->res['categories'][$k];

            shuffle($popular_cats);
            $section['sub_cats'] = collect($sub_cats)->slice(0,4);
            $section['popular_cats'] = array_slice($popular_cats, 0, 14);

            if($products) {
                if($index % 2 == 0) {
                    shuffle($products);
                    $section['best_seller'] = array_slice($products, 0, 2);
                } else {
                    shuffle($products);
                    $section['recommended_products'] =  array_slice($products, 0, 2);
                }
            }
            

            $this->res['sections'][] = $section;
        }
    	
    	return view('pages.general.store', $this->res);
    }
}

