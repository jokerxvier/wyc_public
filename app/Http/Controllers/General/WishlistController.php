<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\ProductModel;
use App\Http\Model\VariantModel;

// use App\Http\Model\CityModel;
// use App\Http\Model\RegionModel;
// use App\Http\Model\AddressBookModel;

// use App\Http\Model\OrderModel;
// use App\Http\Model\OrderDetailModel;

use Cart, Session;
use Input, Request;

class WishlistController extends Controller
{
	protected $res = [];

    public function __construct()
    {	
    	Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }

    public function index()
    {
    	$this->res['wishlist'] = Cart::instance('wishlist')->content();
        return view('pages.general.wishlist', $this->res);
    }

    public function add()
    {
    	$ret['status'] = 0;

        if(Request::ajax()) {
            $variants = [];
            $input = Input::all();
            $product = ProductModel::find($input['prod_id']);
        
            if($product) {
                if($product->discount > 0) 
                    $product->price = $product->price_sale;

                $variants = [];
	            if(isset($input['variants'])) {
	                $variants = VariantModel::whereIn('id', $input['variants'])->get();
	                $variants = $variants->toArray();
	            }

                Cart::instance('wishlist')->associate('ProductModel', 'App\Http\Model')
                    ->add($product->id, $product->name, (int)$input['qty'], (float)$product->price, $variants);

                $ret['status'] = 1;
	            $ret['image'] = '<img src="'. asset('uploads/'.$product->image) .'" alt="'. $product->name .'"/>';
	            $ret['title'] = '<strong>'. $product->name .'</strong> has been added to your wishlist.';
            }
        }

        return json_encode($ret);
    }

    public function remove()
    {
        if(Request::ajax()) {
            $input = Input::all();
            Cart::instance('wishlist')->remove($input['row_id']);
            Session::save();
            return 1;
        }

        return 0;
    }

    public function wishToCart()
    {
    	$ret['status'] = 0;

        if(Request::ajax()) {
            $input = Input::all();
            $item = Cart::instance('wishlist')->get($input['row_id']);

            $options = [];
            if($item) {
                foreach ($item->options as $option) {
                    $options[] = $option;
                }
                Cart::instance('shopping')->associate('ProductModel', 'App\Http\Model')
                    ->add($item->id, $item->name, $item->qty, (float)$item->price, $options);

                Cart::instance('wishlist')->remove($input['row_id']);
                Session::save();

                $product = ProductModel::find($item->id);

                $ret['status'] = 1;
	            $ret['image'] = '<img src="'. asset('uploads/'.$product->image) .'" alt="'. $product->name .'"/>';
	            $ret['title'] = '<strong>'. $product->name .'</strong> has been added to your cart.';
                $ret['cart_item_count'] = Cart::instance('shopping')->count(false);
            }
        }

        return json_encode($ret);
    }
}