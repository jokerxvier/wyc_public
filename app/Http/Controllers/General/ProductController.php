<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\OptionModel;
use App\Http\Model\ProductModel;
use App\Http\Model\ProductOptionModel;

use Cookie;

class ProductController extends Controller
{
	protected $res = [];

    public function __construct()
    {
        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }

    public function detail($slug)
    {
    	$product = ProductModel::where('slug','=', $slug)->get();
    	if($product->count() == 0)
    		abort(404);

    	$product = $product->first();
        $product_options = ProductOptionModel::where('product_id','=', $product->id)->get();
        $product_options = $product_options->pluck('option_id');

        $options = OptionModel::whereIn('id', $product_options)->get();
        

    	// -- saving recently viewed item --
        
        $in_recent = false;
        $recent_count = 0;

        $this->res['recent'] = json_decode(Cookie::get('recent_viewed'));
        $recent_count = count($this->res['recent']);

        if($recent_count > 3) {
            array_shift($this->res['recent']);
        }
        
        if($recent_count > 0) {
            foreach ($this->res['recent'] as $prod) {
                if($prod->slug == $product->slug) {
                    $in_recent = true;
                    break;
                }
            }
        }

        if(!$in_recent) {
            $new = (object)null;
            $new->slug = $product->slug;
            $new->name = $product->name;
            $new->image = $product->image;
            $new->desc_short = str_limit($product->desc_short, $limit = 60, $end = '...');
            $new->price = $product->discount > 0 ? $product->price_sale : $product->price;
            $this->res['recent'][] = $new;
        }

        $this->res['product'] = $product;
    	$this->res['options'] = $options;

    	return response()->view('pages.general.product', $this->res)
            ->withCookie(cookie('recent_viewed', json_encode($this->res['recent']), 240));
    }

}