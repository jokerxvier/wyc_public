<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\ProductModel;
use App\Http\Model\VariantModel;

use App\Http\Model\CityModel;
use App\Http\Model\RegionModel;
use App\Http\Model\AddressBookModel;

use App\Http\Model\OrderModel;
use App\Http\Model\OrderDetailModel;

use Cart, Session;
use Input, Request, Auth;

class CartController extends Controller
{
	protected $res = [];

    public function __construct()
    {
        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }

    private function fillCartInfo()
    {
        $cart = Cart::instance('shopping');
        $this->res['grand_shipping'] = 0;
        $this->res['cart'] = $cart->content();
        $this->res['grand_subtotal'] = $cart->total();
        $this->res['cart_item_count'] = $cart->count(false);
        $this->res['grand_total'] = $this->res['grand_shipping'] + $this->res['grand_subtotal'];
    }

    private function fillAddress($user_id)
    {
        $arr = [
            'default' => '',
            'billing' => '',
            'shipping' => '',
        ];

        $addrs = AddressBookModel::where('user_id','=',$user_id)->get();
            
        if($addrs)
        {
            foreach ($addrs as $addr) {
                $str_address = $addr->street.', '.
                    $addr->city.', '.
                    $addr->region.', '.
                    strtoupper($addr->country).' '.
                    $addr->zip;

                if($addr->type == 'Default') {
                    $arr['default'] = $str_address;
                } else if($addr->type == 'Shipping') {
                    $arr['billing'] = $str_address;
                } else if($addr->type == 'Billing') {
                    $arr['shipping'] = $str_address;
                }
            }

            if($arr['billing'] == '') $arr['billing'] = $arr['default'];
            if($arr['shipping'] == '') $arr['shipping'] = $arr['default'];
        }

        return $arr;
    }

    public function add()
    {
        $ret = [];

    	if(Request::ajax()) {
    		$input = Input::all();
                		
            $product = ProductModel::find($input['prod_id']);
            if(!$product)
                abort(404);

            if($product->discount > 0) 
                $product->price = $product->price_sale;

            $variants = [];
            if(isset($input['variants'])) {
                $variants = VariantModel::whereIn('id', $input['variants'])->get();
                $variants = $variants->toArray();
            }

            $cart = Cart::instance('shopping');
            $cart->associate('ProductModel', 'App\Http\Model')
                ->add($product->id, $product->name, (int)$input['qty'], (float)$product->price, $variants);

            $ret['status'] = 1;
            $ret['image'] = '<img src="'. asset('uploads/'.$product->image) .'" alt="'. $product->name .'"/>';
            $ret['title'] = '<strong>'. $product->name .'</strong> has been added to your cart.';
            $ret['cart_item_count'] = $cart->count(false);
    	}

        return json_encode($ret);
    }

    public function cart()
    {
        $cart = Cart::instance('shopping');
        $this->res['grand_shipping'] = 0;
        $this->res['cart'] = $cart->content();
        $this->res['grand_subtotal'] = $cart->total();
        $this->res['cart_item_count'] = $cart->count(false);
        $this->res['grand_total'] = $this->res['grand_shipping'] + $this->res['grand_subtotal'];
        return view('pages.general.cart', $this->res);
    }

    public function update()
    {
        if(Request::ajax()) {
            $input = Input::all();
            
            $cart = Cart::instance('shopping');
            $cart = $cart->update($input['row_id'], $input['qty']);
            Session::save();

            $item = $cart->get($input['row_id']);

            $shipping = 0;
            $subtotal = Cart::instance('shopping')->total();

            return json_encode([
                'shipping' => '₱ '.number_format($shipping, 2),
                'item_subtotal' => '₱ '.number_format($item->subtotal, 2),
                'grand_subtotal' => '₱ '.number_format($subtotal, 2),
                'grand_total' => '₱ '.number_format($shipping + $subtotal, 2).'<br><small>VAT incl.</small>',
            ]);
        }
    }

    public function delete()
    {
        if(Request::ajax()) {
            $input = Input::all();
            
            Cart::instance('shopping')->remove($input['row_id']);
            Session::save();

            $shipping = 0;
            $subtotal = Cart::instance('shopping')->total();

            return json_encode([
                'shipping' => '₱ '.number_format($shipping, 2),
                'grand_subtotal' => '₱ '.number_format($subtotal, 2),
                'grand_total' => '₱ '.number_format($shipping + $subtotal, 2) .'<br><small>VAT incl.</small>',
                'cart_item_count' => Cart::instance('shopping')->count(false),
            ]);
        }
    }

    public function checkout()
    {
        $current_city = 0;
        $current_region = 0;

        $this->fillCartInfo();
        
        
        $this->res['default'] = '';
        $this->res['billing'] = '';
        $this->res['shipping'] = '';

        if(Auth::check())
        {
            $user = Auth::user();
            $address = $this->fillAddress($user->id);

            $this->res['default'] = $address['default'];
            $this->res['billing'] = $address['billing'];
            $this->res['shipping'] = $address['shipping'];

            $this->res['email'] = $user->email;
            $this->res['name'] = $user->fname .' '. $user->lname;
            $this->res['phone'] = $user->phone;

            return view('pages.general.checkout', $this->res);

        } else {
            return view('pages.general.checkout-guest', $this->res);
        }

    }

    public function payout()
    {
        if(!Auth::check())
            return redirect('login')->send();

        $user = Auth::user();

        Session::put('message', '');
        Session::put('user_id', $user->id);

        $this->res['grand_total'] = Cart::instance('shopping')->total(); // add shipping fee here
        $this->res['items'] = Cart::instance('shopping')->content();
        return view('pages.general.payout', $this->res);
    }

    public function postPayOut()
    {
        return $this->placeOrder();
    }

    private function placeOrder()
    {   
        $user_id = Session::get('user_id');
        $message = Session::get('message');

        // we need to group items by store. if each item will be pulled 
        // from different stores, we cannot put all items in one order
        // the issue here is the delivery since each store will send it
        // individually, each of them has its delivery tracking number,
        // shipment status, and invoice (<-- this one must be in per store basis)
        
        $orders = [];
        $store_items = [];

        $grand_total = 0;
        $grand_shipping = 0;
        $grand_subtotal = 0;

        $group = '';

        foreach (Cart::instance('shopping')->content() as $item) {
            $id = $item->productmodel->store_id;
            
            if(!isset($store_items[$id]['total'])) {
                $store_items[$id]['total'] = $item->subtotal;
            } else {
                $store_items[$id]['total'] += $item->subtotal;
            }

            $store_items[$id]['items'][] = $item;
        }
        
        $address = $this->fillAddress($user_id);

        
        $mask_rnd_token = mt_rand(41,70);

        foreach ($store_items as $store_id => $items)
        {
            $store_tax = 0; // tax calc function here
            $store_shipping = 0; // sipping function here
            $grand_shipping += $store_shipping;
            $store_total = $store_shipping + $items['total'];

            $order = OrderModel::create([
                'user_id' => $user_id,
                'store_id' => $store_id,
                'total' => $store_total,
                'tax' => $store_tax,
                'ship_fee' => $store_shipping,
                'ship_address' => $address['shipping'],
                'message' => $message,
                'status' => 'Pending',
                
            ]);

            // this is just aesthetics, making order id not obviously guessable
            $mask = $order->id .'A'. $mask_rnd_token;
            $mask =  base_convert($mask, 11, 10);

            // order grouping. we do it here becase we need its unique generated id
            if(count($store_items) > 1 && $group == '') {
                $group = $order->id .'A'. mt_rand(71,99).'B';
                $group = base_convert($group, 12, 10);
                // using base12 here to make $mask and $group fields unique
                // and will give ease in searching orders or group orders
            }

            $order->mask = $mask;
            $order->group = $group;
            $order->save();

            foreach ($items['items'] as $item)
            {
                $order_detail = OrderDetailModel::create([
                    'order_id' => $order->id,
                    'product_id' => $item->id,
                    'product_name' => $item->name,
                    'quantity' => $item->qty,
                    'price_per_item' => $item->price,
                    'total_price' => $item->subtotal,
                    'variants' => $item->options->pluck('name')->implode(' | '),
                    'discount' => 0,
                ]);

            }

            $orders[] = $order->order_id;
        }

        if($group != '') {
            $this->res['order_num'] = $group;
        } else {
            $this->res['order_num'] = $mask;
        }
        

        // $this->res['total'] = $total;
        $this->res['items'] = Cart::instance('shopping')->content();
        $this->res['grand_shipping'] = $grand_shipping;
        $this->res['grand_subtotal'] = Cart::instance('shopping')->total();
        $this->res['grand_total'] = $grand_shipping + Cart::instance('shopping')->total();
        $this->res['cart_item_count'] = 0;

        Cart::instance('shopping')->destroy();
        Session::save();
        
        return view('pages.general.order-review', $this->res);
    }

    public function debug()
    {
        Cart::instance('wishlist')->destroy();
        Cart::instance('shopping')->destroy();
        Session::save();

        // dd(Cart::instance('wishlist')->content());
        // dd(base_convert("123252A211", 11, 10));
        // dd((mt_rand(6,10)));
    }


    public function cartToWish()
    {
        $ret['status'] = 0;

        if(Request::ajax()) {
            $input = Input::all();
            $item = Cart::instance('shopping')->get($input['row_id']);

            $options = [];
            if($item) {
                foreach ($item->options as $option) {
                    $options[] = $option;
                }
                Cart::instance('wishlist')->associate('ProductModel', 'App\Http\Model')
                    ->add($item->id, $item->name, $item->qty, (float)$item->price, $options);

                Cart::instance('shopping')->remove($input['row_id']);
                Session::save();

                $product = ProductModel::find($item->id);

                $ret['status'] = 1;
                $ret['image'] = '<img src="'. asset('uploads/'.$product->image) .'" alt="'. $product->name .'"/>';
                $ret['title'] = '<strong>'. $product->name .'</strong> has been added to your wishlist.';
                $ret['cart_item_count'] = Cart::instance('shopping')->count(false);
            }
        }

        return json_encode($ret);
    }
}
