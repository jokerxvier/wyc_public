<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\StoreCategoryModel;

use App\Http\Model\AdvertsModel;
use App\Http\Model\ProductModel;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use Auth, Input, Request, Validator, Image;
use DateTime, DB;

class CategoryController extends Controller
{

    protected $res = [];
    protected $offset = 0;
    protected $per_page = 12;
    protected $page;
    protected $params_page;

    private $tree = [];
    private $categories = [];
    private $adverts = [];

    private $keyword;
    private $sort;
    private $dir;

    public function __construct()
    {
        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
        
    }

    public function index() {
        $this->page = Input::get('page', 1); 
        $arg = [];
        $cats = CategoryModel::where('tree_parent', '=', NULL)->get();

        foreach($cats as $cat) {
            $arg[] = array(
                'name' => str_limit($cat->name, $limit = 30, $end = '...'),
                'slug' => $cat->slug,
                'sku' => $cat->sku,
                'image' => $cat->img_primary,
                'desc' => str_limit($cat->desc, $limit = 50, $end = '...'),
            );

        }



        $items_for_current = array_slice($arg, $this->offset, $this->per_page, true);


        $this->res['cat'] = new LengthAwarePaginator($items_for_current, count($arg),
           $this->per_page, $this->page, ['path' => Paginator::resolveCurrentPath()]);



        return view('pages.general.category', $this->res);

    }

    public function displayProducts($slug = null)
    {   

        $this->page = Input::get('page', 1); 
        $arg = [];
        $collect_cats = [];
        $collect_prods = [];
        $cat = CategoryModel::where('slug', '=', $slug)->first();

        $getCats = CategoryModel::where('tree_parent', 'LIKE', '%'.$cat->id.'%')->groupBy('id')->get(); //use like instead of getting all the category for faster loading time
        $collect_cats[] = $cat->id; //include parent to search

        if ($getCats && count($getCats) > 0) 
        {
            foreach ($getCats as $val) {
                $c = explode('-', $val->tree_parent);
                
                if (count($c) > 0) {
                    foreach($c as $ck => $cv) 
                    {
                        if ($cv == $cat->id)
                            array_push($collect_cats, $val->id);
                    }
                }
            }

        }
        
        if (count($collect_cats) > 0 ) {
            $result = array_unique($collect_cats);
            foreach ($result as $k => $v) {
                if  (count(ProductModel::getCategoryProduct($v)) > 0)
                    $collect_prods[] =  ProductModel::getCategoryProduct($v);
            }

        }else {
            if (count(ProductModel::getCategoryProduct($cat->id)) > 0)
                $collect_prods[] =  ProductModel::getCategoryProduct($cat->id);
        }


        foreach($collect_prods as $prod_k => $prod_v) {
            foreach($prod_v as $p) {
                $arg[] = array(
                    'name' => str_limit($p->name, $limit = 30, $end = '...'),
                    'slug' => $p->slug,
                    'sku' => $p->sku,
                    'brand_id' => $p->brand_id,
                    'image' => $p->image,
                    'short_desc' => str_limit($p->desc_short, $limit = 50, $end = '...'),
                    'store_id' => $p->store_id,
                    'price' => $p->price,
                    'currency' => $p->currency,
                    'sale_price' => $p->price_sale,
                    'discount' => $p->discount,
                );

            }
        }

        $items_for_current = array_slice($arg, $this->offset, $this->per_page, true);


        $this->res['products'] = new LengthAwarePaginator($items_for_current, count($arg),
           $this->per_page, $this->page, ['path' => Paginator::resolveCurrentPath()]);

        return view('pages.general.product-list', $this->res);

    }


    public function search() 
    {
        $prod = '';
        $keyword = (Input::has('q')) ?  Input::get('q') : false;
        if ($keyword)  {
            $prods = ProductModel::where('name', 'LIKE', '%' . $keyword .'%')->get();
        }else {
            $prods = ProductModel::all();
        }

        foreach($prods as $p) {
            $arg[] = array(
                'name' => str_limit($p->name, $limit = 30, $end = '...'),
                'slug' => $p->slug,
                'sku' => $p->sku,
                'brand_id' => $p->brand_id,
                'image' => $p->image,
                'short_desc' => str_limit($p->desc_short, $limit = 50, $end = '...'),
                'store_id' => $p->store_id,
                'price' => $p->price,
                'currency' => $p->currency,
                'sale_price' => $p->price_sale,
                'discount' => $p->discount,
            );

        }


        $items_for_current = array_slice($arg, $this->offset, $this->per_page, true);


        $this->res['products'] = new LengthAwarePaginator($items_for_current, count($arg),
           $this->per_page, $this->page, ['path' => Paginator::resolveCurrentPath()]);

        return view('pages.general.product-list', $this->res);

    }

    
}