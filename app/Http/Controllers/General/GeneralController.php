<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\StoreCategoryModel;

use App\Http\Model\AdvertsModel;
use App\Http\Model\ProductModel;

use Auth, Input, Request, Validator, Image;
use DateTime, DB;

class GeneralController extends Controller
{
	protected $res = [];

    public function __construct()
    {
        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }

    public function getHome()
    {
        $today = new DateTime('today');
        $adverts_all = AdvertsModel::where('end_at', '>=', $today)
            ->where('start_at', '<=', $today)
            ->orderBy(DB::raw('RAND()'))
            ->get();
        
        $this->res['ads_main_slide'] = $adverts_all->where('type_id', 1);
        $this->res['ads_latest_prod'] = $adverts_all->where('type_id', 2)->slice(0,3);
        $this->res['ads_feature_shop'] = $adverts_all->where('type_id', 3);

        return view('pages.user.home', $this->res);
    }

	public function documentUpload()
    {
        $extension = Input::file('file-doc')->getClientOriginalExtension();

        $doc_type = Input::get('doc-type');
        $file_name = uniqid().'.'.$extension;

        $ret['status'] = 0;
        $ret['allowed'] = 0;
        $ret['type'] = $doc_type;
        $ret['file_name'] = $file_name;

        // this one needs special resizing since logo and banner has a strict size to follow
        if(in_array($doc_type, ['file-banner','file-logo'])) {
        	$ret['status'] = 1;

        	if(in_array($extension, ['jpg','jpeg','png','bmp'])) {
        		$ret['allowed'] = 1;
        		

        		$asset_path = asset('uploads/'.$file_name);
		        $dest_path = public_path('uploads/');
		        $dest_path .= $file_name;

		        if($doc_type == 'file-banner') {
		        	$image = Image::make(Input::file('file-doc'))->fit(847, 277)->save($dest_path);
		        	$ret['img'] = '<img style="width: 100%;" src="'. $asset_path .'" />';
		        } else if($doc_type == 'file-logo') {
		        	$image = Image::make(Input::file('file-doc'))->fit(41, 41)->save($dest_path);
		        	$ret['img'] = '<img style="width: inherit;" src="'. $asset_path .'" />';
		        }

        	} else {
        		$ret['msg'] = 'Selected file is not allowed.';
        	}

        } else {

	        if(in_array($extension, ['jpg','jpeg','png','bmp','pdf','doc','docx']))
	        {
		        $dest_path = public_path('uploads/');

		        Input::file('file-doc')->move($dest_path, $file_name);

	        	$ret['status'] = 2;
	        	$ret['allowed'] = 1;
	        	$ret['path'] = asset('uploads/'.$file_name);
	        	

	        } else {
	        	$ret['msg'] = 'Selected file is not allowed.';
	        }
	    }

        return json_encode($ret);
    }

    public function documentDelete()
    {
    	if(Request::ajax()) {
    		$input = Input::all();

    		$path = public_path('uploads/');
    		$file = $path.$input['file'];
    		if(file_exists($file) && is_file($file)) {

                if(isset($input['type'])) {
                    $where_field = '';

                    switch ($input['type']) {
                        case 'file-logo': 
                            $store = StoreModel::where('logo', '=', $input['file'])->update(['logo' => '']);
                            break;
                        case 'file-banner':
                            $store = StoreModel::where('banner', '=', $input['file'])->update(['banner' => '']);
                            break;
                        case 'file-nbi': 
                            $store = StoreModel::where('doc_nbi', '=', $input['file'])->update(['doc_nbi' => '']);
                            break;
                        case 'file-valid-id':
                            $store = StoreModel::where('doc_valid_id', '=', $input['file'])->update(['doc_valid_id' => '']);
                            break;
                        default: break;
                    }
                }

    			unlink($file);
				return 1;
    		}
    	}

    	return 0;
    }

    public function genericUpload()
    {
        $input = Input::all();

        $extension = Input::file('file')->getClientOriginalExtension();
        $file_name = uniqid().'.'.$extension;
        $img_type = Input::get('img-type');

        $image_path = asset('uploads/'.$file_name);
        $dest_path = public_path('uploads/');
        $dest_path .= $file_name;

        $ret['allowed'] = 0;
        $ret['preview'] = 1;
        $ret['link'] = $input['link'];      // link to Main form hidden text
        $ret['style'] = $input['style'];    // preview displaying style
        $ret['file_name'] = $file_name;
        $ret['img_src'] = $image_path;

        if($input['type'] == 'images') {
            $filter = ['jpg','jpeg','png','bmp'];
        } else {
            $filter = ['jpg','jpeg','png','bmp','pdf','doc','docx'];
        }

        foreach ($input['action'] as $k => $value) {
            if($k == 0) {
                $action = $value;
            } else if($k == 1) {
                $width = $value;
            } else if($k == 2) {
                $height = $value;
            }
        }

        if(in_array($extension, $filter)) {

            $ret['allowed'] = 1;
            
            if($action == 'fit') {
                Image::make(Input::file('file'))->fit($width, $height)->save($dest_path);
            } else if($action == 'move') {
                Input::file('file')->move(public_path('uploads/'), $file_name);
                $ret['preview'] = 0;
            }
        }

        return json_encode($ret);
    }

    public function genericUploadDelete()
    {
        if(Request::ajax()) {
            $input = Input::all();

            $path = public_path('uploads/');
            $file = $path.$input['file'];

            if(file_exists($file) && is_file($file)) {
                unlink($file);
                return 1;
            }
        }

        return 0;
    }

    public function brandImageUpload()
    {
        $extension = Input::file('img-file')->getClientOriginalExtension();
        $file_name = uniqid().'.'.$extension;
        $img_type = Input::get('img-type');

        $ret['allowed'] = 0;
        $ret['type'] = $img_type;
        $ret['file_name'] = $file_name;

        if(in_array($extension, ['jpg','jpeg','png','bmp'])) {
            $ret['allowed'] = 1;

            $asset_path = asset('uploads/'.$file_name);
            $dest_path = public_path('uploads/');
            $dest_path .= $file_name;

            if($img_type == 'file-image') {
                $image = Image::make(Input::file('img-file'))->fit(130, 48)->save($dest_path);
            } elseif($img_type == 'file-image-alt') {
                $image = Image::make(Input::file('img-file'))->fit(100, 26)->save($dest_path);
            }

            $ret['img'] = '<img style="width: inherit;" src="'. $asset_path .'" />';
        }

        return json_encode($ret);
    }

    public function brandImageDelete()
    {
        if(Request::ajax()) {
            $input = Input::all();

            $path = public_path('uploads/');
            $file = $path.$input['file'];

            if(file_exists($file) && is_file($file)) {
                unlink($file);
                return 1;
            }
        }

        return 0;
    }

    // why am i here at General?
    // this function will be used by all user types. thats it thank you
    public function postStore_v1()
    {
        if(!Auth::check()) 
            return redirect('/login');

        $input = Input::all();
        $user = Auth::user();

        $store_id = $input['store-id'];
        $input['slug'] = str_slug($input['name'],'-');

        $rules = [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'desc' => 'required',
            'phone' => 'required',
            'categories' => 'required', // array
            'file-logo' => 'required',
            'file-banner' => 'required',
            'file-nbi' => 'required',
            'file-valid-id' => 'required',
        ];

        if($store_id > 0) {
            $rules['slug'] = 'unique:tbl_stores,slug,'.$store_id;
        } else {
            $rules['slug'] = 'unique:tbl_stores,slug';
        }
        
        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }

        // old record, update it
        if($store_id > 0) {
            $store = StoreModel::find($store_id);
            $store->slug = $input['slug'];
            $store->name = $input['name'];
            $store->desc = $input['desc'];
            $store->address = $input['address'];
            $store->phone = $input['phone'];
            $store->logo = $input['file-logo'];
            $store->banner = $input['file-banner'];
            $store->doc_nbi = $input['file-nbi'];
            $store->doc_valid_id = $input['file-valid-id'];
            $store->save();

            // remove all cats in store, then add all new later
            StoreCategoryModel::where('store_id','=', $store_id)->delete();
        
        // no store_id yet, create new
        } else {
            $store = StoreModel::create([
                'user_id' => $user->id,     // <-- !!! WARNING !!! what if support create store for a vendor this is a problem
                'slug' => $input['slug'],   // well, currently support does not create store for a vendor, but leaving this 
                'name' => $input['name'],   // big note if in near future the client  suggest to do the said feature
                'desc' => $input['desc'],
                'address' => $input['address'],
                'phone' => $input['phone'],
                'logo' => $input['file-logo'],
                'banner' => $input['file-banner'],
                'doc_nbi' => $input['file-nbi'],
                'doc_valid_id' => $input['file-valid-id'],
                'status' => 'Pending',
            ]);

            $store_id = $store->id;
        }

        // add store cats if any, but validator dont allow it if empty
        $data = [];
        foreach ($input['categories'] as $cat_id)
            $data[] = ['store_id' => $store_id, 'cat_id' => $cat_id];
            StoreCategoryModel::insert($data);

        // adhoc
        if($user->type == 'vendor') {
            return redirect('vendor/stores');
        }

        return redirect()->back();
    }

    public function postStore()
    {
        if(!Auth::check()) 
            return redirect('/login');

        $input = Input::all();
        $user = Auth::user();

        $store_id = $input['store-id'];
        $input['slug'] = str_slug($input['name'],'-');

        $rules = [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'desc' => 'required',
            'phone' => 'required',
            'pps-category' => 'required', // array
            'file-logo' => 'required',
            'file-banner' => 'required',
            'file-nbi' => 'required',
            'file-valid-id' => 'required',
        ];

        if($store_id > 0) {
            $rules['slug'] = 'unique:tbl_stores,slug,'.$store_id;
        } else {
            $rules['slug'] = 'unique:tbl_stores,slug';
        }
        
        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            // return redirect()
            //     ->back()
            //     ->withInput()
            //     ->withErrors($validator);
            $except = [
                'pps-category',
                'file-logo',
                'file-banner',
                'file-nbi',
                'file-valid-id',
            ];

            $redirect = redirect()
                ->back()
                ->withInput();

            $with = [];
            $withErrors = [];
            foreach ($except as $ex) {
                $withErrors[$ex] = $ex.'X';
                $with[$ex] = Input::get($ex);
                $redirect->with($ex, Input::get($ex));
            }
                
            $redirect->withErrors($withErrors);
            $redirect->withInput(Input::except($except));
            $redirect->withErrors($validator);

            return $redirect;
        }

        // old record, update it
        if($store_id > 0) {
            $store = StoreModel::find($store_id);
            $store->slug = $input['slug'];
            $store->name = $input['name'];
            $store->desc = $input['desc'];
            $store->address = $input['address'];
            $store->phone = $input['phone'];
            $store->logo = $input['file-logo'];
            $store->banner = $input['file-banner'];
            $store->doc_nbi = $input['file-nbi'];
            $store->doc_valid_id = $input['file-valid-id'];
            $store->save();

            // remove all cats in store, then add all new later
            StoreCategoryModel::where('store_id','=', $store_id)->delete();
        
        // no store_id yet, create new
        } else {
            $store = StoreModel::create([
                'user_id' => $user->id,     // <-- !!! WARNING !!! what if support create store for a vendor this is a problem
                'slug' => $input['slug'],   // well, currently support does not create store for a vendor, but leaving this 
                'name' => $input['name'],   // big note if in near future the client  suggest to do the said feature
                'desc' => $input['desc'],
                'address' => $input['address'],
                'phone' => $input['phone'],
                'logo' => $input['file-logo'],
                'banner' => $input['file-banner'],
                'doc_nbi' => $input['file-nbi'],
                'doc_valid_id' => $input['file-valid-id'],
                'status' => 'Pending',
            ]);

            $store_id = $store->id;
        }

        // add store cats if any, but validator dont allow it if empty
        $data = [];
        foreach ($input['pps-category'] as $cat_id)
            $data[] = ['store_id' => $store_id, 'cat_id' => $cat_id];
            StoreCategoryModel::insert($data);

        // adhoc
        if($user->type == 'vendor') {
            return redirect('vendor/stores');
        }

        return redirect()->back();
    }

    
}