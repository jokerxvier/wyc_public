<?php

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\storeModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\StoreCategoryModel;

use Request, Input;
use URL, Auth;

class StoreController extends Controller
{
    protected $res = [];
    public $user;
    public $store;

    public function __construct()
    {
        if(!Common::filterUser(['vendor']))
            return redirect('/login')->send();

        $this->user = Auth::user();

        $stores = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();
        
        $this->store = StoreModel::where('id','=', Request::segment(3))
            ->where('user_id','=', $this->user->id)
            ->first();

        $this->res = [
            'user' => $this->user,
            'stores' => $stores->pluck('name','id'),
            'active_store_id' => Request::segment(3),
            'active_menu' => Request::segment(4),
        ];
    }

    public function index()
    {

        return view('pages.vendor.stores', $this->res);
    }

    public function add()
    {
        $arranged = [];
        $categories = CategoryModel::getStoreCategory(0);
        CategoryModel::arrangeTreetably(0, $categories, $arranged);
        $this->res['categories'] = collect($arranged);
        return view('pages.vendor.store-add', $this->res);
    }

    public function edit($store_id)
    {

        // -- store categories
        $store_cat = StoreCategoryModel::where('store_id','=', $store_id)->get();
        $store_cat = collect($store_cat)->pluck('cat_id');
        $store_cat = CategoryModel::whereIn('id',$store_cat)->get();
        $this->res['store_cat'] = $store_cat->pluck('name','id');

        $store = StoreModel::where('id','=', $store_id)
            ->where('user_id','=', $this->user->id)
            ->first();

        if(!$store)
            abort(404);

        $this->res['store'] = $store;
        $this->res['categories'] = CategoryModel::where('parent', '=', 0)->get();

        return view('pages.vendor.store-edit', $this->res);
    }

    public function page()
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = StoreModel::query();
            $count = $query->count();

            $query->where('user_id', '=', $this->user->id);

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('name' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('desc' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('address' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $stores = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->orderBy('created_at', 'desc')
                ->get();

            $aaData = [];
            foreach ($stores as $store) {
                $action = '<a href="'. URL::route('vendor.store.edit', $store['id']) .'">Edit</a>';
                if($store->status == 'Active')
                    $action .= ' | <a href="'. URL::route('vendor.store.dashboard', $store['id']) .'">Dashboard</a>';

                $aaData[] = [
                    '<img src="'.asset('uploads/'.$store->logo).'" class="user-image" alt="Store Image" style="border-radius: 50%;">',
                    $store->name,
                    $store->user->fname.' '.$store->user->lname,
                    $store->user->email,
                    ucfirst($store->status),
                    $store->address,
                    $action,
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function dashboard($store_id)
    {
        if(!$this->store)
            abort(404);
        
        $this->res['store'] = $this->store;

        $store_cat_count = StoreModel::categoryCount($store_id);
        $store_prod_count = StoreModel::productCount($store_id);
        $prod = StoreModel::product($store_id, 8);


        $this->res['cat_count'] =  ($store_cat_count) ?  $store_cat_count[0] : false;
        $this->res['prod_count'] =  ($store_prod_count) ?  $store_prod_count[0] : false;
        $this->res['prods'] = $prod;


        return view('pages.vendor.store-dashboard', $this->res);
    }

    
}