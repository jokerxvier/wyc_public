<?php

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\storeModel;
use App\Http\Model\OptionModel;
use App\Http\Model\VariantModel;

use Auth, Request, Input, URL;
use Validator;

class ProductOptionController extends Controller
{
    protected $res = [];
    public $user;
    public $store;
    public $store_id;

    public function __construct()
    {
        if(!Common::filterUser(['vendor']))
            return redirect('/login')->send();

        $this->user = Auth::user();

        $stores = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();
        
        $this->store = StoreModel::where('id','=', Request::segment(3))
            ->where('user_id','=', $this->user->id)
            ->first();

        if(!$this->store) 
            abort(404);

        $this->store_id = Request::segment(3);

        $this->res = [
            'user' => $this->user,
            'stores' => $stores->pluck('name','id'),
            'active_store_id' => Request::segment(3),
            'active_menu' => Request::segment(4),
        ];
    }

    public function index()
    {   
        return view('pages.vendor.prod-options', $this->res);
    }

    public function add()
    {   
        return view('pages.vendor.prod-option-add', $this->res);
    }

    public function edit($store_id, $option_id)
    {
        $option = OptionModel::find($option_id);
        if(!$option)
            abort(404);

        $this->res['option'] = $option;
        return view('pages.vendor.prod-option-edit', $this->res);
    }

    public function postProductOption()
    {
        $input = Input::all();

        $validator = Validator::make($input, [
            'option-name' => 'required',
            'display-name' => 'required',
        ]);

        $validator->each('variant-name', ['required']);
        $validator->each('variant-weight', ['required|numeric']);

        if($validator->fails()) {
            $except = [
                'variant-id',
                'variant-name',
                'variant-weight'
            ];

            $redirect = redirect()
                ->back()
                ->withInput();

            $with = [];
            $withErrors = [];
            foreach ($except as $ex) {
                $withErrors[$ex] = $ex.'X';
                $with[$ex] = Input::get($ex);
                $redirect->with($ex, Input::get($ex));
            }
                
            $redirect->withErrors($withErrors);
            $redirect->withInput(Input::except($except));
            $redirect->withErrors($validator);

            return $redirect;

            // return redirect()
            //     ->back()
            //     ->withInput()
            //     ->with('variant-id', Input::get('variant-id'))
            //     ->with('variant-name', Input::get('variant-name'))
            //     ->with('variant-weight', Input::get('variant-weight'))
            //     ->withInput(Input::except(['variant-id','variant-name','variant-weight']))
            //     ->withErrors(['variant-id'=> 'variant-id','variant-name'=> 'variant-name', 'variant-weight'=> 'variant-weight'])
            //     ->withErrors($validator);
        }


        if($input['option-id'] > 0) {
            $option = OptionModel::find($input['option-id']);
            $option->name_unique = $input['option-name'];
            $option->name_alt = $input['display-name'];
            $option->save();

            $data = [];
            foreach ($input['variant-id'] as $key => $id) {
                $variant = VariantModel::find($id);
                if($variant) {
                    $variant->name = $input['variant-name'][$key];
                    $variant->weight = $input['variant-weight'][$key];
                    $variant->save();
                } else {
                    $data[] = [
                        'option_id' => $option->id,
                        'name' => $input['variant-name'][$key], // same as $name
                        'weight' => $input['variant-weight'][$key],
                    ];
                }
            }

            // insert added variants during option edit
            if(count($data) > 0)
                VariantModel::insert($data);

        } else {
            $option = OptionModel::create([
                'store_id' => $this->store_id,
                'name_unique' => $input['option-name'],
                'name_alt' => $input['display-name'],
            ]);

            $data = [];
            foreach ($input['variant-name'] as $key => $name) {
                $data[] = [
                    'option_id' => $option->id,
                    'name' => $input['variant-name'][$key], // same as $name
                    'weight' => $input['variant-weight'][$key],
                ];
            }
            
            VariantModel::insert($data);
        }

    
        return redirect('vendor/store/'.$this->store_id.'/prod-options')->send();
    }

    public function page()
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = OptionModel::query();
            $count = $query->count();

            $query->where('store_id', '=', $this->store_id);

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('name_unique' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('name_alt' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $options = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->get();

            $aaData = [];
            foreach ($options as $option) {
                $action = '<a href="'. URL::route('vendor.store.prod.option.edit', [$this->store_id, $option['id']]) .'">Edit</a>';
                    $action .= ' | <a href="#" class="option-delete" option-id="'.$option['id'].'">Delete</a>';

                $aaData[] = [
                    $option->name_unique,
                    $option->name_alt,
                    $option->variants->count(),
                    $action,
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function deleteVariant()
    {
        if(Request::ajax()) {   
            $input = Input::all();

            $variant = VariantModel::find($input['variant_id']);
            if($variant) {
                $variant->delete();
                return 1;
            }
        }
        return 0;
    }

    public function deleteOption()
    {
        if(Request::ajax()) {   
            $input = Input::all();

            $option = OptionModel::find($input['option_id']);
            if($option){
                VariantModel::where('option_id','=',$input['option_id'])->delete();
                $option->delete();
                return 1;
            }
            
        }
        return 0;
    }
}