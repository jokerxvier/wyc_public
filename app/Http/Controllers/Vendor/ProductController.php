<?php

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\StoreModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\ProductModel;
use App\Http\Model\ProductOptionModel;
use App\Http\Model\ProductCategoryModel;
use App\Http\Model\ProductImageModel;

use App\Http\Model\BrandModel;
use App\Http\Model\OptionModel;

use Auth, Request, Input, URL;
use Validator;

class ProductController extends Controller
{
    protected $res = [];
    public $user;
    public $store;
    public $store_id;

    public function __construct()
    {
        if(!Common::filterUser(['vendor']))
            return redirect('/login')->send();

        $this->user = Auth::user();

        $stores = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();
        
        $this->store = StoreModel::where('id','=', Request::segment(3))
            ->where('user_id','=', $this->user->id)
            ->first();

        if(!$this->store) 
            abort(404);

        $this->store_id = Request::segment(3);

        $this->res = [
            'user' => $this->user,
            'stores' => $stores->pluck('name','id'),
            'active_store_id' => Request::segment(3),
            'active_menu' => Request::segment(4),
        ];
    }

    public function index()
    {   
        $this->res['store'] = $this->store;
        return view('pages.vendor.products', $this->res);
    }

    public function add($store_id)
    {   
        $arranged = [];
        $categories = CategoryModel::getStoreCategory($store_id);
        CategoryModel::arrangeTreetably(0, $categories, $arranged);

        $this->res['categories'] = collect($arranged);
        $this->res['store'] = StoreModel::getUserStore($store_id, $this->user->id);
        $this->res['brands'] = BrandModel::getStoreBrands($store_id);
        $this->res['options'] = OptionModel::getstoreOption($store_id);      

        return view('pages.vendor.product-add', $this->res);   
    }

    public function edit($store_id, $product_id)
    {
        $product = ProductModel::find($product_id);
        if(!$product)
            abort(404);

        $this->res['product'] = $product;
        
        $arranged = [];
        $categories = CategoryModel::getStoreCategory($store_id);
        CategoryModel::arrangeTreetably(0, $categories, $arranged);

        $this->res['categories'] = collect($arranged);
        $this->res['store'] = StoreModel::getUserStore($store_id, $this->user->id);
        $this->res['brands'] = BrandModel::getStoreBrands($store_id);
        $this->res['options'] = OptionModel::getstoreOption($store_id);      

        $product_categories = ProductCategoryModel::where('product_id', '=', $product_id)->get();
        $product_categories = $product_categories->pluck('cat_id');
        $product_categories = CategoryModel::whereIn('id', $product_categories)->get();
        
        $product_options = ProductOptionModel::where('product_id', '=', $product_id)->get();
        $product_options = $product_options->pluck('option_id');
        $product_options = OptionModel::whereIn('id', $product_options)->get();

        $this->res['product_options'] = $product_options;
        $this->res['product_categories'] = $product_categories;

        // -- ugly shit but we dont have time to make it nice and neat
        for ($i=1; $i < 6; $i++)
            $this->res['product_image_'.($i+1)] = '';
        
        $images = ProductImageModel::where('product_id', '=', $product_id)->get();
        foreach ($images as $i => $image) {
            $this->res['product_image_'.($i+1)] = $image->image;
        }

        return view('pages.vendor.product-edit', $this->res);   
    }

    public function page()
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = ProductModel::query();
            $count = $query->count();

            $query->where('store_id', '=', $this->store_id);

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('name' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('desc_short' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('desc_long' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $products = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                // ->orderBy('created_at', 'desc')
                ->get();

            $aaData = [];
            foreach ($products as $product) {
                $action = '<a href="'. URL::route('vendor.store.product.edit', [$this->store_id, $product->id]) .'">Edit</a>';
                    $action .= ' | <a href="#" class="product-delete" product-id="'.$product->id.'">Delete</a>';

                $aaData[] = [
                    '<a href="'.URL::route('general.product.detail', $product->slug).'">'.
                        '<img src="'.asset('uploads/'.$product->image).'" width="41" height="41" class="user-image" alt="Product Image"></a>',
                    $product->sku,
                    '<a href="'.URL::route('general.product.detail', $product->slug).'">'.$product->name.'</a>',
                    '₱'. number_format($product->price, 2),
                    '₱'. number_format($product->price_sale, 2),
                    $product->stock,
                    ($product->active ? 'Active' : 'Inactive'),
                    $action,
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function postProduct()
    {
        $input = Input::all();

        // dd($input);

        $validator = Validator::make($input, [
            'product-name' => 'required',
            'pps-category' => 'required',
            'product-price' => 'required|numeric',
            'product-price-sale' => 'required|numeric',
            'product-desc-short' => 'required',
            'product-desc-long' => 'required',
            'product-sku' => 'required',
            'product-stock' => 'required|numeric',
            'featured-image' => 'required',
            'product-image-1' => 'required',
        ]);

        $input['slug'] = str_slug($input['product-name'],'-');

        if($input['product-id'] > 0 ) {
            $rules['slug'] = 'unique:tbl_products,slug,'.$input['product-id'];
        } else {
            $rules['slug'] = 'unique:tbl_products,slug';
        }

        if($validator->fails()) {

            $except = [
                'featured-image',
                'product-image-1',
                'pps-category',
                'pps-brand',
                'pps-option',
            ];

            $redirect = redirect()
                ->back()
                ->withInput();

            $with = [];
            $withErrors = [];
            foreach ($except as $ex) {
                $withErrors[$ex] = $ex.'X';
                $with[$ex] = Input::get($ex);
                $redirect->with($ex, Input::get($ex));
            }
                
            $redirect->withErrors($withErrors);
            $redirect->withInput(Input::except($except));
            $redirect->withErrors($validator);

            return $redirect;
        }


        if(isset($input['pps-brand'])) {
            $input['pps-brand'] = $input['pps-brand'][0];
        } else {
            $input['pps-brand'] = null;
        }

        if($input['product-id'] > 0 ) {
            $product = ProductModel::find($input['product-id']);
            if(!$product)
                abort(404);

            $product->store_id = $input['store-id'];
            $product->brand_id = $input['pps-brand'];
            $product->sku = $input['product-sku'];
            $product->name = $input['product-name'];
            $product->slug = $input['slug'];
            $product->desc_short = $input['product-desc-short'];
            $product->desc_long = $input['product-desc-long'];
            $product->stock = $input['product-stock'];
            $product->price = $input['product-price'];
            $product->price_sale = $input['product-price-sale'];
            $product->min_order = $input['product-min-qty'];
            $product->max_order = $input['product-max-qty'];
            $product->image = $input['featured-image'];
            $product->save();


            ProductCategoryModel::where('product_id', '=', $product->id)->delete();
            ProductOptionModel::where('product_id', '=', $product->id)->delete();
            ProductImageModel::where('product_id', '=', $product->id)->delete();

        } else {

            $product = ProductModel::create([
                'store_id' => $input['store-id'],
                'brand_id' => $input['pps-brand'],
                'sku' => $input['product-sku'],
                'name' => $input['product-name'],
                'slug' => $input['slug'],
                'desc_short' => $input['product-desc-short'],
                'desc_long' => $input['product-desc-long'],
                'stock' => $input['product-stock'],
                'price' => $input['product-price'],
                'price_sale' => $input['product-price-sale'],
                'min_order' => $input['product-min-qty'],
                'max_order' => $input['product-max-qty'],
                'image' => $input['featured-image'],
            ]);
        }

        
        if(isset($input['pps-category'])) {
            foreach ($input['pps-category'] as $category) {
                ProductCategoryModel::create([
                    'product_id' => $product->id,
                    'cat_id' => $category,
                ]);
            };
        }

        if(isset($input['pps-option'])) {
            foreach ($input['pps-option'] as $option) {
                ProductOptionModel::create([
                    'product_id' => $product->id,
                    'option_id' => $option,
                ]);
            };
        }

        $images = [];

        for ($i=1; $i < 6; $i++) { 
            $key = 'product-image-'. $i;
            if(isset($input[$key]) && $input[$key] != '') {
                $images[] = [
                    'product_id' => $product->id,
                    'image' => $input[$key]
                ];
            }
        }

        if(count($images) > 0) {
            ProductImageModel::insert($images);
        }
        

        return redirect('vendor/store/'.$this->store_id.'/products')->send();
    }

    public function delete()
    {
        if(Request::ajax()) {   
            $input = Input::all();

            $product = ProductModel::find($input['product_id']);
            if($product) {
                
                ProductCategoryModel::where('product_id', '=', $product->id)->delete();
                ProductOptionModel::where('product_id', '=', $product->id)->delete();
                ProductImageModel::where('product_id', '=', $product->id)->delete();

                $product->delete();
                return 1;
            }
        }
        return 0;
    }
}