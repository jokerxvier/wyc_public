<?php

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\StoreDisabledCategoryModel;

use Auth, Request, Input;
use Validator;

class CategoryController extends Controller
{
    protected $res = [];
    public $user;
    public $store;
    public $store_id;

    public function __construct()
    {
        if(!Common::filterUser(['vendor']))
            return redirect('/login')->send();
            // do not rmv '->send()', with this below codes are
            // it will serve as !async. some shit happens here
            // if rmvd. to investigate later.

        $this->user = Auth::user();

        $stores = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();
        
        $this->store = StoreModel::where('id','=', Request::segment(3))
            ->where('user_id','=', $this->user->id)
            ->first();

        if(!$this->store) 
            abort(404);

        $this->store_id = Request::segment(3);
        // NOTE: put a guard here. its a hole! a very good guy can see other store
        // check if segment value is really owned by curretn user

        $this->res = [
            'user' => $this->user,
            'stores' => $stores->pluck('name','id'),
            'active_store_id' => Request::segment(3),
            'active_menu' => Request::segment(4),
        ];
    }

    public function index()
    {
        $categories = CategoryModel::where('active', '=', 1)->get();
        $root_categories = $categories->where('parent', 0);

        $disabled_cat_ids = StoreDisabledCategoryModel::where('store_id','=', $this->store_id)->get();
        $disabled_cat_ids = $disabled_cat_ids->pluck('cat_id');

        
        $disabled_categories = collect([]);
        foreach ($disabled_cat_ids as $cat_id) {
            $disabled_temp = $categories->where('id', $cat_id);
            $disabled_categories = $disabled_categories->merge($disabled_temp);
        }

        // we need to remove disabled from root
        $disabled_cat_ids = $disabled_cat_ids->toArray();
        $filtered_categories = collect([]);

        foreach ($categories as $category) {
            if(!in_array($category->id, $disabled_cat_ids)) {
                $filtered_categories[] = $category;
            }
        }

        // add for approval cats 
        $for_approvals = CategoryModel::whereIN('status',['Pending','Rejected'])
            ->where('store_id','=', $this->store_id)
            
        // $for_approvals->where(function($q){
        //     $q->orWhere('status' , '=', '');
        // });

            ->get();

        $disabled_categories = $disabled_categories->merge($for_approvals);

        $this->res['categories'] = $categories;
        $this->res['root_categories'] = $root_categories;         // root cats onlu
        $this->res['filtered_categories'] = $filtered_categories; // for active cats (inactive cats removed)
        $this->res['disabled_categories'] = $disabled_categories; // list of inactive cats

        return view('pages.vendor.categories', $this->res);
    }

    public function link()
    {
        if(Request::ajax()){
            $input = Input::all();

            StoreDisabledCategoryModel::where('store_id','=', $this->store_id)
                ->where('cat_id','=', $input['cat_id'])
                ->delete();

            return 1;
        }

        return 0;
    }

    public function unlink()
    {
        if(Request::ajax()){
            $input = Input::all();

            StoreDisabledCategoryModel::create([
                'store_id' => $this->store_id,
                'cat_id' => $input['cat_id'],
            ]);

            return 1;
        }

        return 0;
    }

    public function delete()
    {
        if(Request::ajax()){
            $input = Input::all();

            $category = CategoryModel::find($input['cat_id']);
            if($category && $category->store_id == $this->store_id) {
                $category->delete();
                return 1;
            }
        }

        return 0;
    }

    public function add()
    {
        $arranged_categories = [];
        $categories = CategoryModel::where('active', '=', 1)->get();
        CategoryModel::arrangeTreetably(0, $categories, $arranged_categories);

        $this->res['categories'] = collect($arranged_categories);
        return view('pages.vendor.category-add', $this->res);
    }

    public function edit($store_id, $cat_id)
    {
        $category = CategoryModel::find($cat_id);
        if(!$category)
            abort(404);

        $arranged_categories = [];
        $categories = CategoryModel::where('active', '=', 1)->get();
        CategoryModel::arrangeTreetably(0, $categories, $arranged_categories);

        $parent = CategoryModel::find($category->parent);

        $this->res['parent'] = $parent;
        $this->res['category'] = $category;
        $this->res['categories'] = $arranged_categories;
        return view('pages.vendor.category-edit', $this->res);
    }

    public function postCategory()
    {
        $input = Input::all();

        $input['slug'] = str_slug($input['category-name'],'-');

        $rules = [
            'category-name' => 'required',
            'pps-category' => 'required',
            'file-image' => 'required',
            'file-image-alt' => 'required',
        ];

        if($input['cat-id'] > 0 ) {
            $rules['slug'] = 'unique:tbl_category,slug,'.$input['cat-id'];
        } else {
            $rules['slug'] = 'unique:tbl_category,slug';
        }

        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            $except = [
                'pps-category',
                'file-image',
                'file-image-alt',
            ];

            $redirect = redirect()
                ->back()
                ->withInput();

            $with = [];
            $withErrors = [];
            foreach ($except as $ex) {
                $withErrors[$ex] = $ex.'X';
                $with[$ex] = Input::get($ex);
                $redirect->with($ex, Input::get($ex));
            }
                
            $redirect->withErrors($withErrors);
            $redirect->withInput(Input::except($except));
            $redirect->withErrors($validator);

            return $redirect;
        }

        if($input['cat-id'] > 0 ) {
            $category = CategoryModel::find($input['cat-id']);
            if(!$category)
                abor(404);

            $category->parent = $input['pps-category'][0];
            $category->name = $input['category-name'];
            $category->icon = $input['category-icon'];
            $category->slug = $input['slug'];
            $category->img_primary = $input['file-image'];
            $category->img_home = $input['file-image-alt'];
            $category->save();

        } else {
            CategoryModel::create([
                'parent' => $input['pps-category'][0], // we only allow 1 parent
                'name' => $input['category-name'],
                'icon' => $input['category-icon'],
                'slug' => $input['slug'],
                'img_primary' => $input['file-image'],
                'img_home' => $input['file-image-alt'],
                'store_id' => $this->store_id,
                'status' => 'Pending'
            ]);
        }

        return redirect('/vendor/store/'.$this->store_id.'/categories');
    }
}