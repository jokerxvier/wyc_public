<?php

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\storeModel;

use URL, Auth;

class DashboardController extends Controller
{
    protected $res = [];

    public function __construct()
    {
        if(!Common::filterUser(['vendor']))
            return redirect('/login')->send();

        $this->user = Auth::user();

        $store = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();

        $this->res = [
            'active_menu' => '',
            'active_store_id' => 0,
            'user' => $this->user,
            'stores' => $store->pluck('name','id'),
        ];
    }

    public function index()
    {
        return view('pages.vendor.dashboard', $this->res);
    }

}

