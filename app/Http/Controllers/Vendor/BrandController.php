<?php

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\StoreModel;
use App\Http\Model\BrandModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\BrandCategoryModel;

use Auth, Request, Input, Validator, URL;

class BrandController extends Controller
{
    protected $res = [];
    public $user;
    public $store;
    public $store_id;

    public function __construct()
    {
        if(!Common::filterUser(['vendor']))
            return redirect('/login')->send();

        $this->user = Auth::user();

        $stores = StoreModel::where('user_id','=', $this->user->id)
            ->where('status', '=', 'Active')
            ->get();
        
        $this->store = StoreModel::where('id','=', Request::segment(3))
            ->where('user_id','=', $this->user->id)
            ->first();

        if(!$this->store) 
            abort(404);

        $this->store_id = Request::segment(3);

        $this->res = [
            'user' => $this->user,
            'stores' => $stores->pluck('name','id'),
            'active_store_id' => Request::segment(3),
            'active_menu' => Request::segment(4),
        ];
    }

    public function index()
    {   
        return view('pages.vendor.brands', $this->res);
    }

    public function add()
    {   
        $arranged = [];
        $categories = CategoryModel::getStoreCategory($this->store_id);
        CategoryModel::arrangeTreetably(0, $categories, $arranged);
        $this->res['categories'] = collect($arranged);
        return view('pages.vendor.brand-add', $this->res);
    }

    public function edit($store_id, $brand_id)
    {   
        $brand = BrandModel::find($brand_id);

        if($brand && $brand->store_id != $this->store_id)
            abort(404);
        
        $brand_cat = BrandCategoryModel::where('brand_id','=', $brand_id)->get();
        $brand_cat = collect($brand_cat)->pluck('cat_id');
        $brand_cat = CategoryModel::whereIn('id',$brand_cat)->get();
        $this->res['brand_cat'] = $brand_cat->pluck('name','id');

        $this->res['brand'] = $brand;
        $this->res['categories'] = CategoryModel::where('parent', '=', 0)->get();
        return view('pages.vendor.brand-edit', $this->res);
    }

    public function postBrand()
    {
        $input = Input::all();

        $input['slug'] = str_slug($input['name'],'-');

        $validator = Validator::make($input, [
            'name' => 'required',
            'pps-category' => 'required',
            'file-image' => 'required',
            'file-image-alt' => 'required',
        ]);

        if($validator->fails()) {
            $except = [
                'pps-category',
                'file-image',
                'file-image-alt',
            ];

            $redirect = redirect()
                ->back()
                ->withInput();

            $with = [];
            $withErrors = [];
            foreach ($except as $ex) {
                $withErrors[$ex] = $ex.'X';
                $with[$ex] = Input::get($ex);
                $redirect->with($ex, Input::get($ex));
            }
                
            $redirect->withErrors($withErrors);
            $redirect->withInput(Input::except($except));
            $redirect->withErrors($validator);

            return $redirect;
        }

        if($input['brand-id'] > 0) {
            $brand = BrandModel::find($input['brand-id']);
            $brand->name = $input['name'];
            $brand->slug = $input['slug'];
            $brand->image = $input['file-image'];
            $brand->image_alt = $input['file-image-alt'];
            $brand->save();

            // remove all cats of brand, then add all new later
            BrandCategoryModel::where('brand_id','=',$input['brand-id'])->delete();
            $brand_id = $input['brand-id'];

        } else {
            $brand = BrandModel::create([
                'store_id' => $this->store_id,
                'name' => $input['name'],
                'slug' => $input['slug'],
                'image' => $input['file-image'],
                'image_alt' => $input['file-image-alt'],
            ]);
            $brand_id = $brand->id;
        }

        // add brand cats if any, but validator dont allow it if empty
        $data = [];
        foreach ($input['pps-category'] as $cat_id)
            $data[] = ['brand_id' => $brand_id, 'cat_id' => $cat_id];
            BrandCategoryModel::insert($data);

        return redirect('vendor/store/'.$this->store_id.'/brands');
    }

    public function page()
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = BrandModel::query();
            $count = $query->count();

            $query->where('store_id', '=', $this->store_id);

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('name' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $brands = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->get();

            $aaData = [];
            foreach ($brands as $brand) {
                $action = '<a href="'. URL::route('vendor.store.brand.edit', [$this->store_id, $brand['id']]) .'">Edit</a>';
                    $action .= ' | <a href="#" class="brand-delete" brand-id="'.$brand['id'].'">Delete</a>';

                $aaData[] = [
                    '<img src="'.asset('uploads/'.$brand->image).'" class="user-image" alt="Brand Image">',
                    '<img src="'.asset('uploads/'.$brand->image_alt).'" class="user-image" alt="Brand Image">',
                    $brand->name,
                    $brand->slug,
                    $action,
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function delete()
    {
        if(Request::ajax()) {
            $input = Input::all();

            $brand = BrandModel::find($input['brand_id']);

            if($brand && $brand->store_id == $this->store_id)
            {
                $path = public_path('uploads/');
                $image = $path . $brand->image;
                $image_alt = $path . $brand->image_alt;

                // removing associated image file making our server hd happy
                if(file_exists($image) && is_file($image)) unlink($image);
                if(file_exists($image_alt) && is_file($image_alt)) unlink($image_alt);

                BrandCategoryModel::where('brand_id','=', $input['brand_id'])->delete();
                $brand->delete();
                
                return 1;
            }
        }

        return 0;
    }

    
}