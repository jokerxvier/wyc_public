<?php

namespace App\Http\Controllers\Support;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\ProductModel;
use App\Http\Model\StoreModel;


use URL, Auth, Redirect;

class DashboardController extends Controller
{
    protected $res = [];
    protected $user;

    public function __construct()
    {
        if(!Common::filterUser(['support']))
           return redirect('/login')->send();

        $this->user = Auth::user();
        $this->res['user'] = $this->user;
    }

    public function index()
    {
        $prod_count = ProductModel::getProductsCount();
        $user_count = UserModel::getUserCount(); //count user vendor and shopper
        $vendor_count = UserModel::getUserCount('vendor'); //cound vendor only

        $this->res['prod_count'] = $prod_count;
        $this->res['user_count'] = $user_count;
        $this->res['vendor_count'] = $vendor_count;

        $this->res['shoppers_info'] = UserModel::getUserinfo('shopper', 8);
        $this->res['vendor_info'] = UserModel::getUserinfo('vendor', 8);
        $this->res['store_active'] = StoreModel::stores('Active', 8);
        $this->res['store_pending'] = StoreModel::stores('Pending', 8);

        return view('pages.support.dashboard', $this->res);
    }

}

