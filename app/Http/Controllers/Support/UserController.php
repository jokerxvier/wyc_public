<?php

namespace App\Http\Controllers\Support;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\storeModel;
// use App\Http\Model\CategoryModel;
// use App\Http\Model\storeCategoryModel;


use Request, Input, Validator;
use URL, Auth;

class UserController extends Controller
{
    protected $res = [];
    protected $user;

    public function __construct()
    {
        if(!Common::filterUser(['support']))
            return redirect('/login')->send();

        $this->user = Auth::user();
        $this->res['user'] = $this->user;
    }

    public function index()
    {
        // if(!Auth::check())
        //     return redirect('/login');

        $latest_users = UserModel::where('type', '=', 'shopper')
            ->orWhere('type', '=', 'vendor')
            ->orderBy('created_at', 'desc')
            ->take(12)->get();

        foreach ($latest_users as $user) {
            $user->time_elapsed = Common::time_elapsed_string($user->created_at);
        }

        $this->res['latest_users'] = $latest_users;

        return view('pages.support.users', $this->res);
    }

    public function page()
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = UserModel::query();
            $count = $query->count();

            $query->where(function($q) {
                $q->where('type', '=', 'vendor');
                $q->orWhere('type', '=', 'shopper');
            });

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('email' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('fname' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('lname' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $users = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->orderBy('created_at', 'desc')
                ->get();

            $aaData = [];
            foreach ($users as $user) {
                $action = '<a href="'. URL::route('support.user.view', $user['id']) .'">View</a> | '
                    .'<a href="#" class="ban-toggle" user-id="'.$user->id.'">'.($user->active == 1 ? 'Deactivate' : 'Activate') .'</a>';

                $aaData[] = [
                    ($user->active == 1 ? '<i class="fa fa-user text-green"></i> ' : '<i class="fa fa-user-times text-red"></i> ')
                        .($user->type == 'vendor' ? '<i class="fa fa-briefcase text-yellow"></i> ' : '<i class="fa fa-shopping-cart text-light-blue"></i> ')
                        .ucfirst($user->fname).' '.ucfirst($user->lname),
                    $user->email,
                    $user->birthdate,
                    $user->phone,
                    Common::time_elapsed_string($user->created_at),
                    $action
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function banToggle() // ban/unban
    {
        if(Request::ajax()) {
            $input = Input::all();

            $user = UserModel::find($input['user_id']);
            $user->active = ($user->active == 1 ? 0 : 1);
            $user->save();
            return 1;
        }

        return 0;
    }

    public function view($user_id)
    {
        $user = UserModel::find($user_id);
        if(!$user)
            abort(404);

        $this->res['view_user'] = $user;
        return view('pages.support.user-view', $this->res);
    }

    public function add()
    {
        return view('pages.support.user-add', $this->res);
    }

    public function postUser()
    {
        $input = Input::all();

        $rules = [
            'fname' => 'required',
            'lname' => 'required',
            'gender' => 'required',
            'phone' => 'required',
        ];

        if($input['user-id'] > 0) {
            // $rules['email'] = 'required|email|max:255|unique:tbl_users,email,'.$input['user-id'];
        } else {
            $rules['email'] = 'required|email|max:255|unique:tbl_users';
            $rules['password'] = 'required|confirmed|min:6';
        }

        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }


        if($input['user-id'] > 0) {
            $user = UserModel::find($input['user-id']);
            
            // support has an option not to touch the users password
            if(strlen($input['password']) > 0)
                $user->password = bcrypt($input['password']);

            $user->fname = $input['fname'];
            $user->lname = $input['lname'];
            $user->gender = $input['gender'];
            $user->phone = $input['phone'];
            $user->birthdate == $input['birthdate'];
            $user->save();

            return redirect()->back();

        } else {
            UserModel::create([
                'email' => $input['email'],
                'password' => bcrypt($input['password']),
                'fname' => $input['fname'],
                'lname' => $input['lname'],
                'gender' => $input['gender'],
                'phone' => $input['phone'],
                'birthdate' => $input['birthdate'],
            ]);

            return redirect('support/users');
        }
    }

    public function userStores($user_id = null)
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();
            $aaData = [];

            // early exit
            if(!$user_id) {
                $ret['sEcho'] = $input['sEcho'];
                $ret['iTotalRecords'] = $input['iDisplayLength'];
                $ret['iTotalDisplayRecords'] = 0;
                $ret['aaData'] = $aaData;
                return json_encode($ret);
            }

            $query = StoreModel::query();
            $count = $query->count();

            $query->where(function($q) use($user_id){
                $q->where('user_id', '=', $user_id);
            });

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('name' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('desc' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('address' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $stores = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->orderBy('created_at', 'desc')
                ->get();

            foreach ($stores as $store) {
                $action = '<a href="'. URL::route('support.store.view', $store['id']) .'">View</a> | ';
                
                if($store->status == 'Declined') {
                    $action .= '<a href="#" store-id="'.$store['id'].'" class="approve-store">Approve</a> ';
                } else if($store->status == 'Active') {
                    $action .='<a href="#" store-id="'.$store['id'].'" class="ban-store">Ban</a> ';
                } else {
                    $action .='<a href="#" store-id="'.$store['id'].'" class="approve-store">Approve</a> | '
                        .'<a href="#decline-store" store-id="'.$store['id'].'" class="popup-modal decline-store">Decline</a> ';
                }

                $aaData[] = [
                    '<img src="'.asset('uploads/'.$store->logo).'" class="user-image" alt="Store Image" style="border-radius: 50%;">',
                    $store->name,
                    $store->user->fname.' '.$store->user->lname,
                    $store->user->email,
                    $store->address,
                    $store->status,
                    $action,
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }
}

