<?php

namespace App\Http\Controllers\Support;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\storeModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\storeCategoryModel;


use Request, Input;
use URL, Auth;

class StoreController extends Controller
{
    protected $res = [];
    protected $user;

    public function __construct()
    {
        if(!Common::filterUser(['support']))
            return redirect('/login')->send();
        
        $this->user = Auth::user();
        $this->res['user'] = $this->user;
    }

    public function index()
    {   

        $pending_stores =  StoreModel::where('status', '=', 'Pending')->get();
        
        $this->res['pending_store'] = ($pending_stores && count($pending_stores) > 0) ? $pending_stores : false;
    
        return view('pages.support.stores', $this->res);
    }

    public function page($status = 'Pending')
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = StoreModel::query();
            $count = $query->count();

            $query->where(function($q) use($status){
                $q->where('status', '=', $status);
            });

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('name' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('desc' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('address' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $stores = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->orderBy('created_at', 'desc')
                ->get();

            $aaData = [];
            foreach ($stores as $store) {
                $action = '<a href="'. URL::route('support.store.view', $store['id']) .'">View</a> | ';
                
                if($status == 'Declined') {
                    $action .= '<a href="#" store-id="'.$store['id'].'" class="approve-store">Approve</a> ';
                } else if($status == 'Active') {
                    $action .='<a href="#" store-id="'.$store['id'].'" class="ban-store">Ban</a> ';
                } else {
                    $action .='<a href="#" store-id="'.$store['id'].'" class="approve-store">Approve</a> | '
                        .'<a href="#decline-store" store-id="'.$store['id'].'" class="popup-modal decline-store">Decline</a> ';
                }

                $aaData[] = [
                    '<img src="'.asset('uploads/'.$store->logo).'" class="user-image" alt="Store Image" style="border-radius: 50%;">',
                    $store->name,
                    $store->user->fname.' '.$store->user->lname,
                    $store->user->email,
                    $store->address,
                    $action,
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function approve()
    {
        if(Request::ajax()) {
            $input = Input::all();
            $store = StoreModel::find($input['store_id']);
            $store->status = 'Active';
            $store->save();

            $user = UserModel::find($store->user_id);
            $user->type = 'vendor';
            $user->save();
            return 1;
        }
        return 0;
    }

    public function decline()
    {
        if(Request::ajax()) {
            $input = Input::all();
            $store = StoreModel::find($input['store_id']);
            $store->status = 'Declined';
            $store->decline_reason = $input['reason'];
            $store->save();
            return 1;
        }
        return 0;
    }

    public function ban()
    {
        if(Request::ajax()) {
            $input = Input::all();
            $store = StoreModel::find($input['store_id']);
            $store->status = 'Banned';
            $store->save();
            return 1;
        }
        return 0;
    }

    public function view($id)
    {
        $store = StoreModel::find($id);
        if(!$store)
            abort(404);
        
        $arranged = [];
        $categories = CategoryModel::getStoreCategory($id);
        CategoryModel::arrangeTreetably(0, $categories, $arranged);
        $this->res['categories'] = collect($arranged);

        // -- store categories
        $store_cat = StoreCategoryModel::where('store_id','=', $id)->get();
        $store_cat = collect($store_cat)->pluck('cat_id');
        $store_cat = CategoryModel::whereIn('id',$store_cat)->get();

        $this->res['store'] = $store;
        $this->res['store_cat'] = $store_cat->pluck('name','id');
        return view('pages.support.store-view', $this->res);
    }

}

