<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\CityModel;
use App\Http\Model\RegionModel;
use App\Http\Model\CountryModel;
use App\Http\Model\AddressBookModel;

use App\Http\Model\StoreModel;

use Hash;
use Auth, Input, Request;

class UserController extends Controller
{
	protected $res = [];

	public function __construct()
    {
        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }
    
	public function getProfile()
	{
		if(!Auth::check()) {
			return redirect('/login');
		}

		$this->res['user'] = Auth::user();
		$this->res['cities'] = CityModel::get();
		$this->res['regions'] = RegionModel::get();
		$this->res['country'] = CountryModel::get();
		$this->res['addresses'] = AddressBookModel::where('user_id', '=', Auth::user()->id)->get();
		$this->res['types'] = ['Default','Billing','Shipping'];
		return view('pages.user.profile', $this->res);
	}

	public function postProfile()
	{
		if(!Auth::check())
			return redirect('/login');
		
		$input = Input::all();
		$user = Auth::user();

		$user->fname = $input['fname'];
		$user->lname = $input['lname'];
		$user->gender = $input['gender'];
		$user->phone = $input['phone'];
		$user->birthdate = $input['birthdate'];
		$user->save();

		return redirect('/user/profile')->with('status', 'success');	
	}

	public function changePass()
    {
        $ret['status'] = 0;
        $ret['msg'] = 'An error occured while processing the request.';

        if(Request::ajax()) {
            $input = Input::all();

            $user = UserModel::find($input['user_id']);
            if($user) {
                if(Hash::check($input['cp_old_pass'], $user->password)) {
                    $user->password = bcrypt($input['cp_pass1']);
                    $user->save();
                    $ret['status'] = 1;
                } else {
                    $ret['msg'] = 'Incorrect old password.';
                }
            }
        }

        return json_encode($ret);
    }

    public function orders()
    {
    	return view('pages.user.orders', $this->res);
    }

}