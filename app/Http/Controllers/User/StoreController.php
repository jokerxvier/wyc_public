<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\StoreModel;
use App\Http\Model\CategoryModel;
use App\Http\Model\StoreCategoryModel;

use Request, Input, Validator, Image, Auth;

class StoreController extends Controller
{
	protected $res = [];
	protected $user;

	public function __construct()
	{
		if(!Auth::check())
			return redirect('/login');

		$this->user = Auth::user();

		// commenting below line because there is a conflict within $this->res['categories']
		// Common::cass uses 'categories' to feed categories and its contents to sidebar
		// and our add category selection (since we are opening a store and it needs category)
		// uses the variable 'categories' too so it brokes. both functions are called in many
		// functions/pages so altering them needs a time to fix the mess. leaving it for 
		// now since we dont have a time yet

		// Common::feedSidebarCategoryContents($this->res);

        Common::feedFooterPopularProducts($this->res);
	}

	public function getOpenStore()
	{	
		// when vendor tried to access open store page
		if($this->user->type == 'vendor') {
			return redirect('vendor');
		}

		// if no store yet, display empty
		$arr = [
			'id' => 0,
			'user_id' => '', 
			'rate' => '',
			'slug' => '',
			'name' => '',
			'desc' => '',
			'address' => '',
			'phone' => '',
			'logo' => '',
			'banner' => '',
			'status' => '',
			'doc_nbi' => '',
			'doc_valid_id' => '',
			'doc_valid_id' => '',
			'decline_reason' => '',
        ];

        $this->res['store_cat'] = []; // required

        // open store page implies that this is the first store
        // for vendor aspiree, so we will use $store[0]
        $store = StoreModel::where('user_id','=', $this->user->id)->get();
        if($store->count() > 0) {
        	$arr = [
        		'id' => $store[0]->id,
				'user_id' => $store[0]->user_id, 
		        'rate' => $store[0]->rate,
		        'slug' => $store[0]->slug,
		        'name' => $store[0]->name,
		        'desc' => $store[0]->desc,
		        'address' => $store[0]->address,
		        'phone' => $store[0]->phone,
		        'logo' => $store[0]->logo,
		        'banner' => $store[0]->banner,
		        'status' => $store[0]->status,
		        'doc_nbi' => $store[0]->doc_nbi,
		        'doc_valid_id' => $store[0]->doc_valid_id,
		        'decline_reason' => $store[0]->decline_reason,
	        ];

			// -- store categories
			$store_cat = StoreCategoryModel::where('store_id','=', $store[0]->id)->get();
			$store_cat = collect($store_cat)->pluck('cat_id');
			$store_cat = CategoryModel::whereIn('id',$store_cat)->get();
			$this->res['store_cat'] = $store_cat->pluck('name','id');
	    }

		$this->res['store'] = $arr;
		
		$arranged = [];
        $categories = CategoryModel::getStoreCategory(0);
        CategoryModel::arrangeTreetably(0, $categories, $arranged);
        $this->res['categories'] = collect($arranged);

		return view('pages.user.open-store', $this->res);	
	}
}