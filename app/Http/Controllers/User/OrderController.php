<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use App\Classes\Common;
use App\Http\Model\UserModel;
use App\Http\Model\OrderModel;
use App\Http\Model\OrderDetailModel;
use App\Http\Model\ProductModel;


use Request, Input, Auth, URL;

class OrderController extends Controller
{
    protected $res = [];
    protected $user;

    public function __construct()
    {
        if(!Auth::check())
            return redirect('/login')->send();

        $this->user = Auth::user();

        Common::feedSidebarCategoryContents($this->res);
        Common::feedFooterPopularProducts($this->res);
    }

    public function page()
    {
        $ret = [];
        if(Request::ajax()) {
            $input = Input::all();

            $query = OrderModel::query();
            $count = $query->count();

            $query->where('user_id', '=', $this->user->id);

            if ($input['sSearch'] != "") {
                $query->where(function($q) use($input){
                    $q->orWhere('mask' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('group' , 'LIKE', '%'.$input['sSearch'].'%');
                    $q->orWhere('status' , 'LIKE', '%'.$input['sSearch'].'%');
                });
            }

            $orders = $query->take($input['iDisplayLength'])
                ->skip($input['iDisplayStart'])
                ->orderBy('created_at', 'desc')
                ->get();

            $aaData = [];
            foreach ($orders as $order) {
                $action = '<a href="#" class="cancel-order" order-id="'.$order->id.'">Cancel Order</a>';

                switch ($order->status) {
                    case 'Pending': $color = 'warning'; break;
                    case 'Processed': $color = 'success'; break;
                    case 'Shipped': $color = 'primary'; break;
                    case 'Delivered': $color = 'info'; break;
                    case 'Declined': $color = 'danger'; break;
                    case 'Cancelled': $color = 'default'; break;
                    default:
                        $color = 'default'; break;
                }

                $aaData[] = [
                    '<a href="'.URL::route('general.store.detail', $order->store->slug).'">'.$order->store->name.'</a>',
                    '<a href="'.URL::route('user.order.detail',$order->mask).'">OR'.$order->mask.'</a>',
                    '₱ '.number_format($order->total, 2),
                    '<span class="label label-'.$color.'">'.$order->status.'</span>',
                    date_format($order->created_at,"Y/m/d"),
                    ($order->status == 'Pending' ? $action : ''),
                ];
            }

            $ret['sEcho'] = $input['sEcho'];
            $ret['iTotalRecords'] = $input['iDisplayLength'];
            $ret['iTotalDisplayRecords'] = $count;
            $ret['aaData'] = $aaData;
        }

        return json_encode($ret);
    }

    public function cancel()
    {
        if(Request::ajax()) {
            $input = Input::all();

            $order = OrderModel::where('id','=', $input['order_id'])
                ->where('user_id', '=', $this->user->id)
                ->first();

            if($order) {
                $order->status = 'Cancelled';
                $order->save();
                return 1;
            }
        }

        return 0;
    }

    public function detail($mask_id)
    {

        // $p = ProductModel::find(2);
        // dd($p->orderDetails);

        // $d = OrderDetailModel::find(35);
        // dd($d->product);


        $order = OrderModel::where('mask','=',$mask_id)
            ->first();

        if(!$order)
            abort(404);

        if($order->user_id != $this->user->id) {
            if($this->user->type == 'shopper')
                abort(404);
        }
        
        // dd($order->details);

        $this->res['order'] = $order;
        $this->res['items'] = $order->details;

        return view('pages.user.order-detail', $this->res);   
    }
}

