<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use App\Http\Model\UserModel;
use App\Http\Model\AddressBookModel;


use Request, Input, Validator;

class AddressController extends Controller
{
    public function __construct() {
    }

    public function get()
    {
        if(Request::ajax()) {
            $input = Input::all();
            $addr = AddressBookModel::find($input['addr_id']);

            $ret['street'] = $addr->street;
            $ret['region'] = $addr->region;
            $ret['city'] = $addr->city;
            $ret['zip'] = $addr->zip;
            $ret['country'] = $addr->country;
            $ret['type'] = $addr->type;
            $ret['addr_id'] = $addr->id;
            return json_encode($ret);
        }
    }

    public function insert()
    {
        if(Request::ajax()) {
            $input = Input::all();
            
            $validate = Validator::make($input, [
                'street' => 'required',
                'region' => 'required',
                'city' => 'required',
                'zip' => 'required',
            ]);

            if($validate) {
                $addr = AddressBookModel::create([
                    'type' => $input['type'],
                    'user_id' => $input['user_id'],
                    'street' => $input['street'],
                    'region' => $input['region'],
                    'city' => $input['city'],
                    'zip' => $input['zip'],
                    'country' => $input['country'],
                ]);
                
                $ret['row'] =  '<tr role="row" id="addr-'.$addr->id.'">'.
                    '<td>'.$addr->street .', '. $addr->city .', '. $addr->region .', '. strtoupper($addr->country) .' '. $addr->zip .'</td>'.
                    '<td>'.$addr->type.'</td>'.
                    '<td class="delete">'.
                        '<a href="#" class="address-edit" addr_id="'.$addr->id.'"><i class="fa fa-pencil"></i> Edit</a> | '.
                        '<a href="#" class="address-del" addr_id="'.$addr->id.'"><i class="fa fa-times-circle"></i> Delete</a>'.
                    '</td>'.
                '</tr>';

                return json_encode($ret);
            }
        }
    }

    public function delete()
    {
        if(Request::ajax()) {
            $input = Input::all();

            $addr = AddressBookModel::find($input['addr_id']);
            $addr->delete();
            return 1;
        }
        return 0;
    }

    public function update()
    {
        if(Request::ajax()) {
            $input = Input::all();


            $addr = AddressBookModel::find($input['addr_id']);

            $addr->street = $input['street'];
            $addr->zip = $input['zip'];
            $addr->region = $input['region'];
            $addr->city = $input['city'];
            $addr->country = $input['country'];
            $addr->type = $input['type'];
            $addr->save();

            $ret['type'] = $addr->type;
            $ret['str_address'] = $input['street'] .', '.
                $addr->city .', '.
                $addr->region .', '.
                strtoupper($addr->country).' '.
                $addr->zip;

            return json_encode($ret);
        }
    }
}

