<?php

namespace App\Classes;

use App\Http\Model\ProductModel;
use App\Http\Model\CategoryModel;

use DB, DateTime;
use Auth, Cart;

class Common {

	public static function feedSidebarCategoryContents(&$res)
	{
		$tree_order = [];
    	$categories = [];

		// -- for sidebar categories and its popups
        CategoryModel::getSidebarCategoryLookups($tree_order, $categories);
        $res['categories'] = $categories;
        $res['tree_order'] = $tree_order;

        // -- this is just a sample popup ads
        $popup_ads = [];
        for ($i=1; $i < 17; $i++) { 
            $popup_ads[] = 'sample-popup-ad-'. $i .'.jpg';
        }

        $res['popup_ads']  = collect($popup_ads);

        // -- this shouldnt be here, but yeah place it here temporarily
        $cart = Cart::instance('shopping');
        $res['cart_item_count'] = $cart->count(false);
	}

	public static function feedFooterPopularProducts(&$res)
	{
		// -- for footer prod list
		$popular = ProductModel::where('featured','=',1)
			->orderBy(DB::raw('RAND()'))
			->get();

		$res['popular_products']  = $popular;
	}

	public static function time_elapsed_string($datetime, $full = false)
    {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	public static function filterUser($types = [])
	{
		if(Auth::check()) {
			if(in_array(Auth::user()->type, $types)) {
				return true;
			} else {
				Auth::logout();
			}
		}

		return false;
	}

}
